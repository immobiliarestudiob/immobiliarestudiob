let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js').sass('resources/assets/sass/app.scss', 'public/css');

mix.sass('app.scss', 'resources/assets/css/tmp_bootstrap.css');

mix.styles([
      'tmp_bootstrap.css',
      'fancybox.css',
      'admin.css'
      ], 'public/css/admin.css')
	.webpack([
      'app.js',
      'fancybox.js',
      'admin.js'
      ], 'public/js/admin.js');

mix.styles('animate.css', 'public/css/animate.min.css');
mix.styles('web/search.css', 'public/css/web/search.css');
mix.webpack('web/search.js', 'public/js/web/search.js');

mix.copy('resources/assets/css/tmp_bootstrap.css', 'public/css/bootstrap.css');
mix.copy('resources/assets/js/jquery.easing.min.js', 'public/js/jquery.easing.min.js');
mix.copy('resources/assets/js/sortable.js', 'public/js/sortable.js');