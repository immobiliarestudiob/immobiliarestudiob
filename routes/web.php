<?php
if (env('APP_ENV') === 'production') {
	URL::forceSchema('https');
}

Route::get('tmp_login', function () { return redirect()->route('auth::login_get'); })->name('login');
Route::get('tmp_logout', function () { return redirect()->route('auth::logout'); })->name('logout');

// Localization
Route::group([
	'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localize', 'localeSessionRedirect', 'localizationRedirect' ]
], function() {

	// Auth
	Route::group(['namespace' => 'Auth','as' => 'auth::'], function() {
		Route::get('login', 'LoginController@showLoginForm')->name('login_get');
		Route::post('login', 'LoginController@login')->name('login_post');
		Route::get('logout', 'LoginController@logout')->name('logout');

		Route::get('register', 'RegisterController@showRegistrationForm')->name('register_get');
		Route::post('register', 'RegisterController@register')->name('register_post');

		Route::get('confirmation/resend', 'RegisterController@resend');
		Route::get('confirmation/{id}/{token}', 'RegisterController@confirm');

		Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password_reset_get');
		Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password_email');
		Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password_reset_token');
		Route::post('password/reset', 'ResetPasswordController@reset')->name('password_reset_post');

		ROute::get('auth/{provider}', 'LoginController@redirectToProvider');
		Route::get('auth/{provider}/callback', 'LoginController@handleProviderCallback');
	});

	// Web
	Route::group(['namespace' => 'Web','as' => 'web::'], function() {
		Route::get('/', function () { return redirect()->route('web::home'); })->name('root');
		
		Route::get(LaravelLocalization::transRoute('routes.home'), 'WebController@home')->name('home');
		Route::get(LaravelLocalization::transRoute('routes.about'), 'WebController@about')->name('about');
		//Route::get(LaravelLocalization::transRoute('routes.contact'), 'WebController@contact')->name('contact');
		Route::post('request', 'WebController@request')->name('request');

		Route::get(LaravelLocalization::transRoute('routes.search'), 'WebController@search')->name('search');
		Route::get(LaravelLocalization::transRoute('routes.details'), 'WebController@detailsProperty')->name('details');
		Route::get(LaravelLocalization::transRoute('routes.print'), 'WebController@printProperty')->name('print');

		// User
		Route::group(['middleware' => 'auth'], function() {
			Route::get(LaravelLocalization::transRoute('routes.user_root'), function ($id) {
				return redirect()->route('web::user.dashboard', $id);
			})->name('user.root');
			Route::get(LaravelLocalization::transRoute('routes.user_dashboard'), 'UserController@dashboard')->name('user.dashboard');
			Route::get(LaravelLocalization::transRoute('routes.user_preferred'), 'UserController@preferred')->name('user.preferred');
			Route::get(LaravelLocalization::transRoute('routes.user_search'), 'UserController@search')->name('user.search');
			Route::get(LaravelLocalization::transRoute('routes.user_edit'), 'UserController@edit')->name('user.edit');
			Route::put('user/{user_id}', 'UserController@update')->name('user.update');

			Route::get('user/{user_id}/addPreferred/{property_id}', 'UserController@addPreferred')->name('user.add_preferred');
			Route::get('user/{user_id}/removePreferred/{property_id}', 'UserController@removePreferred')->name('user.remove_preferred');
			Route::get('user/{user_id}/addSearch', 'UserController@addSearch')->name('user.add_search');
			Route::get('user/{user_id}/removeSearch/{search_id}', 'UserController@removeSearch')->name('user.remove_search');
		});
	});

	// TV
	Route::group(['namespace' => 'Tv', 'as' => 'tv::'], function() {
		Route::get('tv', 'TvController@home')->name('home');
	});
});

// Ajax
Route::group(['prefix' => 'ajax', 'as' => 'ajax::'], function() {
	Route::get('province/{id}/towns', function ($id) {
		return response()->json(['towns' => \App\Models\Town::toSearch($id)], 200);
	})->name('province.towns');
	Route::get('town/{id}/areas', function ($id) {
		return response()->json(['areas' => \App\Models\Area::toSearch($id)], 200);
	})->name('town.areas');
});

// Admin
Route::get('admin', 'Admin\AdminController@root')->middleware('auth')->name('admin::root');

Route::group([
	'prefix' => 'admin',
	'namespace' => 'Admin',
	'as' => 'admin::',
	'middleware' => ['auth', 'role:agent|admin', 'set_ita_lang']
], function () {

	Route::get('dashboard/statistics', function () {
		return View::make('admin.statistics.statistics');
	})->name('dashboard.statistics');

	// Area
	Route::put('area/{id}/upload_photo', 'AreaController@uploadPhoto')->name('area.upload_photo');
	Route::delete('area/{id}/destroy_photo', 'AreaController@destroyPhoto')->name('area.destroy_photo');
	Route::get('area/statistics', 'AreaController@statistics')->name('area.statistics');
	Route::put('area/statistics_reset/{id}', 'AreaController@statisticsReset')->name('area.statistics_reset');
	Route::resource('area', 'AreaController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'area.index',
			'store' => 'area.store',
			'update' => 'area.update',
			'destroy' => 'area.destroy'
		]
	]);
	
	// Category
	Route::get('category/statistics', 'CategoryController@statistics')->name('category.statistics');
	Route::put('category/statistics_reset/{id}', 'CategoryController@statisticsReset')->name('category.statistics_reset');
	Route::resource('category', 'CategoryController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'category.index',
			'store' => 'category.store',
			'update' => 'category.update',
			'destroy' => 'category.destroy'
		]
	]);
	
	// Property
	Route::get('property/{id}/photos', 'PropertyController@editPhotos')->name('property.edit_photos');
	Route::put('property/{id}/upload_photo', 'PropertyController@uploadPhoto')->name('property.upload_photo');
	Route::delete('property/{property_id}/destroy_photo/{photo_id}', 'PropertyController@destroyPhoto')->name('property.destroy_photo');
	Route::get('property/{property_id}/set_main/{photo_id}', 'PropertyController@setMainPhoto')->name('property.set_photo_main');
	Route::post('property/{id}/sort_photos', 'PropertyController@sortPhotos')->name('property.sort_photos');

	Route::get('property/print', 'PropertyController@print')->name('property.print');
	Route::get('property/trashed', 'PropertyController@trashed')->name('property.trashed');
	Route::get('property/showcase', 'PropertyController@showcase')->name('property.showcase');
	Route::put('property/{id}/restore', 'PropertyController@restore')->name('property.restore');
	Route::delete('property/{id}/force_delete', 'PropertyController@forceDelete')->name('property.force_delete');
	Route::get('property/{id}/share', 'PropertyController@share')->name('property.share');
	Route::get('property/{id}/suspend', 'PropertyController@suspend')->name('property.suspend');
	Route::get('property/{id}/active', 'PropertyController@active')->name('property.active');
	Route::get('property/{id}/remove_showcase', 'PropertyController@removeShocase')->name('property.remove_showcase');
	Route::get('property/{id}/add_showcase', 'PropertyController@addShowcase')->name('property.add_showcase');
	Route::get('property/{id}/remove_showcase_tv', 'PropertyController@removeShocaseTV')->name('property.remove_showcase_tv');
	Route::get('property/{id}/add_showcase_tv', 'PropertyController@addShowcaseTV')->name('property.add_showcase_tv');
	Route::get('property/{id}/print_o', 'PropertyController@printO')->name('property.print_o');
	Route::get('property/{id}/print_v', 'PropertyController@printV')->name('property.print_v');
	Route::get('property/{id}/print_new', 'PropertyController@printNew')->name('property.print_new');
	Route::get('property/{id}/print_test', 'PropertyController@printTest')->name('property.print_test');
	Route::get('property/statistics', 'PropertyController@statistics')->name('property.statistics');
	Route::put('property/statistics_reset/{id}', 'PropertyController@statisticsReset')->name('property.statistics_reset');
	Route::resource('property', 'PropertyController', [
		'except' => ['show'],
		'names' => [
			'index' => 'property.index',
			'create' => 'property.create',
			'store' => 'property.store',
			'edit' => 'property.edit',
			'update' => 'property.update',
			'destroy' => 'property.destroy'
		]
	]);
	
	// Province
	Route::get('province/statistics', 'ProvinceController@statistics')->name('province.statistics');
	Route::put('province/statistics_reset/{id}', 'ProvinceController@statisticsReset')->name('province.statistics_reset');
	Route::resource('province', 'ProvinceController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'province.index',
			'store' => 'province.store',
			'update' => 'province.update',
			'destroy' => 'province.destroy'
		]
	]);
	
	// Realestate
	Route::resource('realestate', 'RealestateController', [
		'only' => ['edit', 'update'],
		'names' => [
			'edit' => 'realestate.edit',
			'update' => 'realestate.update'
		]
	]);
	
	// Typology
	Route::get('typology/statistics', 'TypologyController@statistics')->name('typology.statistics');
	Route::put('typology/statistics_reset/{id}', 'TypologyController@statisticsReset')->name('typology.statistics_reset');
	Route::resource('typology', 'TypologyController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'typology.index',
			'store' => 'typology.store',
			'update' => 'typology.update',
			'destroy' => 'typology.destroy'
		]
	]);
	
	// Town
	Route::put('town/{id}/upload_photo', 'TownController@uploadPhoto')->name('town.upload_photo');
	Route::delete('town/{id}/destroy_photo', 'TownController@destroyPhoto')->name('town.destroy_photo');
	Route::get('town/statistics', 'TownController@statistics')->name('town.statistics');
	Route::put('town/statistics_reset/{id}', 'TownController@statisticsReset')->name('town.statistics_reset');
	Route::resource('town', 'TownController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'town.index',
			'store' => 'town.store',
			'update' => 'town.update',
			'destroy' => 'town.destroy'
		]
	]);
	
	// Type_box
	Route::resource('type_box', 'TypeBoxController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_box.index',
			'store' => 'type_box.store',
			'update' => 'type_box.update',
			'destroy' => 'type_box.destroy'
		]
	]);
	
	// Type_distsm
	Route::resource('type_distsm', 'TypeDistsmController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_distsm.index',
			'store' => 'type_distsm.store',
			'update' => 'type_distsm.update',
			'destroy' => 'type_distsm.destroy'
		]
	]);
	
	// Type_exposure
	Route::resource('type_exposure', 'TypeExposureController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_exposure.index',
			'store' => 'type_exposure.store',
			'update' => 'type_exposure.update',
			'destroy' => 'type_exposure.destroy'
		]
	]);
	
	// Type_floor
	Route::resource('type_floor', 'TypeFloorController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_floor.index',
			'store' => 'type_floor.store',
			'update' => 'type_floor.update',
			'destroy' => 'type_floor.destroy'
		]
	]);
	
	// Type_garden
	Route::resource('type_garden', 'TypeGardenController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_garden.index',
			'store' => 'type_garden.store',
			'update' => 'type_garden.update',
			'destroy' => 'type_garden.destroy'
		]
	]);
	
	// Type_heating
	Route::resource('type_heating', 'TypeHeatingController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_heating.index',
			'store' => 'type_heating.store',
			'update' => 'type_heating.update',
			'destroy' => 'type_heating.destroy'
		]
	]);
	
	// Type_kitchen
	Route::resource('type_kitchen', 'TypeKitchenController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_kitchen.index',
			'store' => 'type_kitchen.store',
			'update' => 'type_kitchen.update',
			'destroy' => 'type_kitchen.destroy'
		]
	]);
	
	// Type_negotiation
	Route::resource('type_negotiation', 'TypeNegotiationController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_negotiation.index',
			'store' => 'type_negotiation.store',
			'update' => 'type_negotiation.update',
			'destroy' => 'type_negotiation.destroy'
		]
	]);
	
	// Type_occupation
	Route::resource('type_occupation', 'TypeOccupationController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_occupation.index',
			'store' => 'type_occupation.store',
			'update' => 'type_occupation.update',
			'destroy' => 'type_occupation.destroy'
		]
	]);
	
	// Type_status
	Route::resource('type_status', 'TypeStatusController', [
		'except' => ['create', 'show', 'edit'],
		'names' => [
			'index' => 'type_status.index',
			'store' => 'type_status.store',
			'update' => 'type_status.update',
			'destroy' => 'type_status.destroy'
		]
	]);

	// Notification
	Route::get('user/{id}/notification', 'UserController@notification')->name('user.notification');
	Route::get('user/{user_id}/notification/{not_id}', 'UserController@notificationShow')->name('user.notification_show');

	// Search
	Route::get('user/{user_id}/research', 'SearchController@index')->name('search.index');
	Route::get('user/{user_id}/research/{search_id}', 'SearchController@show')->name('search.show');
	
	// User
	Route::put('user/{id}/upload_photo', 'UserController@uploadPhoto')->name('user.upload_photo');
	Route::delete('user/{id}/destroy_photo', 'UserController@destroyPhoto')->name('user.destroy_photo');
	Route::get('user/{id}/preferred', 'UserController@preferred')->name('user.preferred');
	Route::resource('user', 'UserController', [
		'names' => [
			'index' => 'user.index',
			'create' => 'user.create',
			'store' => 'user.store',
			'show' => 'user.show',
			'edit' => 'user.edit',
			'update' => 'user.update',
			'destroy' => 'user.destroy'
		]
	]);
});

/*
Route::get('images/{template}/{rifimm}/{number}', 'PhotoController@imagesProperty')->name('images_property');
Route::get('images/{template}/{name}', 'PhotoController@imagesOther')->name('images_other');
Route::any('{all}', function($uri) {
    return abort(404);
})->where('all', '.*');
*/