<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
	'prefix' => 'v1',
	'namespace' => 'Api/v1',
	'as' => 'api::'
], function () {

	/*Route::apiResource([
		'area' => 'AreaController',
		'category' => 'CategoryController',
		'property' => 'PropertyController',
		'province' => 'ProvinceController',
		'realestate' => 'RealestateController',
		'typology' => 'TypologyController',
		'town' => 'TownController',
		'type_box' => 'TypeBoxController',
		'type_distsm' => 'TypeDistsmController',
		'type_exposure' => 'TypeExposureController',
		'type_floor' => 'TypeFloorController',
		'type_garden' => 'TypeGardenController',
		'type_heating' => 'TypeHeatingController',
		'type_kitchen' => 'TypeKitchenController',
		'type_negotiation' => 'TypeNegotiationController',
		'type_occupation' => 'TypeOccupationController',
		'type_status' => 'TypeStatusController',
		'user' => 'UserController'
	]);*/
});

/* TODO {
    Route::post('auth/login', 'api\v1\Auth\AuthController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        // Agent
        Route::put('user/{id}/upload_photo', 'api\v1\UserController@uploadPhoto');
        Route::delete('user/{id}/destroy_photo', 'api\v1\UserController@destroyPhoto');
        Route::resource('user', 'api\v1\UserController', [ 'only' => ['index', 'show', 'update'] ]);
        // Area
        Route::put('area/{id}/upload_photo', 'api\v1\AreaController@uploadPhoto');
        Route::delete('area/{id}/destroy_photo', 'api\v1\AreaController@destroyPhoto');
        Route::resource('area', 'api\v1\AreaController', [ 'except' => ['create', 'edit'] ]);
        // Category
        Route::resource('category', 'api\v1\CategoryController', [ 'except' => ['create', 'edit'] ]);
        // Property
        Route::get('property/{id}/trashed', 'api\v1\PropertyController@trashed');
        Route::put('property/{id}/restore', 'api\v1\PropertyController@restore');
        Route::delete('property/{id}/force_delete', 'api\v1\PropertyController@forceDelete');
        Route::put('property/{id}/visible/{is_visible}', 'api\v1\PropertyController@changeVisible');
        Route::put('property/{id}/showcase/{is_showcase}', 'api\v1\PropertyController@changeShocase');
        Route::put('property/{id}/showcase_tv/{is_showcase}', 'api\v1\PropertyController@changeShocaseTV');
        Route::resource('property', 'api\v1\PropertyController', [ 'except' => ['create', 'edit'] ]);
        // Province
        Route::get('province/{id}/towns', 'api\v1\ProvinceController@getTowns');
        Route::resource('province', 'api\v1\ProvinceController', [ 'except' => ['create', 'edit'] ]);
        // Realestate
        Route::resource('realestate', 'api\v1\RealestateController', [ 'only' => ['index', 'show', 'update'] ]);
        // Typology
        Route::resource('typology', 'api\v1\TypologyController', [ 'except' => ['create', 'edit'] ]);
        // Town
        Route::put('town/{id}/upload_photo', 'api\v1\TownController@uploadPhoto');
        Route::delete('town/{id}/destroy_photo', 'api\v1\TownController@destroyPhoto');
        Route::get('town/{id}/areas', 'api\v1\TownController@getAreas');
        Route::resource('town', 'api\v1\TownController', [ 'except' => ['create', 'edit'] ]);
        // Type_box
        Route::resource('type_box', 'api\v1\TypeBoxController', [ 'except' => ['create', 'edit'] ]);
        // Type_distsm
        Route::resource('type_distsm', 'api\v1\TypeDistsmController', [ 'except' => ['create', 'edit'] ]);
        // Type_exposure
        Route::resource('type_exposure', 'api\v1\TypeExposureController', [ 'except' => ['create', 'edit'] ]);
        // Type_floor
        Route::resource('type_floor', 'api\v1\TypeFloorController', [ 'except' => ['create', 'edit'] ]);
        // Type_garden
        Route::resource('type_garden', 'api\v1\TypeGardenController', [ 'except' => ['create', 'edit'] ]);
        // Type_heating
        Route::resource('type_heating', 'api\v1\TypeHeatingController', [ 'except' => ['create', 'edit'] ]);
        // type_kitchen
        Route::resource('type_kitchen', 'api\v1\TypeKitchenController', [ 'except' => ['create', 'edit'] ]);
        // Type_negotiation
        Route::resource('type_negotiation', 'api\v1\TypeNegotiationController', [ 'except' => ['create', 'edit'] ]);
        // Type_occupation
        Route::resource('type_occupation', 'api\v1\TypeOccupationController', [ 'except' => ['create', 'edit'] ]);
        // Type_status
        Route::resource('type_status', 'api\v1\TypeStatusController', [ 'except' => ['create', 'edit'] ]);
        // Where_we_are
        Route::resource('where_we_are', 'api\v1\WhereWeAareController', [ 'only' => ['index', 'show', 'update'] ]);
    });
} */