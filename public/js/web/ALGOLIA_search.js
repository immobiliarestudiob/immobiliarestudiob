'use strict';
/* global instantsearch */

var search = instantsearch({
  appId: 'DQPHVMNPBN',
  apiKey: '622b31e4661e9f12a41a6618e13d9ccf',
  indexName: 'properties',
  urlSync: true
});

search.addWidget(
  instantsearch.widgets.searchBox({
    container: '#search-input',
    placeholder: 'Search for products'
  })
  );

search.addWidget(
  instantsearch.widgets.hits({
    container: '#hits',
    hitsPerPage: 10,
    templates: {
      item: getTemplate('hit'),
      empty: getTemplate('no-results')
    }
  })
  );

search.addWidget(
  instantsearch.widgets.stats({
    container: '#stats'
  })
  );

search.addWidget(
  instantsearch.widgets.sortBySelector({
    container: '#sort-by',
    autoHideContainer: true,
    indices: [
    { name: search.indexName, label: 'Most relevant' },
    { name: search.indexName + '_price_asc', label: 'Lowest price' },
    { name: search.indexName + '_price_desc', label: 'Highest price'}
    ]
  })
  );

search.addWidget(
  instantsearch.widgets.pagination({
    container: '#pagination'
  })
  );

search.addWidget(
  instantsearch.widgets.menu({
    container: '#province',
    attributeName: 'province.name',
    limit: 10,
    sortBy: ['name:asc'],
    templates: {
      header: '<h5>Province</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.menu({
    container: '#town',
    attributeName: 'town.name',
    limit: 10,
    sortBy: ['name:asc'],
    templates: {
      header: '<h5>Town</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.menu({
    container: '#area',
    attributeName: 'area.name',
    limit: 10,
    sortBy: ['name:asc'],
    templates: {
      header: '<h5>Area</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.menu({
    container: '#category',
    attributeName: 'category.name_it',
    limit: 10,
    sortBy: ['name_it:asc'],
    templates: {
      header: '<h5>Category</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.menu({
    container: '#typology',
    attributeName: 'typology.name_it',
    limit: 30,
    sortBy: ['name_it:asc'],
    templates: {
      header: '<h5>Typology</h5>'
    }
  })
  );

/*search.addWidget(
  instantsearch.widgets.refinementList({
    container: '#category',
    attributeName: 'category.name_it',
    limit: 10,
    sortBy: ['name:asc'],
    operator: 'or',
    templates: {
      header: '<h5>Category</h5>'
    }
  })
  );
*/

search.addWidget(
  instantsearch.widgets.rangeSlider({
    container: '#price',
    attributeName: 'price.value',
    templates: {
      header: '<h5>Price</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.rangeSlider({
    container: '#mq',
    attributeName: 'square_meters',
    templates: {
      header: '<h5>MQ</h5>'
    }
  })
  );

search.addWidget(
  instantsearch.widgets.clearAll({
    container: '#clear-all',
    templates: {
      link: '<i class="fa fa-eraser"></i> Clear all filters'
    },
    cssClasses: {
      root: 'btn btn-block btn-default'
    },
    autoHideContainer: true
  })
  );

function getTemplate(templateName) {
  return document.getElementById(templateName + '-template').innerHTML;
}

search.start();