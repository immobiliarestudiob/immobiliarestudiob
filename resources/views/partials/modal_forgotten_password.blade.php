<div class="modal fade apartment-modal" id="forgot-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-title">
					<h1>@lang('auth.forgot_password')<span class="special-color">?</span></h1>
					<div class="short-title-separator"></div>
				</div>
				<p class="negative-margin forgot-info">@lang('auth.forgot_text')</p>

				{{ Form::open(array('route' => 'auth::password_email', 'method' => 'post')) }}
				{{ Form::text('email', old('username'), array('class' => 'input-full main-input', 'placeholder' => 'Email')) }}
				<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
					<span>Reset password</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-user"></i></div>
				</span >
				{{ Form::submit('submit', array('style' => 'display:none')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>