{{ Form::open(array('route' => 'web::request', 'method' => 'post', 'name' => 'request-form', 'id' => 'request-form')) }}
<div id="form-result">

	@if (session('success_message'))
	<div class="success-box"><p>{{ session('success_message') }}</p></div>
	@endif

	@if (count($errors) > 0)
	<div class="error-box">
		@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
		@endforeach
	</div>
	@endif
</div>
{{ Form::text('name', old('name'), array('class' => 'input-full main-input', 'placeholder' => trans('web.request_form_name'))) }}
{{ Form::text('phone', old('phone'), array('class' => 'input-full main-input', 'placeholder' => trans('web.request_form_phone'))) }}
{{ Form::text('mail', old('mail'), array('class' => 'input-full main-input', 'placeholder' => trans('web.request_form_email'))) }}
@if(isset($request_rifimm))
	{{ Form::textarea('message', $request_rifimm." - ", array('class' => 'input-full agent-textarea main-input')) }}
@else
	{{ Form::textarea('message', old('message'), array('class' => 'input-full agent-textarea main-input', 'placeholder' => trans('web.request_form_question'))) }}
@endif
<input type="submit" class="hidden" />
<div class="form-submit-cont">
	<span class="button-primary pull-right" id="form-submit" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
		<span>@lang('web.request_form_send')</span>
		<div class="button-triangle"></div>
		<div class="button-triangle2"></div>
		<div class="button-icon"><i class="fa fa-paper-plane"></i></div>
	</span >
	<div class="clearfix"></div>
</div>
{{ Form::close() }}