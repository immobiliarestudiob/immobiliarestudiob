@php $random_tmp = rand(0,9); @endphp
<div class="list-offer">
	<div class="list-offer-left">
		<div class="list-offer-front">
			<div class="list-offer-photo">
				<img src="{{$property->mainPhoto()->getLarge()}}" class="img-responsive" style="height: 177px" alt="{{$property->rifimm}}" />
				<div class="type-container">
					<div class="estate-type">{{$property->typology->name}}</div>
				</div>
			</div>
			<div class="list-offer-params">
				@if($property->square_meters > 0)
				<div class="list-area">
					<img src="/images/web/area-icon.png" alt="" />{!!$property->squareMeters!!}
				</div>
				@endif
				@if($property->locals > 0)
				<div class="list-rooms">
					<img src="/images/web/rooms-icon.png" alt="" />{{$property->locals}}
				</div>
				@endif
				@if($property->bath > 0)
				<div class="list-baths">
					<img src="/images/web/bathrooms-icon.png" alt="" />{{$property->bath}}
				</div>
				@endif							
			</div>	
		</div>
		<div class="list-offer-back">
			<div id="row_list_{{$property->rifimm}}-{{$random_tmp}}" class="list-offer-map"></div>
		</div>
	</div>
	<div class="list-offer-right">
		<div class="list-offer-text">
			<i class="fa fa-map-marker list-offer-localization"></i>
			<div class="list-offer-h4"><a href="{{route('web::details', $property->rifimm)}}" name="show_property"><h4 class="list-offer-title">@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})</h4></a></div>
			<div class="clearfix"></div>
			<a href="{{route('web::details', $property->rifimm)}}" name="show_property">{!!$property->shortDescription!!}</a>
			<div class="clearfix"></div>
		</div>
		<div class="profile-list-footer">
			<div class="list-price profile-list-price">
				{{ $property->publicPriceLittle }}
			</div>
			@if(!$hide_preferred)
			@if(Auth::check())
				@if(Auth::user()->hasPreferred($property->id))
					<a href="{{route('web::user.remove_preferred', [$user->id, $property->id])}}" class="profile-list-delete" name="@lang('web.user_preferred_remove')">
						<i class="fa fa-heart fa-lg"></i>
					</a>
				@else
					<a href="{{route('web::user.add_preferred', [Auth::user()->id, $property->id])}}" class="profile-list-edit" name="@lang('web.user_preferred_add')">
						<i class="fa fa-heart-o fa-lg"></i>
					</a>
				@endif
			@endif
			@endif
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="clearfix"></div>

@push('scripts_mapInit')
@if($property->address_geocode != null)
mapInit({{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}}, "row_list_{{$property->rifimm}}-{{$random_tmp}}","/images/web/pin-{{$property->typology->class}}.png", false);
@else
mapInitAddress("{{$property->town->name}}, {{$property->province->name}}, Italy","row_list_{{$property->rifimm}}-{{$random_tmp}}","/images/web/pin-{{$property->typology->class}}.png", false);
@endif
@endpush