<h3 class="sidebar-title">@lang('web.search_title')<span class="special-color">.</span></h3>
<div class="title-separator-primary"></div>

{{ Form::open(array('route' => 'web::search', 'method' => 'get')) }}
<div class="sidebar-select-cont">
	<select name="category_id" class="bootstrap-select" title="@lang('web.search_category'):">
		@foreach($search_parameters['category']['search'] as $key => $value)
			<option value="{{ $key }}" @if( $search_parameters['category']['id'] == $key) selected="selected" @endif>{{ $value }}</option>
		@endforeach
	</select>
	<select data-live-search="true" name="typology_id" class="bootstrap-select select-province" title="@lang('web.search_typology'):">
		@foreach( $search_parameters['typology']['search'] as $key => $value)
			<option value="{{ $key }}" @if( $search_parameters['typology']['id'] == $key) selected="selected" @endif>{{ $value }}</option>
		@endforeach
	</select>
	<select data-live-search="true" name="province_id" class="bootstrap-select select-province" title="@lang('web.search_province'):" data-town-id="#select-town">
		@foreach( $search_parameters['province']['search'] as $key => $value)
			<option value="{{ $key }}" @if( $search_parameters['province']['id'] == $key) selected="selected" @endif>{{ $value }}</option>
		@endforeach
	</select>
	<select data-live-search="true" name="town_id" class="bootstrap-select select-town" title="@lang('web.search_town'):" data-area-id="#select-area" id="select-town">
		@foreach( $search_parameters['town']['search'] as $key => $value)
			<option value="{{ $key }}" @if( $search_parameters['town']['id'] == $key) selected="selected" @endif>{{ $value }}</option>
		@endforeach
	</select>
	<select data-live-search="true" name="area_id" class="bootstrap-select" title="@lang('web.search_area'):" id="select-area">
		@foreach( $search_parameters['area']['search'] as $key => $value)
			<option value="{{ $key }}" @if( $search_parameters['area']['id'] == $key) selected="selected" @endif>{{ $value }}</option>
		@endforeach
	</select>
</div>
<div class="adv-search-range-cont">
	<label for="slider-range-price-sidebar-value-min" class="adv-search-label">@lang('web.search_price'):</label>
	<input type="text" name="min_price" id="slider-range-price-sidebar-value-min" readonly class="hidden">
	<input type="text" name="max_price" id="slider-range-price-sidebar-value-max" readonly class="hidden">
	<div class="adv-search-amount">
		<span>€</span><span id="slider-range-price-sidebar-span"></span>
	</div>
	<div class="clearfix"></div>
	<div id="slider-range-price-sidebar" data-min="{{ $search_parameters['price']['global']['min'] }}" data-max="{{ $search_parameters['price']['global']['max'] }}" data-min-val="{{ $search_parameters['price']['local']['min'] }}" data-max-val="{{ $search_parameters['price']['local']['min'] }}" data-step="1000" class="slider-range"></div>
</div>
<div class="adv-search-range-cont">
	<label for="slider-range-area-sidebar-value-min" class="adv-search-label">@lang('web.search_mq'):</label>
	<input type="text" name="min_square_meters" id="slider-range-area-sidebar-value-min" readonly class="hidden">
	<input type="text" name="max_square_meters" id="slider-range-area-sidebar-value-max" readonly class="hidden">
	<div class="adv-search-amount">
		<span>m<sup>2</sup></span><span id="slider-range-area-sidebar-span"></span>
	</div>
	<div class="clearfix"></div>
	<div id="slider-range-area-sidebar" data-min="{{ $search_parameters['square_meters']['global']['min'] }}" data-max="{{ $search_parameters['square_meters']['global']['max'] }}" data-min-val="{{ $search_parameters['square_meters']['local']['min'] }}" data-max-val="{{ $search_parameters['square_meters']['local']['max'] }}" data-step="10" class="slider-range"></div>
</div>
<div class="adv-search-range-cont">
	<label for="slider-range-locals-sidebar-value-min" class="adv-search-label">@lang('web.search_locals'):</label>
	<input type="text" name="min_locals" id="slider-range-locals-sidebar-value-min" readonly class="hidden">
	<input type="text" name="max_locals" id="slider-range-locals-sidebar-value-max" readonly class="hidden">
	<span id="slider-range-locals-sidebar-span" class="adv-search-amount"></span>
	<div class="clearfix"></div>
	<div id="slider-range-locals-sidebar" data-min="{{ $search_parameters['local']['global']['min'] }}" data-max="{{ $search_parameters['local']['global']['max'] }}" data-min-val="{{ $search_parameters['local']['local']['min'] }}" data-max-val="{{ $search_parameters['local']['local']['max'] }}" data-step="1" class="slider-range"></div>
</div>
<div class="adv-search-range-cont">
	<label for="slider-range-room-sidebar-value-min" class="adv-search-label">@lang('web.search_room'):</label>
	<input type="text" name="min_room" id="slider-range-room-sidebar-value-min" readonly class="hidden">
	<input type="text" name="max_room" id="slider-range-room-sidebar-value-max" readonly class="hidden">
	<span id="slider-range-room-sidebar-span" class="adv-search-amount"></span>
	<div class="clearfix"></div>
	<div id="slider-range-room-sidebar" data-min="{{ $search_parameters['room']['global']['min'] }}" data-max="{{ $search_parameters['room']['global']['max'] }}" data-min-val="{{ $search_parameters['room']['local']['min'] }}" data-max-val="{{ $search_parameters['room']['local']['max'] }}" data-step="1" class="slider-range"></div>
</div>
<div class="adv-search-range-cont">
	<label for="slider-range-bathrooms-sidebar-value-min" class="adv-search-label">@lang('web.search_bath'):</label>
	<input type="text" name="min_bath" id="slider-range-bathrooms-sidebar-value-min" readonly class="hidden">
	<input type="text" name="max_bath" id="slider-range-bathrooms-sidebar-value-max" readonly class="hidden">
	<span id="slider-range-bathrooms-sidebar-span" class="adv-search-amount"></span>
	<div class="clearfix"></div>
	<div id="slider-range-bathrooms-sidebar" data-min="{{ $search_parameters['bath']['global']['min'] }}" data-max="{{ $search_parameters['bath']['global']['max'] }}" data-min-val="{{ $search_parameters['bath']['local']['min'] }}" data-max-val="{{ $search_parameters['bath']['local']['max'] }}" data-step="1" class="slider-range"></div>
</div>
<div class="sidebar-search-button-cont">
	<span class="button-primary button-full button-shadow" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
		<span>@lang('web.search_btn_search')</span>
		<div class="button-triangle"></div>
		<div class="button-triangle2"></div>
		<div class="button-icon"><i class="fa fa-search"></i></div>
	</span>
</div>
{{ Form::close() }}