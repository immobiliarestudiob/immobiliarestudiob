<div class="list-agency">
	<div class="list-agency-left">
		<div class="mySearch list-agency-description">
			<div class="team-desc-line">
				<span>@lang('web.search_price'): {{$search->getMinPrice()}} - {{$search->getMaxPrice()}}</span><br />
				<span>@lang('web.search_mq'): {!!$search->getMinSquareMeters()!!} - {!!$search->getMaxSquareMeters()!!}</span><br />
				<span>@lang('web.search_locals'): {{$search->min_locals}} - {{$search->max_locals}}</span><br />
				<span>@lang('web.search_room'): {{$search->min_room}} - {{$search->max_room}}</span><br />
				<span>@lang('web.search_bath'): {{$search->min_bath}} - {{$search->max_bath}}</span>
			</div>
		</div>
	</div>

	<a class="list-agency-right-large" href="{{route('web::search')}}?{{http_build_query($search->getParamasUrl())}}" name="show_search">
		<div class="list-agency-text">
			<h3 class="list-agency-title">{{$search->getTypologyName()}} {{$search->getCategoryName()}}</h3>
			<i class="fa fa-map-marker"></i>
			<span class="list-agency-address">{{$search->getTownName()}}</span>
			<div class="list-agency-separator"></div>
			{{trans_choice('web.user_search_numberproperties', $search->getMatchProperties()->count(), ['n' => $search->getMatchProperties()->count()])}}
		</div>
	</a>
	<div class="small-triangle"></div>
	<div class="small-triangle2"></div>
	<a class="small-icon" href="{{route('web::user.remove_search', [$user->id, $search->id])}}" title="@lang('web.user_search_remove')" name="@lang('web.user_search_remove')"><i class="fa fa-heart fa-2x"></i></a>
</div>