@if($properties->count() > 0)

@php
if(!isset($hide_preferred))
	$hide_preferred = false;
@endphp

<div class="row list-offer-row">
	<div class="col-xs-12">
		@foreach($properties as $property)
			@include('partials.property_row_little', ['property' => $property, 'hide_preferred' => $hide_preferred])
		@endforeach
	</div>
</div>

@if($properties->total() > 12)
{{ $properties->appends(Input::except('page'))->links() }}
@endif

@if(!$hide_preferred)
@push('scripts_inside')
$(".profile-list-delete")
.mouseover(function() { $(this).find("i").removeClass('fa-heart').addClass('fa-trash'); })
.mouseout(function() { $(this).find("i").removeClass('fa-trash').addClass('fa-heart'); })

$(".profile-list-edit")
.mouseover(function() { $(this).find("i").removeClass('fa-heart-o').addClass('fa-plus'); })
.mouseout(function() { $(this).find("i").removeClass('fa-plus').addClass('fa-heart-o'); })
@endpush
@endif

@endif