@if($properties_promo->count() > 0)
<section class="featured-offers parallax">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				{{--<h5 class="subtitle-margin second-color">@lang('web.properties_promo_subtitle')</h5>--}}
				<h1 class="second-color">@lang('web.properties_promo_title')<span class="special-color">.</span></h1>
			</div>
			<div class="col-xs-12 col-sm-3">
				<a href="#" class="navigation-box navigation-box-next" id="featured-offers-owl-next" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
				<a href="#" class="navigation-box navigation-box-prev" id="featured-offers-owl-prev" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>								
			</div>
			<div class="col-xs-12">
				<div class="title-separator-secondary"></div>
			</div>
		</div>
	</div>
	<div class="featured-offers-container">
		<div class="owl-carousel" id="featured-offers-owl">
			@foreach($properties_promo as $tmp_property)
			@include('partials.home_property_promo_little', ['property' => $tmp_property])
			@endforeach
		</div>
	</div>
</section>
@endif