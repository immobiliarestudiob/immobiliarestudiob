<header>
	<div class="top-bar-wrapper">
		<div class="container top-bar">
			<div class="row">
				<div class="col-xs-5 col-sm-8">
					<div class="top-mail pull-left hidden-xs">
						<span class="top-icon-circle">
							<i class="fa fa-envelope fa-sm"></i>
						</span>
						<span class="top-bar-text"><a href="mailto:{{$realestate->email}}" name="send_email">{{$realestate->email}}</a></span>
					</div>
					<div class="top-phone pull-left hidden-xxs">
						<span class="top-icon-circle">
							<i class="fa fa-phone"></i>
						</span>
						<span class="top-bar-text"><a href="tel:{{$realestate->telephone}}" name="make_call">{{$realestate->telephone}}</a></span>
					</div>
					<div class="top-localization pull-left hidden-sm hidden-md hidden-xs">
						<span class="top-icon-circle pull-left">
							<i class="fa fa-map-marker"></i>
						</span>
						<span class="top-bar-text"><a href="https://www.google.it/maps/place/Studio+B+Immobiliare+S.R.L./@43.825591,7.6907407,15.99z/data=!4m5!3m4!1s0x0:0xa759a6000529b184!8m2!3d43.8266211!4d7.6950659?hl=it" target="_blank" name="maps" rel="noopener">{{$realestate->address}}</a></span>
					</div>
				</div>
				<div class="col-xs-7 col-sm-4">
					
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
					<div class="top-social pull-right">
						<a class="top-icon-flag" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" rel="noopener" name="{{$properties['native']}}">
							<img src="/images/web/flag_{{$localeCode}}.png" class="top_flag" alt="{{$properties['native']}}" />
						</a>
					</div>
					@endforeach
					<style type="text/css">.top-icon-flag{display:block;width:24px;height:24px;margin-top:8px;text-align:center}.top_flag{width:22px;height:22px;-webkit-filter:grayscale(100%);filter:grayscale(100%)}.top_flag:hover{-webkit-filter:grayscale(0%);filter:grayscale(0%)}</style>

					<div class="top-social pull-right">
						<a class="top-icon-circle" href="https://www.facebook.com/immobiliarestudiob" target="_blank" rel="noopener" name="link_to_facebook">
							<i class="fa fa-facebook"></i>
						</a>
					</div>
					<div class="top-social pull-right">
						<a class="top-icon-circle" href="https://plus.google.com/u/0/b/112559124162598172930/+Immobiliarestudiob" target="_blank" rel="noopener" name="link_to_google_plus">
							<i class="fa fa-google-plus"></i>
						</a>
					</div>
				</div>
			</div>
		</div><!-- /.top-bar -->	
	</div><!-- /.Page top-bar-wrapper -->	
	<nav class="navbar main-menu-cont">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar icon-bar1"></span>
					<span class="icon-bar icon-bar2"></span>
					<span class="icon-bar icon-bar3"></span>
				</button>
				<a href="{{route('web::home')}}" name="homepage" class="navbar-brand">
					<img src="/images/web/logo-dark.png" alt="Immobiliare Studio B" />
				</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{route('web::home')}}" name="home">Home</a></li>
					<li><a href="{{route('web::search')}}" name="@lang('web.header_search')">@lang('web.header_search')</a>
					<li><a href="{{route('web::about')}}" name="@lang('web.header_about')">@lang('web.header_about')</a></li>
					{{--<li><a href="{{route('web::contact')}}" name="@lang('web.header_contact_us')">@lang('web.header_contact_us')</a></li></li>--}}
					<li><a href="@if(Auth::check())
						{{route('admin::root')}}
					@else
						@if(isset($hide_menu_admin))
							{{route('auth::login_get')}}
						@else
							#login-modal
						@endif
					@endif" data-toggle="modal" class="special-color" name="@lang('web.header_login')">@lang('web.header_login')</a></li>
				</ul>
			</div>
		</div>
	</nav><!-- /.mani-menu-cont -->	
</header>