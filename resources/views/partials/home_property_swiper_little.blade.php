<!-- Slides -->
<div class="swiper-slide">
	<div class="slide-bg swiper-lazy" data-background="{{$property->mainPhoto()->getOriginalSize()}}"></div>
	<!-- Preloader image -->
	<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-sm-offset-2 col-md-offset-4 col-lg-offset-6 slide-desc-col animated fadeInDown slide-desc-{{$loop->iteration}}">
				<div class="slide-desc pull-right">
					<div class="slide-desc-text">
						<div class="estate-type">{{$property->typology->name}}</div>
						<h4><i class="fa fa-map-marker"></i>@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})</h4>
						<div class="clearfix"></div>
						<p>{!!$property->shortDescription!!}</p>
					</div>
					<div class="slide-desc-params">	
						@if($property->square_meters > 0)
						<div class="slide-desc-area">
							<img src="/images/web/area-icon.png" title="@lang('web.details_area')" />{!!$property->squareMeters!!}
						</div>
						@endif
						@if($property->locals > 0)
						<div class="slide-desc-rooms">
							<img src="/images/web/rooms-icon.png" title="@lang('web.details_rooms')" />{{$property->locals}}
						</div>
						@endif
						@if($property->bath > 0)
						<div class="slide-desc-baths">
							<img src="/images/web/bathrooms-icon.png" title="@lang('web.details_bathrooms')" />{{$property->bath}}
						</div>
						@endif
						@if($property->hasDistsm())
						<div class="slide-desc-sea">
							<img src="/images/web/sea-icon.png" title="@lang('web.details_distmm')" />{{$property->typeDistsm->name}}
						</div>
						@endif
					</div>
					<div class="slide-desc-price">
						{{ $property->publicPriceLittle }}
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="slide-buttons slide-buttons-right">
					<a href="#" class="navigation-box navigation-box-next slide-next" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
					<a href="{{route('web::details', [$property->rifimm])}}" class="navigation-box navigation-box-more slide-more" name="show_property"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont fa-lg">&#xe813;</i></div></a>
					<a href="#" class="navigation-box navigation-box-prev slide-prev" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
				</div>
			</div>
		</div>
	</div>
</div>