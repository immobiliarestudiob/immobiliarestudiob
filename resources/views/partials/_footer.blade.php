<footer class="large-cont">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-lg-4">
				<h4 class="second-color">@lang('web.footer_contact_us')<span class="special-color">.</span></h4>
				<div class="footer-title-separator"></div>
				<address>
					<span><i class="fa fa-map-marker"></i><a href="https://www.google.it/maps/place/Studio+B+Immobiliare+S.R.L./@43.825591,7.6907407,15.99z/data=!4m5!3m4!1s0x0:0xa759a6000529b184!8m2!3d43.8266211!4d7.6950659?hl=it" target="_blank" rel="noopener" name="maps">{{$realestate->address}}</a></span>
					<div class="footer-separator"></div>
					<span><i class="fa fa-envelope fa-sm"></i><a href="mailto::{{$realestate->email}}">{{$realestate->email}}</a></span>
					<div class="footer-separator"></div>
					<span><i class="fa fa-phone"></i><a href="tel::{{$realestate->telephone}}" name="make_call">{{$realestate->telephone}}</a></span>
				</address>
				<div class="clear"></div>
			</div>
			<div class="col-xs-12 col-sm-6 col-lg-4">
				<h4 class="second-color">@lang('web.footer_choose_your_language')<span class="special-color">.</span></h4>
				<div class="footer-title-separator"></div>
				<ul class="footer-ul" id="flag">
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
					<li><span class="custom-ul-bullet"></span><a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" name="lang_{{$localeCode}}">{{ $properties['native'] }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</footer>
<footer class="small-cont">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 small-cont">
				<img src="/images/web/logo-light.png" alt="" class="img-responsive footer-logo" />
			</div>
			<div class="col-xs-12 col-md-6 footer-copyrights">
				&copy; Copyright {{date("Y")}} {{$realestate->name}}
			</div>
		</div>
	</div>
</footer>