<div class="modal fade apartment-modal" id="login-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-title">
					<h1>Login<span class="special-color">.</span></h1>
					<div class="short-title-separator"></div>
				</div>
				{{ Form::open(array('route' => 'auth::login_post', 'method' => 'post')) }}
				{{ Form::text('login', old('login'), array('class' => 'input-full main-input', 'placeholder' => 'Username or Email', 'required')) }}
				{{ Form::password('password', array('class' => 'input-full main-input', 'placeholder' => 'Password', 'required')) }}
				<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
					<span>Login</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-user"></i></div>
				</span >
				{{ Form::submit('submit', array('style' => 'display:none')) }}
				{{ Form::close() }}
				<a href="#" class="forgot-link pull-right" name="forgot_password">@lang('auth.forgot_password')?</a>
				<div class="clearfix"></div>

				<p class="login-or">OR</p>
				<a href="{{ url('/auth/facebook') }}" class="facebook-button" name="with_facebook">
					<i class="fa fa-facebook"></i>
					<span>Login with Facebook</span>
				</a>

				<p class="modal-bottom">@lang('auth.dont_have_account')<a href="#" class="register-link" name="register">@lang('auth.register')</a></p>
			</div>
		</div>
	</div>
</div>