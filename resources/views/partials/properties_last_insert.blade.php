@if($properties_last_insert->count() > 0)
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-9">
			{{--<h5 class="subtitle-margin">@lang('web.properties_last_insert_subtitle')</h5>--}}
			<h1>@lang('web.properties_last_insert_title')<span class="special-color">.</span></h1>
		</div>
		<div class="col-xs-12 col-sm-3">
			<a href="#" class="navigation-box navigation-box-next" id="properties_last_insert-next" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
			<a href="#" class="navigation-box navigation-box-prev" id="properties_last_insert-prev" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
		</div>
		<div class="col-xs-12">
			<div class="title-separator-primary"></div>
		</div>
	</div>
</div>
<div class="grid-offers-container">
	<div class="owl-carousel" id="properties_last_insert">
		@foreach($properties_last_insert as $tmp_property)
		@include('partials.property_little', ['property' => $tmp_property])
		@endforeach
	</div>
</div>
@endif