@if(collect(Cookie::get('recent_properties'))->count() > 0)
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-9">
			{{--<h5 class="subtitle-margin">@lang('web.properties_recent_subtitle')</h5>--}}
			<h1>@lang('web.properties_recent_title')<span class="special-color">.</span></h1>
		</div>
		<div class="col-xs-12 col-sm-3">
			<a href="#" class="navigation-box navigation-box-next" id="properties_recent-next" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
			<a href="#" class="navigation-box navigation-box-prev" id="properties_recent-prev" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
		</div>
		<div class="col-xs-12">
			<div class="title-separator-primary"></div>
		</div>
	</div>
</div>
<div class="grid-offers-container">
	<div class="owl-carousel" id="properties_recent">
	@php
	$recent_properties = collect(Cookie::get('recent_properties'));
	$recent_properties_to_view = collect();
	foreach ($recent_properties as $rifimm)
		if (\App\Models\Property::where('rifimm', $rifimm)->count() > 0)
			$recent_properties_to_view->push(App\Models\Property::where('rifimm', $rifimm)->first());
	@endphp

	@foreach($recent_properties_to_view as $tmp_property)
	@include('partials.property_little', ['property' => $tmp_property])
	@endforeach
	</div>
</div>
@endif