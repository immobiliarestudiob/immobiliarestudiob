<div class="modal fade apartment-modal" id="register-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-title">
					<h1>@lang('auth.register')<span class="special-color">.</span></h1>
					<div class="short-title-separator"></div>
				</div>
				{{ Form::open(array('route' => 'auth::register_post', 'method' => 'post')) }}
				{{ Form::text('username', old('username'), array('class' => 'input-full main-input', 'placeholder' => 'Username')) }}
				{{ Form::text('email', old('email'), array('class' => 'input-full main-input', 'placeholder' => 'Email')) }}
				{{ Form::password('password', array('class' => 'input-full main-input', 'placeholder' => 'Password')) }}
				{{ Form::password('password_confirmation', array('class' => 'input-full main-input', 'placeholder' => 'Repeat password')) }}
				<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
					<span>@lang('auth.register')</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-user"></i></div>
				</span>
				{{ Form::submit('submit', array('style' => 'display:none')) }}
				{{ Form::close() }}
				<div class="clearfix"></div>

				<p class="login-or">OR</p>
				<a href="{{ url('/auth/facebook') }}" class="facebook-button" name="with_facebook">
					<i class="fa fa-facebook"></i>
					<span>Login with Facebook</span>
				</a>

				<p class="modal-bottom">@lang('auth.alread_registered')<a href="#" class="login-link" name="login">Login</a></p>
			</div>
		</div>
	</div>
</div>