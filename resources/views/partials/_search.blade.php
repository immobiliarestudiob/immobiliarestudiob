<div class="adv-search-cont">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-11 adv-search-icons">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs adv-search-tabs" role="tablist">
					<li role="presentation" class="active" data-toggle="tooltip" data-placement="top" title="@lang('web.search_nav_apartments')"><a href="#apartments" aria-controls="apartments" role="tab" data-toggle="tab" id="adv-search-tab1" name="apartments"><i class="fa fa-building"></i></a></li>
					<li role="presentation" data-toggle="tooltip" data-placement="top" title="@lang('web.search_nav_houses')"><a href="#houses" aria-controls="houses" role="tab" data-toggle="tab" id="adv-search-tab2" name="houses"><i class="fa fa-home"></i></a></li>
					<li role="presentation" data-toggle="tooltip" data-placement="top" title="@lang('web.search_nav_commercials')"><a href="#commercials" aria-controls="commercials" role="tab" data-toggle="tab" id="adv-search-tab3" name="commercials"><i class="fa fa-industry"></i></a></li>
					<li role="presentation" data-toggle="tooltip" data-placement="top" title="@lang('web.search_nav_lands')"><a href="#lands" aria-controls="lands" role="tab" data-toggle="tab" id="adv-search-tab4" name="land"><i class="fa fa-tree"></i></a></li>
				</ul>
			</div>
			<div class="col-lg-1 visible-lg">
				<a id="adv-search-hide" href="#" name="hide_search"><i class="jfont">&#xe801;</i></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row tab-content" id="formSearchContent">
			<div role="tabpanel" class="col-xs-12 adv-search-outer tab-pane fade in active" id="apartments">
				{{ Form::open(array('route' => 'web::search', 'method' => 'get')) }}
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="typology_id" class="bootstrap-select" title="@lang('web.search_typology'):">
							@foreach($search_parameters_apartment['typology'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="province_id" class="bootstrap-select select-province" title="@lang('web.search_province'):" data-town-id="#select-town-1">
							@foreach($search_parameters_apartment['province'] as $key => $value)
							<option value="{{ $key }}" @if($key==1) selected="selected" @endif>{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="town_id" class="bootstrap-select select-town" title="@lang('web.search_town'):" id="select-town-1" data-area-id="#select-area-1">
							@foreach($search_parameters_apartment['town'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="area_id" class="bootstrap-select" title="@lang('web.search_area'):" id="select-area-1"></select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-price1-value-min" class="adv-search-label">@lang('web.search_price'):</label>
							<input type="text" name="min_price" id="slider-range-price1-value-min" readonly class="hidden">
							<input type="text" name="max_price" id="slider-range-price1-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>€</span> <span id="slider-range-price1-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-price1" data-min="{{ $search_parameters_apartment['price']['min'] }}" data-max="{{ $search_parameters_apartment['price']['max'] }}" data-min-val="{{ $search_parameters_apartment['price']['min'] }}" data-max-val="{{ $search_parameters_apartment['price']['max'] }}" data-step="1000" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-area1-value-min" class="adv-search-label">@lang('web.search_mq'):</label>
							<input type="text" name="min_square_meters" id="slider-range-area1-value-min" readonly class="hidden">
							<input type="text" name="max_square_meters" id="slider-range-area1-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>m<sup>2</sup></span><span id="slider-range-area1-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-area1" data-min="{{ $search_parameters_apartment['square_meters']['min'] }}" data-max="{{ $search_parameters_apartment['square_meters']['max'] }}" data-min-val="{{ $search_parameters_apartment['square_meters']['min'] }}" data-max-val="{{ $search_parameters_apartment['square_meters']['max'] }}" data-step="10" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-locals1-value-min" class="adv-search-label">@lang('web.search_locals'):</label>
							<input type="text" name="min_locals" id="slider-range-locals1-value-min" readonly class="hidden">
							<input type="text" name="max_locals" id="slider-range-locals1-value-max" readonly class="hidden">
							<span id="slider-range-locals1-span" class="adv-search-amount"></span>
							<div class="clearfix"></div>
							<div id="slider-range-locals1" data-min="{{ $search_parameters_apartment['local']['min'] }}" data-max="{{ $search_parameters_apartment['local']['max'] }}" data-min-val="{{ $search_parameters_apartment['local']['min'] }}" data-max-val="{{ $search_parameters_apartment['local']['max'] }}" data-step="1" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-bathrooms1-value-min" class="adv-search-label">@lang('web.search_bath'):</label>
							<input type="text" name="min_bath" id="slider-range-bathrooms1-value-min" readonly class="hidden">
							<input type="text" name="max_bath" id="slider-range-bathrooms1-value-max" readonly class="hidden">
							<span id="slider-range-bathrooms1-span" class="adv-search-amount"></span>
							<div class="clearfix"></div>
							<div id="slider-range-bathrooms1" data-min="{{ $search_parameters_apartment['bath']['min'] }}" data-max="{{ $search_parameters_apartment['bath']['max'] }}" data-min-val="{{ $search_parameters_apartment['bath']['min'] }}" data-max-val="{{ $search_parameters_apartment['bath']['max'] }}" data-step="1" class="slider-range"></div>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
			<div role="tabpanel" class="col-xs-12 adv-search-outer tab-pane fade" id="houses">
				{{ Form::open(array('route' => 'web::search', 'method' => 'get')) }}
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="typology_id" class="bootstrap-select" title="@lang('web.search_typology'):">
							@foreach($search_parameters_house['typology'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="province_id" class="bootstrap-select select-province" title="@lang('web.search_province'):" data-town-id="#select-town-2">
							@foreach($search_parameters_house['province'] as $key => $value)
							<option value="{{ $key }}" @if($key==1) selected="selected" @endif>{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="town_id" class="bootstrap-select select-town" title="@lang('web.search_town'):" id="select-town-2" data-area-id="#select-area-2">
							@foreach($search_parameters_house['town'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="area_id" class="bootstrap-select" title="@lang('web.search_area'):" id="select-area-2"></select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-price2-value-min" class="adv-search-label">@lang('web.search_price'):</label>
							<input type="text" name="min_price" id="slider-range-price2-value-min" readonly class="hidden">
							<input type="text" name="max_price" id="slider-range-price2-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>€</span> <span id="slider-range-price2-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-price2" data-min="{{ $search_parameters_house['price']['min'] }}" data-max="{{ $search_parameters_house['price']['max'] }}" data-min-val="{{ $search_parameters_house['price']['min'] }}" data-max-val="{{ $search_parameters_house['price']['max'] }}" data-step="1000" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-area2-value-min" class="adv-search-label">@lang('web.search_mq'):</label>
							<input type="text" name="min_square_meters" id="slider-range-area2-value-min" readonly class="hidden">
							<input type="text" name="max_square_meters" id="slider-range-area2-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>m<sup>2</sup></span><span id="slider-range-area2-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-area2" data-min="{{ $search_parameters_house['square_meters']['min'] }}" data-max="{{ $search_parameters_house['square_meters']['max'] }}" data-min-val="{{ $search_parameters_house['square_meters']['min'] }}" data-max-val="{{ $search_parameters_house['square_meters']['max'] }}" data-step="10" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-locals2-value-min" class="adv-search-label">@lang('web.search_locals'):</label>
							<input type="text" name="min_locals" id="slider-range-locals2-value-min" readonly class="hidden">
							<input type="text" name="max_locals" id="slider-range-locals2-value-max" readonly class="hidden">
							<span id="slider-range-locals2-span" class="adv-search-amount"></span>
							<div class="clearfix"></div>
							<div id="slider-range-locals2" data-min="{{ $search_parameters_house['local']['min'] }}" data-max="{{ $search_parameters_house['local']['max'] }}" data-min-val="{{ $search_parameters_house['local']['min'] }}" data-max-val="{{ $search_parameters_house['local']['max'] }}" data-step="1" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-bathrooms2-value-min" class="adv-search-label">@lang('web.search_bath'):</label>
							<input type="text" name="min_bath" id="slider-range-bathrooms2-value-min" readonly class="hidden">
							<input type="text" name="max_bath" id="slider-range-bathrooms2-value-max" readonly class="hidden">
							<span id="slider-range-bathrooms2-span" class="adv-search-amount"></span>
							<div class="clearfix"></div>
							<div id="slider-range-bathrooms2" data-min="{{ $search_parameters_house['bath']['min'] }}" data-max="{{ $search_parameters_house['local']['max'] }}" data-min-val="{{ $search_parameters_house['local']['min'] }}" data-max-val="{{ $search_parameters_house['local']['max'] }}" data-step="1" class="slider-range"></div>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
			<div role="tabpanel" class="col-xs-12 adv-search-outer tab-pane fade" id="commercials">
				{{ Form::open(array('route' => 'web::search', 'method' => 'get')) }}
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<input type="hidden" name="category_id" value="2">
						<select data-live-search="true" name="typology_id" class="bootstrap-select" title="@lang('web.search_typology'):">
							@foreach($search_parameters_commercial['typology'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="province_id" class="bootstrap-select select-province" title="@lang('web.search_province'):" data-town-id="#select-town-3">
							@foreach($search_parameters_commercial['province'] as $key => $value)
							<option value="{{ $key }}" @if($key==1) selected="selected" @endif>{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="town_id" class="bootstrap-select select-town" title="@lang('web.search_town'):" id="select-town-3" data-area-id="#select-area-3">
							@foreach($search_parameters_commercial['town'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="area_id" class="bootstrap-select" title="@lang('web.search_area'):" id="select-area-3"></select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-price3-value-min" class="adv-search-label">@lang('web.search_price'):</label>
							<input type="text" name="min_price" id="slider-range-price3-value-min" readonly class="hidden">
							<input type="text" name="max_price" id="slider-range-price3-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>€</span> <span id="slider-range-price3-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-price3" data-min="{{ $search_parameters_commercial['price']['min'] }}" data-max="{{ $search_parameters_commercial['price']['max'] }}" data-min-val="{{ $search_parameters_commercial['price']['min'] }}" data-max-val="{{ $search_parameters_commercial['price']['max'] }}" data-step="1000" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-area3-value-min" class="adv-search-label">@lang('web.search_mq'):</label>
							<input type="text" name="min_square_meters" id="slider-range-area3-value-min" readonly class="hidden">
							<input type="text" name="max_square_meters" id="slider-range-area3-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>m<sup>2</sup></span><span id="slider-range-area3-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-area3" data-min="{{ $search_parameters_commercial['square_meters']['min'] }}" data-max="{{ $search_parameters_commercial['square_meters']['max'] }}" data-min-val="{{ $search_parameters_commercial['square_meters']['min'] }}" data-max-val="{{ $search_parameters_commercial['square_meters']['max'] }}" data-step="10" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-locals3-value-min" class="adv-search-label">@lang('web.search_locals'):</label>
							<input type="text" name="min_locals" id="slider-range-locals3-value-min" readonly class="hidden">
							<input type="text" name="max_locals" id="slider-range-locals3-value-max" readonly class="hidden">
							<span id="slider-range-locals3-span" class="adv-search-amount"></span>
							<div class="clearfix"></div>
							<div id="slider-range-locals3" data-min="{{ $search_parameters_commercial['local']['min'] }}" data-max="{{ $search_parameters_commercial['local']['max'] }}" data-min-val="{{ $search_parameters_commercial['local']['min'] }}" data-max-val="{{ $search_parameters_commercial['local']['max'] }}" data-step="1" class="slider-range"></div>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
			<div role="tabpanel" class="col-xs-12 adv-search-outer tab-pane fade" id="lands">
				{{ Form::open(array('route' => 'web::search', 'method' => 'get')) }}
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<input type="hidden" name="category_id" value="2">
						<select data-live-search="true" name="typology_id" class="bootstrap-select" title="@lang('web.search_typology'):">
							@foreach($search_parameters_land['typology'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="province_id" class="bootstrap-select select-province" title="@lang('web.search_province'):" data-town-id="#select-town-4">
							@foreach($search_parameters_land['province'] as $key => $value)
							<option value="{{ $key }}" @if($key==1) selected="selected" @endif>{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="town_id" class="bootstrap-select select-town" title="@lang('web.search_town'):" id="select-town-4" data-area-id="#select-area-4">
							@foreach($search_parameters_land['town'] as $key => $value)
							<option value="{{ $key }}">{{ $value }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<select data-live-search="true" name="area_id" class="bootstrap-select" title="@lang('web.search_area'):" id="select-area-4"></select>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-price4-value-min" class="adv-search-label">@lang('web.search_price'):</label>
							<input type="text" name="min_price" id="slider-range-price4-value-min" readonly class="hidden">
							<input type="text" name="max_price" id="slider-range-price4-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>€</span> <span id="slider-range-price4-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-price4" data-min="{{ $search_parameters_land['price']['min'] }}" data-max="{{ $search_parameters_land['price']['max'] }}" data-min-val="{{ $search_parameters_land['price']['min'] }}" data-max-val="{{ $search_parameters_land['price']['max'] }}" data-step="1000" class="slider-range"></div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-3">
						<div class="adv-search-range-cont">
							<label for="slider-range-area4-value-min" class="adv-search-label">@lang('web.search_mq'):</label>
							<input type="text" name="min_square_meters" id="slider-range-area4-value-min" readonly class="hidden">
							<input type="text" name="max_square_meters" id="slider-range-area4-value-max" readonly class="hidden">
							<div class="adv-search-amount">
								<span>m<sup>2</sup></span><span id="slider-range-area4-span"></span>
							</div>
							<div class="clearfix"></div>
							<div id="slider-range-area4" data-min="{{ $search_parameters_land['square_meters']['min'] }}" data-max="{{ $search_parameters_land['square_meters']['max'] }}" data-min-val="{{ $search_parameters_land['square_meters']['min'] }}" data-max-val="{{ $search_parameters_land['square_meters']['max'] }}" data-step="10" class="slider-range"></div>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-3 col-md-offset-6 col-lg-offset-9 adv-search-button-cont">
				<span class="button-primary pull-right" style="cursor:pointer;" onclick="$('#formSearchContent').find('.active').find('form').submit()">
					<span>@lang('web.search_btn_search')</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-search"></i></div>
				</span>
			</div>
		</div>
	</div>
</div>
<style type="text/css">.adv-search-range-cont .adv-search-amount{width:inherit}</style>