<div class="sidebar-featured">
	<a class="sidebar-featured-image" href="{{route('web::details', $property->rifimm)}}" name="show_property">
		<img src="{{$property->mainPhoto()->getMedium()}}" alt="{{$property->rifimm}}" />
		{{--
		<div class="sidebar-featured-type">
			<div class="sidebar-featured-estate">{{substr($property->typology->name, 0, 1)}}</div>
		</div>
		--}}
	</a>
	<div class="sidebar-featured-title">
		<a href="{{route('web::details', $property->rifimm)}}" name="show_property">
			<i class="fa fa-{{$property->typology->getFontawasome()}}" aria-hidden="true"></i>
			@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})
		</a>
	</div>
	<div class="sidebar-featured-price">{{ $property->publicPriceLittle }}</div>
	<div class="clearfix"></div>					
</div>