<div class="featured-offer-col">
	<div class="featured-offer-front">
		<div class="featured-offer-photo">
			<img src="{{$property->mainPhoto()->getLarge()}}" alt="{{$property->rifimm}}" />
			<div class="type-container">
				<div class="estate-type">{{$property->typology->name}}</div>
			</div>
		</div>
		<div class="featured-offer-text">
			<h4 class="featured-offer-title"><i class="fa fa-map-marker"></i> @if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})</h4>
			<p>{!!$property->shortDescription!!}</p>
		</div>
		<div class="featured-offer-params">
			@if($property->square_meters > 0)
			<div class="featured-area">
				<img src="/images/web/area-icon.png" alt="" />{!!$property->squareMeters!!}</sup>
			</div>
			@endif
			@if($property->locals > 0)
			<div class="featured-rooms">
				<img src="/images/web/rooms-icon.png" alt="" />{{$property->locals}}
			</div>
			@endif
			@if($property->bath > 0)
			<div class="featured-baths">
				<img src="/images/web/bathrooms-icon.png" alt="" />{{$property->bath}}
			</div>
			@endif
		</div>
		<div class="featured-price">
			{{ $property->publicPriceLittle }}
		</div>
	</div>
	<div class="featured-offer-back">
		<div id="promo-map{{$loop->iteration}}" class="featured-offer-map"></div>
		<div class="button">	
			<a href="{{route('web::details', $property->rifimm)}}" class="button-primary" name="show_property">
				<span>@lang('web.property_read_more')</span>
				<div class="button-triangle"></div>
				<div class="button-triangle2"></div>
				<div class="button-icon"><i class="jfont">&#xe802;</i></div>
			</a>
		</div>
	</div>
</div>

@push('scripts_mapInit')
@if($property->address_geocode != null)
mapInit({{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}},"promo-map{{$loop->iteration}}","/images/web/pin-{{$property->typology->class}}.png", false);
@else
mapInitAddress("{{$property->town->name}}, {{$property->province->name}}, Italy","promo-map{{$loop->iteration}}","/images/web/pin-{{$property->typology->class}}.png", false);
@endif
@endpush