@php $random_tmp = rand(0,9); @endphp
<div class="grid-offer">
	<div class="grid-offer-front">
		<div class="grid-offer-photo">
			<img src="{{$property->mainPhoto()->getLarge()}}" alt="{{$property->rifimm}}" />
			<div class="type-container">
				<div class="estate-type">{{$property->typology->name}}</div>
			</div>
		</div>
		<div class="grid-offer-text">
			<i class="fa fa-map-marker grid-offer-localization"></i>
			<div class="grid-offer-h4"><h4 class="grid-offer-title">@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})</h4></div>
			<div class="clearfix"></div>
			<p>{!!$property->getShortDescription(120)!!}</p>
			<div class="clearfix"></div>
		</div>
		<div class="price-grid-cont">
			<div class="grid-price-label pull-left">@lang('web.property_price'):</div>
			<div class="grid-price pull-right">
				{{ $property->publicPriceLittle }}
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="grid-offer-params">
			@if($property->square_meters > 0)
			<div class="grid-area">
				<img src="/images/web/area-icon.png" alt="" />{!!$property->squareMeters!!}
			</div>
			@endif
			@if($property->locals > 0)
			<div class="grid-rooms">
				<img src="/images/web/rooms-icon.png" alt="" />{{$property->locals}}
			</div>
			@endif
			@if($property->bath > 0)
			<div class="grid-baths">
				<img src="/images/web/bathrooms-icon.png" alt="" />{{$property->bath}}
			</div>
			@endif							
		</div>	

	</div>
	<div class="grid-offer-back">
		<div id="grid-map-{{$property->rifimm}}-{{$random_tmp}}" class="grid-offer-map"></div>
		<div class="button">	
			<a href="{{route('web::details', $property->rifimm)}}" class="button-primary" name="show_property">
				<span>@lang('web.property_read_more')</span>
				<div class="button-triangle"></div>
				<div class="button-triangle2"></div>
				<div class="button-icon"><i class="jfont">&#xe802;</i></div>
			</a>
		</div>
	</div>
</div>

@push('scripts_mapInit')
@if($property->address_geocode != null)
mapInit({{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}},"grid-map-{{$property->rifimm}}-{{$random_tmp}}","/images/web/pin-{{$property->typology->class}}.png", false);
@else
mapInitAddress("{{$property->town->name}}, {{$property->province->name}}, Italy","grid-map-{{$property->rifimm}}-{{$random_tmp}}","/images/web/pin-{{$property->typology->class}}.png", false);
@endif
@endpush