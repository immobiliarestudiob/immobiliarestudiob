@if($properties_swiper->count() > 0)
<!-- Slider main container -->
<div id="swiper1" class="swiper-container">
	<!-- Additional required wrapper -->
	<div class="swiper-wrapper">
		@foreach($properties_swiper as $tmp_property)
		@include('partials.home_property_swiper_little', ['property' => $tmp_property])
		@endforeach
	</div>
</div>
@endif