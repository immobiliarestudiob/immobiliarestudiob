@if($properties_sidebar->count() > 0)
<div class="sidebar-title-cont">
	<h4 class="sidebar-title">@lang('web.properties_last_insert_title')<span class="special-color">.</span></h4>
	<div class="title-separator-primary"></div>
</div>
<div class="sidebar-featured-cont">
	@foreach($properties_sidebar as $tmp_property)
	@include('partials.sidebar_property_little', ['property' => $tmp_property])
	@endforeach
</div>
@endif