<section class="section-both-shadow section-light top-padding-20 bottom-padding-20">
	<div class="container">
		<div class="row count-container">
			<div class="col-xs-6 col-lg-3">
				<div class="number" id="number1">
					<div class="number-img">	
						<i class="fa fa-building"></i>
					</div>
					<span class="number-label text-color2">@lang('web.counter_apartments')</span>
					<span class="number-big text-color3 count" data-from="0" data-to="{{\App\Models\Property::class('apartment')->count()}}" data-speed="2000"></span>
				</div>
			</div>
			<div class="col-xs-6 col-lg-3 number_border">
				<div class="number" id="number2">
					<div class="number-img">	
						<i class="fa fa-home"></i>	
					</div>			
					<span class="number-label text-color2">@lang('web.counter_houses')</span>
					<span class="number-big text-color3 count" data-from="0" data-to="{{\App\Models\Property::class('house')->count()}}" data-speed="2000"></span>
				</div>
			</div>
			<div class="col-xs-6 col-lg-3 number_border3">
				<div class="number" id="number3">
					<div class="number-img">	
						<i class="fa fa-industry"></i>
					</div>
					<span class="number-label text-color2">@lang('web.counter_commercial')</span>
					<span class="number-big text-color3 count" data-from="0" data-to="{{\App\Models\Property::class('commercial')->count()}}" data-speed="2000"></span>
				</div>
			</div>
			<div class="col-xs-6 col-lg-3 number_border">
				<div class="number" id="number4">
					<div class="number-img">
						<i class="fa fa-tree"></i>
					</div>
					<span class="number-label text-color2">@lang('web.counter_land')</span>
					<span class="number-big text-color3 count" data-from="0" data-to="{{\App\Models\Property::class('land')->count()}}" data-speed="2000"></span>
				</div>
			</div>
		</div>
	</div>
</section>