@extends('layouts.print')

@section('title', 'Stampa Scheda')

@section('content')
<div class="hidden-print">
	<div class="printer"><a href="javascript:window.print()" name="print">
		{{ Html::image('images/print/printer.png', 'Stampa '.$property->rifimm) }}
	</a></div>
	<div class="back"><a href="{{ route('admin::property.print') }}" name="go_back">
		{{ Html::image('images/print/back.png', 'Torna indietro') }}
	</a></div>
</div>

<!--Inizio pagina-->
{{ Html::image('images/print/onde.png', 'onde', array('class' => 'onde')) }}
<div class="box">
	<h1>
		{{ $property->typology->name_ita." in " }}
		@if ($property->is_to_sale)
			vendita
		@else
			affitto
		@endif
			{{ " a ".$property->town->name }}
		@if ($property->hasArea())
			{{ $property->area->name }}
		@endif
	</h1>
	<h2>Riferimento {{ $property->rifimm }} &bull; {{ $property->publicPrice }}</h2>
	@if ($property->hasMainPhoto())
		<div class="imgCut foto_1">
			<img src="{{ $property->mainPhoto()->getLarge() }}" alt="{{ $property->rifimm }}" />
		</div>
	@endif
	@foreach($property->getNormalPhotos($use_main = false) as $photo)
		<div class="imgCut foto_{{$loop->iteration + 1}}">
			<img src="{{ $photo->getMedium() }}" alt="{{ $property->rifimm }}" />
		</div>
		@if (!$loop->first)
			@break
		@endif
	@endforeach
	<div class="last_row">
		<h5>{{ str_limit(strip_tags($property->description_ita), 400) }}</h5>
		{{ Html::image('images/energetic_class/energetic_class_'.$property->energetic_class.'.png', 'Classe energetica', array('class' => 'classe_ener')) }}
	</div>
</div>
@endsection

@push('styles')
{{ Html::style('css/bootstrap.css') }}
<style type="text/css">
	.hidden-print {
		position: absolute;
		top: 10px;
		right: 10px;
	}
	.hidden-print img { border: 0; max-width: 100px; }
	body {
		background: #fff;
		font-family: 'CaviarDreams';
		width: 297mm;
		height: 210mm;
	}
	.onde {
		position: absolute;
		top: 0;
		left: 0;
		z-index: -1;
	}
	.box {
		overflow: hidden;
		width: 261mm;
		height: 175mm;
		margin: 25mm 18mm 10mm 18mm;
	}
	h1, h2, h5, h5 p {
		color: #565A5C !important;
		-webkit-print-color-adjust: exact;
		margin: 0;
	}
	h1 {
		font-size: 40pt;
		margin-bottom: 0;
	}
	h2 {
		font-size: 25pt;
		display: inline-block;
		padding-right: 5mm;
		margin-bottom: 4mm;
	}
	h5 {
		font-size: 6mm;
		text-align: justify;
		width: 206mm;
		display: inline-block;
		float: left;
	}
	img {
		margin: 0;
		padding: 0;
		border: 0;
	}
	.foto_1 {
		width: 172mm;
		height: 100mm;
		margin: 0 4mm 4mm 0;
	}
	.foto_2, .foto_3 {
		width: 84mm;
		height: 48mm;
		margin-bottom: 4mm;
	}
	.foto_1, .foto_2, .foto_3 { float: left; }
	.last_row { float: left; }
	.classe_ener {
		float: left;
		height: 25mm;
		margin-left: 4mm;
	}
	.imgCut {
		overflow: hidden;
		position: relative;
	}
	.imgCut img {
		width: 100%;
		position: absolute;
		left: 0;
	}

	@media screen {
		body { border: 1px solid #000; }
		.onde { width: 297mm; }
	}
	@media print {
		body { -webkit-print-color-adjust: exact; }
		.onde { width: 100%; min-width: 297mm; }
	}
</style>
@endpush

@push('scripts')
<script type="text/javascript">
	function imgCut() {
		repeat = false;
		$(".imgCut").each(function(index, element) {
			var hSmall = $(this).height();
			var hBig = $(this).children("img").height();
			var hTop = -((hBig - hSmall) / 2);
			$(this).children("img").css("top", hTop);
			if (hTop > 0) repeat = true;
		});
		if (repeat) imgCut();
	}

	$(document).ready(function() {
		imgCut();
	})
</script>
@endpush