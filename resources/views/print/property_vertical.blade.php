@extends('layouts.print')

@section('title', 'Stampa Scheda Verticale')

@section('content')
<div class="printer"><a href="javascript:window.print()" name="print">
	{{ Html::image('images/print/printer.png', 'Stampa '.$property->rifimm) }}
</a></div>
<div class="back"><a href="{{ route('admin::property.print') }}" name="go_back">
	{{ Html::image('images/print/back.png', 'Torna indietro', array('height' => '100px')) }}
</a></div>

{{ Html::image('images/print/print_vertical.png', null, array('class' => 'Vscheda')) }}
<div class="Vintestazione">
	{{ $property->town->name.' ('.$property->province->abbreviation.')' }}
	@if($property->hasArea())
		{{ $property->area->name }}
	@endif
</div>
<div class="Vfoto">
	@if ($property->hasMainPhoto())
		<img src="{{ $property->mainPhoto()->getLarge() }}" alt="{{ $property->rifimm }}" class="VfotoH" />
	@endif
</div>
<div class="VclasseE"> Classe Energetica:<br />
	{{ Html::image('images/energetic_class/small_energetic_class_'.$property->energetic_class.'.png', 'Classe energetica') }}
</div>
<div class="Vrif"> rif.{{ $property->rifimm }}</div>
<div class="Vprezzo">{{ $property->publicPrice }}</div>
<div class="Vdescrizione">{{ str_limit(strip_tags($property->description_ita), 600) }}</div>
@endsection

@push('styles')
<style type="text/css">
@media screen {
	body {
		margin:0px;
		padding:0px;
	}
	.printer {
		visibility:visible;
		position:absolute;
		top:0px;
		right:0px;
	}
	.back {
		visibility:visible;
		position:absolute;
		top:200px;
		right:0px;
	}
	.Vscheda {
		position:absolute;
		top:0px;
		left:0px;
		width:421px;
		height:595px;
	}
	.Vintestazione {
		position:absolute;
		left:70px;
		top:50px;
		color:#A2C02F;
		font-weight:bold;
		font-size:18px;
	}
	.Vfoto {
		position:absolute;
		left:70px;
		top:82px;
	}
	.VfotoH {
		max-width:300px;
		height:250px;
	}
	.Vrif {
		position:absolute;
		left:250px;
		top:340px;
		color:#000;
		font-size:12px;
	}
	.Vprezzo {
		position:absolute;
		left:250px;
		top:360px;
		color:#000;
		font-size:12px;
	}
	.Vdescrizione {
		position:absolute;
		left:70px;
		top:385px;
		width:325px;
		height:103px;
		color:#000;
		font-size:12px;
		float:left;
	}
	.VclasseE {
		position:absolute;
		left:70px;
		top:340px;
		color:#000;
		font-size:12px;
	}
}
@media print {
	body {
		margin:0mm;
		padding:0mm;
	}
	.printer { visibility:hidden; }
	.back { visibility:hidden; }
	.Vscheda {
		position:absolute;
		top:0mm;
		left:0mm;
		width:210mm;
		height:297mm;
	}
	.Vintestazione {
		position:absolute;
		left:36mm;
		top:20mm;
		color:#A3CF62;
		font-weight:bold;
		font-size:xx-large;
	}
	.Vfoto {
		position:absolute;
		left:36mm;
		top:35mm;
	}
	.VfotoH {
		max-width:150mm;
		height:115mm;
	}
	.Vrif {
		position:absolute;
		left:127mm;
		top:160mm;
		color:black;
		font-size:x-large;
	}
	.Vprezzo {
		position:absolute;
		left:127mm;
		top:170mm;
		color:black;
		font-size:x-large;
	}
	.Vdescrizione {
		position:absolute;
		left:36mm;
		top:190mm;
		width:140mm;
		height:75mm;
		color:black;
		font-size:x-large;
		float:left;
	}
	.VclasseE {
		position:absolute;
		left:36mm;
		top:160mm;
		color:#000;
		font-size:x-large;
	}
}
</style>
@endpush