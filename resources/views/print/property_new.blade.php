@extends('layouts.print')

@section('title', 'Stampa Scheda')

@section('content')
<div class="hidden-print">
	<div class="printer"><a href="javascript:window.print()" name="print">
		{{ Html::image('images/print/printer.png', 'Stampa '.$property->rifimm) }}
	</a></div>
	<div class="back"><a href="{{ route('admin::property.print') }}" name="go_back">
		{{ Html::image('images/print/back.png', 'Torna indietro') }}
	</a></div>
</div>

<!--Inizio pagina-->
{{ Html::image('images/print/onde.png', 'onde', array('class' => 'onde')) }}
<div class="box">
	@if ($property->hasMainPhoto())
		<img src="{{ $property->mainPhoto()->getLarge() }}" alt="{{ $property->rifimm }}" class="foto_principale" />
	@endif
    <h1>
    	{{ $property->typology->name_ita." in " }}
		@if ($property->is_to_sale)
			vendita
		@else
			affitto
		@endif
			{{ " a ".$property->town->name }}
		@if ($property->hasArea())
			{{ $property->area->name }}
		@endif
    </h1>
    <h2>Riferimento {{ $property->rifimm }}</h2>
    <h2>{{ $property->publicPrice }}</h2>
    {{ Html::image('images/energetic_class/energetic_class_'.$property->energetic_class.'.png', 'Classe energetica', array('class' => 'classe_ener')) }}
    <h5>{{ str_limit(strip_tags($property->description_ita), 1000) }}</h5>
</div>
@endsection

@push('styles')
{{ Html::style('css/bootstrap.css') }}
<style type="text/css">
body {
	font-family: 'CaviarDreams';
	background: #fff;
	width: 297mm;
	height: 210mm;
}
.box {
	width: 261mm;
	height: 175mm;
	margin: 25mm 18mm 10mm 18mm;
}
.onde {
	position: absolute;
	top: 0;
	left: 0;
	z-index: -1;
}
.hidden-print {
	position: absolute;
	top: 10px;
	right: 10px;
}
.hidden-print img { border: 0; max-width: 100px; }
h1, h2, h5, h5 p {
	color: #565A5C !important;
	-webkit-print-color-adjust: exact;
	margin: 0;
}
h1 {
	font-size: 12mm;
	margin-bottom: 7mm;
}
h2 {
	font-size: 10mm;
}
h5 {
	font-size: 7mm;
	text-align: justify;
}
img {
	margin: 0;
	padding: 0;
	border: 0;
}
.classe_ener {
	width: 60mm;
	margin: 7mm 0;
}
.foto_principale {
	margin: 0 0 7mm 7mm;
	width: 160mm;
	float: right;
}

@media screen {
	body {
		border: 1px solid #000;
	}
	.onde {
		width: 297mm;
	}
	.bb_angolo {
		position: absolute;
	    top: 140mm;
	    left: 212mm;
		z-index: -1;
	}
}

@media print {
	body {
		-webkit-print-color-adjust: exact;
		width: 100%;
		height: inherit;
		overflow: hidden;
	}
	.onde {
		width: 100%;
		min-width: 297mm;
	}
	.bb_angolo {
		position: absolute;
		bottom: 0;
		right: 0;
		z-index: -1;
	}
}
</style>
@endpush