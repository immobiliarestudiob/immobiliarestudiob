@extends('layouts.print')

@section('title', 'Stampa Scheda Orizzontale')

@section('content')
<div class="printer"><a href="javascript:window.print()" name="print">
	{{ Html::image('images/print/printer.png', 'Stampa '.$property->rifimm) }}
</a></div>
<div class="back"><a href="{{ route('admin::property.print') }}" name="go_back">
	{{ Html::image('images/print/back.png', 'Torna indietro', array('height' => '100px')) }}
</a></div>

{{ Html::image('images/print/print_horizzontal.png', null, array('class' => 'Oscheda')) }}
<div class="Ointestazione">
	{{ $property->town->name.' ('.$property->province->abbreviation.')' }}
	@if($property->hasArea())
		{{ $property->area->name }}
	@endif
</div>
<div class="Ofoto">
	@if ($property->hasMainPhoto())
		<img src="{{ $property->mainPhoto()->getLarge() }}" alt="{{ $property->rifimm }}" class="OfotoH" />
	@endif
</div>
<div class="OclasseE"> Classe Energetica:<br />
	{{ Html::image('images/energetic_class/small_energetic_class_'.$property->energetic_class.'.png', 'Classe energetica') }}
</div>
<div class="Orif">rif.{{ $property->rifimm }}</div>
<div class="Oprezzo">{{ $property->publicPrice }}</div>
<div class="Odescrizione">{{ str_limit(strip_tags($property->description_ita), 500) }}</div>
@endsection

@push('styles')
<style type="text/css">
@media screen {
	body {
		margin:0px;
		padding:0px;
	}

	.printer {
		visibility:visible;
		position:absolute;
		top:0px;
		right:0px;
	}

	.back {
		visibility:visible;
		position:absolute;
		top:200px;
		right:0px;
	}
	.Oscheda {
		position:absolute;
		top:0px;
		left:0px;
		width:595px;
		height:421px;
	}
	.Ointestazione {
		position:absolute;
		left:60px;
		top:50px;
		color:#A2C02F;
		font-weight:bold;
		font-size:18px;
	}
	.Ofoto {
		position:absolute;
		left:60px;
		top:82px;
	}
	.OfotoH {
		max-width:300px;
		height:200px;
	}
	.Orif {
		position:absolute;
		left:450px;
		top:250px;
		color:#000;
		font-size:12px;
	}
	.Oprezzo {
		position:absolute;
		left:450px;
		top:270px;
		color:#000;
		font-size:12px;
	}
	.Odescrizione {
		position:absolute;
		left:60px;
		top:300px;
		width:500px;
		height:150px;
		color:#000;
		font-size:12px;
		float:left;
	}
	.OclasseE {
		position:absolute;
		left:450px;
		top:100px;
		color:#000;
		font-size:12px;
	}
}
@media print {
	body {
		margin:0mm;
		padding:0mm;
	}
	.printer { visibility:hidden; }
	.back { visibility:hidden; }
	.Oscheda {
		position:absolute;
		top:0mm;
		left:0mm;
		width:297mm;
		height:209mm;
	}
	.Ointestazione {
		position:absolute;
		left:36mm;
		top:20mm;
		color:#A3CF62;
		font-weight:bold;
		font-size:xx-large;
	}
	.Ofoto {
		position:absolute;
		left:36mm;
		top:35mm;
	}
	.OfotoH {
		max-width:150mm;
		height:115mm;
	}
	.Orif {
		position:absolute;
		left:220mm;
		top:130mm;
		color:black;
		font-size:x-large;
	}
	.Oprezzo {
		position:absolute;
		left:220mm;
		top:140mm;
		color:black;
		font-size:x-large;
	}
	.Odescrizione {
		position:absolute;
		left:36mm;
		top:154mm;
		width:200mm;
		height:35mm;
		color:black;
		font-size:x-large;
		float:left;
	}
	.OclasseE {
		position:absolute;
		left:220mm;
		top:80mm;
		color:#000;
		font-size:x-large;
	}
}
</style>
@endpush