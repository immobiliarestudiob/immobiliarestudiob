@extends('layouts.web')

@section('title', 'Page not found')

@php
$menu_header = '';
$hide_menu_admin = true;
@endphp

@section('content')
<section class="short-image no-padding blog-short-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color">ERROR 403</h5>
				<h1 class="second-color">Not authorized</h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>

</section>

<section class="section-light section-top-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="huge-header">403<span class="special-color">.</span></h1>
				<h1 class="error-subtitle text-color4">Not authorized</h1>

				<p class="margin-top-105 centered-text">You are not authorized to visit this page.</p>
				<p class="centered-text">Go to our <strong><a href="{{route('web::home')}}" name="go_to_homepage">HOMEPAGE</a></strong> or return to the <strong><a href="javascript:history.back()" name="go_back">PREVIOUS PAGE</a></strong></p>
			</div>
		</div>
	</div>
</section>
@endsection