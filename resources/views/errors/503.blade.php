@extends('layouts.web')

@section('title', 'Maintenance Mode')

@php
$menu_header = '';
$hide_menu_admin = true;
@endphp

@section('content')
<section class="short-image no-padding blog-short-title">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-12 short-image-title">
                <h5 class="subtitle-margin second-color">ERROR 503</h5>
                <h1 class="second-color">Maintenance Mode</h1>
                <div class="short-title-separator"></div>
            </div>
        </div>
    </div>

</section>

<section class="section-light section-top-shadow">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="huge-header">503<span class="special-color">.</span></h1>
                <h1 class="error-subtitle text-color4">Maintenance Mode</h1>

                <p class="margin-top-105 centered-text">We'll be back soon online, we're working to improve your experience on this website.</p>
            </div>
        </div>
    </div>
</section>
@endsection