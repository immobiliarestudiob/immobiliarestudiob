@component('mail::message')
# Nuova richiesta da {{$name}}
-------------

Telefono: **{{$phone}}**<br />
Email: **{{$mail}}**

Messaggio:<br />
{{$message}}

@endcomponent