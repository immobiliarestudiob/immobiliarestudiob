<div class="offer-pagination margin-top-30">
	<!-- Previous Page Link -->
	@if ($paginator->onFirstPage())
		<a class="prev"><i class="jfont">&#xe800;</i></a>
	@else
		<a href="{{ $paginator->previousPageUrl() }}" class="prev"><i class="jfont">&#xe800;</i></a>
	@endif

	<!-- Next Page Link -->
	@if ($paginator->hasMorePages())
		<a href="{{ $paginator->nextPageUrl() }}" class="next"><i class="jfont">&#xe802;</i></a>
	@else
		<a class="next"><i class="jfont">&#xe802;</i></a>
	@endif

	<div class="clearfix"></div>
</div>