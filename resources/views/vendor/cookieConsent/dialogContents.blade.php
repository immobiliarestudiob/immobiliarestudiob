<div class="js-cookie-consent cookie-consent">

	<span class="cookie-consent__message">
		{!! trans('cookieConsent::texts.message') !!}
	</span>

	<button class="js-cookie-consent-agree cookie-consent__agree">
		{{ trans('cookieConsent::texts.agree') }}
	</button>

</div>

<style type="text/css">
.js-cookie-consent {
	position: fixed;
	bottom: 0;
	left: 0;
	z-index: 600;
	margin: 0 auto;
	width: 100%;
	padding: 10px;
	background: #91ba2d;
	text-align: center;
}
.js-cookie-consent span {
	font-size: 1.5rem;
	color: #fff;
	font-weight: bold;
}
</style>