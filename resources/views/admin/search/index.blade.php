@extends('layouts.admin')

@section('title', 'Ricerche salvate dall\'utente')
@php
$menu = 'user';
App::setLocale('it');
@endphp

@section('content')
<a href="{{ route('admin::user.index') }}" class="btn btn-default">Torna alla lista utenti</a>
<br />

@if($user->search->count() == 0)
<h3>Questo utente non ha nessuna ricerca salvata.</h3>
@else
<h3>Questo utente ha {{$user->search->count()}} ricerche salvate.</h3>
<div class="row">
	@foreach ($user->search as $search)
	<div class="col-md-6">
		<div class="jumbotron">
			<h3><b>{{$search->fullName}}</b></h3>
			<p>
				Prezzo <b>{{$search->getMinPrice()}} - {{$search->getMaxPrice()}}</b><br />
				Metriquadri <b>{!!$search->getMinSquareMeters()!!} - {!!$search->getMaxSquareMeters()!!}</b><br />
				Locali <b>{{$search->min_locals}} - {{$search->max_locals}}</b><br />
				Camere <b>{{$search->min_room}} - {{$search->max_room}}</b><br />
				Bagni <b>{{$search->min_bath}} - {{$search->max_bath}}</b><br />
				Aggiunto il <b>{{$search->created_at}}</b>
			</p>
			@if ($search->getMatchProperties($visible = false)->count() > 0)
				<p><a class="btn btn-primary btn-lg" href="{{route('admin::search.show', [$user->id, $search->id])}}" role="button">Visualizza immobili</a></p>
			@else
				<p><a class="btn btn-warning btn-lg" disabled role="button">Non ci sono immobili per questa ricerca</a></p>
			@endif
		</div>
	</div>
	@endforeach
</div>
@endif
@endsection

@push('styles')
<style type="text/css">.jumbotron h3{margin-top:0}</style>
@endpush