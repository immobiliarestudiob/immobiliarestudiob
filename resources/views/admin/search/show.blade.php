@extends('layouts.admin')

@section('title', 'Ricerche salvate dall\'utente')
@php
$menu = 'user';
App::setLocale('it');
@endphp

@section('content')
<a href="{{ route('admin::search.index', [$user->id]) }}" class="btn btn-default">Torna alla lista delle ricerche di {{$user->name}} {{$user->surname}}</a>
<br />

@if($search->getMatchProperties($visible = false)->count() == 0)
<h3>Non sono disponibili immobili per questa ricerca.</h3>
@else
<h3>Ci sono {{$search->getMatchProperties($visible = false)->count()}} immobili relativi a questa ricerca.</h3>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th data-field="prezzo" data-sortable="true">Prezzo</th>
				<th data-field="prezzo" data-sortable="true">Metriquadri</th>
				<th>Visualizza l'immobile</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($search->getMatchProperties($visible = false) as $property)
			<tr>
				<th>{{ $loop->iteration }}</th>
				<th><a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a></th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->priceEuro }}</th>
				<th>{!! $property->squareMeters !!}</th>
				<th><a href="{{ route('web::details', [$property->rifimm]) }}" target="_blank" class="btn btn-default">Visualizza</a></th>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endif
@endsection