@extends('layouts.admin')

@section('title', 'Modifica utente')
<?php $menu = 'user'; ?>

@section('content')
<div class="row"><div class="col-sm-10 col-sm-offset-2">
	<h3>Carica una nuova foto</h3>
	{{ Form::open(array('route' => array('admin::user.upload_photo', $user->id), 'method' => 'put', 'files' => true)) }}
	{{ Form::file('photo', array('required')) }}<br />
	{{ Form::submit('Carica', array('class' => 'btn btn-default')) }}
	{{ Form::close() }}
</div></div>
<hr />

@if($user->hasPhoto())
<div class="row"><div class="col-sm-10 col-sm-offset-2">
	<h3>Rimuovi la foto</h3>
	<img src="{{$user->photo->getThumb()}}" /> 
	{{ Form::open(array('route' => array('admin::user.destroy_photo', $user->id), 'method' => 'delete', 'files' => true)) }}
	{{ Form::submit('Rimuovi', array('class' => 'btn btn-default')) }}
	{{ Form::close() }}
</div></div>
<hr />
@endif

{{ Form::open(array('route' => array('admin::user.update', $user->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	{{ Form::label('name', 'Nome', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Nome', 'required']) }}
		@if ($errors->has('name'))<span class="help-block">{{ $errors->first('name') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
	{{ Form::label('surname', 'Cognome', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('surname', $user->surname, ['class' => 'form-control', 'placeholder' => 'Cognome', 'required']) }}
		@if ($errors->has('surname'))<span class="help-block">{{ $errors->first('surname') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	{{ Form::label('email', 'Email', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Email', 'required']) }}
		@if ($errors->has('email'))<span class="help-block">{{ $errors->first('email') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
	{{ Form::label('telephone', 'Telefono', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('telephone', $user->telephone, ['class' => 'form-control', 'placeholder' => 'Telefono']) }}
		@if ($errors->has('telephone'))<span class="help-block">{{ $errors->first('telephone') }}</span>@endif
	</div>
</div>
<br />
<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
	{{ Form::label('username', 'Username', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('username', $user->username, ['class' => 'form-control', 'placeholder' => 'Username']) }}
		@if ($errors->has('username'))<span class="help-block">{{ $errors->first('username') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	{{ Form::label('password', 'Password', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
		@if ($errors->has('password'))<span class="help-block">{{ $errors->first('password') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
	{{ Form::label('password_confirmation', 'Conferma Password', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Conferma Password']) }}
		@if ($errors->has('password_confirmation'))<span class="help-block">{{ $errors->first('password_confirmation') }}</span>@endif
	</div>
</div>
<div class="form-group{{ $errors->has('confirmed') ? ' has-error' : '' }}">
	{{ Form::label('confirmed', 'Utente attivo', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::checkbox('confirmed', '1', $user->confirmed) }}
		@if ($errors->has('confirmed'))<span class="help-block">{{ $errors->first('confirmed') }}</span>@endif
	</div>
</div>
<hr />
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		{{ Form::submit('Salva', array('class' => 'btn btn-default')) }}
	</div>
</div>
<hr />
{{ Form::close() }}

@endsection