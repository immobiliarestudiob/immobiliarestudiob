@extends('layouts.admin')

@section('title', 'Lista notifiche')
<?php $menu = 'user'; ?>

@section('content')
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Cliente</th>
				<th>Messaggio</th>
				<th>Data</th>
				<th>Apri</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($notifications as $notification)
			<tr @if($notification->read_at==null) class="info" @endif>
				<th>{{ $loop->iteration }}</th>
				<th><a href="{{route('admin::user.show', $notification->data['user_id'])}}">{{ \App\Models\User::find($notification->data['user_id'])->fullName }}</a></th>
				<th>{{ $notification->data['message'] }}</th>
				<th>{{ $notification->created_at }}</th>
				<th><a href="{{route('admin::user.notification_show', [$notification->notifiable_id, $notification->id])}}" class="btn btn-default">Apri</a></th>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection