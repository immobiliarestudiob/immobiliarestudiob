@extends('layouts.admin')

@section('title', 'Immobili salvati dall\'utente')
<?php $menu = 'user'; ?>

@section('content')
<a href="{{ route('admin::user.index') }}" class="btn btn-default">Torna alla lista utenti</a>
<br />

@if($preferredProperties->count() == 0)
<h3>Questo utente non ha nessun immobile salvato tra i preferiti.</h3>
@else
<h3>Questo utente ha {{ $preferredProperties->count() }} immobili salvati tra i preferiti.</h3>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th data-field="prezzo" data-sortable="true">Prezzo</th>
				<th data-field="added" data-sortable="true">Aggiunto il</th>
				<th>Visualizza l'immobile</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($preferredProperties as $property)
			<tr>
				<th>{{ $loop->iteration }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
					@else
					<img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" />
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->priceEuro }}</th>
				<th>{{ $property->pivot->created_at }}</th>
				<th><a href="{{ route('web::details', [$property->rifimm]) }}" target="_blank" class="btn btn-default">Visualizza</a></th>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endif
@endsection