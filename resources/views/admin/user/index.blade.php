@extends('layouts.admin')

@section('title', 'Lista utenti')
<?php $menu = 'user'; ?>

@section('content')
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome</th>
				<th data-field="surname" data-sortable="true">Cognome.</th>
				<th data-field="email" data-sortable="true">Email</th>
				<th data-field="preferred" data-sortable="true">Immobili</th>
				<th data-field="search" data-sortable="true">Ricerche</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
				@php
				$disable_modal = $user->hasAnyRole(['agent', 'admin'])
				@endphp
			<tr>
				<th>{{ $loop->iteration }}</th>
				<th>{{ $user->name }}</th>
				<th>{{ $user->surname }}</th>
				<th>{{ $user->email }}</th>
				<th><a href="{{ route('admin::user.preferred', [$user->id]) }}" class="btn btn-default">Immobili</a></th>
				<th><a href="{{ route('admin::search.index', [$user->id]) }}" class="btn btn-default">Ricerche</a></th>
				<th><a href="{{ route('admin::user.edit', [$user->id]) }}" class="btn btn-default">Modifica</a></th>
				<th><a class="btn btn-default btn-danger @if($disable_modal) disabled @endif" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $user->id }}">Rimuovi</a></th>
			</tr>
			@if(!$disable_modal)
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi l'utente</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere l'utente <strong>{{ $user->fullName }}</strong></div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::user.destroy', $user->id), 'method' => 'delete')) }}
							{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
							{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
		</tbody>

	</table>
</div>
@endsection