@extends('layouts.admin')

@section('title', 'Statistiche Immobili')
<?php $menu = 'statistics'; ?>

@section('content')
<h2>Top 5</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($top_properties->all() as $index => $property)
			<tr>
				{{ Form::open(array('route' => array('admin::property.statistics_reset', $property->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<h2>All</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Categoria</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($properties->all() as $index => $property)
			<tr>
				{{ Form::open(array('route' => array('admin::property.statistics_reset', $property->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection