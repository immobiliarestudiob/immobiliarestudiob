@extends('layouts.admin')

@section('title', 'Statistiche Zona')
<?php $menu = 'statistics'; ?>

@section('content')
<h2>Top 5</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Zona</th>
				<th data-field="province_id" data-sortable="true">Nome Comune</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($top_areas->all() as $index => $area)
			<tr>
				{{ Form::open(array('route' => array('admin::area.statistics_reset', $area->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $area->name }}</th>
				<th>{{ $area->town->name }}</th>
				<th>{{ $area->properties()->count() }}</th>
				<th>{{ $area->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<h2>All</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Zona</th>
				<th data-field="province_id" data-sortable="true">Nome Comune</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($areas->all() as $index => $area)
			<tr>
				{{ Form::open(array('route' => array('admin::area.statistics_reset', $area->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $area->name }}</th>
				<th>{{ $area->town->name }}</th>
				<th>{{ $area->properties()->count() }}</th>
				<th>{{ $area->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection