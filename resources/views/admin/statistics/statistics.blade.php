@extends('layouts.admin')

@section('title', 'Statistiche')
<?php $menu = 'statistics'; ?>

@section('content')
<p class="text-center lead">
	Per visualizzare tutte le statistiche relative alle visite del sito, la provvenienza dei visitatori, la durata della visita, le pagine visualizzate, le ricerche effettuate, gli utenti online, la geolocalizzazione deli visitatori e tutte le statistiche sulla provvenienza da motori di ricerca o siti, clicca sul link qui sotto.
	<br /><br />
	<a href="http://www.histats.com/viewstats/?DOLOGIN=1&user=lele9.2@hotmail.it&pass=immobiliarestudiob.com&b1=accedi" style="color:#00C; text-decoration:none;">CLICCA QUI!!!</a>
	<br /><br />
	Dopodich&eacute; cliccare <a href="http://www.histats.com/viewstats/?act=2&sid=1632202" style="color:#00C;text-decoration:underline;">qui</a> o sul link come da immagine:
	<br /><br />
	<a href="http://www.histats.com/viewstats/?act=2&sid=1632202">
		{{ Html::image('images/admin/statistics.png', null, array('class' => 'img-responsive', 'border' => 0)) }}
	</a>
</p>

@endsection