@extends('layouts.admin')

@section('title', 'Statistiche Comune')
<?php $menu = 'statistics'; ?>

@section('content')
<h2>Top 5</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Comune</th>
				<th>Nome Provincia</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($top_towns->all() as $index => $town)
			<tr>
				{{ Form::open(array('route' => array('admin::town.statistics_reset', $town->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $town->name }}</th>
				<th>{{ $town->province->name }}</th>
				<th>{{ $town->properties()->count() }}</th>
				<th>{{ $town->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<h2>All</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Categoria</th>
				<th>Nome Provincia</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($towns->all() as $index => $town)
			<tr>
				{{ Form::open(array('route' => array('admin::town.statistics_reset', $town->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $town->name }}</th>
				<th>{{ $town->province->name }}</th>
				<th>{{ $town->properties()->count() }}</th>
				<th>{{ $town->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection