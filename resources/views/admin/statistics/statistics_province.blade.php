@extends('layouts.admin')

@section('title', 'Statistiche Provincia')
<?php $menu = 'statistics'; ?>

@section('content')
<h2>Top 5</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Provincia</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($top_provinces->all() as $index => $province)
			<tr>
				{{ Form::open(array('route' => array('admin::province.statistics_reset', $province->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $province->name }}</th>
				<th>{{ $province->properties()->count() }}</th>
				<th>{{ $province->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<h2>All</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Categoria</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($provinces->all() as $index => $province)
			<tr>
				{{ Form::open(array('route' => array('admin::province.statistics_reset', $province->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $province->name }}</th>
				<th>{{ $province->properties()->count() }}</th>
				<th>{{ $province->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection