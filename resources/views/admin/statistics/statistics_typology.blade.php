@extends('layouts.admin')

@section('title', 'Statistiche Tipologia')
<?php $menu = 'statistics'; ?>

@section('content')
<h2>Top 5</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Tipologia</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($top_typologies->all() as $index => $typology)
			<tr>
				{{ Form::open(array('route' => array('admin::typology.statistics_reset', $typology->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $typology->name_ita }}</th>
				<th>{{ $typology->properties()->count() }}</th>
				<th>{{ $typology->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
<h2>All</h2>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Categoria</th>
				<th>Numero immobili</th>
				<th>Visite</th>
				<th>Azzera</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($typologies->all() as $index => $typology)
			<tr>
				{{ Form::open(array('route' => array('admin::typology.statistics_reset', $typology->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ $typology->name_ita }}</th>
				<th>{{ $typology->properties()->count() }}</th>
				<th>{{ $typology->views }}</th>
				<th>{{ Form::submit('Azzera', array('class' => 'btn btn-default btn-danger')) }}</th>
				{{ Form::close() }}
			</tr>
			@endforeach
		</tbody>

	</table>
</div>
@endsection