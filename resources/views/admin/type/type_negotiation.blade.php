@extends('layouts.admin')

@section('title', 'Negoziazione')
<?php $menu = 'something_type'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Negoziazione</th>
				<th>Nome Negoziazione ita</th>
				<th>Nome Negoziazione fra</th>
				<th>Nome Negoziazione eng</th>
				<th>Prezzo visibile</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::type_negotiation.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name_ita', old('name_ita'), array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione ita', 'required')) }}</th>
					<th>{{ Form::text('name_fra', old('name_fra'), array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione fra', 'required')) }}</th>
					<th>{{ Form::text('name_eng', old('name_eng'), array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione eng', 'required')) }}</th>
					<th>{{ Form::checkbox('is_visible_price', 1, old('is_visible_price')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name_ita" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name_ita" data-sortable="true">Nome Negoziazione ita</th>
				<th data-field="name_fra" data-sortable="true">Nome Negoziazione fra</th>
				<th data-field="name_eng" data-sortable="true">Nome Negoziazione eng</th>
				<th data-field="is_visible_price" data-sortable="true">Prezzo visibile</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($type_negotiations->all() as $index => $type_negotiation)
			<tr>
			{{ Form::open(array('route' => array('admin::type_negotiation.update', $type_negotiation->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name_ita', $type_negotiation->name_ita, array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione ita', 'required')) }}</th>
				<th>{{ Form::text('name_fra', $type_negotiation->name_fra, array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione fra', 'required')) }}</th>
				<th>{{ Form::text('name_eng', $type_negotiation->name_eng, array('class' => 'form-control', 'placeholder' => 'Nome Negoziazione eng', 'required')) }}</th>
				<th>{{ Form::checkbox('is_visible_price', 1, $type_negotiation->is_visible_price) }}
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $type_negotiation->id }}">Rimuovi</a></th>
			{{ Form::close() }}
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $type_negotiation->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la negozzazione</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la negozzazione <strong>{{ $type_negotiation->name_ita }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::type_negotiation.destroy', $type_negotiation->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection