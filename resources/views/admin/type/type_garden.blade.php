@extends('layouts.admin')

@section('title', 'Giardino')
<?php $menu = 'something_type'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Giardino</th>
				<th>Nome Giardino ita</th>
				<th>Nome Giardino fra</th>
				<th>Nome Giardino eng</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::type_garden.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name_ita', old('name_ita'), array('class' => 'form-control', 'placeholder' => 'Nome Giardino ita', 'required')) }}</th>
					<th>{{ Form::text('name_fra', old('name_fra'), array('class' => 'form-control', 'placeholder' => 'Nome Giardino fra', 'required')) }}</th>
					<th>{{ Form::text('name_eng', old('name_eng'), array('class' => 'form-control', 'placeholder' => 'Nome Giardino eng', 'required')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name_ita" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name_ita" data-sortable="true">Nome Giardino ita</th>
				<th data-field="name_fra" data-sortable="true">Nome Giardino fra</th>
				<th data-field="name_eng" data-sortable="true">Nome Giardino eng</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($type_gardens->all() as $index => $type_garden)
			<tr>
			{{ Form::open(array('route' => array('admin::type_garden.update', $type_garden->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name_ita', $type_garden->name_ita, array('class' => 'form-control', 'placeholder' => 'Nome Giardino ita', 'required')) }}</th>
				<th>{{ Form::text('name_fra', $type_garden->name_fra, array('class' => 'form-control', 'placeholder' => 'Nome Giardino fra', 'required')) }}</th>
				<th>{{ Form::text('name_eng', $type_garden->name_eng, array('class' => 'form-control', 'placeholder' => 'Nome Giardino eng', 'required')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $type_garden->id }}">Rimuovi</a></th>
			{{ Form::close() }}
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $type_garden->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi il giardino</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere il giardino <strong>{{ $type_garden->name_ita }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::type_garden.destroy', $type_garden->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection