@extends('layouts.admin')

@section('title', 'Piani')
<?php $menu = 'something_type'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Piano</th>
				<th>Nome Piano ita</th>
				<th>Nome Piano fra</th>
				<th>Nome Piano eng</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::type_floor.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name_ita', old('name_ita'), array('class' => 'form-control', 'placeholder' => 'Nome Piano ita', 'required')) }}</th>
					<th>{{ Form::text('name_fra', old('name_fra'), array('class' => 'form-control', 'placeholder' => 'Nome Piano fra', 'required')) }}</th>
					<th>{{ Form::text('name_eng', old('name_eng'), array('class' => 'form-control', 'placeholder' => 'Nome Piano eng', 'required')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name_ita" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name_ita" data-sortable="true">Nome Piano ita</th>
				<th data-field="name_fra" data-sortable="true">Nome Piano fra</th>
				<th data-field="name_eng" data-sortable="true">Nome Piano eng</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($type_floors->all() as $index => $type_floor)
			<tr>
			{{ Form::open(array('route' => array('admin::type_floor.update', $type_floor->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name_ita', $type_floor->name_ita, array('class' => 'form-control', 'placeholder' => 'Nome Piano ita', 'required')) }}</th>
				<th>{{ Form::text('name_fra', $type_floor->name_fra, array('class' => 'form-control', 'placeholder' => 'Nome Piano fra', 'required')) }}</th>
				<th>{{ Form::text('name_eng', $type_floor->name_eng, array('class' => 'form-control', 'placeholder' => 'Nome Piano eng', 'required')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $type_floor->id }}">Rimuovi</a></th>
			{{ Form::close() }}
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $type_floor->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi il piano</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere il piano <strong>{{ $type_floor->name_ita }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::type_floor.destroy', $type_floor->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection