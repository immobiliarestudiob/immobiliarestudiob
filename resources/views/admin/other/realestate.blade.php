@extends('layouts.admin')

@section('title', 'Modifica Contatti')
<?php $menu = 'something_date'; ?>

@section('content')

{{ Form::open(array('route' => array('admin::realestate.update', $realestate->id), 'method' => 'put', 'class' => 'form-horizontal')) }}
<div class="form-group">
	{{ Form::label('name', 'Nome agenzia', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('name', $realestate->name, ['class' => 'form-control', 'placeholder' => 'Nome agenzia', 'required']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('address', 'Indirizzo', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('address', $realestate->address, ['class' => 'form-control', 'placeholder' => 'Indirizzo', 'required']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('p_iva', 'P. Iva', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('p_iva', $realestate->p_iva, ['class' => 'form-control', 'placeholder' => 'P. Iva', 'required']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('telephone', 'Telefono', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('telephone', $realestate->telephone, ['class' => 'form-control', 'placeholder' => 'Telefono', 'required']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('telephone_2', 'Telefono 2', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('telephone_2', $realestate->telephone_2, ['class' => 'form-control', 'placeholder' => 'Telefono 2']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('cellular', 'Cell', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('cellular', $realestate->cellular, ['class' => 'form-control', 'placeholder' => 'Cell']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('cellular_2', 'Cell 2', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('cellular_2', $realestate->cellular_2, ['class' => 'form-control', 'placeholder' => 'Cell 2']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('fax', 'Fax', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('fax', $realestate->fax, ['class' => 'form-control', 'placeholder' => 'Fax']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('fax_2', 'Fax 2', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('fax_2', $realestate->fax_2, ['class' => 'form-control', 'placeholder' => 'Fax 2']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('email', 'Email', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('email', $realestate->email, ['class' => 'form-control', 'placeholder' => 'Email', 'required']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('agenti_imm', 'Numero FIAIP', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('agenti_imm', $realestate->agenti_imm, ['class' => 'form-control', 'placeholder' => 'Numero FIAIP']) }}
	</div>
</div>
<div class="form-group">
	{{ Form::label('rea', 'REA', array('class' => 'col-sm-2 control-label')) }}
	<div class="col-sm-10">
		{{ Form::text('rea', $realestate->rea, ['class' => 'form-control', 'placeholder' => 'REA']) }}
	</div>
</div>
<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		{{ Form::submit('Salva', array('class' => 'btn btn-default')) }}
	</div>
</div>
{{ Form::close() }}

@endsection