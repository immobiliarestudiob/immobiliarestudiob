@extends('layouts.admin')

@section('title', 'Tipologie')
<?php $menu = 'something_type'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Tipologia</th>
				<th>Nome Tipologia ita</th>
				<th>Nome Tipologia fra</th>
				<th>Nome Tipologia eng</th>
				<th>Classe</th>
				<th>Tipologia superiore (opzionale)</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::typology.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name_ita', old('name_ita'), array('class' => 'form-control', 'placeholder' => 'Nome Tipologia ita', 'required')) }}</th>
					<th>{{ Form::text('name_fra', old('name_fra'), array('class' => 'form-control', 'placeholder' => 'Nome Tipologia fra', 'required')) }}</th>
					<th>{{ Form::text('name_eng', old('name_eng'), array('class' => 'form-control', 'placeholder' => 'Nome Tipologia eng', 'required')) }}</th>
					<th>{{ Form::select('class', ['apartment'=>'Appartamento', 'house'=>'Casa', 'commercial'=>'Commerciale', 'land'=>'Esterno'], old('class'), array('class' => 'form-control', 'placeholder' => 'Classe')) }}</th>
					<th>{{ Form::select('uptypology_id', $typologies_list, old('uptypology_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona (opzionale)')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name_ita" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name_ita" data-sortable="true">Nome Tipologia ita</th>
				<th data-field="name_fra" data-sortable="true">Nome Tipologia fra</th>
				<th data-field="name_eng" data-sortable="true">Nome Tipologia eng</th>
				<th>Classe</th>
				<th data-field="typology_sup" data-sortable="true">Tipologia superiore (opzionale)</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($typologies->all() as $index => $typology)
			<tr>
			{{ Form::open(array('route' => array('admin::typology.update', $typology->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name_ita', $typology->name_ita, array('class' => 'form-control', 'placeholder' => 'Nome Tipologia ita', 'required')) }}</th>
				<th>{{ Form::text('name_fra', $typology->name_fra, array('class' => 'form-control', 'placeholder' => 'Nome Tipologia fra', 'required')) }}</th>
				<th>{{ Form::text('name_eng', $typology->name_eng, array('class' => 'form-control', 'placeholder' => 'Nome Tipologia eng', 'required')) }}</th>
				<th>{{ Form::select('class', ['apartment'=>'Appartamento', 'house'=>'Casa', 'commercial'=>'Commerciale', 'land'=>'Esterno'], $typology->class, array('class' => 'form-control', 'placeholder' => 'Classe')) }}</th>
				<th>{{ Form::select('uptypology_id', $typologies_list, $typology->uptypology_id, array('class' => 'form-control', 'placeholder' => 'Seleziona (opzionale)')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $typology->id }}">Rimuovi</a></th>
			{{ Form::close() }}
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $typology->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la tipologia</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la tipologia <strong>{{ $typology->name_ita }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::typology.destroy', $typology->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection