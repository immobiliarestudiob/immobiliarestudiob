@extends('layouts.admin')

@section('title', 'Categorie')
<?php $menu = 'something_type'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Categoria</th>
				<th>Nome Categoria ita</th>
				<th>Nome Categoria fra</th>
				<th>Nome Categoria eng</th>
				<th>Categoria superiore (opzionale)</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::category.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name_ita', old('name_ita'), array('class' => 'form-control', 'placeholder' => 'Nome Zona ita', 'required')) }}</th>
					<th>{{ Form::text('name_fra', old('name_fra'), array('class' => 'form-control', 'placeholder' => 'Nome Zona fra', 'required')) }}</th>
					<th>{{ Form::text('name_eng', old('name_eng'), array('class' => 'form-control', 'placeholder' => 'Nome Zona eng', 'required')) }}</th>
					<th>{{ Form::select('upcategory_id', $categories_list, old('upcategory_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una categoria superiore (opzionale)')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-toggle="table" data-sort-name="name_ita" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name_ita" data-sortable="true">Nome Categoria ita</th>
				<th data-field="name_fra" data-sortable="true">Nome Categoria fra</th>
				<th data-field="name_eng" data-sortable="true">Nome Categoria eng</th>
				<th data-field="category_sup" data-sortable="true">Categoria superiore (opzionale)</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories->all() as $index => $category)
			<tr>
			{{ Form::open(array('route' => array('admin::category.update', $category->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name_ita', $category->name_ita, array('class' => 'form-control', 'placeholder' => 'Nome Categoria ita', 'required')) }}</th>
				<th>{{ Form::text('name_fra', $category->name_fra, array('class' => 'form-control', 'placeholder' => 'Nome Categoria fra', 'required')) }}</th>
				<th>{{ Form::text('name_eng', $category->name_eng, array('class' => 'form-control', 'placeholder' => 'Nome Categoria eng', 'required')) }}</th>
				<th>{{ Form::select('upcategory_id', $categories_list, $category->upcategory_id, array('class' => 'form-control', 'placeholder' => 'Seleziona una categoria superiore (opzionale)')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $category->id }}">Rimuovi</a></th>
			{{ Form::close() }}
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la categoria</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la categoria <strong>{{ $category->name_ita }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::category.destroy', $category->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection