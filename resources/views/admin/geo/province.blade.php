@extends('layouts.admin')

@section('title', 'Province')
<?php $menu = 'something_where'; ?>

@section('content')

<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Provincia</th>
				<th>Nome Provincia</th>
				<th>Sigla Provincia</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::province.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Nome Provincia', 'required')) }}</th>
					<th>{{ Form::text('abbreviation', old('abbreviation'), array('class' => 'form-control', 'placeholder' => 'Sigla Provincia', 'required')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Provincia</th>
				<th data-field="province" data-sortable="true">Sigla Provincia</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($provinces->all() as $index => $province)
			{{ Form::open(array('route' => array('admin::province.update', $province->id), 'method' => 'put')) }}
				<tr>
					<th>{{ ++$index }}</th>
					<th>{{ Form::text('name', $province->name, array('class' => 'form-control', 'placeholder' => 'Nome Provincia', 'required')) }}</th>
					<th>{{ Form::text('abbreviation', $province->abbreviation, array('class' => 'form-control', 'placeholder' => 'Sigla Comune', 'required')) }}</th>
					<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
					<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $province->id }}">Rimuovi</a></th>
				</tr>
			{{ Form::close() }}
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $province->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la provincia</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la provincia <strong>{{ $province->name }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::province.destroy', $province->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection