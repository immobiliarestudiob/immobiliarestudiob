@extends('layouts.admin')

@section('title', 'Comuni')
<?php $menu = 'something_where'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Comune</th>
				<th>Nome Comune</th>
				<th>Provincia</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::town.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Nome Comune', 'required')) }}</th>
					<th>{{ Form::select('province_id', $provinces, old('province_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una provincia', 'required')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Comune</th>
				<th data-field="province_id" data-sortable="true">Provincia</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
				<th>Anteprima</th>
				<th>Azione</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($towns->all() as $index => $town)
			<tr>
			{{ Form::open(array('route' => array('admin::town.update', $town->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name', $town->name, array('class' => 'form-control', 'placeholder' => 'Nome Comune', 'required')) }}</th>
				<th>{{ Form::select('province_id', $provinces, $town->province_id, array('class' => 'form-control', 'placeholder' => 'Seleziona una provincia', 'required')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $town->id }}">Rimuovi</a></th>
			{{ Form::close() }}

				@if ($town->hasPhoto())
					<th><a href="{{ $town->photo->getLarge() }}" class="fancybox"><img src="{{ $town->photo->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a></th>
					<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_foto_{{ $town->id }}">Rimuovi foto</a></th>
				@else
					{{ Form::open(array('route' => array('admin::town.upload_photo', $town->id), 'method' => 'put', 'files' => true)) }}
					<th>{{ Form::file('photo', array('required')) }}</th>
					<th>{{ Form::submit('Carica', array('class' => 'btn btn-default')) }}</th>
					{{ Form::close() }}
				@endif

			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $town->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi il comune</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere il comune <strong>{{ $town->name }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::town.destroy', $town->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@if ($town->hasPhoto())
			<!-- Modal -->
			<div class="modal fade" id="myModal_foto_{{ $town->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi il comune</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la foto del comune di <strong>{{ $town->name }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::town.destroy_photo', $town->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi foto', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		    @endif
			@endforeach
		</tbody>

	</table>
</div>

@endsection