@extends('layouts.admin')

@section('title', 'Zone')
<?php $menu = 'something_where'; ?>

@section('content')
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>Inserisci Zona</th>
				<th>Nome Zona</th>
				<th>Comune</th>
				<th>Inserisci</th>
			</tr>
		</thead>
		<tbody>
			{{ Form::open(array('route' => array('admin::area.store'), 'method' => 'post')) }}
				<tr>
					<th></th>
					<th>{{ Form::text('name', old('name'), array('class' => 'form-control', 'placeholder' => 'Nome Zona', 'required')) }}</th>
					<th>{{ Form::select('town_id', $towns, old('town_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un comune', 'required')) }}</th>
					<th>{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}</th>
				</tr>
			{{ Form::close() }}
		</tbody>
	</table>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="name" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th data-field="name" data-sortable="true">Nome Zona</th>
				<th data-field="town" data-sortable="true">Comune</th>
				<th>Modifica</th>
				<th>Rimuovi</th>
				<th>Anteprima</th>
				<th>Azione</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($areas->all() as $index => $area)
			<tr>
			{{ Form::open(array('route' => array('admin::area.update', $area->id), 'method' => 'put')) }}
				<th>{{ ++$index }}</th>
				<th>{{ Form::text('name', $area->name, array('class' => 'form-control', 'placeholder' => 'Nome Zona', 'required')) }}</th>
				<th>{{ Form::select('town_id', $towns, $area->town_id, array('class' => 'form-control', 'placeholder' => 'Seleziona un comune', 'required')) }}</th>
				<th>{{ Form::submit('Modifica', array('class' => 'btn btn-default')) }}</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $area->id }}">Rimuovi</a></th>
			{{ Form::close() }}

				@if ($area->hasPhoto())
					<th><a href="{{ $area->photo->getLarge() }}" class="fancybox"><img src="{{ $area->photo->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a></th>
					<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_foto_{{ $area->id }}">Rimuovi foto</a></th>
				@else
					{{ Form::open(array('route' => array('admin::area.upload_photo', $area->id), 'method' => 'put', 'files' => true)) }}
					<th>{{ Form::file('photo', array('required')) }}</th>
					<th>{{ Form::submit('Carica', array('class' => 'btn btn-default')) }}</th>
					{{ Form::close() }}
				@endif

			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $area->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la zona</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la zona <strong>{{ $area->name }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::area.destroy', $area->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@if ($area->hasPhoto())
			<!-- Modal -->
			<div class="modal fade" id="myModal_foto_{{ $area->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi la zona</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere la foto del zona di <strong>{{ $area->name }}</strong>?</div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::area.destroy_photo', $area->id), 'method' => 'delete')) }}
								{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
								{{ Form::submit('Rimuovi foto', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
		    @endif
			@endforeach
		</tbody>

	</table>
</div>

@endsection