@extends('layouts.admin')

@section('title', 'Vetrina Immobili')
<?php $menu = 'showcase_property'; ?>

@section('content')

<div class="form-inline">
	<div class="form-group">
		<label class="sr-only" for="search_rif">Riferimento</label>
		<input type="text" class="form-control" id="search_rif" placeholder="Riferimento">
	</div>
	<button type="button" class="btn btn-default" id="search_btn" >Cerca</button>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th data-field="prezzo" data-sortable="true">Prezzo</th>
				<th>Vetrina Sito</th>
				<th>Vetrina TV</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($properties->all() as $index => $property)
			<tr id="search_{{ $property->rifimm }}">
				<th>{{ ++$index }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->priceEuro }}</th>
				@if ($property->is_showcase)
				<th><a href="{{ route('admin::property.remove_showcase', [$property->id]) }}" class="btn btn-default btn-warning">Rimuovi dalla vetrina</a></th>
				@else
				<th><a href="{{ route('admin::property.add_showcase', [$property->id]) }}" class="btn btn-default">Aggiungi alla vetrina</a></th>
				@endif
				@if ($property->is_showcase_tv)
				<th><a href="{{ route('admin::property.remove_showcase_tv', [$property->id]) }}" class="btn btn-default btn-warning">Rimuovi dalla vetrina TV</a></th>
				@else
				<th><a href="{{ route('admin::property.add_showcase_tv', [$property->id]) }}" class="btn btn-default">Aggiungi alla vetrina TV</a></th>
				@endif
			</tr>
			@endforeach
		</tbody>

	</table>
</div>

@endsection