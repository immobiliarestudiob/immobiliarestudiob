@extends('layouts.admin')

@section('title', 'Stampa Immobili')
<?php $menu = 'print_property'; ?>

@section('content')

<div class="form-inline">
	<div class="form-group">
		<label class="sr-only" for="search_rif">Riferimento</label>
		<input type="text" class="form-control" id="search_rif" placeholder="Riferimento">
	</div>
	<button type="button" class="btn btn-default" id="search_btn" >Cerca</button>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th data-field="prezzo" data-sortable="true">Prezzo</th>
				<th>Orizzontale</th>
				<th>Verticale</th>
				<th>Stampa nuovo</th>
				<th>Stampa test</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($properties->all() as $index => $property)
			<tr id="search_{{ $property->rifimm }}">
				<th>{{ ++$index }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->priceEuro }}</th>
				<th><a class="btn btn-default" href="{{ route('admin::property.print_o', [$property->id]) }}" role="button">Stampa O</a></th>
		        <th><a class="btn btn-default" href="{{ route('admin::property.print_v', [$property->id]) }}" role="button">Stampa V</a></th>
		        <th><a class="btn btn-default btn-success" href="{{ route('admin::property.print_new', [$property->id]) }}" role="button">Stampa nuovo</a></th>
		        <th><a class="btn btn-default btn-info" href="{{ route('admin::property.print_test', [$property->id]) }}" role="button">Stampa test</a></th>
			</tr>
			@endforeach
		</tbody>

	</table>
</div>

@endsection