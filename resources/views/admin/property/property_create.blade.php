@extends('layouts.admin')

@section('title', 'Inserisci nuovo immobile')
<?php $menu = 'new_property'; ?>

@section('content')

{{ Html::script('js/ckeditor/ckeditor.js') }}

{{ Form::open(array('route' => array('admin::property.store'), 'method' => 'post', 'class' => 'form-horizontal')) }}
<div class="container-fluid">
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				{{ Form::label('rifimm', 'Riferimento') }}
				{{ Form::text('rifimm', old('rifimm'), array('class' => 'form-control', 'placeholder' => 'Superiore a '.$max_bl.' o '.$max_be, 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('user_id', 'Funzionario') }}
				{{ Form::select('user_id', $users_list, old('user_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Funzionario', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('category_id', 'Categoria') }}
				{{ Form::select('category_id', $categories_list, old('category_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Categoria', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('typology_id', 'Tipologia') }}
				{{ Form::select('typology_id', $typologies_list, old('typology_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Tipologia', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('province_id', 'Provincia') }}
				{{ Form::select('province_id', $provinces_list, old('province_id'), array('id' => 'select_province', 'class' => 'form-control', 'placeholder' => 'Seleziona una Provincia', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('town_id', 'Comune') }}
				{{ Form::select('town_id', [], old('town_id'), array('id' => 'select_town', 'class' => 'form-control', 'placeholder' => 'Seleziona prima una provincia', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('area_id', 'Zona') }}
				{{ Form::select('area_id', [], old('area_id'), array('id' => 'select_area', 'class' => 'form-control', 'placeholder' => 'Seleziona prima un comune')) }}
			</div>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<div class="form-group">
				{{ Form::label('is_to_sale', 'Tipo di contratto') }}
				{{ Form::select('is_to_sale', array('1' => 'Vendita', '0' => 'Affitto'), old('is_to_sale'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Tipo di contratto', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('price', 'Prezzo') }}
				<div class="input-group">
					{{ Form::text('price', old('price'), array('class' => 'form-control', 'placeholder' => 'Prezzo', 'required')) }}
					<div class="input-group-addon">&euro;</div>
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('type_negotiation_id', 'Trattativa') }}
				{{ Form::select('type_negotiation_id', $type_negotiations_list, old('type_negotiation_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Trattativa', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_floor_id', 'Piano') }}
				{{ Form::select('type_floor_id', $type_floors_list, old('type_floor_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Piano', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('total_floor', 'Totale piani') }}
				{{ Form::text('total_floor', old('total_floor'), array('class' => 'form-control', 'placeholder' => 'Totale piani')) }}
			</div>
			<div class="row">
				<div class="col-xs-5">
					<div class="form-group">
						{{ Form::label('room', 'Camere') }}
						{{ Form::text('room', old('room'), array('class' => 'form-control', 'placeholder' => 'Camere')) }}
					</div>
				</div>
				<div class="col-xs-5 col-xs-offset-2">
					<div class="form-group">
						{{ Form::label('locals', 'Locali') }}
						{{ Form::text('locals', old('locals'), array('class' => 'form-control', 'placeholder' => 'Locali')) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-5">
					<div class="form-group">
						{{ Form::label('bath', 'Bagni') }}
						{{ Form::text('bath', old('bath'), array('class' => 'form-control', 'placeholder' => 'Bagni')) }}
					</div>
				</div>
				<div class="col-xs-5 col-xs-offset-2">
					<div class="form-group">
						{{ Form::label('square_meters', 'MQ') }}
						<div class="input-group">
							{{ Form::text('square_meters', old('square_meters'), array('class' => 'form-control', 'placeholder' => 'MQ')) }}
							<div class="input-group-addon">mq</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				{{ Form::label('type_heating_id', 'Riscaldamento') }}
				{{ Form::select('type_heating_id', $type_heatings_list, old('type_heating_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Riscaldamento', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_kitchen_id', 'Cucina') }}
				{{ Form::select('type_kitchen_id', $type_kitchens_list, old('type_kitchen_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Cucina', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_occupation_id', 'Occupazione') }}
				{{ Form::select('type_occupation_id', $type_occupations_list, old('type_occupation_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Cucina', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_status_id', 'Stato') }}
				{{ Form::select('type_status_id', $type_statuss_list, old('type_status_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona uno Stato', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_box_id', 'Box') }}
				{{ Form::select('type_box_id', $type_boxs_list, old('type_box_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Box', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_distsm_id', 'Distanza Mare/Monti') }}
				{{ Form::select('type_distsm_id', $type_distsms_list, old('type_distsm_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Distanza Mare/Monti', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('type_garden_id', 'Giardino') }}
				{{ Form::select('type_garden_id', $type_gardens_list, old('type_garden_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona un Giardino', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('garden_square_meters', 'MQ Giardino') }}
				<div class="input-group">
					{{ Form::text('garden_square_meters', old('garden_square_meters'), array('class' => 'form-control', 'placeholder' => 'MQ Giardino')) }}
					<div class="input-group-addon">mq</div>
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('type_exposure_id', 'Esposizione') }}
				{{ Form::select('type_exposure_id', $type_exposures_list, old('type_exposure_id'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Esposizione', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('condominium_fees', 'Spese condominiali') }}
				<div class="input-group">
					{{ Form::text('condominium_fees', old('condominium_fees'), array('class' => 'form-control', 'placeholder' => 'Spese condominiali')) }}
					<div class="input-group-addon">&euro;</div>
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('energetic_class', 'Classe energetica') }}
				{{ Form::select('energetic_class', array(
				'A+' => 'A+ &lt;15kWh/m²anno',
				'A' => 'A &lt;30kWh/m²anno',
				'B' => 'B &lt;50kWh/m²anno',
				'C' => 'C &lt;70kWh/m²anno',
				'D' => 'D &lt;90kWh/m²anno',
				'E' => 'E &lt;120kWh/m²anno',
				'F' => 'F &lt;160kWh/m²anno',
				'G' => 'G &gt;160kWh/m²anno',
				), old('energetic_class'), array('class' => 'form-control', 'placeholder' => 'Seleziona una Classe energetica', 'required')) }}
			</div>
			<div class="form-group">
				{{ Form::label('energetic_class_value', 'Valore classe energetica') }}
				<div class="input-group">
					{{ Form::text('energetic_class_value', old('energetic_class_value'), array('class' => 'form-control', 'placeholder' => 'Valore classe energetica')) }}
					<div class="input-group-addon">kWh/m<sup>2</sup>a</div>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<div class="form-group"> 
				{{ Form::label('is_elevator', 'Ascensore') }} <br />
				<label class="radio-inline">{{ Form::radio('is_elevator', '1', old('is_elevator')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_elevator', '0', !old('is_elevator')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_terrace', 'Terrazza') }} <br />
				<label class="radio-inline">{{ Form::radio('is_terrace', '1', old('is_terrace')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_terrace', '0', !old('is_terrace')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_balcony', 'Balcone') }} <br />
				<label class="radio-inline">{{ Form::radio('is_balcony', '1', old('is_balcony')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_balcony', '0', !old('is_balcony')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_act_of_commission', 'Atto di Prov.') }} <br />
				<label class="radio-inline">{{ Form::radio('is_act_of_commission', '1', old('is_act_of_commission')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_act_of_commission', '0', !old('is_act_of_commission')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_floorplans', 'Planimetrie cat.') }} <br />
				<label class="radio-inline">{{ Form::radio('is_floorplans', '1', old('is_floorplans')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_floorplans', '0', !old('is_floorplans')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_furnished', 'Arredato') }} <br />
				<label class="radio-inline">{{ Form::radio('is_furnished', '1', old('is_furnished')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_furnished', '0', !old('is_furnished')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_seafront', 'Frontemare') }} <br />
				<label class="radio-inline">{{ Form::radio('is_seafront', '1', old('is_seafront')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_seafront', '0', !old('is_seafront')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_pool', 'Piscina') }} <br />
				<label class="radio-inline">{{ Form::radio('is_pool', '1', old('is_pool')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_pool', '0', !old('is_pool')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_special_price', 'Offerta speciale') }} <br />
				<label class="radio-inline">{{ Form::radio('is_special_price', '1', old('is_special_price')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_special_price', '0', !old('is_special_price')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_showcase', 'Vetrina web') }} <br />
				<label class="radio-inline">{{ Form::radio('is_showcase', '1', old('is_showcase')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_showcase', '0', !old('is_showcase')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_showcase_tv', 'Vetrina TV') }} <br />
				<label class="radio-inline">{{ Form::radio('is_showcase_tv', '1', old('is_showcase_tv')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_showcase_tv', '0', !old('is_showcase_tv')) }} No</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_visible', 'Pubblicato / Sospeso') }} <br />
				<label class="radio-inline">{{ Form::radio('is_visible', '1', old('is_visible')) }} Pubblicato</label>
				<label class="radio-inline">{{ Form::radio('is_visible', '0', !old('is_visible')) }} Sospeso</label>
			</div>
			<div class="form-group"> 
				{{ Form::label('is_old_town', 'Centro Storico') }} <br />
				<label class="radio-inline">{{ Form::radio('is_old_town', '1', old('is_old_town')) }} Si</label>
				<label class="radio-inline">{{ Form::radio('is_old_town', '0', !old('is_old_town')) }} No</label>
			</div>
		</div>
	</div>
	<div class="form-group"> 
		{{ Form::label('address_visible', 'Indirizzo visibile') }} <br />
		<label class="radio-inline">{{ Form::radio('address_visible', '1', old('address_visible')) }} Si</label>
		<label class="radio-inline">{{ Form::radio('address_visible', '0', !old('address_visible')) }} No</label>
	</div>
	<div class="form-group">
		{{ Form::hidden('address_geocode', old('address_geocode')) }}
		{{ Form::text('address', old('address'), array('class' => 'controls', 'placeholder' => 'Indirizzo', 'id' => 'address')) }}
		<div id="map"></div>
	</div>
	<div class="form-group">
		{{ Form::label('description_ita', 'Descrizione italiano') }}
		{{ Form::textarea('description_ita', old('description_ita'), array('class' => 'form-control')) }}
		<script type="text/javascript">CKEDITOR.replace('description_ita');CKEDITOR.add;</script> 
	</div>
	<div class="form-group">
		{{ Form::label('description_fra', 'Descrizione francese') }}
		{{ Form::textarea('description_fra', old('description_fra'), array('class' => 'form-control')) }}
		<script type="text/javascript">CKEDITOR.replace('description_fra');CKEDITOR.add;</script> 
	</div>
	<div class="form-group">
		{{ Form::label('description_eng', 'Descrizione inglese') }}
		{{ Form::textarea('description_eng', old('description_eng'), array('class' => 'form-control')) }}
		<script type="text/javascript">CKEDITOR.replace('description_eng');CKEDITOR.add;</script> 
	</div>
	{{ Form::submit('Inserisci', array('class' => 'btn btn-default')) }}
</div>
{{ Form::close() }}
@endsection

@push('scripts')
<script>
	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: {lat: 43.8270016, lng: 7.6941323}
		});
		var input = (document.getElementById('address'));
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);
		autocomplete.setTypes([]);

		var marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		});

		if ($("input[name=address_geocode]").val()) {
			var str = $("input[name=address_geocode]").val().replace(' ', '').replace('(', '').replace(')', '').split(',');
			var location = new google.maps.LatLng(str[0], str[1]);
			setMarker(marker, location, google);
			map.setCenter(location);
		}

		autocomplete.addListener('place_changed', function() {
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				window.alert("Autocomplete's returned place contains no geometry");
				return;
			}
			map.setCenter(place.geometry.location);
		    setMarker(marker, place.geometry.location, google);
		});

		var geocoder = new google.maps.Geocoder();
		google.maps.event.addListener(map, "click", function(event) {
			var location = event.latLng;
			geocoder.geocode({'location': location}, function(results, status) {
				if (status === 'OK') {
					map.setCenter(location);
					setMarker(marker, location, google);
					if (results[1]) {
						$("input[name=address]").val( results[1].formatted_address );
					} else {
						$("input[name=address]").val('no_address_found');
					}
				} else {
					window.alert('Geocoder failed due to: ' + status);
				}
			});
		});
	}

	function setMarker(marker, location, google) {
		marker.setIcon(({
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(35, 35)
		}));
		marker.setPosition(location);
		marker.setVisible(true);

		$("input[name=address_geocode]").val( location.toString() );
	}

	$('#address').keypress(function (e) {
		if (e.which == 13) {
			return false;
		}
	});

	$('#select_province').on('change', function() {
		$.ajaxSetup({
			header:$('meta[name="_token"]').attr('content')
		});
		$.ajax({
			type: 'GET',
			url: '/ajax/province/' + this.value + '/towns',
			data: $(this).serialize(),
			dataType: 'json',
			success: function(data){
				$('#select_town').find('option').remove();
				$('#select_area').find('option').remove();

				$('#select_town').append($('<option>', {value: -1, text: "Seleziona una città", selected: true}));
				$.each(data['towns'], function (id, name) {
					$('#select_town').append($('<option>', {
						value: id,
						text : name 
					}));
				});
			}
		});
	});

	$('#select_town').on('change', function() {
		$.ajaxSetup({
			header:$('meta[name="_token"]').attr('content')
		});
		$.ajax({
			type: 'GET',
			url: '/ajax/town/' + this.value + '/areas',
			data: $(this).serialize(),
			dataType: 'json',
			success: function(data){
				$('#select_area').find('option').remove();

				$('#select_area').append($('<option>', {value: -1, text: "Seleziona una zona", selected: true}));
				$.each(data['areas'], function (id, name) {
					$('#select_area').append($('<option>', {
						value: id,
						text : name 
					}));
				});
			}
		});
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5MbyXjcQ8iT3LeuISr5S3gNf8TilIq9E&libraries=places&callback=initMap" async defer></script>
@endpush

@push('styles')
<style type="text/css">
	#map {
		margin-top: 5px;
		height: 200px;
	}
	.controls {
		margin-top: 10px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	}
	#address {
		background-color: #fff;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		margin-left: 12px;
		padding: 0 11px 0 13px;
		text-overflow: ellipsis;
		width: 500px;
	}
	#address:focus {
		border-color: #4d90fe;
	}
</style>
@endpush