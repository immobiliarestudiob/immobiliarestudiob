@extends('layouts.admin')

@section('title', 'Cestino Immobili')
<?php $menu = 'trashed_property'; ?>

@section('content')

<div class="form-inline">
	<div class="form-group">
		<label class="sr-only" for="search_rif">Riferimento</label>
		<input type="text" class="form-control" id="search_rif" placeholder="Riferimento">
	</div>
	<button type="button" class="btn btn-default" id="search_btn" >Cerca</button>
</div>
<div class="table-responsive">
	<table class="table table-hover" data-sort-name="rifimm" data-sort-order="asc">
		<thead>
			<tr>
				<th>#</th>
				<th>Anteprima</th>
				<th data-field="rifimm" data-sortable="true">Rif.</th>
				<th data-field="tipologia" data-sortable="true">Tipologia</th>
				<th data-field="comune" data-sortable="true">Comune</th>
				<th data-field="prezzo" data-sortable="true">Prezzo</th>
				<th>Ripristina</th>
				<th>Rimuovi</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($properties->all() as $index => $property)
			<tr id="search_{{ $property->rifimm }}">
				<th>{{ ++$index }}</th>
				<th>@if ($property->hasMainPhoto())
					<a href="{{ $property->mainPhoto()->getLarge() }}" class="fancybox"><img src="{{ $property->mainPhoto()->getThumb() }}" alt="anteprima" class="img-responsive img-thumbnail" /></a>
				@endif</th>
				<th>{{ $property->rifimm }}</th>
				<th>{{ $property->typology->name_ita }}</th>
				<th>{{ $property->town->name }}</th>
				<th>{{ $property->priceEuro }}</th>
				<th>
					{{ Form::open(array('route' => array('admin::property.restore', $property->id), 'method' => 'put')) }}
					{{ Form::submit('Ripristina', array('class' => 'btn btn-default')) }}
					{{ Form::close() }}
				</th>
				<th><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $property->id }}">Rimuovi</a></th>
			</tr>
			<!-- Modal -->
			<div class="modal fade" id="myModal_{{ $property->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Rimuovi l'immobile</h4>
						</div>
						<div class="modal-body">Sei sicuro di voler rimuovere l'immobile <strong>{{ $property->rifimm }}</strong></div>
						<div class="modal-footer">
							{{ Form::open(array('route' => array('admin::property.force_delete', $property->id), 'method' => 'delete')) }}
							{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
							{{ Form::submit('Rimuovi', array('class' => 'btn btn-primary btn-danger')) }}
							{{ Form::close() }}
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</tbody>

	</table>
</div>

@endsection