@extends('layouts.admin')

@section('title', 'Modifica le foto del&#180;immobile '.$property->rifimm)
<?php $menu = 'property'; ?>

@section('content')
<div class="row list-group" id="property_photos">
	<div class="col-sm-12">
		<h2>Carica una nuova foto</h2>
		{{ Form::open(array('route' => array('admin::property.upload_photo', $property->id), 'method' => 'put', 'files' => true)) }}
		{{ Form::hidden('property_id', $property->id) }}
		{{ Form::file('photo', array('required')) }}<br />
		{{ Form::submit('Carica', array('class' => 'btn btn-default')) }}
		{{ Form::close() }}
		<br /><br />
	</div>
	@foreach ($photos as $index => $photo)
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail list-group-item">
			<a href="{{ $photo->getLarge() }}" class="fancybox" rel="photos"><img src="{{ $photo->getSmall() }}" alt="{{ $photo->name }}" class="img-responsive img-thumbnail" /></a>
			<div class="caption">
				<h3>Posizione: <span class="position">{{ $index + 1 }}</span></h3>
				<span class="photo_id hidden">{{ $photo->id }}</span>
				<p>
					@if (!$photo->is_main)
					<a href="{{ route('admin::property.set_photo_main', [$property->id, $photo->id]) }}" class="btn btn-primary" role="button">Imposta principale</a>
					@endif
					<a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $photo->id }}">Rimuovi</a>
				</p>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal_{{ $photo->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Rimuovi la foto</h4>
				</div>
				<div class="modal-body">Sei sicuro di voler rimuovere la foto</div>
				<div class="modal-footer">
					{{ Form::open(array('route' => array('admin::property.destroy_photo', $property->id, $photo->id), 'method' => 'delete')) }}
					{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
					{{ Form::submit('Rimuovi foto', array('class' => 'btn btn-primary btn-danger')) }}
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div>

<hr />

<div class="row">
	<div class="col-sm-6 col-md-4">
		<h2>Carica una planimetria</h2>
		{{ Form::open(array('route' => array('admin::property.upload_photo', $property->id), 'method' => 'put', 'files' => true)) }}
		{{ Form::hidden('property_id', $property->id) }}
		{{ Form::hidden('is_planimetry', true) }}
		{{ Form::file('photo', array('required')) }}<br />
		{{ Form::submit('Carica', array('class' => 'btn btn-default')) }}
		{{ Form::close() }}
	</div>
	@foreach ($planimetries as $index => $photo)
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<a href="{{ $photo->getLarge() }}" class="fancybox" rel="planimetries"><img src="{{ $photo->getSmall() }}" alt="{{ $photo->name }}" class="img-responsive img-thumbnail" /></a>
			<div class="caption">
				<h3>Planimetria</h3>
				<p><a class="btn btn-default btn-danger" href="#" role="button" data-toggle="modal" data-target="#myModal_{{ $photo->id }}">Rimuovi</a></p>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal_{{ $photo->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Rimuovi la foto</h4>
					</div>
					<div class="modal-body">Sei sicuro di voler rimuovere la foto</div>
					<div class="modal-footer">
						{{ Form::open(array('route' => array('admin::property.destroy_photo', $property->id, $photo->id), 'method' => 'delete')) }}
						{{ Form::button('Annulla', array('class' => 'btn btn-default', 'data-dismiss' => 'modal')) }}
						{{ Form::submit('Rimuovi foto', array('class' => 'btn btn-primary btn-danger')) }}
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div>
@endsection

@push('styles')
<style type="text/css">
	.thumbnail { background: #fff; }
	.list-group-item { padding: 4px !important; margin-bottom: 22px !important; }
	.list-group-item img { height: 90px !important; }
</style>
@endpush

@push('scripts')
	{{ Html::script('js/sortable.js') }}
	<script>
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') }
		});

    	Sortable.create(property_photos, {
    		sort: true,
    		animation: 150,
    		onEnd: function (evt) {
    			reorder();
		    },
    	});

    	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    	var photos = [$('.list-group-item').length];

    	function reorder() {
    		$('.list-group-item').each(function( index ) {
    			$(this).find('.position').text(index + 1);
    			photos[index] = $(this).find('.photo_id').text();
    		});

    		$.ajax({
	            url: '/admin/property/{{ $property->id }}/sort_photos',
	            type: 'POST',
	            data: {
	            	'photos': photos,
	            },
	            dataType: 'JSON',
	            success: function(data) {
	                console.log(data);
	            }
	        });
    	}
	</script>
@endpush