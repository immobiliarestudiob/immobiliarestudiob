@extends('layouts.web')

@section('title', 'Home')

@section('content')
<section class="no-padding adv-search-section">
	@include('partials.home_properties_swiper')
	@include('partials._search')
</section>
@include('partials.counters')
@include('partials.home_properties_promo')
<section class="section-light bottom-padding-45 section-top-shadow">
	@include('partials.properties_last_insert')
</section>
<section class="section-light top-padding-45">
	@include('partials.properties_recent')
</section>
@endsection