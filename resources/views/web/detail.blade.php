@extends('layouts.web')

@section('title', trans('web.title_details', ['rifimm' => $property->rifimm]))

@section('content')
{{--Sezione foto--}}
<section class="section-dark no-padding">
	<!-- Slider main container -->
	<div id="swiper-gallery" class="swiper-container" style="position:relative;">
		<!-- Additional required wrapper -->
		<div class="swiper-wrapper">
			<!-- Slides -->
			@foreach($property->photos()->orderBy('position')->get() as $photo)
			<div class="swiper-slide">
				<div class="slide-bg swiper-lazy" data-background="{{$photo->getOriginalSize()}}"></div>
				<!-- Preloader image -->
				<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
			</div>
			@endforeach
		</div>
		<div class="slide-buttons slide-buttons-center">
			<a href="#" class="navigation-box navigation-box-next slide-next" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
			<div id="slide-more-cont"></div>
			<a href="#" class="navigation-box navigation-box-prev slide-prev" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
		</div>

		<div style="position:absolute;z-index:1;top:0;left:0;width:100%">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-9 col-lg-8 slide-desc-col animated fadeInUp">
						<div class="gallery-slide-cont">
							<div class="gallery-slide-cont-inner">	
								<div class="gallery-slide-title pull-right">
									<h5 class="subtitle-margin">{{trans_choice('web.details_subtitle', $property->is_to_sale, ['typology' => $property->typology->name])}}</h5>
									<h3>@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})<span class="special-color">.</span></h3>
								</div>
								<div class="gallery-slide-estate pull-right hidden-xs">
									<i class="fa fa-{{$property->typology->getFontawasome()}}"></i>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
							<div class="gallery-slide-desc-price pull-right">
								{{ $property->publicPriceLittle }}
							</div>	
							<div class="clearfix"></div>
						</div>	
					</div>			
				</div>
			</div>
		</div>
	</div>
</section>
<section class="thumbs-slider section-both-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-1">
				<a href="#" class="thumb-box thumb-prev pull-left" name="prev"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe800;</i></div></a>
			</div>
			<div class="col-xs-10">
				<!-- Slider main container -->
				<div id="swiper-thumbs" class="swiper-container">
					<!-- Additional required wrapper -->
					<div class="swiper-wrapper">
						<!-- Slides -->
						@foreach($property->photos()->orderBy('position')->get() as $photo)
						<div class="swiper-slide">
							<img class="slide-thumb" src="{{$photo->getSmall()}}" alt="">
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xs-1">
				<a href="#" class="thumb-box thumb-next pull-right" name="next"><div class="navigation-triangle"></div><div class="navigation-box-icon"><i class="jfont">&#xe802;</i></div></a>
			</div>
		</div>
	</div>
</section>
{{--Fine sezione foto--}}

<section class="section-light no-bottom-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-9 col-md-push-3">
				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-8">
						<div class="details-image pull-left hidden-xs">
							<i class="fa fa-{{$property->typology->getFontawasome()}}"></i>
						</div>
						<div class="details-title pull-left">
							<h5 class="subtitle-margin">{{trans_choice('web.details_subtitle', $property->is_to_sale, ['typology' => $property->typology->name])}}</h5>
							<h3>@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})<span class="special-color">.</span></h3>
						</div>

						@if(Auth::check())
						<div class="details-title pull-right">
							@if(Auth::user()->hasPreferred($property->id))
							<a href="{{route('web::user.remove_preferred', [Auth::user()->id, $property->id])}}" class="preferred preferred_remove" title="@lang('web.user_preferred_remove')" name="@lang('web.user_preferred_remove')">
								<i class="fa fa-heart fa-3x"></i>
							</a>
							@else
							<a href="{{route('web::user.add_preferred', [Auth::user()->id, $property->id])}}" class="preferred preferred_add" title="@lang('web.user_preferred_add')" name="@lang('web.user_preferred_add')">
								<i class="fa fa-heart-o fa-3x"></i>
							</a>
							@endif
						</div>
						@endif

						<div class="clearfix"></div>
						<div class="title-separator-primary"></div>
						<div class="details-desc text-justify">{!!$property->description!!}</div>
						
						<div class="clearfix"></div>
						<ul class="details-ticks">
							@foreach($property->getChecked() as $checked)
							<li class="col-xs-6 col-sm-4"><i class="jfont">&#xe815;</i>{{$checked}}</li>
							@endforeach
						</ul>

						@if($property->typology->class != 'land')
						<div class="clearfix"></div>
						<div class="row margin-top-60">
							<div class="col-xs-12">
								<h3 class="title-negative-margin">@lang('web.details_energetic_class')<span class="special-color">.</span></h3>
								<div class="title-separator-primary"></div>
							</div>
						</div>
						<img src="/images/energetic_class/energetic_class_{{$property->energetic_class}}.png" style="height: 120px" class="img-responsive margin-top-20 margin-bottom-20" title="{{$property->energetic_class}}" />
						<div class="clearfix"></div>
						@endif
					</div>
					<div class="col-xs-12 col-sm-5 col-md-4">
						<div class="details-parameters-price">{{ $property->publicPriceLittle }}</div>
						<div class="details-parameters">
							@if($property->square_meters > 0)
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_area')</div>
								<div class="details-parameters-val">{!!$property->squareMeters!!}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->locals > 0)
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_rooms')</div>
								<div class="details-parameters-val">{{$property->locals}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->bath > 0)
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_bathrooms')</div>
								<div class="details-parameters-val">{{$property->bath}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->total_floor > 0)
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_total_floor')</div>
								<div class="details-parameters-val">{{$property->total_floor}}</div>
								<div class="clearfix"></div>
							</div>
							@endif

							@if($property->hasFloor())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_floor')</div>
								<div class="details-parameters-val">{{$property->typeFloor->name}} / {{$property->total_floor}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasHeating())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_heating')</div>
								<div class="details-parameters-val">{{$property->typeHeating->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasKitchen())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_kitchen')</div>
								<div class="details-parameters-val">{{$property->typeKitchen->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasOccupation())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_occupation')</div>
								<div class="details-parameters-val">{{$property->typeOccupation->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasStatus())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_status')</div>
								<div class="details-parameters-val">{{$property->typeStatus->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasBox())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_box')</div>
								<div class="details-parameters-val">{{$property->typeBox->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasDistsm())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_distmm')</div>
								<div class="details-parameters-val">{{$property->typeDistsm->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasGarden())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_garden')</div>
								<div class="details-parameters-val">{{$property->typeGarden->name}} {!!$property->gardenSquareMeters!!}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->hasExposure())
							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_type_exposure')</div>
								<div class="details-parameters-val">{{$property->typeExposure->name}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->condominium_fees > 0)

							<div class="details-parameters-cont">
								<div class="details-parameters-name">@lang('web.details_condominium_fees')</div>
								<div class="details-parameters-val">{{$property->condominium_fees}}</div>
								<div class="clearfix"></div>
							</div>
							@endif
							@if($property->is_old_town)
							<div class="details-parameters-cont">
								<div class="details-parameters-name"></div>
								<div class="details-parameters-val">@lang('web.details_is_old_town')</div>
								<div class="clearfix"></div>
							</div>
							@endif
						</div>
						<div class="details-parameters-price details-parameters-print">
							<a href="{{route('web::print', $property->rifimm)}}" name="print">
								<i class="fa fa-print"></i>
								@lang('web.details_print')
							</a>
						</div>
					</div>
				</div>
				<div class="row margin-top-45">
					<div class="col-xs-12 apartment-tabs">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#tab-map" aria-controls="tab-map" role="tab" data-toggle="tab" name="map">
									<span>Map</span>
									<div class="button-triangle2"></div>
								</a>
							</li>
							@if($property->address_geocode != null && $property->address_visible)
							<li role="presentation">
								<a href="#tab-street-view" aria-controls="tab-street-view" role="tab" data-toggle="tab" name="street-view">
									<span>Street view</span>
									<div class="button-triangle2"></div>
								</a>
							</li>
							@endif
						</ul>
						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-map">
								<div id="estate-map" class="details-map"></div>
							</div>
							@if($property->address_geocode != null && $property->address_visible)
							<div role="tabpanel" class="tab-pane" id="tab-street-view">
								<div id="estate-street-view" class="details-map"></div>
							</div>
							@endif
						</div>
					</div>
				</div>
				<div class="row margin-top-60">
					<div class="col-xs-12">
						<h3 class="title-negative-margin">@lang('web.details_contact_title')<span class="special-color">.</span></h3>
						<div class="title-separator-primary"></div>
					</div>
				</div>
				<div class="row margin-top-45">
					<div class="col-xs-8 col-xs-offset-2 col-sm-3 col-sm-offset-0">
						<h5 class="subtitle-margin">@lang('web.details_contact_agent')</h5>
						<h3 class="title-negative-margin">{{$property->user->name}} {{$property->user->surname}}<span class="special-color">.</span></h3>
						@if($property->user->hasPhoto())
						<a class="agent-photo" name="agent">
							<img src="{{$property->user->photo->getOriginalSize()}}" alt="{{$property->user->name}} {{$property->user->surname}}" class="img-responsive" />
						</a>
						@endif
					</div>
					<div class="col-xs-12 col-sm-9">
						<div class="agent-social-bar">
							<div class="pull-left">
								<span class="agent-icon-circle">
									<i class="fa fa-phone"></i>
								</span>
								<span class="agent-bar-text"><a href="tel::{{$property->user->telephone}}" name="make_call">{{$property->user->telephone}}</a></span>
							</div>
							<div class="pull-left">
								<span class="agent-icon-circle">
									<i class="fa fa-envelope fa-sm"></i>
								</span>
								<span class="agent-bar-text"><a href="mailto::{{$property->user->email}}" name="send_email">{{$property->user->email}}</a></span>
							</div>
							<div class="clearfix"></div>
						</div>
						@include('partials.request_form', ['request_rifimm' => $property->rifimm])
					</div>
				</div>
				<div class="margin-top-45"></div>
			</div>
			<div class="col-xs-12 col-md-3 col-md-pull-9 hidden-xs hidden-sm">
				<div class="sidebar-left">
					@include('partials._search_sidebar')
					@include('partials.sidebar_properties', ['max' => 5])
				</div>
			</div>
		</div>
	</div>
</section>
<section class="section-light top-padding-45">
	@include('partials.details_similar')
	@include('partials.properties_recent')
</section>
@endsection

@push('scripts_mapInit')
@if($property->address_geocode != null)
mapInit({{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}},"estate-map","/images/web/pin-{{$property->typology->class}}.png", true);
streetViewInit({{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}},"estate-street-view");
@else
mapInitAddress("{{$property->town->name}}, {{$property->province->name}}, Italy","estate-map","/images/web/pin-{{$property->typology->class}}.png", true);
@endif
@endpush

@push('scripts_inside')
$(".preferred_remove")
.mouseover(function() { $(this).find("i").removeClass('fa-heart').addClass('fa-trash'); })
.mouseout(function() { $(this).find("i").removeClass('fa-trash').addClass('fa-heart'); })

$(".preferred_add")
.mouseover(function() { $(this).find("i").removeClass('fa-heart-o').addClass('fa-plus'); })
.mouseout(function() { $(this).find("i").removeClass('fa-plus').addClass('fa-heart-o'); })
@endpush

@push('styles')
<style type="text/css">
.preferred{color:#c6454a !important};
.preferred:hover{color:#a2292d !important};
</style>
@endpush