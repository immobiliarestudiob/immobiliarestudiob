<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Studio B Immobiliare S.R.L. &bull; Print {{$property->rifimm}}</title>

	{{ Html::style('css/web/bootstrap.min.css') }}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css">
	<style type="text/css">@charset "utf-8";@font-face{font-family:CaviarDreams;src:url(../../fonts/CaviarDreams/CaviarDreams.eot);src:url(../../fonts/CaviarDreams/CaviarDreams.eot?#iefix) format('embedded-opentype'),url(../../fonts/CaviarDreams/CaviarDreams.woff) format('woff'),url(../../fonts/CaviarDreams/CaviarDreams.ttf) format('truetype'),url(../../fonts/CaviarDreams/CaviarDreams.svg#CaviarDreams) format('svg')}body{font-family:CaviarDreams;margin:0;color:#565A5C!important;-webkit-print-color-adjust:exact;background:url(../../images/web/angle.png) bottom right no-repeat fixed}h1,h2,h3,h3 p{margin:0;padding:0;font-weight:400}h2{font-size:6mm}h3,h3 p{font-size:4mm}.back,.printer{position:absolute;top:20px;left:20px}.back{top:200px}.back img,.printer img{width:100px}.page{overflow:hidden;width:190mm;height:277mm;padding:10mm;margin:0 auto}.logo,.summary{margin-bottom:5mm}.clearfix:after,.clearfix:before{content:" "}.clearfix:after{clear:both}.row_logo{text-align:center}.logo{border:0;height:30mm}.summary{float:left;width:40%}.summary h3{font-size:6mm}.photo{float:left;width:60%}.foto_1,.photo img{width:100%}img{margin:0;padding:0;border:0}.row,.row h2{margin-bottom:5mm}.foto_1,.foto_2,.foto_3{float:left;margin-bottom:5mm;overflow:hidden}.foto_1{height:50mm}.foto_2,.foto_3{width:calc(50% - 2.5mm);height:30mm}.foto_2{margin-right:5mm}.row{width:100%;float:left}.row h2{font-weight:700;border-bottom:1px solid #AFCB51}.row .column{float:left;width:33%}@media screen{.page{border:1px solid #AFCB51}}@media print{body{-webkit-print-color-adjust:exact}.back,.printer{display:none}}</style>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body onLoad="imgCut()">

	<div class="printer"><a href="javascript:window.print()"><img src="/images/print/printer.png" border="0" alt="Stampa" title="Stampa" /></a></div>
	<div class="back"><a href="{{route('web::details', $property->rifimm)}}"><img src="/images/print/back.png" border="0" alt="Back" title="Back" /></a></div>

	<!--Inizio pagina-->
	<div class="page">
		<div class="row_logo"><img src="/images/web/logo-dark.png" class="logo" alt="logo" /></div>
		<div class="summary clearfix">
			<h3>@lang('web.details_rifimm'): <strong>{{$property->rifimm}}</strong></h3>
			<h3>@lang('web.details_price'): <strong>{{ $property->publicPriceLittle }}</strong></h3>
			<h3>@lang('web.search_town'): <strong>@if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})</strong></h3>
			<h3>@lang('web.search_category'): <strong>{{$property->category->name}}</strong></h3>
			<h3>@lang('web.search_typology'): <strong>{{$property->typology->name}}</strong></h3>
			<h3>@lang('web.details_area'): <strong>{!!$property->squareMeters!!}</strong></h3>
		</div>
		<div class="photo clearfix">
			@if ($property->hasMainPhoto())
			<div class="imgCut foto_1"><img src="{{ $property->mainPhoto()->getLarge() }}" alt="{{ $property->rifimm }}" /></div>
			@endif
			@foreach($property->getNormalPhotos($use_main = false) as $photo)
			<div class="imgCut foto_{{$loop->iteration + 1}}">
				<img src="{{ $photo->getMedium() }}" alt="{{ $property->rifimm }}" />
			</div>
			@if (!$loop->first)
			@break
			@endif
			@endforeach
		</div>
		<div class="description row clearfix">
			<h2>@lang('web.details_description')</h2>
			<h3>{!!$property->getShortDescription(600)!!}</h3>
		</div>
		<div class="characteristics row clearfix">
			<h2>@lang('web.details_car')</h2>
			<div class="column">
				<h3 class="little_title"><strong>@lang('web.details_car_gen')</strong></h3>
				<h3>@lang('web.search_category'): <strong>{{$property->category->name}}</strong></h3>
				<h3>@lang('web.search_typology'): <strong>{{$property->typology->name}}</strong></h3>
				@if($property->bath > 0)
				<h3>@lang('web.search_bath'): <strong>{{$property->bath}}</strong></h3>
				@endif
				@if($property->locals > 0)
				<h3>@lang('web.search_locals'): <strong>{{$property->locals}}</strong></h3>
				@endif
				@if($property->hasFloor())
				<h3>@lang('web.details_type_floor'): <strong>{{$property->typeFloor->name}}</strong></h3>
				@endif
				@if($property->total_floor > 0)
				<h3>@lang('web.details_total_floor'): <strong>{{$property->total_floor}}</strong></h3>
				@endif
				@if($property->hasStatus())
				<h3>@lang('web.details_type_status'): <strong>{{$property->typeStatus->name}}</strong></h3>
				@endif
				@if($property->square_meters > 0)
				<h3>@lang('web.details_area'): <strong>{!!$property->squareMeters!!}</strong></h3>
				@endif
				@if($property->condominium_fees > 0)
				<h3>@lang('web.details_condominium_fees'): <strong>{{$property->condominium_fees}}</strong></h3>
				@endif
				@if($property->is_old_town)
				<h3><strong>@lang('web.details_is_old_town')</strong></h3>
				@endif
				@if($property->typology->class != 'land')
				<h3>@lang('web.details_energetic_class'): <img src="/images/energetic_class/small_energetic_class_{{$property->energetic_class}}.png" class="classe_energetica" alt="classe_energetica"/></h3>
				@if($property->energetic_class_value > 0)
				<h3>@lang('web.details_energetic_class_value'): <strong>{{number_format($property->energetic_class_value, 0, '', '.')}} kWh/m<sup>2</sup>a</strong></h3>
				@endif
				@endif
			</div>
			<div class="column">
				<h3 class="little_title"><strong>@lang('web.details_car_int')</strong></h3>
				@if($property->hasHeating())
				<h3>@lang('web.details_type_heating'): <strong>{{$property->typeHeating->name}}</strong></h3>
				@endif
				@if($property->hasKitchen())
				<h3>@lang('web.details_type_kitchen'): <strong>{{$property->typeKitchen->name}}</strong></h3>
				@endif
				@if($property->is_elevator)
				<h3><strong>@lang('web.details_is_elevator')</strong></h3>
				@endif
				@if($property->is_furnished)
				<h3><strong>@lang('web.details_is_furnished')</strong></h3>
				@endif
			</div>
			<div class="column">
				<h3 class="little_title"><strong>@lang('web.details_car_est')</strong></h3>
				@if($property->hasBox())
				<h3>@lang('web.details_type_box'): <strong>{{$property->typeBox->name}}</strong></h3>
				@endif
				@if($property->hasExposure())
				<h3>@lang('web.details_type_exposure'): <strong>{{$property->typeExposure->name}}</strong></h3>
				@endif
				@if($property->hasGarden())
				<h3>@lang('web.details_type_garden'): <strong>{{$property->typeGarden->name}}</strong></h3>
				@if($property->garden_square_meters > 0)
				<h3>@lang('web.details_type_garden_value'): <strong>{{$property->garden_square_meters}}</strong></h3>
				@endif
				@endif
				@if($property->is_terrace)
				<h3><strong>@lang('web.details_is_terrace')</strong></h3>
				@endif
				@if($property->is_balcony)
				<h3><strong>@lang('web.details_is_balcony')</strong></h3>
				@endif
				@if($property->is_seafront)
				<h3><strong>@lang('web.details_is_seafront')</strong></h3>
				@endif
				@if($property->is_pool)
				<h3><strong>@lang('web.details_is_pool')</strong></h3>
				@endif
			</div>
		</div>
		<div class="contact row clearfix">
			<h2>Contatti</h2>
			<div class="column">
				<h3><strong>Studio B Immobiliare S.R.L.</strong></h3>
				<h3>Via Miranda 7C, 18012 Seborga (IM)</h3>
				<h3>@lang('web.request_form_phone'): +39 0184 22 38 44</h3>
				<h3>info@immobiliarestudiob.com</h3>
				<h3>www.immobiliarestudiob.com</h3>
			</div>
			<div class="column">
				<h3><strong>{{$property->user->name}} {{$property->user->surname}}</strong></h3>
				<h3>@lang('web.request_form_phone'): +39 {{$property->user->telephone}}</h3>
				<h3>{{$property->user->email}}</h3>
			</div>
		</div>
	</div>
	
	{{ Html::script('js/web/jQuery/jquery.min.js') }}
	<script type="text/javascript">
		function imgCut() {
			repeat = false;
			$(".imgCut").each(function(index, element) {
				var hSmall = $(this).height();
				var hBig = $(this).children("img").height();
				var hTop = -((hBig - hSmall) / 2);
				$(this).children("img").css("top", hTop);
				if (hTop > 0) repeat = true;
			});
			if (repeat) imgCut();
		}
	</script>
</body>
</html>