@extends('layouts.web')

@section('title', trans('web.title_contact'))

@section('content')
<section class="short-image no-padding contact-short-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color">@lang('web.contact_subtitle')</h5>
				<h1 class="second-color">@lang('web.contact_title')</h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>
</section>
<section class="section-light section-top-shadow top-padding-45">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 margin-top-45">
				<p class="negative-margin">@lang('web.contact_text')</p>
				<img src="/images/web/contact-image.jpg" alt="" class="pull-left margin-top-45" />
				<address class="contact-info pull-left">
					<span><i class="fa fa-map-marker"></i><a href="https://www.google.it/maps/place/Studio+B+Immobiliare+S.R.L./@43.825591,7.6907407,15.99z/data=!4m5!3m4!1s0x0:0xa759a6000529b184!8m2!3d43.8266211!4d7.6950659?hl=it" name="link_to_maps" target="_blank" rel="noopener">{{$realestate->address}}</a></span>
					<span><i class="fa fa-envelope"></i><a href="mailto::{{$realestate->email}}" name="send_email">{{$realestate->email}}</a></span>
					<span><i class="fa fa-phone"></i><a href="tel::{{$realestate->telephone}}" name="make_call_tel">{{$realestate->telephone}}</a></span>
					<span><i class="fa fa-mobile"></i><a href="tel::{{$realestate->cellular}}" name="make_call_cell">{{$realestate->cellular}}</a></span>
					<span><i class="fa fa-globe"></i><a href="http://immobiliarestudiob.com" name="homepage">http://immobiliarestudiob.com</a></span>
					<span><i class="fa fa-clock-o"></i>@lang('web.contact_clock_time_1')</span>	
					<span class="span-last">@lang('web.contact_clock_time_2')</span>
				</address>
			</div>
			<div class="col-xs-12 col-md-6 margin-top-45">				@include('partials.request_form')
			</div>
		</div>
	</div>
</section>

<section class="contact-map2" id="contact-map2">
</section>

<section class="section-light section-both-shadow top-padding-45">
	@include('partials.properties_promo')
	@include('partials.properties_recent')
</section>
@endsection

@push('scripts_mapInit')
mapInit(43.826621,7.695066,"contact-map2","/images/web/pin-contact.png", true);
@endpush