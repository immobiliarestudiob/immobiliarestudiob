@extends('layouts.web')

@section('title', trans('web.title_about'))

@section('content')
<section class="section-light section-top-shadow no-bottom-padding">
	<div class="container">
		<div class="details-title pull-left">
			<h1>@lang('web.about_title')<span class="special-color">.</span></h1>
			{{--<h3 class="title-negative-margin">@lang('web.about_title')<span class="special-color">.</span></h3>--}}
		</div>
		<div class="clearfix"></div>
		<div class="title-separator-primary"></div>
		<div class="row margin-top-60">
			<div class="col-xs-12 col-sm-6 col-lg-4">
				<img src="/images/web/about-image.png" alt="about" />
			</div>
			<div class="col-xs-12 col-sm-6 col-lg-8 about_text">
				<p class="negative-margin">@lang('web.about_text')</p>
			</div>
		</div>
	</div>
</section>
<section class="section-light">
	<div class="container">
		<div class="details-title pull-left">
			<h1>@lang('web.contact_title')<span class="special-color">.</span></h1>
		</div>
		<div class="clearfix"></div>
		<div class="title-separator-primary"></div>
		<div class="row margin-top-60">
			<div class="col-xs-12 col-sm-6">
				<address class="contact-info">
					<span><i class="fa fa-map-marker"></i><a href="https://www.google.it/maps/place/Studio+B+Immobiliare+S.R.L./@43.825591,7.6907407,15.99z/data=!4m5!3m4!1s0x0:0xa759a6000529b184!8m2!3d43.8266211!4d7.6950659?hl=it" name="link_to_maps" target="_blank" rel="noopener">{{$realestate->address}}</a></span>
					<span><i class="fa fa-envelope"></i><a href="mailto::{{$realestate->email}}" name="send_email">{{$realestate->email}}</a></span>
					<span><i class="fa fa-phone"></i><a href="tel::{{$realestate->telephone}}" name="make_call_tel">{{$realestate->telephone}}</a></span>
					<span><i class="fa fa-mobile"></i><a href="tel::{{$realestate->cellular}}" name="make_call_cell">{{$realestate->cellular}}</a></span>
					<span><i class="fa fa-globe"></i><a href="http://immobiliarestudiob.com" name="homepage">http://immobiliarestudiob.com</a></span>
					<span><i class="fa fa-clock-o"></i>@lang('web.contact_clock_time_1')</span>	
					<span class="span-last">@lang('web.contact_clock_time_2')</span>
				</address>
			</div>
			<div class="col-xs-12 col-sm-6">
				@include('partials.request_form')
			</div>
		</div>
	</div>
</section>

<section class="contact-map2" id="agency-map">
</section>

<section class="section-light top-padding-45">
	@include('partials.properties_promo')
	@include('partials.properties_last_insert')
</section>
@endsection

@push('scripts_mapInit')
mapInit(43.826621,7.695066,"agency-map","/images/web/pin-house.png", true);
@endpush