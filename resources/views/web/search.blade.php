@extends('layouts.web')

@section('title', 'Search')

@section('content')
<section class="adv-search-section no-padding">
	<div id="offers-map"></div>
	{{--@include('partials._search')--}}
</section>
<section class="section-light section-top-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<div class="sidebar-left">
					@include('partials._search_sidebar')
					@if(Auth::check() and !Auth::user()->hasPreferredSearch($search_parameters))
					<div class="sidebar-search-button-cont">
						<a href="{{route('web::user.add_search', Auth::user()->id)}}?{{http_build_query(request()->all())}}" class="button-secondary button-shadow button-full{{--button-alternative button-shadow button-full button-alternative-red--}}" name="save_search">
							<span>@lang('web.user_search_add')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-heart"></i></div>
						</a>
					</div>
					@endif
					<div class="sidebar-search-button-cont">
						<a href="{{route('web::search')}}" class="button-secondary button-shadow button-full{{--button-alternative button-shadow button-full--}}" name="reset_search">
							<span>@lang('web.search_btn_reset')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-eraser"></i></div>
						</a>
					</div>
					<div class="hidden-xs hidden-sm">	
						@php
						$tmp_promo_max = max(0, (ceil($properties->count()/3)-2)*4 );
						@endphp
						@if($tmp_promo_max > 0)
						@include('partials.sidebar_properties', ['max' => $tmp_promo_max])
						@endif
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-9">
				<div class="row">
					<div class="col-xs-12 col-lg-6">
						<h5 class="subtitle-margin">{{$title_search}}</h5>
						<h1>{{trans_choice('web.search_properties_found', $properties->total(), ['n' => $properties->total()])}}<span class="special-color">.</span></h1>
					</div>
					<div class="col-xs-12 col-lg-6">											
						{{--
						<div class="view-icons-container">
							<a class="view-box @if($view_grid) view-box-active @endif" @if(!$view_grid) href="{{route('web::search')}}?{{http_build_query(request()->except('view_grid'))}}&view_grid=1" @endif name="view_grid"><img src="/images/web/grid-icon.png" alt="" /></a>
							<a class="view-box @if(!$view_grid) view-box-active @endif" @if($view_grid) href="{{route('web::search')}}?{{http_build_query(request()->except('view_grid'))}}&view_grid=0" @endif name="view_list"><img src="/images/web/list-icon.png" alt="" /></a>
						</div>
						<div class="order-by-container">
							<select name="sort" class="bootstrap-select" title="@lang('web.search_orderby'):" onchange="location = this.options[this.selectedIndex].value;">
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=plh">@lang('web.search_oder_price_lh')</option>
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=phl">@lang('web.search_oder_price_hl')</option>
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=alh">@lang('web.search_oder_area_lh')</option>
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=ahl">@lang('web.search_oder_area_hl')</option>
							</select>
						</div>
						--}}
						<div class="order-by-container-2">
							<select name="sort_price" class="bootstrap-select" title="@lang('web.search_price')" onchange="location = this.options[this.selectedIndex].value;">
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=plh">@lang('web.search_asc')</option>
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=phl">@lang('web.search_desc')</option>
							</select>
							<select name="sort_area" class="bootstrap-select" title="@lang('web.search_sup')" onchange="location = this.options[this.selectedIndex].value;">
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=alh">@lang('web.search_asc')</option>
								<option value="{{route('web::search')}}?{{http_build_query(request()->except('order'))}}&order=ahl">@lang('web.search_desc')</option>
							</select>
						</div>
					</div>							
					<div class="col-xs-12">
						<div class="title-separator-primary"></div>
					</div>
				</div>
				@if($view_grid)
				<div class="row grid-offer-row">
					@foreach($properties as $tmp_property)
					<div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
						@include('partials.property_little_grid', ['property' => $tmp_property])
					</div>
					@endforeach
				</div>

				@if($properties->total() > 12)
				{{ $properties->appends(Input::except('page'))->links() }}
				@endif

				@else
				@include('partials.properties_row_list', ['properties' => $properties, 'hide_preferred' => true])
				@endif
			</div>
		</div>
	</div>
</section>
@endsection

@if ($properties_map->count() > 0)

@push('scripts_mapInit')
var locations = [
@foreach($properties_map as $property)
[{{\App\Models\Property::getLat($property->address_geocode)}}, {{\App\Models\Property::getLng($property->address_geocode)}}, "/images/web/pin-{{$property->typology->class}}.png", "{{route('web::details', $property->rifimm)}}", "{{$property->mainPhoto()->getMedium()}}", "{{trans_choice('web.details_subtitle', $property->is_to_sale, ['typology' => $property->typology->name])}} - @if($property->hasArea()) {{$property->area->name}}, @endif {{$property->town->name}} ({{$property->province->abbreviation}})", "{{ $property->publicPriceLittle }}"],
@endforeach
];
offersMapInit("offers-map",locations);
@endpush

@else

@push('scripts_mapInit')
new google.maps.Map(document.getElementById('offers-map'),{center: {lat: 43.8364617, lng: 7.7081539},zoom: 12});
@endpush

@endif

@push('styles')
<style type="text/css">
.order-by-container-2{float:right;}
.order-by-container-2 .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn):first-child{margin:0 15px}
.order-by-container-2 .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn){width:inherit}
.order-by-container-2 .bootstrap-select>.dropdown-toggle{padding-right:60px}
</style>
@endpush