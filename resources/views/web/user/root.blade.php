@extends('layouts.web')

@section('content')

<section class="short-image no-padding agency">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color">dashboard</h5>
				<h1 class="second-color">@yield('title')</h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>
</section>
<section class="section-light section-top-shadow top-padding-45 bottom-padding-45">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-3">
				<div class="sidebar-left">
					<h3 class="sidebar-title">{{$user->fullName}}<span class="special-color">.</span></h3>
					<div class="title-separator-primary"></div>
					<div class="center-button-cont margin-top-30">
						<a href="{{route('web::user.dashboard', [$user->id])}}" class="button-primary button-shadow button-full" name="dashboard">
							<span>@lang('web.user_menu_dashboard')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-tachometer"></i></div>
						</a>
					</div>
					<div class="center-button-cont margin-top-15">
						<a href="{{route('web::user.preferred', [$user->id])}}" class="button-primary button-shadow button-full" name="preferred">
							<span>@lang('web.user_menu_preferred')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-star-o"></i></div>
						</a>
					</div>
					<div class="center-button-cont margin-top-15">
						<a href="{{route('web::user.search', [$user->id])}}" class="button-primary button-shadow button-full" name="save_search">
							<span>@lang('web.user_menu_search')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-search"></i></div>
						</a>
					</div>
					<div class="center-button-cont margin-top-15">
						<a href="{{route('web::user.edit', [$user->id])}}" class="button-primary button-shadow button-full" name="edit">
							<span>@lang('web.user_menu_edit')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-user"></i></div>
						</a>
					</div>
					<div class="center-button-cont margin-top-15">
						<a href="{{route('auth::logout')}}" class="button-alternative button-shadow button-full" name="logout">
							<span>@lang('web.user_menu_logout')</span>
							<div class="button-triangle"></div>
							<div class="button-triangle2"></div>
							<div class="button-icon"><i class="fa fa-sign-out"></i></div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-9">
				<div class="row">
					<div class="col-xs-12">
						<h5 class="subtitle-margin">@yield('subtitle')</h5>
						<h1>@yield('title')<span class="special-color">.</span></h1>
						<div class="title-separator-primary"></div>
					</div>
				</div>
				@yield('content_user')
				<div class="row margin-top-60"></div>
			</div>
		</div>
	</div>
</section>
@endsection