@extends('web.user.root')

@section('title', trans('web.user_menu_preferred'))
@section('subtitle', trans('web.user_preferred_subtitle'))

@php
$menu_customer = 'preferred';
@endphp

@section('content_user')
@include('partials.properties_row_list', ['properties' => $preferred_properties])
@endsection