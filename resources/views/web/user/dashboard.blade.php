@extends('web.user.root')

@section('title', trans('web.user_menu_dashboard'))
@section('subtitle', trans('web.user_dashboard_subtitle'))

@php
$menu_customer = 'dashboard';
@endphp

@section('content_user')
<br />
<h3>{{trans_choice('web.user_dashboard_numberproperties', $recommended->total(), ['n' => $recommended->total()])}}</h3>

<div class="row grid-offer-row">
	@foreach($recommended as $tmp_property)
	<div class="col-xs-12 col-sm-6 col-lg-4 grid-offer-col">
		@include('partials.property_little_grid', ['property' => $tmp_property])
	</div>
	@endforeach
</div>

@if($recommended->total() > 12)
{{ $recommended->links() }}
@endif
@endsection