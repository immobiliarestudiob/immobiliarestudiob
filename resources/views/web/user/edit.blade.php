@extends('web.user.root')

@section('title', trans('web.user_menu_edit'))
@section('subtitle', trans('web.user_edit_subtitle'))

@php
$remove_flash_message = true;
@endphp

@section('content_user')
{{ Form::open(array('route' => array('web::user.update', $user->id), 'method' => 'put')) }}
<div class="row margin-top-60">
	@include('flash::message')
	{{--
	<div class="col-xs-6 col-xs-offset-3 col-sm-offset-0 col-sm-3 col-md-4">	
		<div class="agent-photos">
			<img src="/images/web/agent3.jpg" id="agent-profile-photo" class="img-responsive" alt="" />
			<div class="change-photo">
				<i class="fa fa-pencil fa-lg"></i>
				<input type="file" name="agent-photo" id="agent-photo" />
			</div>
			<input type="text" disabled="disabled" id="agent-file-name" class="main-input" />
		</div>
	</div>
	<div class="col-xs-12 col-sm-9 col-md-8">
	--}}
	<div class="col-xs-12">
		<div class="labelled-input">
			<label for="username">@lang('web.user_edit_username')</label><input id="username" name="username" type="text" class="input-full main-input" placeholder="@lang('web.user_edit_username')" value="{{$user->username}}"/>
			<div class="clearfix"></div>
		</div>
		<div class="labelled-input">
			<label for="name">@lang('web.user_edit_name')</label><input id="surnamnamee" name="name" type="text" class="input-full main-input" placeholder="@lang('web.user_edit_name')" value="{{$user->name}}"/>
			<div class="clearfix"></div>
		</div>
		<div class="labelled-input">
			<label for="surname">@lang('web.user_edit_surname')</label><input id="surname" name="surname" type="text" class="input-full main-input" placeholder="@lang('web.user_edit_surname')" value="{{$user->surname}}"/>
			<div class="clearfix"></div>
		</div>
		<div class="labelled-input">
			<label for="email">@lang('web.user_edit_email')</label><input id="email" name="email" type="email" class="input-full main-input" placeholder="@lang('web.user_edit_email')" value="{{$user->email}}"/>
			<div class="clearfix"></div>
		</div>
		<div class="labelled-input last">
			<label for="telephone">@lang('web.user_edit_telephone')</label><input id="telephone" name="telephone" type="tel" class="input-full main-input" placeholder="@lang('web.user_edit_telephone')" value="{{$user->telephone}}"/>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div class="row margin-top-30">
	<div class="col-xs-12">
		<div class="info-box">
			<p>@lang('web.user_edit_onlyforpassword')</p>
			<div class="small-triangle"></div>
			<div class="small-icon"><i class="fa fa-info fa-lg"></i></div>
		</div>
	</div>
</div>
<div class="row margin-top-15">
	<div class="col-xs-12 col-lg-6">
		<div class="labelled-input-short">
			<label for="password">@lang('web.user_edit_password')</label>
			<input id="password" name="password" type="password" class="input-full main-input" placeholder="" value=""/>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="col-xs-12 col-lg-6">
		<div class="labelled-input-short">
			<label for="password_confirmation">@lang('web.user_edit_confirm_password')</label>
			<input id="password_confirmation" name="password_confirmation" type="password" class="input-full main-input" placeholder="" value=""/>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<input type="submit" value="" class="hidden">
<div class="row margin-top-15">
	<div class="col-xs-12">
		<div class="center-button-cont center-button-cont-border">
			<span class="button-primary button-shadow" id="form-submit" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
				<span>@lang('web.user_edit_save')</span>
				<div class="button-triangle"></div>
				<div class="button-triangle2"></div>
				<div class="button-icon"><i class="fa fa-lg fa-floppy-o"></i></div>
			</span >
		</div>
	</div>
</div>
{{ Form::close() }}
@endsection