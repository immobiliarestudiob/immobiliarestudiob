@extends('web.user.root')

@section('title', trans('web.user_menu_search'))
@section('subtitle', trans('web.user_search_subtitle'))

@php
$menu_customer = 'search';
@endphp

@section('content_user')
<div class="row list-agency-row">
	<div class="col-xs-12">
		@foreach($searchs as $search)
		@include('partials.search_row_little', ['search' => $search])
		@endforeach
	</div>
</div>

@if($searchs->total() > 12)
{{ $searchs->links() }}
@endif
@endsection

@push('scripts_inside')
$(".small-icon")
.mouseover(function() { $(this).find("i").removeClass('fa-heart').addClass('fa-trash'); })
.mouseout(function() { $(this).find("i").removeClass('fa-trash').addClass('fa-heart'); })

$(".list-agency")
.mouseover(function() { $(this).find(".list-agency-description").removeClass('mySearch'); })
.mouseout(function() { $(this).find(".list-agency-description").addClass('mySearch'); })
@endpush

@push('styles')
<style type="text/css">
.small-icon:hover i{color:#a2292d !important}
.mySearch.list-agency-description{opacity:1}
.mySearch.list-agency-description, .mySearch.list-agency-description span, .mySearch.list-agency-description i{color:#898989}
</style>
@endpush