@extends('layouts.web')

@section('title', 'Reset Password')

@php
$menu_header = '';
$remove_flash_message = true;
$hide_menu_admin = true;
@endphp

@section('content')
<section class="short-image no-padding blog-short-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color"></h5>
				<h1 class="second-color">@lang('auth.forgot_password')<span class="special-color">?</span></h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>

</section>

<section class="section-light section-top-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
				
				@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
				@endif
				
				{{ Form::open(array('route' => 'auth::password_email', 'method' => 'post')) }}
				@include('flash::message')
				@if (count($errors) > 0)
				<div class="error-box">
					@foreach ($errors->all() as $error)
					<p>{{ $error }}</p>
					@endforeach
				</div>
				@endif
				{{ Form::text('email', old('username'), array('class' => 'input-full main-input', 'placeholder' => 'Email')) }}
				<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
					<span>Reset password</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-user"></i></div>
				</span>
				{{ Form::submit('submit', array('style' => 'display:none')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</section>
@endsection