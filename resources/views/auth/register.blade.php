@extends('layouts.web')

@section('title', trans('auth.register'))

@php
$menu_header = '';
$remove_flash_message = true;
$hide_menu_admin = true;
@endphp

@section('content')
<section class="short-image no-padding blog-short-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color"></h5>
				<h1 class="second-color">@lang('auth.register')<span class="special-color">.</span></h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>

</section>

<section class="section-light section-top-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
				@if (session('confirmation-success'))
				<div class="alert alert-success">
					{{ session('confirmation-success') }}
				</div>
				@else

					{{ Form::open(array('route' => 'auth::register_post', 'method' => 'post')) }}
					@include('flash::message')
					@if (count($errors) > 0)
					<div class="error-box">
						@foreach ($errors->all() as $error)
						<p>{{ $error }}</p>
						@endforeach
					</div>
					@endif
					{{ Form::text('username', old('username'), array('class' => 'input-full main-input', 'placeholder' => 'Username')) }}
					{{ Form::text('email', old('email'), array('class' => 'input-full main-input', 'placeholder' => 'Email')) }}
					{{ Form::password('password', array('class' => 'input-full main-input', 'placeholder' => 'Password')) }}
					{{ Form::password('password_confirmation', array('class' => 'input-full main-input', 'placeholder' => 'Repeat password')) }}
					<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
						<span>@lang('auth.register')</span>
						<div class="button-triangle"></div>
						<div class="button-triangle2"></div>
						<div class="button-icon"><i class="fa fa-user"></i></div>
					</span>
					{{ Form::submit('submit', array('style' => 'display:none')) }}
					{{ Form::close() }}

					<div class="clearfix"></div>

					<p class="login-or">OR</p>
					<a href="{{ url('/auth/facebook') }}" class="facebook-button" name="with_facebook">
						<i class="fa fa-facebook"></i>
						<span>Login with Facebook</span>
					</a>

					<p class="modal-bottom">@lang('auth.alread_registered')<a href="{{ route('auth::login_get') }}" class="login-link" name="login">Login</a></p>
				@endif
			</div>
		</div>
	</div>
</section>
@endsection