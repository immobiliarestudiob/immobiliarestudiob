@extends('layouts.web')

@section('title', 'Login')

@php
$menu_header = '';
$remove_flash_message = true;
$hide_menu_admin = true;
@endphp

@section('content')
<section class="short-image no-padding blog-short-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-12 short-image-title">
				<h5 class="subtitle-margin second-color"></h5>
				<h1 class="second-color">Login<span class="special-color">.</span></h1>
				<div class="short-title-separator"></div>
			</div>
		</div>
	</div>

</section>

<section class="section-light section-top-shadow">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

				@if (session('confirmation-success'))
				<div class="alert alert-success">
					{{ session('confirmation-success') }}
				</div>
				@endif
				@if (session('confirmation-danger'))
				<div class="alert alert-danger">
					{!! session('confirmation-danger') !!}
				</div>
				@endif

				@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
				@endif
				
				{{ Form::open(array('route' => 'auth::login_post', 'method' => 'post')) }}
				@include('flash::message')
				@if (count($errors) > 0)
				<div class="error-box">
					@foreach ($errors->all() as $error)
					<p>{{ $error }}</p>
					@endforeach
				</div>
				@endif
				{{ Form::text('login', old('login'), array('class' => 'input-full main-input', 'placeholder' => 'Username or Email', 'autofocus', 'required')) }}
				{{ Form::password('password', array('class' => 'input-full main-input', 'placeholder' => 'Password', 'required')) }}
				<span class="button-primary button-shadow button-full" style="cursor:pointer;" onclick="$(this).closest('form').submit()">
					<span>Login</span>
					<div class="button-triangle"></div>
					<div class="button-triangle2"></div>
					<div class="button-icon"><i class="fa fa-user"></i></div>
				</span>
				{{ Form::submit('submit', array('style' => 'display:none')) }}
				{{ Form::close() }}
				<a href="{{ route('auth::password_reset_get') }}" class="forgot-link pull-right" name="forgot_password">@lang('auth.forgot_password')?</a>
				<div class="clearfix"></div>

				<p class="login-or">OR</p>
				<a href="{{ url('/auth/facebook') }}" class="facebook-button" name="with_facebook">
					<i class="fa fa-facebook"></i>
					<span>Login with Facebook</span>
				</a>

				<p class="modal-bottom">@lang('auth.dont_have_account')<a href="{{ route('auth::register_get') }}" class="register-link" name="register">@lang('auth.register')</a></p>
			</div>
		</div>
	</div>
</section>
@endsection