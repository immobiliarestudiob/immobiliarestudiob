<!DOCTYPE html>
<html lang="it">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Area Riservata &bull; @yield('title')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	@include('partials._favicon')

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css">
	<!-- CSS -->
	{{ Html::style('css/admin.css') }}
	@stack('styles')

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
	@if (Auth::check())
	<div class="navbar navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target=".sidebar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="{{ route('admin::root') }}"><span class="glyphicon glyphicon-home"></span> Area Riservata</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-right:40px">
						<a href="{{ route('admin::user.notification', Auth::id()) }}">Notifiche 
							@if(Auth::user()->unreadNotifications->count() > 0)<span class="badge">{{Auth::user()->unreadNotifications->count()}}</span>@endif
						</a>
					</li>
					
					<li><a href="{{ route('web::root') }}"><span class="glyphicon glyphicon-eye-open"></span> Visualizza Sito</a></li>
					<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> {{ Auth::user()->name }} {{ Auth::user()->surname }}<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('admin::user.edit', [Auth::user()->id]) }}">Dati personali</a></li>
							<li class="divider"></li>
							<li><a href="{{ route('auth::logout') }}"><span class="glyphicon glyphicon-log-out"></span> Esci</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 .hidden-xs sidebar sidebar-collapse collapse">
				<ul class="nav nav-sidebar">
					<li class="{{ $menu == 'property' ? 'active' : '' }}"> <a href="{{ route('admin::property.index') }}">Immobili</a></li>
					<li class="{{ $menu == 'new_property' ? 'active' : '' }}"><a href="{{ route('admin::property.create') }}">Inserisci Immobile</a></li>
					<li class="{{ $menu == 'print_property' ? 'active' : '' }}"><a href="{{ route('admin::property.print') }}">Stampa Immobile</a></li>
					<li class="{{ $menu == 'trashed_property' ? 'active' : '' }}"><a href="{{ route('admin::property.trashed') }}">Visualizza Cestino</a></li>
					<li class="{{ $menu == 'showcase_property' ? 'active' : '' }}"><a href="{{ route('admin::property.showcase') }}">Gestisci Vetrina</a></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li class="{{ $menu == 'user' ? 'active' : '' }}"><a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#user_collapse">Utenti <b class="caret"></b></a></li>
					<li>
						<ul id="user_collapse" class="collapse nav nav-2">
							<li><a href="{{ route('admin::user.index') }}">Lista utenti</a></li>
							<li><a href="{{ route('admin::user.create') }}">Inserisci utente</a></li>
						</ul>
					</li>
					<li class="{{ $menu == 'something_type' ? 'active' : '' }}"><a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#car_collapse">Caratteristiche <b class="caret"></b></a></li>
					<li>
						<ul id="car_collapse" class="collapse nav nav-2">
							<li><a href="{{ route('admin::typology.index') }}">Tipologie</a></li>
							<li><a href="{{ route('admin::category.index') }}">Categorie</a></li>
							<li><a href="{{ route('admin::type_box.index') }}">Box</a></li>
							<li><a href="{{ route('admin::type_kitchen.index') }}">Cucina</a></li>
							<li><a href="{{ route('admin::type_distsm.index') }}">Distanza MareMonti</a></li>
							<li><a href="{{ route('admin::type_exposure.index') }}">Esposizione</a></li>
							<li><a href="{{ route('admin::type_floor.index') }}">Piani</a></li>
							<li><a href="{{ route('admin::type_garden.index') }}">Giardino</a></li>
							<li><a href="{{ route('admin::type_negotiation.index') }}">Negoziazione</a></li>
							<li><a href="{{ route('admin::type_occupation.index') }}">Occupazione</a></li>
							<li><a href="{{ route('admin::type_heating.index') }}">Riscaldamento</a></li>
							<li><a href="{{ route('admin::type_status.index') }}">Stato</a></li>
						</ul>
					</li>
					<li class="{{ $menu == 'something_where' ? 'active' : '' }}"><a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#luoghi_collapse">Luoghi <b class="caret"></b></a></li>
					<li>
						<ul id="luoghi_collapse" class="collapse nav nav-2">
							<li><a href="{{ route('admin::province.index') }}">Province</a></li>
							<li><a href="{{ route('admin::town.index') }}">Comuni</a></li>
							<li><a href="{{ route('admin::area.index') }}">Zone</a></li>
						</ul>
					</li>
					<li class="{{ $menu == 'something_date' ? 'active' : '' }}"><a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#dati_collapse">Dati Agenzia <b class="caret"></b></a></li>
					<li>
						<ul id="dati_collapse" class="collapse nav nav-2">
							<li><a href="{{ route('admin::realestate.edit', [1]) }}">Modifica Contatti</a></li>
						</ul>
					</li>
					<li class="{{ $menu == 'statistics' ? 'active' : '' }}"><a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#stat_collapse">Statistiche <b class="caret"></b></a></li>
					<li>
						<ul id="stat_collapse" class="collapse nav nav-2">
							<li><a href="{{ route('admin::dashboard.statistics') }}">Visualizzazioni sito web</a></li>
							<li><a href="{{ route('admin::property.statistics') }}">Immobili più cliccati</a></li>
							<li><a href="{{ route('admin::category.statistics') }}">Categorie più cliccati</a></li>
							<li><a href="{{ route('admin::typology.statistics') }}">Tipologie più cliccati</a></li>
							<li><a href="{{ route('admin::province.statistics') }}">Province più cliccati</a></li>
							<li><a href="{{ route('admin::town.statistics') }}">Comuni più cliccati</a></li>
							<li><a href="{{ route('admin::area.statistics') }}">Zone più cliccati</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">@yield('title')</h1>

				@include('flash::message')
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@yield('content')
			</div>
		</div>
	</div>
	@else
	@yield('content')
	@endif
	{{ Html::script('js/admin.js') }}
	@stack('scripts')

	<script>
		$('#flash-overlay-modal').modal();
	</script>

</body>
</html>