<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

{!! SEOMeta::generate() !!}

{!! OpenGraph::generate() !!}

	@include('partials._favicon')

	<!-- Bootstrap -->
	{{ Html::style('css/web/bootstrap.min.css') }}
	<!-- Font awesome styles -->
	{{ Html::style('fonts/apartment-font/css/font-awesome.min.css') }}
	<!-- Custom styles -->
	{{ Html::style('http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,500,500italic,700,700italic&amp;subset=latin,latin-ext') }}
	{{ Html::style('css/web/plugins.css') }}
	{{ Html::style('css/web/web-layout.css') }}
	{{ Html::style('css/web/web.css') }}
	@stack('styles')

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
{{--TODO
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-25472621-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-25472621-1');
</script>
--}}

<div class="loader-bg"></div>
<div id="wrapper">
	@include('partials._header')
	@if(!isset($remove_flash_message))
		@include('flash::message')
	@endif
	@yield('content')
	@include('partials._footer')
</div>

<!-- Move to top button -->
<div class="move-top">
	{{--<div class="big-triangle-second-color"></div>--}}
	<div class="big-icon-second-color"><i class="jfont fa-lg">&#xe803;</i></div>
</div>

@if(!isset($hide_menu_admin))
<!-- Modal -->
@include('partials.modal_login')
@include('partials.modal_register')
@include('partials.modal_forgotten_password')
@endif
<!-- jQuery  -->
{{ Html::script('js/web/jQuery/jquery.min.js') }}
{{ Html::script('js/web/jQuery/jquery-ui.min.js') }}
<!-- Bootstrap-->
{{ Html::script('js/web/bootstrap.min.js') }}
<!-- Google Maps -->
{{ Html::script('http://maps.googleapis.com/maps/api/js?key=AIzaSyD5MbyXjcQ8iT3LeuISr5S3gNf8TilIq9E&amp;sensor=false&amp;libraries=places') }}

<!-- plugins script -->
{{ Html::script('js/web/plugins.js') }}
<!-- template scripts -->
{{--{{ Html::script('mail/validate.js') }}--}}
{{ Html::script('js/web/apartment.js') }}
@stack('scripts_outside')

<!-- google maps initialization -->
<script type="text/javascript">
	$('#flash-overlay-modal').modal();

	google.maps.event.addDomListener(window, 'load', init);
	function init() {
		 {{-- TODO @stack('scripts_mapInit') --}}
	}
@stack('scripts_inside')
</script>

</body>
</html>