<?php

return [
	// Meta
	'meta_description' => 'StudioB Immobiliare S.R.L. si occupa con serietà e competenza delle trattative di intermediazione immobiliare assistendovi a partire dalla proposta d\'acquisto fino all\'atto notarile',
	//

	// General
	'reserved_negotiation' => 'Trattativa riservata',
	'reserved_negotiation_little' => 'Riservato',
	'public_price' => 'Prezzo :price',
	//

	'about_title' => 'Chi siamo',
	'about_subtitle' => '',
	'about_text' => '<p style="text-align:justify">Ci occupiamo con serietà e competenza delle trattative di intermediazione immobiliare relative a compravendita di appartamenti, immobili di pregio, ville, rustici e terreni.</p><p style="text-align:justify">Professionalità, trasparenza e impegno sono le caratteristiche che contraddistinguono i nostri servizi.</p><p style="text-align:justify">È nostra premura farvi sentire a proprio agio fornendovi assistenza completa a partire dalla proposta d\'acquisto fino all\'atto notarile, sia per chi compra sia per chi vende.</p><p> </p><p style="text-align:center"><strong>Il nostro obiettivo è la vostra soddisfazione</strong></p><p> </p><p style="text-align:center"><strong>Elisa Barelli</strong> e <strong>Loredana Barone</strong></p>',

	'contact_title' => 'Contattaci',
	'contact_subtitle' => '',
	'contact_text' => '<p>Per qualsiasi informazione e in qualsiasi momento puoi utilizzare i seguenti contatti per inviarci un messaggio. Saremo lieti di risponderti nel più breve tempo possibile.</p><p>bla bla bla</p>',
	'contact_clock_time_1' => 'lun-ven: 8:30 - 12:00',
	'contact_clock_time_2' => 'lun-ven: 14:30 - 18:00',

	'counter_apartments' => 'Appartamenti',
	'counter_houses' => 'Case',
	'counter_commercial' => 'Commerciali',
	'counter_land' => 'Terreni',

	'details_subtitle' => '{0}:Typology in affitto|{1}:Typology in vendita',
	'details_area' => 'Superficie',
	'details_rooms' => 'Locali',
	'details_bathrooms' => 'Bagni',
	'details_rifimm' => 'Riferimento',
	'details_price' => 'Prezzo',
	'details_total_floor' => 'Piani totali',
	'details_type_floor' => 'Piano',
	'details_type_heating' => 'Riscaldamento',
	'details_type_kitchen' => 'Cucina',
	'details_type_occupation' => 'Occupazione',
	'details_type_status' => 'Stato',
	'details_type_box' => 'Box',
	'details_distmm' => 'Distanza mare',
	'details_type_garden' => 'Giardino',
	'details_type_garden_value' => 'Superficie giardino',
	'details_type_exposure' => 'Espozione',
	'details_condominium_fees' => 'Spese condominiali',
	'details_print' => 'Stampa l\'immobile',
	'details_description' => 'Descrizione',
	'details_car' => 'Caratteristiche',
	'details_car_gen' => 'Caratteristiche generali',
	'details_car_int' => 'Caratteristiche interne',
	'details_car_est' => 'Caratteristiche esterne',
	//
	'details_is_elevator' => 'Ascensore',
	'details_is_terrace' => 'Terrazzo',
	'details_is_balcony' => 'Balcone',
	'details_is_act_of_commission' => 'Atto di provenienza',
	'details_is_floorplans' => 'Planimetrie',
	'details_is_furnished' => 'Arredato',
	'details_is_seafront' => 'Frontemare',
	'details_is_pool' => 'Piscina',
	'details_is_old_town' => 'Centro storico',
	//
	'details_energetic_class' => 'Classe energetica',
	'details_energetic_class_value' => 'Valore classe energetica',
	'details_contact_title' => 'Contattaci',
	'details_contact_agent' => 'incaricato',

	'footer_contact_us' => 'Contattaci',
	'footer_quick_links' => 'Collegamenti rapidi',
	'footer_choose_your_language' => 'Seleziona la tua lingua',
	'footer_newsletter' => 'Newsletter',
	'footer_newsletter_text' => 'Iscriviti alla nostra newsletter per rimanere aggiornato sulle novità.',
	'footer_enter_your_mail' => 'inserisci la tua mail',

	'header_search' => 'Immobili',
	'header_about' => 'Agenzia',
	//'header_contact_us' => 'Contattaci',
	'header_login' => 'Area Riservata',

	'properties_promo_title' => 'Vetrina immobili',
	//'properties_promo_subtitle' => 'una selezione dei nostri immobili',
	'properties_last_insert_title' => 'Ultimi inseriti',
	//'properties_last_insert_subtitle' => 'i nostri immobili più recenti',
	'properties_recent_title' => 'Visti di recente',
	//'properties_recent_subtitle' => 'gli utlimi immobili che hai visualizzato',
	'properties_similar_title' => 'Immobili simili',
	//'properties_similar_subtitle' => 'alcuni immobili simili a questo',

	'property_read_more' => 'visualizza',
	'property_price' => 'Prezzo',
	'property_added_preferred' => 'Immobile aggiunto ai preferiti',
	'property_removed_preferred' => 'Immobile rimosso dai preferiti',

	'request_message_sent' => 'Richiesta inviata con successo.',
	'request_form_name' => 'Nome e cognome',
	'request_form_phone' => 'Telefono',
	'request_form_email' => 'Email',
	'request_form_question' => 'Messaggio',
	'request_form_send' => 'Invia',

	'search_title' => 'Effettua una ricerca',
	'search_nav_apartments' => 'appartamenti',
	'search_nav_houses' => 'case',
	'search_nav_commercials' => 'commerciali',
	'search_nav_lands' => 'terreni',
	'search_typology' => 'Tipologia',
	'search_category' => 'Categoria',
	'search_province' => 'Provincia',
	'search_town' => 'Città',
	'search_area' => 'Zona',
	'search_price' => 'Prezzo',
	'search_sup' => 'Superficie',
	'search_asc' => 'Crescente',
	'search_desc' => 'Decrescente',
	'search_mq' => 'Metri quadri',
	'search_locals' => 'Locali',
	'search_room' => 'Camere',
	'search_bath' => 'Bagni',
	'search_btn_search' => 'cerca',
	'search_btn_reset' => 'azzera i parametri',

	'search_orderby' => 'Ordina per', //Order By
	'search_oder_price_lh' => 'Prezzo dal basso verso l\'alto', //Price low to high
	'search_oder_price_hl' => 'Prezzo dall\'alto vero il basso', //Price high to low
	'search_oder_area_lh' => 'Area dal basso verso l\'alto', //Area low to high
	'search_oder_area_hl' => 'Area dall\'alto vero il basso', //Area high to low
	'search_properties_title' => ':What a :where',
	'search_properties_found' => '{1}Un immobile trovato|[*]:n immobili trovati',

	'title_about' => 'Chi siamo',
	'title_contact' => 'Contattaci',
	'title_details' => 'Dettagli :rifimm',


	'user_menu' => 'Menu utente',
	'user_menu_dashboard' => 'Panoramica',
	'user_menu_preferred' => 'Immobili salvati',
	'user_menu_search' => 'Ricerche salvate',
	'user_menu_edit' => 'Profilo personale',
	'user_menu_logout' => 'Esci',
	'user_update' => 'Dati utente aggiornati.',

	'user_dashboard_subtitle' => 'breve riassunto sul tuo account',
	'user_dashboard_numberproperties' => '{0}Per il momento non abbiamo immobili da consigliarti, ci spiace.|{1}Abbiamo un immobile da consigliarti.|[2,*]Ci sono :n immobili consigliati per te.',
	'user_edit_subtitle' => 'modifica',
	'user_edit_username' => 'Username',
	'user_edit_name' => 'Nome',
	'user_edit_surname' => 'Cognome',
	'user_edit_email' => 'Email',
	'user_edit_telephone' => 'Telefono',
	'user_edit_onlyforpassword' => 'Compila questi campi solo se vuoi cambiare la tua password', //Fill this fields only if you want to change your password
	'user_edit_password' => 'Nuova password', //New Password
	'user_edit_confirm_password' => 'Ripeti password', //Repeat Password
	'user_edit_save' => 'salva',
	'user_preferred_subtitle' => 'le tue preferenze sugli immobili',
	'user_preferred_remove' => 'rimuovi dai preferiti', //remove from preferred
	'user_preferred_add' => 'aggiungi ai preferiti', //add to preferred
	'user_search_subtitle' => 'le tue preferenze sulle ricerche',
	'user_search_add' => 'Salva ricerca',
	'user_search_remove' => 'rimuovi dalle ricerche',
	'user_search_notown' => 'Nessuna città preferita',
	'user_search_notypology' => 'Nessuna tipologia preferita',
	'user_search_noprice' => 'Nessun prezzo preferito',
	'user_search_nosquaremeters' => 'Nessuna metratura preferita',
	'user_search_numberproperties' => '{0}Non ci sono immobili per questa ricerca|{1}È presente un immobile per questa ricerca|[2,*]Sono presenti :n immobili per questa ricerca',
	'property_search_added' => 'La ricerca è stata aggiunta alle ricerca salvate',
	'property_search_removed' => 'La ricerca è stata elimante dalle ricerche salvate',

	'where_we_are_text' => '<p style="text-align:center">La nostra agenzia si trova nell\'<strong>Antico Principato di Seborga</strong>, in Via A.Miranda 7C.</p><p style="text-align:center">Come arrivare a Seborga...</p><p style="text-align:center">Se siete al volante, è molto semplice: dopo aver lascito la strada principale (Via Aurelia) o l\'autostrada A10 (Nizza-Genova) uscita di Bordighera, seguite le indicazioni per Sasso e Seborga. I cartelli sono facilmente visibili.</p><p style="text-align:center">Se viaggiate in treno, dovete arrivare alla Stazione di Bordighera e usufruire del servizio autobus di linea Bordighera-Seborga</p><p style="text-align:center">Se arrivate in aereo, l\'aeroporto più vicino è quello di Nice Cote D\'Azur, prendere l\'autostrada direzione Italia, uscita di Bordighera. Seguite le indicazioni per Seborga.</p>',
];