<?php

return [

    'home' => 'home',
    'about' => 'agenzia',
    //'contact' => 'contatti',
    'search' => 'cerca',
    'details' => 'dettagli-{rifimm}',
    'print' => 'stampa-{rifimm}',

    'user_root' => 'utente/{user_id}',
    'user_dashboard' => 'utente/{user_id}/panoramica',
    'user_preferred' => 'utente/{user_id}/preferiti',
    'user_search' => 'utente/{user_id}/ricerche',
    'user_edit' => 'utente/{user_id}/modifica',

];