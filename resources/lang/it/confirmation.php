<?php

return [
    'email-title' => 'Verifica email',
    'email-intro'=> 'Per validare la tua email clicca sul bottone seguente',
    'email-button' => 'Verifica email',
    'message' => 'Grazie per esserti registrato! Ti preghiamo di controllare la tua email.',
    'success' => 'Hai completato la verifica del tuo account! Ora puoi effettuare il login.',
    'again' => 'Debi verificare la tua email prima di poter accedere al sito. ' .
                '<br>Se non hai ricevuto l\'email di conferma controlla la tua cartella dello spam.'.
                '<br>Per ricevere una nuovo email di conferma <a href="' . url('confirmation/resend') . '" class="alert-link">clicca qui</a>.',
    'resend' => 'Un messaggio di conferma è stato inviato. Ti preghiamo di controllare la tua email.'
];