<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Credenziali non corrispondenti ai dati registrati.',
    'throttle' => 'Troppi tentativi di accesso. Riprova tra :seconds secondi.',

	'forgot_password' => 'Password dimenticata', //Forgot your password?
	'forgot_text' => 'Inserisci la tua email personale.<br /> Ti manderemo il link per il reset della password.', //Instert your account email address.<br/>We will send you a link to reset your password.
	
	'register' => 'Registrati', //REGISTER

	'dont_have_account' => 'Non hai ancora un account? ', //Don't have an account?
	'alread_registered' => 'Sei già registrato? ', //Already registered?

	'logout_succesfull' => 'Utente disconnesso correttamente',

];