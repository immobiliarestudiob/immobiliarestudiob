<?php

return [

    'male' => [
        'store' => ':Class inserito con successo.',
        'update' => ':Class aggiornato con successo.',
        'delete' => ':Class rimosso con successo.',
        'restore' => ':Class ripristinato con successo.',
        'delete_def' => ':Class rimosso definitivamente con successo.',
    ],

    'female' => [
        'store' => ':Class inserita con successo.',
        'update' => ':Class aggiornata con successo.',
        'delete' => ':Class rimossa con successo.',
        'restore' => ':Class ripristinata con successo.',
        'delete_def' => ':Class rimossa definitivamente con successo.',
    ],

    'error' => [
        'store' => 'Errore durante l\'inserimento.',
        'update' => 'Errore durante l\'aggiornamento.',
        'delete' => 'Errore durante la rimozione.',
        'restore' => 'Errore durante il ripristino.',
        'delete_def' => 'Errore durante la rimozione definitiva.',
    ]

];