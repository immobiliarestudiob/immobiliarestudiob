<?php

return [

    'reset_text' => 'Hai ricevuto questa email perchè ci è arrivata una richiesta di reset password del tuo account.',
    'reset_action' => 'Resetta Password',
    'reset_ifnot' => 'Se non hai richiesto il reset della password, ignora questa email.',

];