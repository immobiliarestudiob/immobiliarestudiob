<?php

return [

    'reset_text' => 'Vous avez reçu cet e-mail, car nous avons reçu une demande de réinitialisation de mot de passe à partir de votre compte.',
    'reset_action' => 'Réinitialiser le mot',
    'reset_ifnot' => 'Si vous n\'avez pas demandé la réinitialisation du mot de passe, ignorez cet e-mail.',

];
