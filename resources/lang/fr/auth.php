<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Ces identifiants ne correspondent pas à nos enregistrements',
    'throttle' => 'Tentatives de connexion trop nombreuses. Veuillez essayer de nouveau dans :seconds secondes.',

    'forgot_password' => 'Mot de passe oublié',
	'forgot_text' => 'Insérez l\'adresse e-mail de votre compte.<br />Nous vous enverrons un lien pour réinitialiser votre mot de passe.',
	
	'register' => 'Inscription',

	'dont_have_account' => 'Vous n\'avez pas de compte? ',
	'alread_registered' => 'Déjà enregistré? ',

	'logout_succesfull' => 'Utilisateur déconnecté correctement'

];