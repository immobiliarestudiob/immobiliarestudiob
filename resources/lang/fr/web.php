<?php

return [

	'reserved_negotiation' => 'Négociation réservés',
	'public_price' => 'Prix :price',

	'property_read_more' => '',

	'header_about' => 'À propos de nous',
	'header_where_we_are' => 'Où nous sommes',
	'header_contact_us' => 'Contactez-nous',
	'header_search' => 'Propriété',

	'about_text' => '<p style="text-align:justify">Nous traitons avec fiabilit&egrave;&nbsp;&nbsp;et comp&eacute;tence les n&eacute;gociations li&eacute;es &agrave; la vente &nbsp;immobilier d&acute;appartements, propri&eacute;t&eacute;s de prestige, villas, chalets et terrains.</p><p style="text-align:justify">Professionnalisme, la transparence et l&acute;engagement sont les ma&icirc;tres mots de nos services.</p><p style="text-align:justify">Il nous fait plaisir de vous faire sentir &agrave; l&acute;aise en fournissant une assistance compl&egrave;te de la proposition jusqu&acute;&agrave;&nbsp;l&acute;acte d&acute;achat, &agrave; la fois pour les acheteurs et pour les vendeurs.</p><p>&nbsp;</p><p style="text-align:center"><strong>Notre objectif est votre satisfaction</strong></p><p>&nbsp;</p><p style="text-align:center"><strong>Elisa Barelli</strong> et <strong>Loredana Barone</strong></p>',
	
	'contact_text' => '<p>Pour toute information, &agrave; tout moment vous pouvez utiliser les contacts suivants pour nous envoyer un message. Nous serons heureux de r&eacute;pondre d&egrave;s que possible.</p>',
	
	'where_we_are_text' => '<p style="text-align:center">Notre agence se trouve dans la <strong>Principaut&eacute; de Seborga</strong>, en&nbsp;Via A.Miranda 7C.</p><p style="text-align:center">Comment arriver &agrave; Seborga ...</p><p style="text-align:center">Si vous arrivez en voiture, il&nbsp;est tr&egrave;s simple, sur&nbsp;la route principale (Via Aurelia) ou sur l&#39;autoroute&nbsp;A10 (Nice-G&ecirc;nes) , sortie Bordighera, suivre les indications pour Sasso et Seborga. Les cartels sont facilement visibles.</p><p style="text-align:center">Si vous voyagez en train, vous devez rejoindre la gare de Bordighera et profiter du service&nbsp;bus Seborga - Bordighera.</p><p style="text-align:center">Si vous arrivez&nbsp;par avion, l&#39;a&eacute;roport le plus proche est celui de Nice C&ocirc;te d&#39;Azur, prendre l&#39;autoroute vers l&#39;Italie , sortir&nbsp;a la sortie de Bordighera.</p>',
];