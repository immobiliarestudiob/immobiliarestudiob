<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

	'forgot_password' => 'Forgot your password',
	'forgot_text' => 'Instert your account email address.<br/>We will send you a link to reset your password.',
	
	'register' => 'Register',

	'dont_have_account' => 'Don\'t have an account? ',
	'alread_registered' => 'Already registered? ',

	'logout_succesfull' => 'User logged out correctly',

];
