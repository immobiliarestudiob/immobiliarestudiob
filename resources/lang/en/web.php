<?php

return [
	// Meta
	'meta_description' => 'StudioB Immobiliare S.R.L. si occupa con serietà e competenza delle trattative di intermediazione immobiliare assistendovi a partire dalla proposta d\'acquisto fino all\'atto notarile',
	//

	// General
    'reserved_negotiation' => 'Reserved negotiation',
    'reserved_negotiation_little' => 'Reserved',
    'public_price' => 'Price :price',
	//


	'property_read_more' => 'read more',

	'header_about' => 'About',
	'header_where_we_are' => 'Where we are',
	'header_contact_us' => 'Contact us',
	'header_search' => 'Properties',

	'about_text' => '<p style="text-align:justify">We deal with seriousness and competence of negotiations estate brokerage relating to the sale of apartments, prestigious properties, villas, cottages and land.</p><p style="text-align:justify">Professionalism, transparency and commitment are the hallmarks of our services.</p><p style="text-align:justify">It is our pleasure to make you feel at ease by providing full assistance from the proposal until the deed of purchase, both for buyers and for sellers.</p><p>&nbsp;</p><p style="text-align:center"><strong>Our goal is your satisfaction</strong></p><p>&nbsp;</p><p style="text-align:center"><strong>Elisa Barelli</strong> and <strong>Loredana Barone</strong></p>',
	'contact_text' => '<p>For any information, at any time you can use the following contacts to send us a message. We will be glad to respond as soon as possible.</p>',

	'where_we_are_text' => '<p style="text-align:center">Our agency is found in the <strong>Principality of Seborga</strong>, in Via A.Miranda 7C.</p><p style="text-align:center">How to get to Seborga ...</p><p style="text-align:center">If you arrive by car, it&nbsp;&nbsp;is very simple, on the&nbsp;main road (Via Aurelia) or on the highway&nbsp;A10 (Nice-Genova) exit Bordighera, follow signs for Sasso and Seborga. Cartels are easily visible.</p><p style="text-align:center">If you travel by train, you have to reach the train station of Bordighera and take advantage of the&nbsp;bus line&nbsp;Seborga - Bordighera</p><p style="text-align:center">If you arrive&nbsp;by plane, the nearest airport is Nice Cote D&#39;Azur, take the highway towards Italy , exit of Bordighera.</p>',
];