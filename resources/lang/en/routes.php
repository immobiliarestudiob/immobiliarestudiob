<?php

return [

    'home' => 'home',
    'about' => 'about',
    //'contact' => 'contact',
    'search' => 'search',
    'details' => 'details-{rifimm}',
    'print' => 'print-{rifimm}',

    'user_root' => 'user/{user_id}',
    'user_dashboard' => 'user/{user_id}/dashboard',
    'user_preferred' => 'user/{user_id}/preferred',
    'user_search' => 'user/{user_id}/search',
    'user_edit' => 'user/{user_id}/edit',

];