<?php

return [

    'reset_text' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_action' => 'Reset Password',
    'reset_ifnot' => 'If you did not request a password reset, no further action is required.',

];
