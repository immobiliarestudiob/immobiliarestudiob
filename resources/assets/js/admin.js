$(document).ready(function() {
    $(".fancybox").fancybox();
});
$(document).ready(function(){
	$('#search_btn').on('click', function () {
		searchRiferimento();
	});
});
$('#search_rif').keypress(function (e) {
	if (e.which == 13) {
		searchRiferimento();
		return false;
	}
});

function searchRiferimento() {
	var rif = $("#search_rif").val();
	rif = rif.toUpperCase().replace(/\s+/, "");
	if( $("#search_" + rif).length ) {
		$("html, body").animate({scrollTop:$("#search_" + rif).offset().top - 60}, "slow");
	} else if( $("#search_BE" + rif).length ) {
		$("html, body").animate({scrollTop:$("#search_BE" + rif).offset().top - 60}, "slow");
	} else if( $("#search_BL" + rif).length ) {
		$("html, body").animate({scrollTop:$("#search_BL" + rif).offset().top - 60}, "slow");
	} else {
		alert("Riferimento non trovato!");
	}
}

$(function() {
    $('#province_id').change(function() {
        $.ajax({
            url: '/province/' + $('#province_id').val() + '/towns',
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#town_id').empty()
            		.append($('<option></option>')
            		.attr('value', null)
            		.attr('selected', 'selected')
            		.text('Seleziona un Comune'));

                if (data.length == 0)
                	$('#town_id').append($('<option></option>')
                		.attr('value', null)
                		.text('Nessun Comune disponibile'));

            	for (var i = 0; i < data.length; i++)
                	$('#town_id').append($('<option></option>')
                		.attr('value', data[i].id)
                		.text(data[i].name));

                $('#area_id').empty()
            		.append($('<option></option>')
            		.attr('value', null)
            		.attr('selected', 'selected')
            		.text('Seleziona prima un Comune'));
            }
        });
    });
});

$(function() {
    $('#town_id').change(function() {
        $.ajax({
            url: '/town/' + $('#town_id').val() + '/areas',
            dataType: 'json',
            type: 'GET',
            success: function(data) {
                $('#area_id').empty()
                	.append($('<option></option>')
                	.attr('value', null)
                	.attr('selected', 'selected')
                	.text('Seleziona una Zona'));

                if (data.length == 0)
                	$('#area_id').append($('<option></option>')
                		.attr('value', null).
                		text('Nessuna Zona disponibile'));

            	for (var i = 0; i < data.length; i++)
                	$('#area_id').
                		append($('<option></option>')
                		.attr('value', data[i].id)
                		.text(data[i].name));
            }
        });
    });
});