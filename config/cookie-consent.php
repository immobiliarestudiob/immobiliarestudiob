<?php

return [

    /*
     * Use this setting to enable the cookie consent dialog.
     */
    'enabled' => env('COOKIE_CONSENT_ENABLED', true),

    /*
     * The name of the cookie in which we store if the user
     * has agreed to accept the conditions.
     */
    //'cookie_name' => 'immobiliarestudiob_cookie_consent',
    'cookie_name' => env(
        'COOKIE_CONSENT_NAME',
        str_slug(env('APP_NAME', 'laravel'), '_').'_cookie_consent'
    ),
];
