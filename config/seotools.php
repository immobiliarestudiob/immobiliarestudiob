<?php

return [
	'meta' => [
		'defaults' => [
			'title' => 'Studio B Immobiliare S.R.L',
			'description' => '',
			'separator' => ' - ',
			'keywords' => ['agenzia', 'immobiliare', 'seborga', 'liguria', 'imperia', 'bordighera', 'perinaldo', 'ventimiglia', 'airole', 'apricale', 'dolceacqua', 'camporosso', 'vendita', 'consulenza immobiliare', 'appartamenti', 'villa', 'rustico', 'terreno'],
			'canonical'=> null,
		],

		'webmaster_tags' => [
			'google' => null,
			'bing' => null,
			'alexa' => null,
			'pinterest' => null,
			'yandex' => null,
		],
	],
	'opengraph' => [
		'defaults' => [
			'title' => 'Studio B Immobiliare S.R.L.',
			'description' => '',
			'url' => null,
			'type' => 'website',
			'site_name' => 'Studio B Immobiliare S.R.L.',
			'images' => ['http://immobiliarestudiob.com/img/loghi/logo_600.png'], // TODO
		],
	]
];