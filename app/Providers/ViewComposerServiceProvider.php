<?php

namespace App\Providers;

use App\Models\Property;
use App\Models\Realestate;

use App\Models\Search;
use Artesaos\SEOTools\SEOMeta;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composeHomePropertiesPromo();
        $this->composeHomePropertiesSwiper();

        $this->composeHeader();
        $this->composeFooter();

        $this->composeSearch();
        $this->composeSearchSidebar();

        $this->composePropertiesPromo();
        $this->composePropertiesLastInsert();
        $this->composeSidebarProperties();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composeHomePropertiesPromo()
    {
        view()->composer('partials.home_properties_promo', function ($view) {
            $view->with('properties_promo', Property::visible()->showcase()->get());
        });
    }

    private function composeHomePropertiesSwiper()
    {
        view()->composer('partials.home_properties_swiper', function ($view) {
            $tmp_count = Property::visible()->showcase()->count();
            if ($tmp_count >= 3)
                $view->with('properties_swiper', Property::visible()->showcase()->get()->random(3));
            else
                $view->with('properties_swiper', Property::visible()->showcase()->get()->random($tmp_count));
        });
    }

    private function composeHeader()
    {
        view()->composer('partials._header', function ($view) {
            $view->with('realestate', Realestate::first());
        });
    }

    private function composeFooter()
    {
        view()->composer('partials._footer', function ($view) {
            $view->with('realestate', Realestate::first());
        });
    }

    private function composeSearch()
    {
	    view()->composer('partials._search', function ($view) {
	        $view->with('search_parameters_apartment', Search::getGlobalParameters('apartment'));
	        $view->with('search_parameters_house', Search::getGlobalParameters('house'));
	        $view->with('search_parameters_commercial', Search::getGlobalParameters('commercial'));
	        $view->with('search_parameters_land', Search::getGlobalParameters('land'));
        });
    }

    private function composeSearchSidebar()
    {
        view()->composer('partials._search_sidebar', function ($view) {
            if (!isset($view->search_parameters)) {
                $search = new Search();
                $view->with('search_parameters', $search->getSearchParameters());
            }
        });
    }

    private function composePropertiesPromo()
    {
        view()->composer('partials.properties_promo', function ($view) {
            $view->with('properties_promo', Property::visible()->showcase()->get());
        });
    }

    private function composePropertiesLastInsert()
    {
        view()->composer('partials.properties_last_insert', function ($view) {
            $view->with('properties_last_insert', Property::visible()->orderBy('updated_at', 'desc')->take(10)->get());
        });
    }

    private function composeSidebarProperties()
    {
        view()->composer('partials.sidebar_properties', function ($view) {
            //$max_disp = Property::visible()->showcase()->count();
            $max_fromview = isset($view->max) ? $view->max : 1;
            //$view->with('properties_promo', Property::visible()->showcase()->get()->random(min($max_fromview, $max_disp)));
            $view->with('properties_sidebar', Property::visible()->orderBy('created_at', 'desc')->take($max_fromview)->get());
        });
    }
}