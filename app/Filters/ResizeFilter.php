<?php
// TODO
namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ResizeFilter implements FilterInterface {

	const DEFAULT_SIZE = 600;

	private $width;
	private $height;

	public function __construct($width = null, $height = null) {
		$this->width = is_numeric($width) ? intval($width) : self::DEFAULT_SIZE;
		$this->height = is_numeric($height) ? intval($height) : null;
	}

	public function applyFilter(Image $image) {
		return $image->resize($this->width, $this->height, function ($constraint) {
			$constraint->aspectRatio();
		});
	}
}