<?php

namespace App\Repositories;

use App\Models\TypeHeating;

/**
 * Class TypeHeatingRepository
 * @package DummyNamespace
 */
class TypeHeatingRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeHeating::class;
    }
}
