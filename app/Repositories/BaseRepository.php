<?php

namespace App\Repositories;

use Czim\Repository\BaseRepository as CzimBaseRepository;

/**
 * Class BaseRepository
 * @package App\Repositories
 */
abstract class BaseRepository extends CzimBaseRepository
{
}