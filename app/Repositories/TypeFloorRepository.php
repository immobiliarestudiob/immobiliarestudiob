<?php

namespace App\Repositories;

use App\Models\TypeFloor;

/**
 * Class TypeFloorRepository
 * @package DummyNamespace
 */
class TypeFloorRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeFloor::class;
    }
}
