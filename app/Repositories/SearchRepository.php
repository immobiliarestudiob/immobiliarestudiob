<?php

namespace App\Repositories;

use App\Models\Search;

/**
 * Class SearchRepository
 * @package DummyNamespace
 */
class SearchRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Search::class;
    }
}
