<?php

namespace App\Repositories;

use App\Models\Realestate;

/**
 * Class RealestateRepository
 * @package DummyNamespace
 */
class RealestateRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Realestate::class;
    }
}
