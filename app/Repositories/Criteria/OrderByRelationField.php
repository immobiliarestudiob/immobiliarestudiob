<?php

namespace App\Repositories\Criteria;

use App\Constants;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Czim\Repository\Criteria\AbstractCriteria;

/**
 * Class OrderByRelationField
 * @package App\Repositories\Criteria
 */
class OrderByRelationField extends AbstractCriteria
{
    const DEFAULT_DIRECTION = 'asc';

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $relations;

    /**
     * @var string
     */
    protected $column;

    /**
     * @var string
     */
    protected $direction;

    /**
     * OrderByRelationField constructor.
     *
     * @param $table
     * @param $relations
     * @param $column
     * @param string $direction
     */
    public function __construct($table, $relations, $column, $direction = self::DEFAULT_DIRECTION)
    {
        $this->table = $table;
        $this->relations = $relations;
        $this->column = $column;
        $this->direction = $direction;
    }

    /**
     * @param Builder $model
     * @return mixed
     */
    public function applyToQuery($model)
    {
        $table = $this->table;
        $relations = $this->relations;
        foreach ($relations as $relation) {
            $relation_class = Constants::MODELS_DIRECTORY . $this->getModelName($relation);
            /** @var Model $relation_model */
            $relation_model = app($relation_class);
            $relation_table = $relation_model->getTable();
            $model = $model->leftJoin($relation_table, "{$table}.{$relation}_id", '=', "{$relation_table}.id");
            $table = $relation_table;
        }
        $model = $model->orderBy("{$table}.{$this->column}", $this->direction);
        return $model;
    }

    /**
     * Convert a snake_case string into a CamelCase one.
     *
     * @param $model_name
     * @return mixed
     */
    private function getModelName($model_name)
    {
        return str_replace('_', '', ucwords($model_name, '_'));
    }

}
