<?php

namespace App\Repositories;

use App\Models\Role;

/**
 * Class RoleRepository
 * @package DummyNamespace
 */
class RoleRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}
