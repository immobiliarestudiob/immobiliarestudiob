<?php

namespace App\Repositories;

use App\Models\Photo;

/**
 * Class PhotoRepository
 * @package DummyNamespace
 */
class PhotoRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Photo::class;
    }
}
