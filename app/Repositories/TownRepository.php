<?php

namespace App\Repositories;

use App\Models\Town;

/**
 * Class TownRepository
 * @package DummyNamespace
 */
class TownRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Town::class;
    }
}
