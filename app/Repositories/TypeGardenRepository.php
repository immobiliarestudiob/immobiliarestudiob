<?php

namespace App\Repositories;

use App\Models\TypeGarden;

/**
 * Class TypeGardenRepository
 * @package DummyNamespace
 */
class TypeGardenRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeGarden::class;
    }
}
