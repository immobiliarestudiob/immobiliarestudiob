<?php

namespace App\Repositories;

use App\Models\TypeStatus;

/**
 * Class TypeStatusRepository
 * @package DummyNamespace
 */
class TypeStatusRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeStatus::class;
    }
}
