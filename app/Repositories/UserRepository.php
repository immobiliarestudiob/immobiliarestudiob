<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Class UserRepository
 * @package DummyNamespace
 */
class UserRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}
