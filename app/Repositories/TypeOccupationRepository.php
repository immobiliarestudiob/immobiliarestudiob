<?php

namespace App\Repositories;

use App\Models\TypeOccupation;

/**
 * Class TypeOccupationRepository
 * @package DummyNamespace
 */
class TypeOccupationRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeOccupation::class;
    }
}
