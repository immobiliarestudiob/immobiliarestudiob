<?php

namespace App\Repositories;

use App\Models\Property;

/**
 * Class PropertyRepository
 * @package DummyNamespace
 */
class PropertyRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Property::class;
    }
}
