<?php

namespace App\Repositories;

use App\Models\TypeNegotiation;

/**
 * Class TypeNegotiationRepository
 * @package DummyNamespace
 */
class TypeNegotiationRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeNegotiation::class;
    }
}
