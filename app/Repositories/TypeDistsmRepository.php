<?php

namespace App\Repositories;

use App\Models\TypeDistsm;

/**
 * Class TypeDistsmRepository
 * @package DummyNamespace
 */
class TypeDistsmRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeDistsm::class;
    }
}
