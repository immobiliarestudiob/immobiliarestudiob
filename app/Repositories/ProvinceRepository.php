<?php

namespace App\Repositories;

use App\Models\Province;

/**
 * Class ProvinceRepository
 * @package DummyNamespace
 */
class ProvinceRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Province::class;
    }
}
