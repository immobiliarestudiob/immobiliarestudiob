<?php

namespace App\Repositories;

use App\Models\TypeExposure;

/**
 * Class TypeExposureRepository
 * @package DummyNamespace
 */
class TypeExposureRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeExposure::class;
    }
}
