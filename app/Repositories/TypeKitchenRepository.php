<?php

namespace App\Repositories;

use App\Models\TypeKitchen;

/**
 * Class TypeKitchenRepository
 * @package DummyNamespace
 */
class TypeKitchenRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeKitchen::class;
    }
}
