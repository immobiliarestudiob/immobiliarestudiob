<?php

namespace App\Repositories;

use App\Models\Area;

/**
 * Class AreaRepository
 * @package DummyNamespace
 */
class AreaRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Area::class;
    }
}
