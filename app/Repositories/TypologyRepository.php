<?php

namespace App\Repositories;

use App\Models\Typology;

/**
 * Class TypologyRepository
 * @package DummyNamespace
 */
class TypologyRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return Typology::class;
    }
}
