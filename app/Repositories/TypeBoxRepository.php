<?php

namespace App\Repositories;

use App\Models\TypeBox;

/**
 * Class TypeBoxRepository
 * @package DummyNamespace
 */
class TypeBoxRepository extends BaseRepository
{
    /**
     * Returns specified model class name.
     *
     * @return string
     */
    public function model()
    {
        return TypeBox::class;
    }
}
