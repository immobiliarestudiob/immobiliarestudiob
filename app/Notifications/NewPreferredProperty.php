<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewPreferredProperty extends Notification
{
    use Queueable;

    protected $rifimm;
    protected $user_id;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($rifimm, $user_id)
    {
        $this->rifimm = $rifimm;
        $this->user_id = $user_id;
        $this->message = 'Immobile '.$rifimm.' aggiunto ai preferiti.';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'rifimm' => $this->rifimm,
            'user_id' => $this->user_id,
            'message' => $this->message,
        ];
    }
}