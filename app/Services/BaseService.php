<?php

namespace App\Services;

use App\Constants;
use App\Models\User;
use ReflectionClass;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Czim\Repository\Criteria\Common\Scope;
use Czim\Repository\Criteria\Common\OrderBy;

/**
 * Class BaseService
 * @package App\Services
 */
class BaseService
{
    // Events
    const FIRE_EVENTS = false;
    const EVENT_BASE_NAMESPACE = '\App\Events\\';

    // Relation needle
    const RELATION_NEEDLE = Constants::RELATION_NEEDLE;

    /**
     * @var array
     */
    protected $events = [
        'create' => '',
        'update' => '',
        'delete' => '',
    ];

    /**
     * @var \App\Repositories\BaseRepository
     */
    protected $baseRepository;

    /**
     * BaseService constructor.
     * @throws \Throwable
     */
    public function __construct()
    {
        $this->baseRepository = app()->make('\App\Repositories\\'.$this->getRepositoryClassName());
        $this->registerEvents();
    }

    /**
     * Get entity name from the class name using ReflectionClass and regex.
     * CompanyService => Company
     *
     * @return string
     * @throws \ReflectionException
     */
    public function getEntityName()
    {
        $shortClassName = (new ReflectionClass($this))->getShortName();
        return preg_replace('/(Service){1}$/', '', $shortClassName);
    }

    /**
     * Guesses the service's repository name.
     * Override this method for "non standard" repository names.
     *
     * @return string
     * @throws \Throwable
     */
    protected function getRepositoryClassName(): string
    {
        return $this->getEntityName()."Repository";
    }

    /**
     * Associate event namespace for every event in $events property
     *
     * @throws \ReflectionException
     */
    protected function registerEvents()
    {
        if (static::FIRE_EVENTS) {
            $entityEventNamespacePart = str_plural($this->getEntityName());
            $events_namespace = static::EVENT_BASE_NAMESPACE.$entityEventNamespacePart."\\";
            array_walk($this->events, function (&$value, $key) use ($events_namespace) {
                $value = $events_namespace.studly_case($key.'_event');
            });
        }
    }

    /**
     * This method fire event registered in $events property
     *
     * @param string $eventName
     * @param $entityId
     */
    protected function fireEvent($eventName, $entityId)
    {
        if (!static::FIRE_EVENTS || !$this->additionalCheck($entityId)) {
            return;
        }
        event(new $this->events[$eventName]($entityId));
    }

    /**
     * Check additional parameters before fire an event
     * @param $entityId
     * @return bool
     */
    protected function additionalCheck($entityId)
    {
        return true;
    }

    /**
     * @param $data
     * @return User|null
     */
    public function store($data)
    {
        /** @var User $user */
        $user = $this->baseRepository->create($data);
        if (!is_null($user)) {
            $this->fireEvent('create', $user->id);
            return $user;
        }
        return null;
    }

    /**
     * @param array $data
     * @param $id
     * @return bool
     */
    public function update(array $data, int $id)
    {
        if ($this->baseRepository->update($data, $id)) {
            $this->fireEvent('update', $id);
            return true;
        }
        return false;
    }

    /**
     * Delete entity with given ID
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        if ($this->baseRepository->delete($id)) {
            $this->fireEvent('delete', $id);
            return true;
        }
        return false;
    }

    /**
     * Extract from an array the elements defined in another array of keys.
     *
     * @param array $data
     * @param array $keys
     * @return array
     */
    public function only(array $data, array $keys)
    {
        return array_filter($data, function ($key) use ($keys) {
            return in_array($key, $keys);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Find all records
     *
     * @param array $columns
     * @return mixed
     * @throws \Throwable
     */
    public function all(array $columns = ['*'])
    {
        return $this->baseRepository->all($columns);
    }

    /**
     * @param $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Throwable
     */
    public function find(int $id, array $columns = ['*'])
    {
        return $this->baseRepository->find($id, $columns);
    }

    /**
     * Finds a record using primary key or throws an exception
     *
     * @param $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Throwable
     */
    public function findOrFail($id, array $columns = ['*'])
    {
        return $this->baseRepository->findOrFail($id, $columns);
    }

    /**
     * Find a record by a given field
     *
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     * @throws \Throwable
     */
    public function findBy(string $attribute, $value, array $columns = ['*'])
    {
        return $this->baseRepository->findBy($attribute, $value, $columns);
    }

    /**
     * Push order on repository.
     *
     * @param $column
     * @param $direction
     * @param bool $once pushCriteriaOnce (true) or pushCriteria (false), default true
     * @throws \Throwable
     */
    public function pushOrder($column, $direction = 'asc', $once = true)
    {
        $scope = new Scope(new OrderBy($column, $direction));
        if ($once) {
            $this->baseRepository->pushCriteriaOnce($scope);
        } else {
            $this->baseRepository->pushCriteriaOnce($scope);
        }
    }

    /**
     * Include trashed entries for the next service method call
     *
     * @param bool $once Whether or not to include trashed only for the first
     *                   query or all of them.
     * @return $this
     * @throws \Throwable
     */
    public function withTrashed(bool $once = false)
    {
        $scope = new Scope('withTrashed');

        if ($once) {
            $this->baseRepository->pushCriteriaOnce($scope);
        } else {
            $this->baseRepository->pushCriteria($scope);
        }

        return $this;
    }

    /**
     * Creates a query builder instance and apply a given filter array.
     * If any of the given filters contains a predefined needle, it is a relationship field.
     * Handle relations from field values and build the query.
     *
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Throwable
     */
    public function filterQuery(array $filters): Builder
    {
        $query = $this->baseRepository->query();
        if (count($filters) > 0) {
            foreach ($filters as $field => $value) {
                if (strpos($field, self::RELATION_NEEDLE)) {
                    $this->handleQueryRelations($query, $field, $value);
                } else {
                    $this->buildQuery($query, $field, $value);
                }
            }
        }
        return $query;
    }

    /**
     * Function invoked if a field is relational only (i.e. 'user__email').
     * Get the first relation from field name (user) and set in variable $relation.
     * Get the second fragment of the original field (email) and set in variable $field.
     * Loop while $field has more relations.
     *
     * @param $query
     * @param $field
     * @param $value
     */
    private function handleQueryRelations(&$query, $field, $value)
    {
        $relations = [];
        do {
            $relation = strstr($field, self::RELATION_NEEDLE, true);
            $field = substr(strstr($field, self::RELATION_NEEDLE), strlen(self::RELATION_NEEDLE));
            array_push($relations, $relation);
        } while (strpos($field, self::RELATION_NEEDLE));
        $this->buildQueryRelations($query, $relations, $field, $value);
    }

    /**
     * Recursive function.
     * Use the first element of a relations array to build the query.
     * Unset the relation from the array and re-index it.
     * If the array still contains relations, recursively invoke the function to build the nested query sections.
     * Eventually, build the query with the resulting column-value pair.
     *
     * @param Builder $query
     * @param $relations
     * @param $column
     * @param $value
     */
    private function buildQueryRelations(&$query, $relations, $column, $value)
    {
        $query->whereHas($relations[0], function ($query) use ($relations, $column, $value) {
            unset($relations[0]);
            $relations = array_values($relations);
            if (count($relations) > 0) {
                $this->buildQueryRelations($query, $relations, $column, $value);
            } else {
                $this->buildQuery($query, $column, $value);
            }
        });
    }

    /**
     * Build the last part of the query.
     * Check if the field is a date one and build the date query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $field
     * @param $value
     */
    private function buildQuery(&$query, $field, $value)
    {
        if (is_date_field($field)) {
            // if the field is a "from" date field
            if (in_array($field, Constants::FROM_DATE_FIELDS)) {
                $operator = '>=';
                $value .= Constants::BEGINNING_OF_DAY;
            } else {
                $operator = '<=';
                $value .= Constants::END_OF_DAY;
            }
            if (in_array($field, Constants::CREATED_AT_FIELDS)) {
                $field = 'created_at';
            }
            $query->whereDate($field, $operator, date($value));
        } elseif (is_select_field($field)) {
            $query->where($field, '=', $value);
        } else {
            $query->where($field, 'like', "%{$value}%");
        }
    }

    /**
     * Filter and paginate.
     *
     * @param $filters
     * @param $relations
     * @param null|int $perPage
     * @param array $columns
     * @param string $pageName
     * @param null $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Throwable
     */
    public function filterPaginate(
        array $filters,
        array $relations = [],
        $perPage = Constants::PAGINATE_DEFAULT_PER_PAGE,
        $columns = ['*'],
        $pageName = 'page',
        $page = null
    ) {
        if (count($columns) == 1 && $columns[0] == '*') {
            $columns[0] = app($this->baseRepository->model())->getTable().'.*';
        }
        $filters = $this->sanitizeFilters($filters);
        $query = $this->filterQuery($filters);
        if (count($relations) > 0) {
            $query->with($relations);
        }
        return $query->paginate($perPage, $columns, $pageName, $page);
    }

    /**
     * Sanitize request inputs to keep needed fields only (the object empty ones).
     *
     * @param Model $model
     * @param $data
     * @return array $data
     */
    public function sanitizeData($model, $data): array
    {
        foreach ($data as $key => $value) {
            $attributes = $model->getAttributes();
            if (!empty($attributes[$key])) {
                unset($data[$key]);
            }
        }
        return $data;
    }

    /**
     * Filter search parameters returning an array of not null values.
     *
     * @param array $filters
     * @return array
     */
    private function sanitizeFilters($filters)
    {;
        return array_filter($filters, function ($value) {
            return !is_null($value);
        });
    }
}
