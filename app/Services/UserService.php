<?php

namespace App\Services;

use App\Models\User;

/**
 * Class UserService
 * @package App\Services
 */
class UserService extends BaseService
{
    public function store($data)
    {
        if (empty($data['username'])) {
            $data['username'] = "{$data['name']}_{$data['surname']}_" . rand(0, 99);
        }
        $data['password'] = bcrypt($data['password']);
        $data['confirmed '] = empty($data['confirmed']);

        return parent::store($data);
    }

    /**
     * @param array $data
     * @param int $id
     * @return bool
     */
    public function update(array $data, int $id)
    {
        if (empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }
        return parent::update($data, $id);
    }

    /**
     * @param $id
     * @return bool
     * @throws \Throwable
     */
    public function delete($id)
    {
        /** @var User $user */
        $user = $this->find($id);
        $user->preferredProperties()->detach();
        $user->search()->detach();

        return parent::delete($id);
    }
}