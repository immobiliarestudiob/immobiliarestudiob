<?php

namespace App\Console\Commands;

use App\Models\Property;
use App\Models\Photo;
use Illuminate\Console\Command;

class GeneratePositionImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:generateposition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->confirm('Sei sicuro di voler continuare? [y|N]')) {
            $this->line('Procedura iniziata, non interrompere!');
            
            //Position of photos
            $this->line('Position of photos -> start');
            $properties = Property::withTrashed()->get();
            $this->output->progressStart($properties->count());
            foreach ($properties as $property) {
                $photos = $property->photos;
                foreach ($photos as $index => $photo) {
                	$photo->position = $index;
                    $photo->save();
                }

                $this->output->progressAdvance();
            }
            $this->line('Position of photos -> finish');

            $this->output->progressFinish();
            $this->line('Operazione completata.');
        } else {
            $this->line('Operazione annullata.');
        }
    }
}
