<?php

namespace App\Console\Commands;

use App\Models\Area;
use App\Models\Category;
use App\Models\Photo;
use App\Models\Property;
use App\Models\Province;
use App\Models\Realestate;
use App\Models\Role;
use App\Models\Typology;
use App\Models\Town;
use App\Models\TypeBox;
use App\Models\TypeDistsm;
use App\Models\TypeExposure;
use App\Models\TypeFloor;
use App\Models\TypeGarden;
use App\Models\TypeHeating;
use App\Models\TypeKitchen;
use App\Models\TypeNegotiation;
use App\Models\TypeOccupation;
use App\Models\TypeStatus;
use App\Models\User;

use Illuminate\Console\Command;

class MigrateFromOldDatabase extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:oldtonew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script is used for migrate from older database to new, considering the old structure.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Exception
     */
    public function handle() {
        if ($this->confirm('Sei sicuro di voler continuare? [y|N]')) {
            $this->line('Procedura iniziata, non interrompere!');

            require_once(storage_path('db529532344.php'));
            $nrecords = count($agenti) + count($agenzia) + count($box) + count($categorie) + count($chi_siamo) + count($comuni) + count($contattaci) + count($cucina) + count($distmm) + count($dove_siamo) + count($esposizione) + count($foto) + count($foto_immobili) + count($giardino) + count($immobili) + count($occupazione) + count($piani) + count($province) + count($riscaldamento) + count($stato) + count($tipologie) + count($trattative) + count($utenti) + count($zone);
            $this->output->progressStart($nrecords);

            //Immobili to Properties
            $this->line('Immobili to Properties -> start');
            foreach ($immobili as $old) {
                $new = new Property;
                $new->rifimm = $old['rifimm'];
                $new->user_id = $old['idagente'];
                $new->category_id = $old['idcategoria'];
                $new->typology_id = $old['idtipologia'];
                $new->province_id = $old['idprovincia'];
                $new->town_id = $old['idcomune'];
                if ($old['idzona']!=1)
                    $new->area_id = $old['idzona'];
                $new->is_to_sale = ($old['contratto']==0) ? false : true;
                $new->price = $old['prezzo'];
                $new->type_negotiation_id = $old['idtrattativa'];
                $new->type_floor_id = ($old['idpiano']!=51) ? $old['idpiano'] : null;
                $new->total_floor = $old['pianitotali'];
                $new->room = $old['camere'];
                $new->bath = $old['bagni'];
                $new->locals = $old['locali'];
                $new->square_meters = $old['mq'];
                $new->description_ita = $old['descrizioneita'];
                $new->description_fra = $old['descrizionefra'];
                $new->description_eng = $old['descrizioneeng'];
                $new->type_heating_id = ($old['idriscaldamento']!=4) ? $old['idriscaldamento'] : null;
                $new->type_kitchen_id = ($old['idcucina']!=6) ? $old['idcucina'] : null;
                $new->type_occupation_id = ($old['idoccupazione']!=5) ? $old['idoccupazione'] : null;
                $new->type_status_id = ($old['idstato']!=5) ? $old['idstato'] : null;
                $new->type_box_id = ($old['idbox']!=5) ? $old['idbox'] : null;
                $new->type_distsm_id = ($old['iddistmm']!=12) ? $old['iddistmm'] : null;
                $new->type_garden_id = ($old['idgiardino']!=4) ? $old['idgiardino'] : null;
                $new->garden_square_meters = $old['mqgiardino'];
                $new->type_exposure_id = $old['idesposizione'];
                $new->condominium_fees = $old['spesecondominiali'];
                $new->is_elevator = ($old['ascensore']==0) ? false : true;
                $new->is_terrace = ($old['terrazza']==0) ? false : true;
                $new->is_balcony = ($old['balcone']==0) ? false : true;
                $new->is_act_of_commission = ($old['attodiprov']==0) ? false : true;
                $new->is_floorplans = ($old['planimetrie']==0) ? false : true;
                $new->is_furnished = ($old['arredato']==0) ? false : true;
                $new->is_seafront = ($old['frontemare']==0) ? false : true;
                $new->is_pool = ($old['piscina']==0) ? false : true;
                $new->views = $old['visite'];
                $new->is_showcase = ($old['vetrina']==0) ? false : true;
                $new->is_showcase_tv = ($old['vetrina_tv']==0) ? false : true;
                $new->is_special_price = ($old['offertaspec']==0) ? false : true;
                $new->is_visible = ($old['visibile']==0) ? false : true;
                $new->is_old_town = ($old['centro_storico']==0) ? false : true;
                $new->energetic_class = $old['classe_ener'];
                $new->energetic_class_value = $old['classe_ener_value'];
                $new->created_at = $old['dtinserita'];
                $new->save();
                if ($old['cancellato']!=0)
                    $new->delete();

                $this->output->progressAdvance();
            }
            $this->line('Immobili to Properties -> finish');

            //Foto to Photo
            $this->line('Foto to Photos -> start');
            foreach ($foto as $old) {
                $new = new Photo;
                $new->id = $old['idfoto'];
                $new->name = $old['percfoto'];
                $new->original_name = $old['percfoto'];
                $new->perc_photo = $old['percfoto'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Foto to Photos -> finish');

            //Foto_immobili to Photos
            $this->line('Foto_immobili to Photos -> start');
            $tmp_index = 0;
            foreach ($foto_immobili as $old) {
                if (Property::withTrashed()->where('rifimm', $old['rifimm'])->count() > 0
                    && Photo::where('id', $old['idfoto'])->count() > 0) {
                    $new = Photo::find($old['idfoto']);
                    $new->property_id = Property::withTrashed()->where('rifimm', $old['rifimm'])->first()->id;
                    $tmp_index++;
                    $new->perc_photo = 'properties/'.$old['rifimm'].'/'.$new->perc_photo;
                    $new->is_main = ($old['principale']==0) ? false : true;
                    $new->is_hidden = ($old['nascosta']==0) ? false : true;
                    $new->save();
                }

                $this->output->progressAdvance();
            }
            $this->line('Foto_immobili to Photos -> finish');

            //Agenti to Users
            $this->line('Agenti to Users -> start');
            foreach ($agenti as $old) {
                $new = new User;
                $new->id = $old['idagente'];
                $new->username = "tmp";
                $new->password = "tmp";
                $new->name = $old['nomeagente'];
                $new->surname = $old['cognagente'];
                $new->email = $old['emailagente'];
                $new->telephone = $old['telefonoagente'];
                $new->confirmed = true;
                $new->save();

                $this->output->progressAdvance();
            }
            foreach ($utenti as $old) {
                if (User::where('id', $old['idagente'])->count() > 0) {
                    $new = User::find($old['idagente']);
                    $new->username = $old['nomeutente'];
                    if ($old['nomeutente'] == "loredana")
                        $new->password = bcrypt("mauro");
                    elseif ($old['nomeutente'] == "elisa")
                        $new->password = bcrypt("andrea");
                    else
                        $new->password = bcrypt("admin");
                    $new->save();
                }
                $this->output->progressAdvance();
            }
            $this->line('Agenti to User -> finish');

            //Agenzia to RealEstates
            $this->line('Agenzia to RealEstates -> start');
            foreach ($agenzia as $old) {
                $new = new Realestate;
                $new->name = $old['nomeagenzia'];
                $new->address = $old['indirizzo'];
                $new->p_iva = $old['piva'];
                $new->telephone = $old['tel'];
                $new->cellular = $old['cell'];
                $new->fax = $old['fax'];
                $new->fax_2 = $old['fax2'];
                $new->email = $old['email'];
                $new->agenti_imm = $old['agenti_imm'];
                $new->rea = $old['rea'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Agenzia to RealEstates -> finish');

            //Box to Type_box
            $this->line('Box to Type_box -> start');
            foreach ($box as $old) {
                if ($old['tipoboxita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeBox;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tipoboxita'];
                $new->name_fra = $old['tipoboxfra'];
                $new->name_eng = $old['tipoboxeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Box to Type_box -> finish');

            //Categoria to Categories
            $this->line('Categoria to Categories -> start');
            foreach ($categorie as $old) {
                if ($old['nomecategoriaita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new Category;
                $new->id = $old['idcategoria'];
                $new->name_ita = $old['nomecategoriaita'];
                $new->name_fra = $old['nomecategoriafra'];
                $new->name_eng = $old['nomecategoriaeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Categoria to Categories -> finish');

            //Comuni to Town
            $this->line('Comuni to Town -> start');
            foreach ($comuni as $old) {
                if ($old['nomecomune'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new Town;
                $new->id = $old['idcomune'];
                $new->name = $old['nomecomune'];
                $new->province_id = $old['idprovincia'];
                $new->views = $old['visite'];
                $new->lat_long = $old['latlong'];
                if ($old['percfoto_com']) {
                    $new_photo = new Photo;
                    $new_photo->name = 'town_of_'.str_slug(strtolower($old['nomecomune']), '_').'.jpg';
                    $new_photo->original_name = $old['percfoto_com'];
                    $new_photo->perc_photo = 'towns/town_of_'.str_slug(strtolower($old['nomecomune']), '_').'.jpg';
                    $new_photo->save();
                    $new->photo_id = $new_photo->id;
                }
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Comuni to Town -> finish');

            //Cucina to Type_kitchens
            $this->line('Cucina to Type_kitchens -> start');
            foreach ($cucina as $old) {
                if ($old['tipocucinaita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeKitchen;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tipocucinaita'];
                $new->name_fra = $old['tipocucinafra'];
                $new->name_eng = $old['tipocucinaeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Cucina to Type_kitchens -> finish');

            //Distmm to Type_distsm
            $this->line('Distmm to Type_distsm -> start');
            foreach ($distmm as $old) {
                if ($old['tiposervizio'] == null)
                    continue;
                $new = new TypeDistsm;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tiposervizio'];
                $new->name_fra = $old['tiposervizio'];
                $new->name_eng = $old['tiposervizio'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Distmm to Type_distsm -> finish');

            //Esposizione to Type_exposures
            $this->line('Esposizione to Type_exposures -> start');
            foreach ($esposizione as $old) {
                if ($old['tipoesposizioneita'] == null)
                    continue;
                $new = new TypeExposure;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tipoesposizioneita'];
                $new->name_fra = $old['tipoesposizionefra'];
                $new->name_eng = $old['tipoesposizioneeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Esposizione to Type_exposures -> finish');

            //Giardino to Type_gardens
            $this->line('Giardino to Type_gardens -> start');
            foreach ($giardino as $old) {
                if ($old['tipogiardinoita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeGarden;
                $new->id = $old['idgiardino'];
                $new->name_ita = $old['tipogiardinoita'];
                $new->name_fra = $old['tipogiardinofra'];
                $new->name_eng = $old['tipogiardinoeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Giardino to Type_gardens -> finish');

            //Occupazione to Type_occupations
            $this->line('Occupazione to Type_occupations -> start');
            foreach ($occupazione as $old) {
                if ($old['tiposervizioita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeOccupation;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tiposervizioita'];
                $new->name_fra = $old['tiposerviziofra'];
                $new->name_eng = $old['tiposervizioeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Occupazione to Type_occupations -> finish');

            //Piani to Type_floors
            $this->line('Piani to Type_floors -> start');
            foreach ($piani as $old) {
                if ($old['nomepianoita'] == null)
                    continue;
                $new = new TypeFloor;
                $new->id = $old['idpiano'];
                $new->name_ita = $old['nomepianoita'];
                $new->name_fra = $old['nomepianofra'];
                $new->name_eng = $old['nomepianoeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Piani to Type_floors -> finish');

            //Province to Provinces
            $this->line('Province to Provinces -> start');
            foreach ($province as $old) {
                if ($old['cancellato'] != 0)
                    continue;
                $new = new Province;
                $new->id = $old['idprovincia'];
                $new->name = $old['nomeprovincia'];
                $new->abbreviation = $old['siglaprovincia'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Province to Provinces -> finish');

            //Riscaldamento to Type_heating
            $this->line('Riscaldamento to Type_heating -> start');
            foreach ($riscaldamento as $old) {
                if ($old['tiporiscaldamentoita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeHeating;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tiporiscaldamentoita'];
                $new->name_fra = $old['tiporiscaldamentofra'];
                $new->name_eng = $old['tiporiscaldamentoeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Riscaldamento to Type_heating -> finish');

            //Stato to Type_status
            $this->line('Stato to Type_status -> start');
            foreach ($stato as $old) {
                if ($old['tipostatoita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeStatus;
                $new->id = $old['idservizio'];
                $new->name_ita = $old['tipostatoita'];
                $new->name_fra = $old['tipostatofra'];
                $new->name_eng = $old['tipostatoeng'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Stato to Type_status -> finish');

            //Tipologie to Tipologies
            $this->line('Tipologie to Typologies -> start');
            foreach ($tipologie as $old) {
                if ($old['nometipologiaita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new Typology;
                $new->id = $old['idtipologia'];
                $new->name_ita = $old['nometipologiaita'];
                $new->name_fra = $old['nometipologiafra'];
                $new->name_eng = $old['nometipologiaeng'];
                $new->views = $old['visite'];
                $new->class = 'apartment';
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Tipologie to Typologies -> finish');

            //Trattative to Type_negotiation
            $this->line('Trattative to Type_negotiation -> start');
            foreach ($trattative as $old) {
                if ($old['nometrattativaita'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new TypeNegotiation;
                $new->id = $old['idtrattativa'];
                $new->name_ita = $old['nometrattativaita'];
                $new->name_fra = $old['nometrattativafra'];
                $new->name_eng = $old['nometrattativaeng'];
                $new->is_visible_price = true;
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Trattative to Type_negotiation -> finish');

            //Zone to Areas
            $this->line('Zone to Areas -> start');
            foreach ($zone as $old) {
                if ($old['nomezona'] == null
                    || $old['cancellato'] != 0)
                    continue;
                $new = new Area;
                $new->id = $old['idzona'];
                $new->name = $old['nomezona'];
                $new->town_id = $old['idcomune'];
                $new->save();

                $this->output->progressAdvance();
            }
            $this->line('Zone to Areas -> finish');

            $this->output->progressFinish();
            $this->line('Operazione completata.');
        } else {
            $this->line('Operazione annullata.');
        }
    }
}
