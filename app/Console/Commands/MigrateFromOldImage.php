<?php
namespace App\Console\Commands;

use App\Models\Photo;
use App\Models\Property;
use File;
use Image;

use Illuminate\Console\Command;

class MigrateFromOldImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image:oldtonew';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This script is used for migrate from old image folder to new, considering the old structure.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->confirm('Sei sicuro di voler continuare? [y|N]')) {
            $this->info('Procedura iniziata, non interrompere!');

            require_once(storage_path('db529532344.php'));

            if ($this->confirm('Iniziare procedura Immobili to Properties? [y|N]')) {
            	//Immobili check photo
	            $this->line('Immobili check photo -> start');
	            foreach ($foto as $old) {
	                if (!File::exists( storage_path('app/public/old/immobili/'.$old['percfoto']) ))
	                	$this->error( "Property photo: ".$old['percfoto']." not found!" );
	            }
	            $this->line('Immobili check photo -> finish');

                //Immobili to Properties
                $this->line('Immobili to Properties -> start');
                foreach ($foto as $old) {
                    /** @var Photo $photo */
                    $photo = Photo::findOrFail($old['idfoto']);
                    if (!is_null($photo->property_id)) {
                        if (File::exists( storage_path('app/public/old/immobili/'.$old['percfoto']) )) {
	                    	if (!File::exists( storage_path('app/public/images/properties/'.$photo->property->rifimm)))
	                    		File::makeDirectory(storage_path('app/public/images/properties/'.$photo->property->rifimm));
	                        $new_percphoto = 'app/public/images/properties/'.$photo->property->rifimm;
	                        do {
	                            $new_namephoto = $this->randomizeFileName($photo->property->rifimm);
	                        } while (File::exists( storage_path($new_percphoto.'/'.$new_namephoto) ));

                            $this->info( $old['percfoto']." start..." );
                        	$img = Image::make( storage_path('app/public/old/immobili/'.$old['percfoto']) );
                        	$img->save( storage_path($new_percphoto.'/'.$new_namephoto), 60);
                        	$photo->perc_photo = 'properties/'.$photo->property->rifimm.'/'.$new_namephoto;
                        	$photo->save();
                        	$img->destroy();
                        	File::delete( storage_path('app/public/old/immobili/'.$old['percfoto']) );
                            $this->info( $old['percfoto']." done!" );
                        } else {
                            $this->error( $old['percfoto']." not found!" );
                        }
                    } else {
                        $this->error( "Property for ".$old['percfoto']." (id:".$photo->property_id.") not found!" );
                    }
                }
                $this->line('Immobili to Properties -> finish');
            }

            if ($this->confirm('Iniziare procedura Comuni to Town? [y|N]')) {
            	//Comuni check photo
	            $this->line('Comuni check photo -> start');
	            foreach ($comuni as $old) {
	                if ($old['percfoto_com'] != null)
	                    if (!File::exists( storage_path('app/public/old/comuni/'.$old['percfoto_com']) ))
	                        $this->error( "Town photo: ".$old['percfoto_com']." not found!" );
	            }
	            $this->line('Comuni check photo -> finish');

                //Comuni to Town
                $this->info('Comuni to Town -> start');
                foreach ($comuni as $old) {
                    if ($old['percfoto_com'] != null)
                        if (File::exists( storage_path('app/public/old/comuni/'.$old['percfoto_com']) )) {
                            $this->info( $old['percfoto_com']." start..." );
                            $img = Image::make(storage_path('app/public/old/comuni/'.$old['percfoto_com']) );
                            $img->save(storage_path('app/public/images/towns/town_of_'.str_slug(strtolower($old['nomecomune']), '_').'.jpg'), 60);
                            $img->destroy();
                            File::delete( storage_path('app/public/old/comuni/'.$old['percfoto_com']) );
                            $this->info( $old['percfoto_com']." done!" );
                        } else {
                            $this->error( $old['percfoto_com']." not found!" );
                        }
                }
                $this->info('Comuni to Town -> finish');
            }

            $this->info('Operazione completata.');
        } else {
            $this->info('Operazione annullata.');
        }
    }

    public function randomizeFileName($rifimm) {
        return $rifimm . '_' . rand(100, 999) . '.jpg';
    }

}