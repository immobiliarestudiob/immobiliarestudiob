<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeBox
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeBox whereUpdatedAt($value)
 */
class TypeBox extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_box';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return mixed
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}