<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeNegotiation
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property bool $is_visible_price
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereIsVisiblePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeNegotiation whereUpdatedAt($value)
 */
class TypeNegotiation extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_negotiations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng', 'is_visible_price'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['is_visible_price' => 'boolean'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}
