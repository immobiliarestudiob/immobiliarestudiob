<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeHeating
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeHeating whereUpdatedAt($value)
 */
class TypeHeating extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_heating';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}