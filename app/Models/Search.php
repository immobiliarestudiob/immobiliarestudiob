<?php

namespace App\Models;

use App\Models\Property;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Search
 *
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property int $typology_id
 * @property int $province_id
 * @property int $town_id
 * @property int $area_id
 * @property int $min_price
 * @property int $max_price
 * @property int $min_room
 * @property int $max_room
 * @property int $min_bath
 * @property int $max_bath
 * @property int $min_locals
 * @property int $max_locals
 * @property int $min_square_meters
 * @property int $max_square_meters
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Category $category
 * @property \App\Models\Typology $typology
 * @property \App\Models\Town $town
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMaxBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMaxLocals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMaxPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMaxRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMaxSquareMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMinBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMinLocals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMinRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereMinSquareMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereTownId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereTypologyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Search whereUserId($value)
 */
class Search extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'search';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id', 'typology_id',
        'province_id', 'town_id', 'area_id',
        'min_price', 'max_price',
        'min_room', 'max_room',
        'min_bath', 'max_bath',
        'min_locals', 'max_locals',
        'min_square_meters', 'max_square_meters'
    ];

    /**
     * Get the associated user
     * App\Models\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }

    /**
     * Get the associated category
     * App\Models\Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('\App\Models\Category');
    }

    /**
     * Get the associated typology
     * App\Models\Typology
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typology()
    {
        return $this->belongsTo('\App\Models\Typology');
    }

    /**
     * Get the associated town
     * App\Models\Town
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town()
    {
        return $this->belongsTo('\App\Models\Town');
    }

    /**
     * Get the properties that match the search
     *
     * @param bool $visible
     * @param null $except
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getMatchProperties($visible = true, $except = null)
    {
        $percentage = .2;

        $properties = Property::query();
        if ($visible) {
            $properties = $properties->visible();
        }

        if ($except != 'price') {
            if ($this->min_price != null) {
                $properties = $properties->where('price', '>=', $this->min_price - ($this->min_price * $percentage));
            }
            if ($this->max_price != null) {
                $properties = $properties->where('price', '<=', $this->max_price + ($this->max_price * $percentage));
            }
        }

        if ($except != 'square_meters') {
            if ($this->min_square_meters != null) {
                $properties = $properties->where('square_meters', '>=', $this->min_square_meters - ($this->min_square_meters * $percentage));
            }
            if ($this->max_square_meters != null) {
                $properties = $properties->where('square_meters', '<=', $this->max_square_meters + ($this->max_square_meters * $percentage));
            }
        }

        if ($except != 'room') {
            if ($this->min_room != null) {
                $properties = $properties->where('room', '>=', $this->min_room);
            }
            if ($this->max_room != null) {
                $properties = $properties->where('room', '<=', $this->max_room);
            }
        }

        if ($except != 'bath') {
            if ($this->min_bath != null) {
                $properties = $properties->where('bath', '>=', $this->min_bath);
            }
            if ($this->max_bath != null) {
                $properties = $properties->where('bath', '<=', $this->max_bath);
            }
        }

        if ($except != 'locals') {
            if ($this->min_locals != null) {
                $properties = $properties->where('locals', '>=', $this->min_locals);
            }
            if ($this->max_locals != null) {
                $properties = $properties->where('locals', '<=', $this->max_locals);
            }
        }

        if ($this->category_id != null && $except != 'category_id') {
            $properties = $properties->where('category_id', $this->category_id);
        }

        if ($this->typology_id != null && $except != 'typology_id') {
            $properties = $properties->where('typology_id', $this->typology_id);
        }

        if ($this->town_id != null && $except != 'town_id') {
            $properties = $properties->where('town_id', $this->town_id);
        }

        return $properties;
    }

    /**
     * Get the formatted name
     *
     * @return string
     */
    public function getName()
    {
        $name = "";
        if ($this->typology_id != null) {
            $name .= $this->typology->name;
        }
        if ($this->category_id != null) {
            $name .= " ({$this->category->name})";
        }
        if ($this->town_id != null) {
            $name .= " {$this->town->name}";
        }
        return $name;
    }

    /**
     * Get the formatted title
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitle()
    {
        $title = '';
        if ($this->typology_id != null) {
            $title = $what = $this->typology->name;
        }
        if ($this->town_id != null) {
            $title = $where = "{$this->town->name} ({$this->town->province->abbreviation})";
        }
        if ($this->typology_id != null and $this->town_id != null) {
            $title = trans('web.search_properties_title', ['what' => $what, 'where' => $where]);
        }

        return $title;
    }

    /**
     * Get the formatted min price
     *
     * @return string
     */
    public function getMinPrice()
    {
        return $this->min_price != null ? number_format($this->min_price, 0, ',', '.') . " €" : '';
    }

    /**
     * Get the formatted max price
     *
     * @return string
     */
    public function getMaxPrice()
    {
        return $this->max_price != null ? number_format($this->max_price, 0, ',', '.') . " €" : '';
    }

    /**
     * Get the formatted min square meters
     *
     * @return string
     */
    public function getMinSquareMeters()
    {
        return $this->min_square_meters != null ? number_format($this->min_square_meters, 0, ',', '.') . " m<sup>2</sup>" : '';
    }

    /**
     * Get the formatted max square meters
     *
     * @return string
     */
    public function getMaxSquareMeters()
    {
        return $this->max_square_meters != null ? number_format($this->max_square_meters, 0, ',', '.') . " m<sup>2</sup>" : '';
    }

    /**
     * Get the formatted created at date
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at->format('d/m/Y');
    }

    /**
     * Get the formatted town name
     *
     * @return string
     */
    public function getTownName()
    {
        return $this->town_id != null ? $this->town->name : trans('web.user_search_notownname');
    }

    /**
     * Get the formatted typology name
     *
     * @return string
     */
    public function getTypologyName()
    {
        return $this->typology_id != null ? $this->typology->name : trans('web.user_search_notypology');
    }

    /**
     * Get the formatted category name
     *
     * @return string
     */
    public function getCategoryName()
    {
        return $this->category_id != null ? ' - ' . $this->category->name : '';
    }

    /**
     * Get the array of urls for the query build
     *
     * @return array
     */
    public function getParamasUrl()
    {
        return collect($this)->except(['id', 'user_id', 'updated_at', 'created_at'])->toArray();
    }

    /**
     * Get a formatted search array of the global search model with only min and max value
     *
     * @param null $class
     * @return mixed
     */
    private static function getGlobalMaxMinParameters($class = null)
    {
        $parameters = Property::query()
            ->select(\DB::raw('
            min(price) as min_price,
            max(price) as max_price,
            min(room) as min_room,
            max(room) as max_room,
            min(bath) as min_bath,
            max(bath) as max_bath,
            min(locals) as min_locals,
            max(locals) as max_locals,
            min(square_meters) as min_square_meters,
            max(square_meters) as max_square_meters
            '))
            ->visible();
        if ($class != null) {
            $parameters = $parameters->class($class);
        }
        $parameters = $parameters->get()->first();

        $parameters->min_price = round($parameters->min_price, -4, PHP_ROUND_HALF_DOWN);
        $parameters->max_price = round($parameters->max_price, -4, PHP_ROUND_HALF_UP);

        return $parameters;
    }

    /**
     * Get a formatted search array of the global search model
     *
     * @param null $class
     * @return array
     */
    public static function getGlobalParameters($class = null)
    {
        $parameters = Search::getGlobalMaxMinParameters($class);
        return [
            'category' => Category::toSearch(),
            'typology' => Typology::toSearch($class),
            'province' => Province::toSearch(),
            'town' => Town::toSearch(1),
            'price' => ['min' => $parameters->min_price, 'max' => $parameters->max_price],
            'room' => ['min' => $parameters->min_room, 'max' => $parameters->max_room],
            'bath' => ['min' => $parameters->min_bath, 'max' => $parameters->max_bath],
            'local' => ['min' => $parameters->min_locals, 'max' => $parameters->max_locals],
            'square_meters' => ['min' => $parameters->min_square_meters, 'max' => $parameters->max_square_meters]
        ];
    }

    /**
     * Get a formatted search array of this search model
     *
     * @return array
     */
    public function getSearchParameters()
    {
        $parameters = Search::getGlobalMaxMinParameters();
        return [
            'category' => ['id' => $this->category_id, 'search' => Category::toSearch()],
            'typology' => ['id' => $this->typology_id, 'search' => Typology::toSearch()],
            'province' => ['id' => $this->province_id, 'search' => Province::toSearch()],
            'town' => ['id' => $this->town_id, 'search' => Town::toSearch($this->province_id)],
            'area' => ['id' => $this->area_id, 'search' => Area::toSearch($this->town_id)],
            'price' => [
                'local' => ['min' => $this->min_price, 'max' => $this->max_price],
                'global' => ['min' => $parameters->min_price, 'max' => $parameters->max_price]
            ],
            'room' => [
                'local' => ['min' => $this->min_room, 'max' => $this->max_room],
                'global' => ['min' => $parameters->min_room, 'max' => $parameters->max_room]
            ],
            'bath' => [
                'local' => ['min' => $this->min_bath, 'max' => $this->max_bath],
                'global' => ['min' => $parameters->min_bath, 'max' => $parameters->max_bath]
            ],
            'local' => [
                'local' => ['min' => $this->min_locals, 'max' => $this->max_locals],
                'global' => ['min' => $parameters->min_locals, 'max' => $parameters->max_locals]
            ],
            'square_meters' => [
                'local' => ['min' => $this->min_square_meters, 'max' => $this->max_square_meters],
                'global' => ['min' => $parameters->min_square_meters, 'max' => $parameters->max_square_meters]
            ]
        ];
    }

    /**
     * Check if another search is similar to this
     * The search parameters are taken by getSearchParameters method
     *
     * @param $search_params
     * @return bool
     */
    public function isSimilarSearch($search_params)
    {
        if ($search_params instanceof Search) {
            return count(array_diff_assoc($search_params->only($this->fillable), $this->only($this->fillable))) == 0;
        }

        if ($this->category_id != $search_params['category']['id']
            || $this->typology_id != $search_params['typology']['id']
            || $this->province_id != $search_params['province']['id']
            || $this->town_id != $search_params['town']['id']
            || $this->area_id != $search_params['area']['id']

            || $this->min_price != $search_params['price']['local']['min']
            || $this->max_price != $search_params['price']['local']['max']

            || $this->min_room != $search_params['room']['local']['min']
            || $this->max_room != $search_params['room']['local']['max']

            || $this->min_bath != $search_params['bath']['local']['min']
            || $this->max_bath != $search_params['bath']['local']['max']

            || $this->min_locals != $search_params['locals']['local']['min']
            || $this->max_locals != $search_params['locals']['local']['max']

            || $this->min_square_meters != $search_params['square_meters']['local']['min']
            || $this->max_square_meters != $search_params['square_meters']['local']['max']) {

            return false;
        }

        return true;
    }
}