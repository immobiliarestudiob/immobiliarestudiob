<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property int $views
 * @property int $upcategory_id
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \Illuminate\Database\Eloquent\Collection $research
 * @property \App\Models\Category $upCategory
 * @property \Illuminate\Database\Eloquent\Collection $subCategories
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpcategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereViews($value)
 */
class Category extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ita',
        'name_fra',
        'name_eng',
        'views',
        'upcategory_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Get the associated properties
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated search
     * App\Models\Search
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function research()
    {
        return $this->hasMany('\App\Models\Search');
    }

    /**
     * Get the parent category
     * Get the category that owns the area.
     * App\Models\Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function upCategory()
    {
        return $this->belongsTo('\App\Models\Category', 'upcategory_id');
    }

    /**
     * Get the children category
     * App\Models\Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subCategories()
    {
        return $this->hasMany('\App\Models\Category', 'upcategory_id');
    }

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }

    /**
     * Get the array key => value (id => name) used to fill the input select in form search
     *
     * @return \Illuminate\Support\Collection
     */
    public static function toSearch()
    {
        $areas = Category::query()
            ->has('properties', '>', 0)
            ->get(['id', 'name_ita', 'name_fra', 'name_eng']);

        return $areas->pluck('name', 'id');
    }
}