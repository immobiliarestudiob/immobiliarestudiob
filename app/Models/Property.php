<?php

namespace App\Models;

use Lang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Property
 *
 * @package App\Models
 * @property int $id
 * @property string $rifimm
 * @property int $user_id
 * @property int $category_id
 * @property int $typology_id
 * @property int $province_id
 * @property int $town_id
 * @property int $area_id
 * @property string $address
 * @property string $address_geocode
 * @property bool $address_visible
 * @property bool $is_to_sale
 * @property int $price
 * @property int $type_negotiation_id
 * @property int $type_floor_id
 * @property int $total_floor
 * @property int $room
 * @property int $bath
 * @property int $locals
 * @property int $square_meters
 * @property string $description_ita
 * @property string $description_fra
 * @property string $description_eng
 * @property int $type_heating_id
 * @property int $type_kitchen_id
 * @property int $type_occupation_id
 * @property int $type_status_id
 * @property int $type_box_id
 * @property int $type_distsm_id
 * @property int $type_garden_id
 * @property int $garden_square_meters
 * @property int $type_exposure_id
 * @property int $condominium_fees
 * @property bool $is_elevator
 * @property bool $is_terrace
 * @property bool $is_balcony
 * @property bool $is_act_of_commission
 * @property bool $is_floorplans
 * @property bool $is_furnished
 * @property bool $is_seafront
 * @property bool $is_pool
 * @property int $views
 * @property bool $is_showcase
 * @property bool $is_showcase_tv
 * @property bool $is_special_price
 * @property bool $is_visible
 * @property bool $is_old_town
 * @property int $energetic_class
 * @property int $energetic_class_value
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string|\Carbon\Carbon|null $deleted_at
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $preferredUsers
 * @property \App\Models\Category $category
 * @property \App\Models\Typology $typology
 * @property \App\Models\Province $province
 * @property \App\Models\Town $town
 * @property \App\Models\Area $area
 * @property \App\Models\TypeNegotiation $typeNegotiation
 * @property \App\Models\TypeFloor $typeFloor
 * @property \App\Models\TypeHeating $typeHeating
 * @property \App\Models\TypeKitchen $typeKitchen
 * @property \App\Models\TypeOccupation $typeOccupation
 * @property \App\Models\TypeStatus $typeStatus
 * @property \App\Models\TypeBox $typeBox
 * @property \App\Models\TypeDistsm $typeDistsm
 * @property \App\Models\TypeGarden $typeGarden
 * @property \App\Models\TypeExposure $typeExposure
 * @property \App\Models\Photo $photos
 * @property string priceEuro
 * @property string squareMeters
 * @property string gardenSquareMeters
 * @property string publicPrice
 * @property string publicPriceLittle
 * @property string shortDescription
 * @property string description
 * @property-read string $price_euro
 * @property-read string $public_price
 * @property-read string $public_price_little
 * @property-read string $short_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property class($typologyClass)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Property onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property showcase()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property visible()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property visibleOnMap()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Property withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Property withoutTrashed()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereAddressGeocode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereAddressVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereAreaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereBath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereCondominiumFees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereDescriptionEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereDescriptionFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereDescriptionIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereEnergeticClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereEnergeticClassValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereGardenSquareMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsActOfCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsBalcony($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsElevator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsFloorplans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsFurnished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsOldTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsPool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsSeafront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsShowcase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsShowcaseTv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsSpecialPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsTerrace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsToSale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereIsVisible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereLocals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereRifimm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereRoom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereSquareMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTotalFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTownId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeBoxId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeDistsmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeExposureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeFloorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeGardenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeHeatingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeKitchenId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeNegotiationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeOccupationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypeStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereTypologyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Property whereViews($value)
 */
class Property extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rifimm',
        'user_id',
        'category_id',
        'typology_id',
        'province_id',
        'town_id',
        'area_id',
        'address',
        'address_geocode',
        'address_visible',
        'is_to_sale',
        'price',
        'type_negotiation_id',
        'type_floor_id',
        'total_floor',
        'room',
        'bath',
        'locals',
        'square_meters',
        'description_ita',
        'description_fra',
        'description_eng',
        'type_heating_id',
        'type_kitchen_id',
        'type_occupation_id',
        'type_status_id',
        'type_box_id',
        'type_distsm_id',
        'type_garden_id',
        'garden_square_meters',
        'type_exposure_id',
        'condominium_fees',
        'is_elevator',
        'is_terrace',
        'is_balcony',
        'is_act_of_commission',
        'is_floorplans',
        'is_furnished',
        'is_seafront',
        'is_pool',
        'views',
        'is_showcase',
        'is_showcase_tv',
        'is_special_price',
        'is_visible',
        'is_old_town',
        'energetic_class',
        'energetic_class_value'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'address_visible' => 'boolean',
        'is_to_sale' => 'boolean',
        'is_elevator' => 'boolean',
        'is_terrace' => 'boolean',
        'is_balcony' => 'boolean',
        'is_act_of_commission' => 'boolean',
        'is_floorplans' => 'boolean',
        'is_furnished' => 'boolean',
        'is_seafront' => 'boolean',
        'is_pool' => 'boolean',
        'is_showcase' => 'boolean',
        'is_showcase_tv' => 'boolean',
        'is_special_price' => 'boolean',
        'is_visible' => 'boolean',
        'is_old_town' => 'boolean',
    ];

    /**
     * Get the associated user
     * App\Models\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('\App\Models\User');
    }

    /**
     * Get the user that has this property on wishlist
     * App\Models\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function preferredUsers()
    {
        // TODO perchè usa with trashed?
        return $this->belongsToMany('App\Models\User', 'users_properties_preferred')->withTimestamps();
    }

    /**
     * Get the associated category
     * App\Models\Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('\App\Models\Category');
    }

    /**
     * Get the associated typology
     * App\Models\Typology
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typology()
    {
        return $this->belongsTo('\App\Models\Typology');
    }

    /**
     * Get the associated province
     * App\Models\Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo('\App\Models\Province');
    }

    /**
     * Get the associated town
     * App\Models\Town
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town()
    {
        return $this->belongsTo('\App\Models\Town');
    }

    /**
     * Get the associated area
     * App\Models\Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function area()
    {
        return $this->belongsTo('\App\Models\Area');
    }

    /**
     * Check if the property has an area
     *
     * @return bool
     */
    public function hasArea()
    {
        return $this->area()->count() > 0;
    }

    /**
     * Get the associated type negotiation
     * App\Models\TypeNegotiation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeNegotiation()
    {
        return $this->belongsTo('\App\Models\TypeNegotiation', 'type_negotiation_id');
    }

    /**
     * Check if the property has a type negotiation
     *
     * @return bool
     */
    public function hasNegotiation()
    {
        return $this->typeNegotiation()->count() > 0;
    }

    /**
     * Get the associated type floor
     * App\Models\TypeFloor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeFloor()
    {
        return $this->belongsTo('\App\Models\TypeFloor', 'type_floor_id');
    }

    /**
     * Check if the property has a type floor
     *
     * @return bool
     */
    public function hasFloor()
    {
        return $this->typeFloor()->count() > 0;
    }

    /**
     * Get the associated type heating
     * App\Models\TypeHeating
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeHeating()
    {
        return $this->belongsTo('\App\Models\TypeHeating', 'type_heating_id');
    }

    /**
     * Check if the property has a type heating
     *
     * @return bool
     */
    public function hasHeating()
    {
        return $this->typeHeating()->count() > 0;
    }

    /**
     * Get the associated type kitchen
     * App\Models\TypeKitchen
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeKitchen()
    {
        return $this->belongsTo('\App\Models\TypeKitchen', 'type_kitchen_id');
    }

    /**
     * Check if the property has a type kitchen
     *
     * @return bool
     */
    public function hasKitchen()
    {
        return $this->typeKitchen()->count() > 0;
    }

    /**
     * Get the associated type occupation
     * App\Models\TypeOccupation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeOccupation()
    {
        return $this->belongsTo('\App\Models\TypeOccupation', 'type_occupation_id');
    }

    /**
     * Check if the property has a type occupation
     *
     * @return bool
     */
    public function hasOccupation()
    {
        return $this->typeOccupation()->count() > 0;
    }

    /**
     * Get the associated type status
     * App\Models\TypeStatus
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeStatus()
    {
        return $this->belongsTo('\App\Models\TypeStatus', 'type_status_id');
    }

    /**
     * Check if the property has a type status
     *
     * @return bool
     */
    public function hasStatus()
    {
        return $this->typeStatus()->count() > 0;
    }

    /**
     * Get the associated type box
     * App\Models\TypeBox
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeBox()
    {
        return $this->belongsTo('\App\Models\TypeBox', 'type_box_id');
    }

    /**
     * Check if the property has a type box
     *
     * @return bool
     */
    public function hasBox()
    {
        return $this->typeBox()->count() > 0;
    }

    /**
     * Get the associated type distsm
     * App\Models\TypeDistsm
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeDistsm()
    {
        return $this->belongsTo('\App\Models\TypeDistsm', 'type_distsm_id');
    }

    /**
     * Check if the property has a type distsm
     *
     * @return bool
     */
    public function hasDistsm()
    {
        return $this->typeDistsm()->count() > 0;
    }

    /**
     * Get the associated type garden
     * App\Models\TypeGarden
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeGarden()
    {
        return $this->belongsTo('\App\Models\TypeGarden', 'type_garden_id');
    }

    /**
     * Check if the property has a type garden
     *
     * @return bool
     */
    public function hasGarden()
    {
        return $this->typeGarden()->count() > 0;
    }

    /**
     * Get the associated type exposure
     * App\Models\TypeExposure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeExposure()
    {
        return $this->belongsTo('\App\Models\TypeExposure', 'type_exposure_id');
    }

    /**
     * Check if the property has a type exposure
     *
     * @return bool
     */
    public function hasExposure()
    {
        return $this->typeExposure()->count() > 0;
    }

    /**
     * Get the associated photos
     * App\Models\Photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('\App\Models\Photo');
    }

    /**
     * Check if the property has a photo
     *
     * @return bool
     */
    public function hasPhotos()
    {
        return $this->photos()->count() > 0;
    }

    /**
     * Get the number of non planimetry photo
     * @return int
     */
    public function countNormalPhotos()
    {
        return $this->photos()->where('is_planimetry', false)->count();
    }

    /**
     * Get the associated photo non planimetry (and non main ?)
     * App\Models\Photo
     *
     * @param bool $use_main
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNormalPhotos($use_main = true)
    {
        $photos = $this->photos()->where('is_planimetry', false);
        if (!$use_main) {
            $photos = $photos->where('is_main', false);
        }
        return $photos->get();
    }

    /**
     * Get the main associated photo
     * App\Models\Photo
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    public function mainPhoto()
    {
        return $this->photos()->where('is_main', true)->first();
    }

    /**
     * Check if the property has a main photo
     * @return bool
     */
    public function hasMainPhoto()
    {
        return $this->photos()->where('is_main', true)->count() > 0;
    }

    /**
     * Set the property's rifimm
     *
     * @param string $value
     * @return void
     */
    public function setRifimmAttribute($value)
    {
        $this->attributes['rifimm'] = strtoupper($value);
    }

    /**
     * Get the euro formatted price
     *
     * @return string
     */
    public function getPriceEuroAttribute()
    {
        return number_format($this->attributes['price'], 0, ',', '.') . " €";
    }

    /**
     * Get the formatted square meters
     *
     * @return string
     */
    public function getSquareMetersAttribute()
    {
        return number_format($this->attributes['square_meters'], 0, ',', '.') . " m<sup>2</sup>";
    }

    /**
     * Get the formatted garden square meters
     *
     * @return string
     */
    public function getGardenSquareMetersAttribute($value)
    {
        if ($value > 0) {
            return number_format($value, 0, ',', '.') . "m<sup>2</sup>";
        }
        return '';
    }

    /**
     * Get the public formatted price
     *
     * @return string
     */
    public function getPublicPriceAttribute()
    {
        if (!$this->typeNegotiation->is_visible_price) {
            return trans('web.reserved_negotiation');
        }
        return trans('web.public_price', ['price' => $this->priceEuro]);
    }

    /**
     * Get the little public formatted price
     *
     * @return string
     */
    public function getPublicPriceLittleAttribute()
    {
        if (!$this->typeNegotiation->is_visible_price) {
            return trans('web.reserved_negotiation_little');
        }
        return $this->priceEuro;
    }

    /**
     * Get the short description
     *
     * @param int $max_char
     * @return string
     */
    public function getShortDescription($max_char = 150)
    {
        $shortDescription = strip_tags($this->description);

        if (strlen($shortDescription) > $max_char) {
            $shortDescription= substr($shortDescription, 0, $max_char);
            $last_space = strrpos($shortDescription, " ");
            $shortDescription = substr($shortDescription, 0, $last_space);
            return $shortDescription . "...";
        } else {
            return $shortDescription;
        }
    }

    /**
     * Get the short description
     *
     * @return string
     */
    public function getShortDescriptionAttribute()
    {
        return $this->getShortDescription();
    }

    /**
     * Get the correct translated description
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->description_ita;
            case 'fr':
                return $this->description_fra;
            case 'en':
            default:
                return $this->description_eng;
        }
    }

    public function similar()
    {
        $percentage = .2;
        return Property::visible()
            ->where('price', '>=', $this->price - ($this->price * $percentage))
            ->where('price', '<=', $this->price + ($this->price * $percentage))
            ->where('typology_id', $this->typology_id)
            ->where('rifimm', '!=', $this->rifimm)
            ->get();
    }

    /**
     * Scope a query to only include visible properties
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('is_visible', true);
    }

    /**
     * Scope a query to only include property with typology of type
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $typologyClass
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeClass($query, $typologyClass)
    {
        return $query->whereHas('typology', function ($q) use ($typologyClass) {
            /** @var \Illuminate\Database\Eloquent\Builder $q */
            $q->where('class', $typologyClass);
        });
    }

    /**
     * Scope a query to only include property in showcase
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShowcase($query)
    {
        return $query->where('is_showcase', true);
    }

    /**
     * Scope a query to only include property visible on map
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisibleOnMap($query)
    {
        return $query->whereNotNull('address_geocode')->where('address_visible', true);
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'properties';
    }

    /**
     * Get the latitude from a geolocation
     *
     * @param $geolocation
     * @return string
     */
    public static function getLat($geolocation): string
    {
        if ($geolocation == null) {
            return "";
        }

        $geolocation = str_replace("(", "", $geolocation);
        $geolocation = str_replace(")", "", $geolocation);
        $geolocation = str_replace(" ", "", $geolocation);
        $geo = explode(",", $geolocation);
        return $geo[0];
    }

    /**
     * Get the longitude from a geolocation
     *
     * @param $geolocation
     * @return string
     */
    public static function getLng($geolocation): string
    {
        if ($geolocation == null) {
            return "";
        }

        $geolocation = str_replace("(", "", $geolocation);
        $geolocation = str_replace(")", "", $geolocation);
        $geolocation = str_replace(" ", "", $geolocation);
        $geo = explode(",", $geolocation);
        return $geo[1];
    }

    /**
     * Get a collection with all the boolean value
     *
     * @return \Illuminate\Support\Collection
     */
    public function getChecked()
    {
        $checked = collect();

        $this->is_elevator ? $checked->push(trans('web.details_is_elevator')) : null;
        $this->is_terrace ? $checked->push(trans('web.details_is_terrace')) : null;
        $this->is_balcony ? $checked->push(trans('web.details_is_balcony')) : null;
        $this->is_act_of_commission ? $checked->push(trans('web.details_is_act_of_commission')) : null;
        $this->is_floorplans ? $checked->push(trans('web.details_is_floorplans')) : null;
        $this->is_furnished ? $checked->push(trans('web.details_is_furnished')) : null;
        $this->is_seafront ? $checked->push(trans('web.details_is_seafront')) : null;
        $this->is_pool ? $checked->push(trans('web.details_is_pool')) : null;
        $this->is_old_town ? $checked->push(trans('web.details_is_old_town')) : null;

        return $checked;
    }
}