<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App\Models
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $telephone
 * @property int $photo_id
 * @property bool $confirmed
 * @property string $provider
 * @property string $provider_id
 * @property string $confirmation_code
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \Illuminate\Database\Eloquent\Collection $preferredProperties
 * @property \Illuminate\Database\Eloquent\Collection $search
 * @property \Illuminate\Database\Eloquent\Collection $roles
 * @property \App\Models\Photo $photo
 * @property string $fullName
 * @property-read mixed|string $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @mixin \Eloquent
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereProvider($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 */
class User extends Authenticatable
{

    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'name', 'surname', 'email', 'telephone', 'photo_id', 'confirmed', 'confirmation_code', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'confirmed' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the associated properties make by this agent
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated properties on wishlist
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function preferredProperties()
    {
        return $this->belongsToMany('App\Models\Property', 'users_properties_preferred')->withTimestamps();
    }

    /**
     * Check if the property id is in the wislist of the user
     *
     * @param $property_id
     * @return bool
     */
    public function hasPreferred($property_id)
    {
        return $this->preferredProperties->where('id', '=', $property_id)->count() > 0;
    }

    /**
     * Get the associated research saved on wishlist
     * App\Models\Search
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function search()
    {
        return $this->hasMany('\App\Models\Search');
    }

    /**
     * Check if the search parameters are saved in the wislist of the user
     *
     * @param $search_params
     * @return bool
     */
    public function hasPreferredSearch($search_params)
    {
        foreach ($this->search as $search) {
            /** @var \App\Models\Search $search */
            if ($search->isSimilarSearch($search_params)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the associated roles
     * App\Models\Role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('\App\Models\Role');
    }

    /**
     * Check if the user has almost one of the roles
     *
     * @param $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return $this->roles()->whereIn('name', $roles)->count() > 0;
    }

    /**
     * Check if the user has the role
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->roles()->where('name', $role)->count() > 0;
    }

    /**
     * Get the associated photo
     * App\Models\Photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('\App\Models\Photo');
    }

    /**
     * Check if the user has a photo
     *
     * @return bool
     */
    public function hasPhoto()
    {
        return $this->photo()->count() > 0;
    }

    /**
     * Get the full name
     * @return mixed|string
     */
    public function getFullNameAttribute()
    {
        if ($this->name != null || $this->surname != null) {
            return "{$this->name} {$this->surname}";
        }

        return $this->username;
    }

    /**
     * Get the recommended properties based on search and other preferred properties
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getRecommended()
    {
        $rifimm_collection = collect();
        foreach ($this->preferredProperties()->visible()->get(['rifimm']) as $preferred) {
            foreach ($preferred->similar()->get(['rifimm']) as $rifimm) {
                $rifimm_collection->push($rifimm);
            }
        }

        foreach ($this->search as $search) {
            foreach ($search->getMatchProperties()->get(['rifimm']) as $rifimm) {
                $rifimm_collection->push($rifimm);
            }
        }

        $rifimm_collection = $rifimm_collection->unique();
        return Property::query()->whereIn('rifimm', $rifimm_collection);
    }

    /**
     * Get the user's first name.
     *
     * @param  string $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Get the user's surname.
     *
     * @param string $value
     * @return string
     */
    public function getSurnameAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Set the user's first name.
     *
     * @param string $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * Set the user's surname.
     *
     * @param string $value
     * @return void
     */
    public function setSurnameAttribute($value)
    {
        $this->attributes['surname'] = strtolower($value);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\ResetPasswordNotification($token));
    }
}