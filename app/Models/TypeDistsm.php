<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeDistsm
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeDistsm whereUpdatedAt($value)
 */
class TypeDistsm extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_distsm';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}