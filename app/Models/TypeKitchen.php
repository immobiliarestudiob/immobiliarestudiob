<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeKitchen
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeKitchen whereUpdatedAt($value)
 */
class TypeKitchen extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_kitchens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}