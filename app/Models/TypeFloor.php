<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeFloor
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeFloor whereUpdatedAt($value)
 */
class TypeFloor extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_floors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}