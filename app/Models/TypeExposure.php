<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeExposure
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeExposure whereUpdatedAt($value)
 */
class TypeExposure extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_exposures';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}