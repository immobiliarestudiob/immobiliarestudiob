<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Town
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property int $province_id
 * @property int $views
 * @property int $photo_id
 * @property string $lat_long
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \Illuminate\Database\Eloquent\Collection $research
 * @property \Illuminate\Database\Eloquent\Collection $areas
 * @property \App\Models\Province $province
 * @property \App\Models\Photo $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereLatLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereProvinceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Town whereViews($value)
 */
class Town extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'towns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'province_id', 'views', 'photo_id', 'lat_long'];

    /**
     * Get the associated properties
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated research
     * App\Models\Search
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function research()
    {
        return $this->hasMany('\App\Models\Search');
    }

    /**
     * Get the associated area
     * App\Models\Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function areas()
    {
        return $this->hasMany('\App\Models\Area');
    }

    /**
     * Get the associated province
     * App\Models\Province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo('\App\Models\Province');
    }

    /**
     * Get the associated photo
     * App\Models\Photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('\App\Models\Photo');
    }

    /**
     * Check if the town has a photo
     * @return bool
     */
    public function hasPhoto()
    {
        return $this->photo()->count() > 0;
    }

    /**
     * Get the array key => value (id => name) used to fill the input select in form search
     *
     * @param null $province_id
     * @return \Illuminate\Support\Collection
     */
    public static function toSearch($province_id = null)
    {
        $towns = Town::query();
        if ($province_id != null) {
            $towns = $towns->where('province_id', $province_id);
        }
        $towns = $towns
            ->has('properties', '>', 0)
            ->get(['id', 'name']);

        return $towns->pluck('name', 'id');
    }
}