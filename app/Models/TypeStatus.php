<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeStatus
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeStatus whereUpdatedAt($value)
 */
class TypeStatus extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}