<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Typology
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_en
 * @property int $uptypology_id
 * @property int $views
 * @property string $class
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \Illuminate\Database\Eloquent\Collection $research
 * @property \App\Models\Typology $upTypology
 * @property \Illuminate\Database\Eloquent\Collection $subTypology
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Typology[] $subTypologies
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology query()
 * @mixin \Eloquent
 * @property string|null $name_eng
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereUptypologyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Typology whereViews($value)
 */
class Typology extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'typologies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng', 'uptypology_id', 'views', 'class'];

    /**
     * Get the associated properties
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated research
     * App\Models\Search
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function research()
    {
        return $this->hasMany('\App\Models\Search');
    }

    /**
     * Get the father typology
     * App\Models\Typology
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function upTypology()
    {
        return $this->belongsTo('\App\Models\Typology', 'uptypology_id');
    }

    /**
     * Get the associated sub typologies
     * App\Models\Typology
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subTypologies()
    {
        return $this->hasMany('\App\Models\Typology', 'uptypology_id');
    }

    /**
     * Get the correct translated name
     *
     * @return mixed
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }

    /**
     * Get the correct class icon based on the typology class
     * @return string
     */
    public function getFontawasome()
    {
        switch ($this->class) {
            case 'apartment':
                return 'building';
            case 'commercial':
                return 'industry';
            case 'land':
                return 'tree';
            case 'house':
            default:
                return 'home';
        }
    }

    /**
     * Get the array key => value (id => name) used to fill the input select in form search
     *
     * @param $class
     * @return \Illuminate\Support\Collection
     */
    public static function toSearch($class = null)
    {
        $typologies = Typology::query();
        if ($class != null) {
            $typologies = $typologies->where('class', $class);
        }
        $typologies = $typologies
            ->has('properties', '>', 0)
            ->get(['id', 'name_ita', 'name_fra', 'name_eng']);

        return $typologies->pluck('name', 'id');
    }
}