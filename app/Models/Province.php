<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Province
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $abbreviation
 * @property int $views
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \Illuminate\Database\Eloquent\Collection $town
 * @property \Illuminate\Database\Eloquent\Collection $area
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Area[] $areas
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Town[] $towns
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereAbbreviation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Province whereViews($value)
 */
class Province extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'provinces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'abbreviation', 'views'];

    /**
     * Get the associated property
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated town
     * App\Models\Town
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function towns()
    {
        return $this->hasMany('\App\Models\Town');
    }

    /**
     * Get the associated area
     * App\Models\Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function areas()
    {
        return $this->hasManyThrough('\App\Models\Area', '\App\Models\Town');
    }

    /**
     * Get the array key => value (id => name) used to fill the input select in form search
     *
     * @return \Illuminate\Support\Collection
     */
    public static function toSearch()
    {
        $provinces = Province::query()
            ->has('properties', '>', 0)
            ->get(['id', 'name']);

        return $provinces->pluck('name', 'id');
    }
}