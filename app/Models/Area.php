<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property int $town_id
 * @property int $views
 * @property int $photo_id
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $properties
 * @property \App\Models\Town $town
 * @property \App\Models\Photo $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area wherePhotoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereTownId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Area whereViews($value)
 */
class Area extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'town_id',
        'views',
        'photo_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Get the associated properties
     * App\Models\Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany('\App\Models\Property');
    }

    /**
     * Get the associated town
     * App\Models\Town
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function town()
    {
        return $this->belongsTo('\App\Models\Town');
    }

    /**
     * Get the associated photo
     * App\Models\Photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('\App\Models\Photo');
    }

    /**
     * Check if the area has a photo
     *
     * @return bool
     */
    public function hasPhoto()
    {
        return $this->photo()->count() > 0;
    }

    /**
     * Get the array key => value (id => name) used to fill the input select in form search
     *
     * @param null $town_id
     * @return \Illuminate\Support\Collection
     */
    public static function toSearch($town_id = null)
    {
        $areas = Area::query();
        if ($town_id != null) {
            $areas = $areas->where('town_id', $town_id);
        }
        $areas = $areas
            ->has('properties', '>', 0)
            ->get(['id', 'name']);

        return $areas->pluck('name', 'id');
    }
}