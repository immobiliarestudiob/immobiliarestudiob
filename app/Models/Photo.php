<?php

namespace App\Models;

use File;
use Image;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property string $perc_photo
 * @property int $property_id
 * @property bool $is_planimetry
 * @property bool $is_main
 * @property bool $is_hidden
 * @property int $position
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Area $area
 * @property \App\Models\Town $town
 * @property \App\Models\Property $property
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo normalPhotos()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo planimetries()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereIsHidden($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereIsMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereIsPlanimetry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereOriginalName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePercPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo wherePropertyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Photo whereUpdatedAt($value)
 */
class Photo extends Model
{

    /**
     * The path of the default photo
     *
     * @var string
     */
    protected $path_default = '/default/photo_not_available.jpg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'original_name',
        'perc_photo',
        'property_id',
        'is_planimetry',
        'is_main',
        'is_hidden',
        'position'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_planimetry' => 'boolean',
        'is_main' => 'boolean',
        'is_hidden' => 'boolean',
    ];

    /**
     * Get the associated user
     * App\Models\User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('\App\Models\User');
    }

    /**
     * Get the associated area
     * App\Models\Area
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function area()
    {
        return $this->hasOne('\App\Models\Area');
    }

    /**
     * Get the associated town
     * App\Models\Town
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function town()
    {
        return $this->hasOne('\App\Models\Town');
    }

    /**
     * Get the associated properties
     * App\Models\Property
     *
     * @return mixed
     */
    public function property()
    {
        // TODO perchè usa with trashed?
        return $this->belongsTo('\App\Models\Property', 'property_id')->withTrashed();
    }

    /**
     * Scope a query to only include non planimetry photo
     *
     * @param $query
     * @return mixed
     */
    public function scopeNormalPhotos($query)
    {
        return $query->where('is_planimetry', false);
    }

    /**
     * Scope a query to only include planimetry photo
     *
     * @param $query
     * @return mixed
     */
    public function scopePlanimetries($query)
    {
        return $query->where('is_planimetry', true);
    }

    /**
     * Check if the photo has a user
     *
     * @return bool
     */
    public function hasUser()
    {
        return $this->user()->count() > 0;
    }

    /**
     * Check if the photo has an area
     *
     * @return bool
     */
    public function hasArea()
    {
        return $this->area()->count() > 0;
    }

    /**
     * Check if the photo has a town
     *
     * @return bool
     */
    public function hasTown()
    {
        return $this->town()->count() > 0;
    }

    /**
     * Check if the photo has a property
     *
     * @return bool
     */
    public function hasProperty()
    {
        return $this->property()->count() > 0;
    }

    /**
     * Check if the file exist
     *
     * @return mixed
     */
    private function fileExist()
    {
        return File::exists(storage_path('app/public/images/' . $this->perc_photo));
    }

    /**
     * Get the path path of the photo
     * If not exist use the default path
     *
     * @return string
     */
    private function getPath()
    {
        return $this->fileExist() ? $this->perc_photo : $this->path_default;
    }

    /**
     * Get the thumb path url
     *
     * @return string
     */
    public function getThumb()
    {
        return '/images/thumb/' . $this->getPath();
    }

    /**
     * Get the small path url
     *
     * @return string
     */
    public function getSmall()
    {
        return '/images/small/' . $this->getPath();
    }

    /**
     * Get the medium path url
     *
     * @return string
     */
    public function getMedium()
    {
        return '/images/medium/' . $this->getPath();
    }

    /**
     * Get the large path url
     *
     * @return string
     */
    public function getLarge()
    {
        return '/images/large/' . $this->getPath();
    }

    /**
     * Get the original size path url
     *
     * @return string
     */
    public function getOriginalSize()
    {
        return '/images/original/' . $this->getPath();
    }
}