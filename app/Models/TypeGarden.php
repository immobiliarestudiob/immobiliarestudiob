<?php

namespace App\Models;

use Lang;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeGarden
 *
 * @package App\Models
 * @property int $id
 * @property string $name_ita
 * @property string $name_fra
 * @property string $name_eng
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereNameEng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereNameFra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereNameIta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TypeGarden whereUpdatedAt($value)
 */
class TypeGarden extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'type_gardens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_ita', 'name_fra', 'name_eng'];

    /**
     * Get the correct translated name
     *
     * @return string
     */
    public function getNameAttribute()
    {
        $lang = Lang::getLocale();
        switch ($lang) {
            case 'it':
                return $this->name_ita;
            case 'fr':
                return $this->name_fra;
            case 'en':
            default:
                return $this->name_eng;
        }
    }
}