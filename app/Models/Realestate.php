<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Realestate
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $p_iva
 * @property string $telephone
 * @property string $telephone_2
 * @property string $cellular
 * @property string $cellular_2
 * @property string $fax
 * @property string $fax_2
 * @property string $email
 * @property string $agenti_imm
 * @property string $rea
 * @property string|\Carbon\Carbon|null $created_at
 * @property string|\Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereAgentiImm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereCellular($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereCellular2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereFax2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate wherePIva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereRea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereTelephone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Realestate whereUpdatedAt($value)
 */
class Realestate extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'realestates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'p_iva',
        'telephone',
        'telephone_2',
        'cellular',
        'cellular_2',
        'fax',
        'fax_2',
        'email',
        'agenti_imm',
        'rea'
    ];
}