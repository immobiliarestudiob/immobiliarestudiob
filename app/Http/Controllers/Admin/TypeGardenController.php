<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeGarden;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeGardenController extends Controller {

	public function index() {
		return view('admin.type.type_garden')
			->with('type_gardens', TypeGarden::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_garden = new TypeGarden;
		$type_garden->name_ita = $request->input('name_ita');
		$type_garden->name_fra = $request->input('name_fra');
		$type_garden->name_eng = $request->input('name_eng');
		$type_garden->save();

		flash()->success('Giardino creato con successo.');
		return redirect()->route('admin::type_garden.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_garden = TypeGarden::findOrFail($id);
		$type_garden->name_ita = $request->input('name_ita');
		$type_garden->name_fra = $request->input('name_fra');
		$type_garden->name_eng = $request->input('name_eng');
		$type_garden->save();

		flash()->success('Giardino aggiornato con successo.');
		return redirect()->route('admin::type_garden.index');
	}

	public function destroy($id) {
		$type_garden = TypeGarden::findOrFail($id);
		$type_garden->delete();

		flash()->success('Giardino rimosso con successo.');
		return redirect()->route('admin::type_garden.index');
	}
}