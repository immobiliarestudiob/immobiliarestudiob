<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeOccupation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeOccupationController extends Controller {

	public function index() {
		return view('admin.type.type_occupation')
			->with('type_occupations', TypeOccupation::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_occupation = new TypeOccupation;
		$type_occupation->name_ita = $request->input('name_ita');
		$type_occupation->name_fra = $request->input('name_fra');
		$type_occupation->name_eng = $request->input('name_eng');
		$type_occupation->save();

		flash()->success('Occupazione creata con successo.');
		return redirect()->route('admin::type_occupation.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_occupation = TypeOccupation::findOrFail($id);
		$type_occupation->name_ita = $request->input('name_ita');
		$type_occupation->name_fra = $request->input('name_fra');
		$type_occupation->name_eng = $request->input('name_eng');
		$type_occupation->save();

		flash()->success('Occupazione aggiorntata con successo.');
		return redirect()->route('admin::type_occupation.index');
	}

	public function destroy($id) {
		$type_occupation = TypeOccupation::findOrFail($id);
		$type_occupation->delete();

		flash()->success('Occupazione rimossa con successo.');
		return redirect()->route('admin::type_occupation.index');
	}
}