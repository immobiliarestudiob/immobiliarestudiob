<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeHeating;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeHeatingController extends Controller {

	public function index() {
		return view('admin.type.type_heating')
			->with('type_heatings', TypeHeating::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_heating = new TypeHeating;
		$type_heating->name_ita = $request->input('name_ita');
		$type_heating->name_fra = $request->input('name_fra');
		$type_heating->name_eng = $request->input('name_eng');
		$type_heating->save();

		flash()->success('Riscaldamento creato con successo.');
		return redirect()->route('admin::type_heating.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_heating = TypeHeating::findOrFail($id);
		$type_heating->name_ita = $request->input('name_ita');
		$type_heating->name_fra = $request->input('name_fra');
		$type_heating->name_eng = $request->input('name_eng');
		$type_heating->save();

		flash()->success('Riscaldamento aggiornato con successo.');
		return redirect()->route('admin::type_heating.index');
	}

	public function destroy($id) {
		$type_heating = TypeHeating::findOrFail($id);
		$type_heating->delete();

		flash()->success('Riscaldamento rimosso con successo.');
		return redirect()->route('admin::type_heating.index');
	}
}