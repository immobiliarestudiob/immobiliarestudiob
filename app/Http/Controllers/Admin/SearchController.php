<?php
namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Search;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class SearchController extends Controller {

	public function index($user_id) {
		return view('admin.search.index')
			->with('user', User::findOrFail($user_id));
	}

	public function show($user_id, $search_id) {
		$user = User::findOrFail($user_id);
		$search = User::findOrFail($user_id)->search->where('id', $search_id)->first();
		$search->save();
		return view('admin.search.show')
			->with('user', $user)
			->with('search', $search);
	}
}