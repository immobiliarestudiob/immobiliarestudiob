<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeStatus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeStatusController extends Controller {

	public function index() {
		return view('admin.type.type_status')
			->with('type_statuss', TypeStatus::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_status = new TypeStatus;
		$type_status->name_ita = $request->input('name_ita');
		$type_status->name_fra = $request->input('name_fra');
		$type_status->name_eng = $request->input('name_eng');
		$type_status->save();

		flash()->success('Stato creato con successo.');
		return redirect()->route('admin::type_status.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_status = TypeStatus::findOrFail($id);
		$type_status->name_ita = $request->input('name_ita');
		$type_status->name_fra = $request->input('name_fra');
		$type_status->name_eng = $request->input('name_eng');
		$type_status->save();

		flash()->success('Stato aggioranto con successo.');
		return redirect()->route('admin::type_status.index');
	}

	public function destroy($id) {
		$type_status = TypeStatus::findOrFail($id);
		$type_status->delete();

		flash()->success('Stato rimosso con successo.');
		return redirect()->route('admin::type_status.index');
	}
}