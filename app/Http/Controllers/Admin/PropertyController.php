<?php

namespace App\Http\Controllers\Admin;

use Response;
use App\Models\Area;
use App\Models\Town;
use App\Models\Role;
use App\Models\Photo;
use App\Http\Requests;
use App\Models\TypeBox;
use App\Models\Property;
use App\Models\Category;
use App\Models\Province;
use App\Models\Typology;
use App\Models\TypeFloor;
use App\Models\TypeStatus;
use App\Models\TypeDistsm;
use App\Models\TypeGarden;
use App\Models\TypeHeating;
use App\Models\TypeKitchen;
use App\Models\TypeExposure;
use Illuminate\Http\Request;
use App\Models\TypeOccupation;
use App\Models\TypeNegotiation;
use App\Services\PropertyService;
use App\Http\Controllers\Controller;

class PropertyController extends Controller
{

    /**
     * @var PropertyService
     */
    protected $propertyService;

    public function __construct(PropertyService $propertyService)
    {
        $this->propertyService = $propertyService;
    }

    public function index()
    {
        //return $this->propertyService->all();
        $properties = Property::orderBy('rifimm')->get();
        return view('admin.property.property_index')
            ->with('properties', $properties);
    }

    public function create()
    {
        $max_bl = Property::withTrashed()->where('rifimm', 'like', 'BL%')->orderBy('rifimm', 'desc')->first()->rifimm;
        if ($max_bl == null)
            $max_bl = '-- nessun BL presente --';

        $max_be = Property::withTrashed()->where('rifimm', 'like', 'BE%')->orderBy('rifimm', 'desc')->first()->rifimm;
        if ($max_be == null)
            $max_be = '-- nessun BE presente --';

        return view('admin.property.property_create', [
            'users_list' => Role::where('name', 'agent')->first()->users()->orderBy('users.name')->get()->pluck('name', 'id'),
            'areas_list' => Area::orderBy('name')->pluck('name', 'id'),
            'categories_list' => Category::orderBy('name_ita')->pluck('name_ita', 'id'),
            'provinces_list' => Province::orderBy('name')->pluck('name', 'id'),
            'typologies_list' => Typology::orderBy('name_ita')->pluck('name_ita', 'id'),
            'towns_list' => Town::orderBy('name')->pluck('name', 'id'),
            'type_boxs_list' => TypeBox::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_distsms_list' => TypeDistsm::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_exposures_list' => TypeExposure::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_floors_list' => TypeFloor::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_gardens_list' => TypeGarden::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_heatings_list' => TypeHeating::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_kitchens_list' => TypeKitchen::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_negotiations_list' => TypeNegotiation::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_occupations_list' => TypeOccupation::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_statuss_list' => TypeStatus::orderBy('name_ita')->pluck('name_ita', 'id'),
            'max_bl' => $max_bl,
            'max_be' => $max_be,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'rifimm' => 'required|unique:properties,rifimm',
            'user_id' => 'required|integer|exists:users,id',
            'category_id' => 'required|integer|exists:categories,id',
            'typology_id' => 'required|integer|exists:typologies,id',
            'province_id' => 'required|integer|exists:provinces,id',
            'town_id' => 'required|integer|exists:towns,id',
            'is_to_sale' => 'required|boolean',
            'address_visible' => 'boolean',
            'price' => 'required|integer',
            'type_negotiation_id' => 'required|integer|exists:type_negotiations,id',
            'type_floor_id' => 'integer|exists:type_floors,id',
            'total_floor' => 'integer',
            'room' => 'integer',
            'bath' => 'integer',
            'locals' => 'integer',
            'square_meters' => 'integer',
            'type_heating_id' => 'integer|exists:type_heating,id',
            'type_kitchen_id' => 'integer|exists:type_kitchens,id',
            'type_occupation_id' => 'integer|exists:type_occupations,id',
            'type_status_id' => 'integer|exists:type_status,id',
            'type_box_id' => 'integer|exists:type_box,id',
            'type_distsm_id' => 'integer|exists:type_distsm,id',
            'type_garden_id' => 'integer|exists:type_gardens,id',
            'garden_square_meters' => 'integer',
            'type_exposure_id' => 'integer|exists:type_exposures,id',
            'condominium_fees' => 'integer',
            'is_elevator' => 'boolean',
            'is_terrace' => 'boolean',
            'is_balcony' => 'boolean',
            'is_act_of_commission' => 'boolean',
            'is_floorplans' => 'boolean',
            'is_furnished' => 'boolean',
            'is_seafront' => 'boolean',
            'is_pool' => 'boolean',
            'views' => 'integer',
            'is_showcase' => 'boolean',
            'is_showcase_tv' => 'boolean',
            'is_special_price' => 'boolean',
            'is_visible' => 'boolean',
            'is_old_town' => 'boolean',
            'energetic_class_value' => 'integer',
        ]);

        $property = new Property;
        $property->rifimm = $request->rifimm;
        $property->user_id = $request->user_id;
        $property->category_id = $request->category_id;
        $property->typology_id = $request->typology_id;
        $property->province_id = $request->province_id;
        $property->town_id = $request->town_id;
        if ($request->area_id > 0)
            $property->area_id = $request->area_id;
        $property->address = $request->address;
        $property->address_geocode = $request->address_geocode;
        $property->address_visible = $request->address_visible;
        $property->is_to_sale = $request->is_to_sale;
        $property->price = $request->price;
        $property->type_negotiation_id = $request->type_negotiation_id;
        $property->type_floor_id = $request->type_floor_id;
        if ($request->total_floor > 0)
            $property->total_floor = $request->total_floor;
        if ($request->room > 0)
            $property->room = $request->room;
        if ($request->bath > 0)
            $property->bath = $request->bath;
        if ($request->locals > 0)
            $property->locals = $request->locals;
        if ($request->square_meters > 0)
            $property->square_meters = $request->square_meters;
        $property->description_ita = $request->description_ita;
        $property->description_fra = $request->description_fra;
        $property->description_eng = $request->description_eng;
        $property->type_heating_id = $request->type_heating_id;
        $property->type_kitchen_id = $request->type_kitchen_id;
        $property->type_occupation_id = $request->type_occupation_id;
        $property->type_status_id = $request->type_status_id;
        $property->type_box_id = $request->type_box_id;
        $property->type_distsm_id = $request->type_distsm_id;
        $property->type_garden_id = $request->type_garden_id;
        if ($request->garden_square_meters > 0)
            $property->garden_square_meters = $request->garden_square_meters;
        $property->type_exposure_id = $request->type_exposure_id;
        if ($request->condominium_fees > 0)
            $property->condominium_fees = $request->condominium_fees;
        $property->is_elevator = $request->is_elevator;
        $property->is_terrace = $request->is_terrace;
        $property->is_balcony = $request->is_balcony;
        $property->is_act_of_commission = $request->is_act_of_commission;
        $property->is_floorplans = $request->is_floorplans;
        $property->is_furnished = $request->is_furnished;
        $property->is_seafront = $request->is_seafront;
        $property->is_pool = $request->is_pool;
        $property->views = 0;
        $property->is_showcase = $request->is_showcase;
        $property->is_showcase_tv = $request->is_showcase_tv;
        $property->is_special_price = $request->is_special_price;
        $property->is_visible = $request->is_visible;
        $property->is_old_town = $request->is_old_town;
        $property->energetic_class = $request->energetic_class;
        if ($request->energetic_class_value > 0)
            $property->energetic_class_value = $request->energetic_class_value;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' cretao con successo.');
        return redirect()->route('admin::property.edit', [$property->id]);
    }

    public function edit($id)
    {
        $max_bl = Property::withTrashed()->where('rifimm', 'like', 'BL%')->orderBy('rifimm', 'desc')->first()->rifimm;
        if ($max_bl == null)
            $max_bl = '-- nessun BL presente --';

        $max_be = Property::withTrashed()->where('rifimm', 'like', 'BE%')->orderBy('rifimm', 'desc')->first()->rifimm;
        if ($max_be == null)
            $max_be = '-- nessun BE presente --';

        $property = Property::findOrFail($id);
        return view('admin.property.property_edit', [
            'property' => $property,
            'users_list' => Role::where('name', 'agent')->first()->users()->orderBy('users.name')->get()->pluck('name', 'id'),
            'areas_list' => $property->town->areas()->orderBy('name')->pluck('name', 'id'),
            'categories_list' => Category::orderBy('name_ita')->pluck('name_ita', 'id'),
            'provinces_list' => Province::orderBy('name')->pluck('name', 'id'),
            'typologies_list' => Typology::orderBy('name_ita')->pluck('name_ita', 'id'),
            'towns_list' => $property->province->towns()->orderBy('name')->pluck('name', 'id'),
            'type_boxs_list' => TypeBox::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_distsms_list' => TypeDistsm::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_exposures_list' => TypeExposure::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_floors_list' => TypeFloor::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_gardens_list' => TypeGarden::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_heatings_list' => TypeHeating::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_kitchens_list' => TypeKitchen::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_negotiations_list' => TypeNegotiation::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_occupations_list' => TypeOccupation::orderBy('name_ita')->pluck('name_ita', 'id'),
            'type_statuss_list' => TypeStatus::orderBy('name_ita')->pluck('name_ita', 'id'),
            'max_bl' => $max_bl,
            'max_be' => $max_be,
        ]);
    }

    public function update(Request $request, $id)
    {
        $property = Property::findOrFail($id);

        $this->validate($request, [
            'rifimm' => 'required|unique:properties,rifimm,' . $property->id,
            'user_id' => 'required|integer|exists:users,id',
            'category_id' => 'required|integer|exists:categories,id',
            'typology_id' => 'required|integer|exists:typologies,id',
            'province_id' => 'required|integer|exists:provinces,id',
            'town_id' => 'required|integer|exists:towns,id',
            'is_to_sale' => 'required|boolean',
            'address_visible' => 'boolean',
            'price' => 'required|integer',
            'type_negotiation_id' => 'required|integer|exists:type_negotiations,id',
            'type_floor_id' => 'integer|exists:type_floors,id',
            'total_floor' => 'integer',
            'room' => 'integer',
            'bath' => 'integer',
            'locals' => 'integer',
            'square_meters' => 'integer',
            'type_heating_id' => 'integer|exists:type_heating,id',
            'type_kitchen_id' => 'integer|exists:type_kitchens,id',
            'type_occupation_id' => 'integer|exists:type_occupations,id',
            'type_status_id' => 'integer|exists:type_status,id',
            'type_box_id' => 'integer|exists:type_box,id',
            'type_distsm_id' => 'integer|exists:type_distsm,id',
            'type_garden_id' => 'integer|exists:type_gardens,id',
            'garden_square_meters' => 'integer',
            'type_exposure_id' => 'integer|exists:type_exposures,id',
            'condominium_fees' => 'integer',
            'is_elevator' => 'boolean',
            'is_terrace' => 'boolean',
            'is_balcony' => 'boolean',
            'is_act_of_commission' => 'boolean',
            'is_floorplans' => 'boolean',
            'is_furnished' => 'boolean',
            'is_seafront' => 'boolean',
            'is_pool' => 'boolean',
            'views' => 'integer',
            'is_showcase' => 'boolean',
            'is_showcase_tv' => 'boolean',
            'is_special_price' => 'boolean',
            'is_visible' => 'boolean',
            'is_old_town' => 'boolean',
            'energetic_class_value' => 'integer',
        ]);

        $property->rifimm = $request->rifimm;
        $property->user_id = $request->user_id;
        $property->category_id = $request->category_id;
        $property->typology_id = $request->typology_id;
        $property->province_id = $request->province_id;
        $property->town_id = $request->town_id;
        if ($request->area_id > 0)
            $property->area_id = $request->area_id;
        else
            $property->area_id = null;
        $property->address = $request->address;
        $property->address_geocode = $request->address_geocode;
        $property->address_visible = $request->address_visible;
        $property->is_to_sale = $request->is_to_sale;
        $property->price = $request->price;
        $property->type_negotiation_id = $request->type_negotiation_id;
        $property->type_floor_id = $request->type_floor_id;
        if ($request->total_floor > 0)
            $property->total_floor = $request->total_floor;
        else
            $property->total_floor = 0;
        if ($request->room > 0)
            $property->room = $request->room;
        else
            $property->room = 0;
        if ($request->bath > 0)
            $property->bath = $request->bath;
        else
            $property->bath = 0;
        if ($request->locals > 0)
            $property->locals = $request->locals;
        else
            $property->locals = 0;
        if ($request->square_meters > 0)
            $property->square_meters = $request->square_meters;
        else
            $property->square_meters = 0;
        $property->description_ita = $request->description_ita;
        $property->description_fra = $request->description_fra;
        $property->description_eng = $request->description_eng;
        $property->type_heating_id = $request->type_heating_id;
        $property->type_kitchen_id = $request->type_kitchen_id;
        $property->type_occupation_id = $request->type_occupation_id;
        $property->type_status_id = $request->type_status_id;
        $property->type_box_id = $request->type_box_id;
        $property->type_distsm_id = $request->type_distsm_id;
        $property->type_garden_id = $request->type_garden_id;
        if ($request->garden_square_meters > 0)
            $property->garden_square_meters = $request->garden_square_meters;
        else
            $property->garden_square_meters = 0;
        $property->type_exposure_id = $request->type_exposure_id;
        if ($request->condominium_fees > 0)
            $property->condominium_fees = $request->condominium_fees;
        else
            $property->condominium_fees = 0;
        $property->is_elevator = $request->is_elevator;
        $property->is_terrace = $request->is_terrace;
        $property->is_balcony = $request->is_balcony;
        $property->is_act_of_commission = $request->is_act_of_commission;
        $property->is_floorplans = $request->is_floorplans;
        $property->is_furnished = $request->is_furnished;
        $property->is_seafront = $request->is_seafront;
        $property->is_pool = $request->is_pool;
        $property->is_showcase = $request->is_showcase;
        $property->is_showcase_tv = $request->is_showcase_tv;
        $property->is_special_price = $request->is_special_price;
        $property->is_visible = $request->is_visible;
        $property->is_old_town = $request->is_old_town;
        $property->energetic_class = $request->energetic_class;
        if ($request->energetic_class_value > 0)
            $property->energetic_class_value = $request->energetic_class_value;
        else
            $property->energetic_class_value = 0;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' aggiornato con successo.');
        return redirect()->route('admin::property.edit', [$property->id]);
    }

    public function destroy($id)
    {
        $property = Property::findOrFail($id);
        $rifimm = $property->rifimm;
        $property->delete();

        flash()->success('Immobile ' . $rifimm . ' rimosso con successo.');
        return redirect()->route('admin::property.index');
    }

    public function editPhotos($id)
    {
        $property = Property::findOrFail($id);
        $photos = $property->photos()->normalPhotos()->orderBy('position')->get();
        $planimetries = $property->photos()->planimetries()->get();

        return view('admin.property.property_edit_photos')
            ->with('property', $property)
            ->with('photos', $photos)
            ->with('planimetries', $planimetries);
    }

    public function uploadPhoto(Request $request, $id)
    {
        $property = Property::findOrFail($id);

        $tmp_photo_id = app('App\Http\Controllers\Admin\PhotoController')->store($request);
        $photo = Photo::findOrFail($tmp_photo_id);

        if ($request->is_planimetry) {
            $photo->is_planimetry = true;
        } else {
            if ($property->countNormalPhotos() == 1)
                $photo->is_main = true;
            $photo->position = $property->photos()->normalPhotos()->count();
        }

        $photo->save();

        flash()->success('Foto caricata con successo per l\'immobile ' . $property->rifimm . '.');
        return redirect()->route('admin::property.edit_photos', $property->id);
    }

    public function setMainPhoto($property_id, $photo_id)
    {
        $property = Property::findOrFail($property_id);
        $photo = Photo::findOrFail($photo_id);

        $tmp_photo = $property->mainPhoto();
        $tmp_photo->is_main = false;
        $tmp_photo->save();

        $photo->is_main = true;
        $photo->save();

        flash()->success('Foto impostata come principale per l\'immobile ' . $property->rifimm . '.');
        return redirect()->route('admin::property.edit_photos', $property->id);
    }

    public function sortPhotos(Request $request, $id)
    {
        // call by ajax
        $property = Property::findOrFail($id);
        foreach ($property->photos as $tmp_photo) {
            foreach ($request->photos as $index => $tmp_photo_2) {
                if ($tmp_photo->id == $tmp_photo_2) {
                    $tmp_photo->position = $index;
                    $tmp_photo->save();
                }
            }
        }

        return Response::json([
            'message' => 'Foto riordinate.',
            'property_id' => $id,
            'photo' => $request->photos,
        ]);
    }

    public function destroyPhoto($property_id, $photo_id)
    {
        $property = Property::findOrFail($property_id);
        $photo = Photo::findOrFail($photo_id);

        if ($photo->property->id != $property->id) {
            flash()->error('La foto non appartiene all\'immobile ' . $property->rifimm . '.');
            return redirect()->route('admin::property.edit_photos', $property->id);
        }

        if ($photo->is_main) {
            if ($property->photos()->normalPhotos()->where('id', '<>', $photo->id)->count() > 0) {
                $tmp_photo = $property->photos()->normalPhotos()->where('id', '<>', $photo->id)->first();
                $tmp_photo->is_main = true;
                $tmp_photo->save();
            }
        }

        foreach ($property->photos as $tmp_photo) {
            if ($tmp_photo->position > $photo->position) {
                $tmp_photo->position = $tmp_photo->position - 1;
                $tmp_photo->save();
            }
        }

        app('App\Http\Controllers\Admin\PhotoController')->destroy($photo->id);
        $photo->delete();

        flash()->success('Foto rimossa con successo per l\'immobile ' . $property->rifimm . '.');
        return redirect()->route('admin::property.edit_photos', $property->id);
    }

    public function print()
    {
        return view('admin.property.property_print')
            ->with('properties', Property::orderBy('rifimm')->get());
    }

    public function trashed()
    {
        return view('admin.property.property_trashed')
            ->with('properties', Property::onlyTrashed()->orderBy('rifimm')->get());
    }

    public function showcase()
    {
        return view('admin.property.property_showcase')
            ->with('properties', Property::orderBy('rifimm')->get());
    }

    public function restore($id)
    {
        $property = Property::onlyTrashed()->findOrFail($id);
        $property->restore();

        flash()->success('Immobile ' . $property->rifimm . ' ripristinato con successo.');
        return redirect()->route('admin::property.trashed');
    }

    public function forceDelete($id)
    {
        $property = Property::onlyTrashed()->findOrFail($id);
        $photos = $property->photos();
        foreach ($photos as $photo) {
            app('App\Http\Controllers\Admin\PhotoController')->destroy($photo->id);
        }

        if (File::exists(storage_path('app/public/images/properties/' . $property->rifimm))) {
            File::deleteDirectory(storage_path('app/public/images/properties/' . $property->rifimm));
        }

        $rifimm = $property->rifimm;
        $property->forceDelete();

        flash()->success('Immobile ' . $rifimm . ' rimosso definitivamente con successo.');
        return redirect()->route('admin::property.trashed');
    }

    public function share($id)
    {
        $property = Property::findOrFail($id);

        $appId = config('facebook.app_id');
        $appSecret = config('facebook.app_secret');
        $pageId = config('facebook.page_id');
        $userAccessToken = config('facebook.user_access_token');

        $fb = new \Facebook\Facebook([
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.10',
            'default_access_token' => $userAccessToken,
        ]);

        $post_message = $property->typology->name . " in ";
        $property->is_to_sale ? $post_message .= "vendita" : $post_message .= "affitto";
        $post_message .= " a " . $property->town->name . ", " . $property->publicPrice;

        $linkData = [
            'link' => route('web::details', $property->rifimm),
            'message' => $post_message,
            'picture' => 'http://immobiliarestudiob.com' . $property->mainPhoto()->getLarge(),
            'name' => $post_message,
            'description' => strip_tags(html_entity_decode($property->description_ita)),
        ];

        try {
            $response = $fb->post('/' . $pageId . '/feed', $linkData);
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            flash()->error('Graph returned an error: ' . $e->getMessage());
            return back();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            flash()->error('Facebook SDK returned an error: ' . $e->getMessage());
            return back();
        } catch (Exception $e) {
            flash()->error('Errore condivisione: ' . $e->getMessage());
            return back();
        }

        flash()->success('Immobile ' . $property->rifimm . ' condiviso con successo.');
        return redirect()->route('admin::property.index');
    }

    public function suspend($id)
    {
        $property = Property::findOrFail($id);
        $property->is_visible = false;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' sospeso con successo.');
        return redirect()->route('admin::property.index');
    }

    public function active($id)
    {
        $property = Property::findOrFail($id);
        $property->is_visible = true;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' riattivato con successo.');
        return redirect()->route('admin::property.index');
    }

    public function removeShocase($id)
    {
        $property = Property::findOrFail($id);
        $property->is_showcase = false;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' rimosso dalla vetrina.');
        return redirect()->route('admin::property.showcase');
    }

    public function addShowcase($id)
    {
        $property = Property::findOrFail($id);
        $property->is_showcase = true;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' aggiunto alla vetrina.');
        return redirect()->route('admin::property.showcase');
    }

    public function removeShocaseTV($id)
    {
        $property = Property::findOrFail($id);
        $property->is_showcase_tv = false;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' rimosso dalla vetrina tv.');
        return redirect()->route('admin::property.showcase');
    }

    public function addShowcaseTV($id)
    {
        $property = Property::findOrFail($id);
        $property->is_showcase_tv = true;
        $property->save();

        flash()->success('Immobile ' . $property->rifimm . ' aggiunto alla vetrina tv.');
        return redirect()->route('admin::property.showcase');
    }

    public function printO($id)
    {
        return view('print.property_horizontal')
            ->with('property', Property::findOrFail($id));
    }

    public function printV($id)
    {
        return view('print.property_vertical')
            ->with('property', Property::findOrFail($id));
    }

    public function printNew($id)
    {
        return view('print.property_new')
            ->with('property', Property::findOrFail($id));
    }

    public function printTest($id)
    {
        return view('print.property_test')
            ->with('property', Property::findOrFail($id));
    }

    public function statistics()
    {
        $properties = Property::orderBy('rifimm')->get();
        $top_properties = Property::orderBy('views', 'desc')->take(5)->get();

        return view('admin.statistics.statistics_property')
            ->with('properties', $properties)
            ->with('top_properties', $top_properties);
    }

    public function statisticsReset($id)
    {
        $property = Property::findOrFail($id);
        $property->views = 0;
        $property->save();

        flash()->success('Statistiche azzerate per l\'immobile ' . $property->rifimm . '.');
        return redirect()->route('admin::property.statistics');
    }
}