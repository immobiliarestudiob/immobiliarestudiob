<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeFloor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeFloorController extends Controller {

	public function index() {
		return view('admin.type.type_floor')
			->with('type_floors', TypeFloor::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_floor = new TypeFloor;
		$type_floor->name_ita = $request->input('name_ita');
		$type_floor->name_fra = $request->input('name_fra');
		$type_floor->name_eng = $request->input('name_eng');
		$type_floor->save();

		flash()->success('Piano creato con successo.');
		return redirect()->route('admin::type_floor.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_floor = TypeFloor::findOrFail($id);
		$type_floor->name_ita = $request->input('name_ita');
		$type_floor->name_fra = $request->input('name_fra');
		$type_floor->name_eng = $request->input('name_eng');
		$type_floor->save();

		flash()->success('Piano aggiornato con successo.');
		return redirect()->route('admin::type_floor.index');
	}

	public function destroy($id) {
		$type_floor = TypeFloor::findOrFail($id);
		$type_floor->delete();

		flash()->success('Piano rimosso con successo.');
		return redirect()->route('admin::type_floor.index');
	}
}