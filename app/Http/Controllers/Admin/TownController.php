<?php

namespace App\Http\Controllers\Admin;

use App\Models\Town;
use App\Models\Province;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TownController extends Controller {

	public function index() {
		return view('admin.geo.town')
			->with('towns', Town::orderBy('province_id')->orderBy('name')->get())
			->with('provinces', Province::orderBy('name')->pluck('name', 'id'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'province_id' => 'integer',
		]);

		$town = new Town;
		$town->name = $request->input('name');
		$town->province_id = $request->input('province_id');
		$town->save();

		flash()->success('Comune creato con successo.');
		return redirect()->route('admin::town.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required',
			'province_id' => 'integer',
		]);

		$town = Town::findOrFail($id);
		$town->name = $request->input('name');
		$town->province_id = $request->input('province_id');
		$town->save();

		flash()->success('Comune aggiornato con successo.');
		return redirect()->route('admin::town.index');
	}

	public function destroy($id) {
		$town = Town::findOrFail($id);
		app('App\Http\Controllers\Admin\PhotoController')->destroy($town->photo_id);
		$town->delete();

		flash()->success('Comune rimosso con successo.');
		return redirect()->route('admin::town.index');
	}

	public function uploadPhoto(Request $request, $id) {
		$town = Town::findOrFail($id);

		if ($town->hasPhoto())
			app('App\Http\Controllers\Admin\PhotoController')->destroy($town->photo_id);
		$photo_name = 'town_of_'.str_slug(strtolower($town->name), '_');
		$tmp_photo_id = app('App\Http\Controllers\Admin\PhotoController')->store($request, $photo_name, 'towns');

		$town->photo_id = $tmp_photo_id;
		$town->save();

		flash()->success('Foto del comune caricata con successo.');
		return redirect()->route('admin::town.index');
	}

	public function destroyPhoto($id) {
		$town = Town::findOrFail($id);
		app('App\Http\Controllers\Admin\PhotoController')->destroy($town->photo_id);
		$town->photo_id = null;
		$town->save();

		flash()->success('Foto del comune rimossa con successo.');
		return redirect()->route('admin::town.index');
	}

	public function statistics() {
		$towns = Town::orderBy('name')->get();
		$top_towns = Town::orderBy('views', 'desc')->take(5)->get();

		return view('admin.statistics.statistics_town')
			->with('towns', $towns)
			->with('top_towns', $top_towns);
	}

	public function statisticsReset($id) {
		$town = Town::findOrFail($id);
		$town->views = 0;
		$town->save();

		flash()->success('Statistiche azzerate per il comune '.$town->name.'.');
		return redirect()->route('admin::town.statistics');
	}
}