<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeKitchen;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeKitchenController extends Controller {

	public function index() {
		return view('admin.type.type_kitchen')
			->with('type_kitchens', TypeKitchen::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_kitchen = new TypeKitchen;
		$type_kitchen->name_ita = $request->input('name_ita');
		$type_kitchen->name_fra = $request->input('name_fra');
		$type_kitchen->name_eng = $request->input('name_eng');
		$type_kitchen->save();

		flash()->success('Cucina creata con successo.');
		return redirect()->route('admin::type_kitchen.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_kitchen = TypeKitchen::findOrFail($id);
		$type_kitchen->name_ita = $request->input('name_ita');
		$type_kitchen->name_fra = $request->input('name_fra');
		$type_kitchen->name_eng = $request->input('name_eng');
		$type_kitchen->save();

		flash()->success('Cucina aggiornata con successo.');
		return redirect()->route('admin::type_kitchen.index');
	}

	public function destroy($id) {
		$type_kitchen = TypeKitchen::findOrFail($id);
		$type_kitchen->delete();

		flash()->success('Cucina rimossa con successo.');
		return redirect()->route('admin::type_kitchen.index');
	}
}