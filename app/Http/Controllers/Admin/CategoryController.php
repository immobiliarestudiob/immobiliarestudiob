<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class CategoryController extends Controller {

	public function index() {
		$categories = Category::orderBy('upcategory_id')->orderBy('name_ita')->get();
		$categories_list = Category::where('upcategory_id', null)->orderBy('name_ita')->pluck('name_ita', 'id');

		return view('admin.other.category')
			->with('categories', $categories)
			->with('categories_list', $categories_list);
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$category = new Category;
		$category->name_ita = $request->input('name_ita');
		$category->name_fra = $request->input('name_fra');
		$category->name_eng = $request->input('name_eng');
		if ($request->has('upcategory_id'))
			$category->upcategory_id = $request->input('upcategory_id');
		$category->save();

		flash()->success('Categoria creata con successo.');
		return redirect()->route('admin::category.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$category = Category::findOrFail($id);
		$category->name_ita = $request->input('name_ita');
		$category->name_fra = $request->input('name_fra');
		$category->name_eng = $request->input('name_eng');
		if ($request->has('upcategory_id'))
			$category->upcategory_id = $request->input('upcategory_id');
		$category->save();

		flash()->success('Categoria aggiornata con successo.');
		return redirect()->route('admin::category.index');
	}

	public function destroy($id) {
		$category = Category::findOrFail($id);
		$subcategories = $category->subCategories();
		$subcategories->delete();
		$category->delete();

		flash()->success('Categoria rimossa con successo.');
		return redirect()->route('admin::category.index');
	}

	public function statistics() {
		$categories = Category::orderBy('name_ita')->get();
		$top_categories = Category::orderBy('views', 'desc')->take(5)->get();

		return view('admin.statistics.statistics_category')
			->with('categories', $categories)
			->with('top_categories', $top_categories);
	}

	public function statisticsReset($id) {
		$category = Category::findOrFail($id);
		$category->views = 0;
		$category->save();

		flash()->success('Statistiche azzerate per la categoria '.$category->name_ita.'.');
		return redirect()->route('admin::category.statistics');
	}
}