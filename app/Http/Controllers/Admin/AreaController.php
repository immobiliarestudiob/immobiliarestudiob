<?php

namespace App\Http\Controllers\Admin;

use App\Models\Area;
use App\Models\Town;
use App\Models\Province;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class AreaController extends Controller {

	public function index() {
		return view('admin.geo.area')
			->with('areas', Area::orderBy('town_id')->orderBy('name')->get())
			->with('towns', Town::orderBy('name')->pluck('name', 'id'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'town_id' => 'integer',
		]);

		$area = new Area;
		$area->name = $request->input('name');
		$area->town_id = $request->input('town_id');
		$area->save();

		flash()->success('Zona creata con successo.');
		return redirect()->route('admin::area.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required',
			'town_id' => 'required|integer',
		]);

		$area = Area::findOrFail($id);
		$area->name = $request->input('name');
		$area->town_id = $request->input('town_id');
		$area->save();

		flash()->success('Zona aggiornata con successo.');
		return redirect()->route('admin::area.index');
	}

	public function destroy($id) {
		$area = Area::findOrFail($id);
		app('App\Http\Controllers\Admin\PhotoController')->destroy($area->photo_id);
		$area->delete();

		flash()->success('Zona rimossa con successo.');
		return redirect()->route('admin::area.index');
	}

	public function uploadPhoto(Request $request, $id) {
		$area = Area::findOrFail($id);

		if ($area->hasPhoto())
			app('App\Http\Controllers\Admin\PhotoController')->destroy($area->photo_id);
		$photo_name = 'town_of_'.str_slug(strtolower($area->town->name), '_').'_'.str_slug(strtolower($area->name), '_');
		$tmp_photo_id = app('App\Http\Controllers\Admin\PhotoController')->store($request, $photo_name, 'areas');

		$area->photo_id = $tmp_photo_id;
		$area->save();

		flash()->success('Foto della zona caricata con successo.');
		return redirect()->route('admin::area.index');
	}

	public function destroyPhoto($id) {
		$area = Area::findOrFail($id);
		app('App\Http\Controllers\Admin\PhotoController')->destroy($area->photo_id);
		$area->photo_id = null;
		$area->save();

		flash()->success('Foto della zona rimossa con successo.');
		return redirect()->route('admin::area.index');
	}

	public function statistics() {
		$areas = Area::orderBy('name')->get();
		$top_areas = Area::orderBy('views', 'desc')->take(5)->get();

		return view('admin.statistics.statistics_area')
			->with('areas', $areas)
			->with('top_areas', $top_areas);
	}

	public function statisticsReset($id) {
		$area = Area::findOrFail($id);
		$area->views = 0;
		$area->save();

		flash()->success('Statistiche azzerate per la zona '.$area->name.'.');
		return redirect()->route('admin::area.statistics');
	}
}