<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeExposure;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeExposureController extends Controller {

	public function index() {
		return view('admin.type.type_exposure')
			->with('type_exposures', TypeExposure::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_exposure = new TypeExposure;
		$type_exposure->name_ita = $request->input('name_ita');
		$type_exposure->name_fra = $request->input('name_fra');
		$type_exposure->name_eng = $request->input('name_eng');
		$type_exposure->save();

		flash()->success('Esposizione creata con successo.');
		return redirect()->route('admin::type_exposure.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_exposure = TypeExposure::findOrFail($id);
		$type_exposure->name_ita = $request->input('name_ita');
		$type_exposure->name_fra = $request->input('name_fra');
		$type_exposure->name_eng = $request->input('name_eng');
		$type_exposure->save();

		flash()->success('Esposizione aggiornata con successo.');
		return redirect()->route('admin::type_exposure.index');
	}

	public function destroy($id) {
		$type_exposure = TypeExposure::findOrFail($id);
		$type_exposure->delete();

		flash()->success('Esposizione rimossa con successo.');
		return redirect()->route('admin::type_exposure.index');
	}
}