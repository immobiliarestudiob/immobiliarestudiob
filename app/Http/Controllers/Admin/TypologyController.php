<?php

namespace App\Http\Controllers\Admin;

use App\Models\Typology;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypologyController extends Controller {

	public function index() {
		$typologies = Typology::orderBy('uptypology_id')->orderBy('name_ita')->get();
		$typologies_list = Typology::where('uptypology_id', null)->orderBy('name_ita')->pluck('name_ita', 'id');

		return view('admin.other.typology')
			->with('typologies', $typologies)
			->with('typologies_list', $typologies_list);
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'class' => 'required',
		]);

		$typology = new Typology;
		$typology->name_ita = $request->input('name_ita');
		$typology->name_fra = $request->input('name_fra');
		$typology->name_eng = $request->input('name_eng');
		$typology->class = $request->input('class');
		if ($request->has('uptypology_id'))
			$typology->uptypology_id = $request->input('uptypology_id');
		$typology->save();

		flash()->success('Tipologia creata con successo.');
		return redirect()->route('admin::typology.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'class' => 'required',
		]);

		$typology = Typology::findOrFail($id);
		$typology->name_ita = $request->input('name_ita');
		$typology->name_fra = $request->input('name_fra');
		$typology->name_eng = $request->input('name_eng');
		$typology->class = $request->input('class');
		if ($request->has('uptypology_id'))
			$typology->uptypology_id = $request->input('uptypology_id');
		$typology->save();

		flash()->success('Tipologia aggiornata con successo.');
		return redirect()->route('admin::typology.index');
	}

	public function destroy($id) {
		$typology = Typology::findOrFail($id);
		$subtypologies = $typology->subTypologies();
		$subtypologies->delete();
		$typology->delete();

		flash()->success('Tipologia rimossa con successo.');
		return redirect()->route('admin::typology.index');
	}

	public function statistics() {
		$typologies = Typology::orderBy('name_ita')->get();
		$top_typologies = Typology::orderBy('views', 'desc')->take(5)->get();
		return view('admin.statistics.statistics_typology')
			->with('typologies', $typologies)
			->with('top_typologies', $top_typologies);
	}

	public function statisticsReset($id) {
		$typology = Typology::findOrFail($id);
		$typology->views = 0;
		$typology->save();

		flash()->success('Statistiche azzerate per la tipologia '.$typology->name_ita.'.');
		return redirect()->route('admin::typology.statistics');
	}
}