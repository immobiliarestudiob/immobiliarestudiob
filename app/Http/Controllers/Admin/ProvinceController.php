<?php

namespace App\Http\Controllers\Admin;

use App\Models\Province;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProvinceController extends Controller {

	public function index() {
		return view('admin.geo.province')
			->with('provinces', Province::orderBy('name')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'abbreviation' => 'required',
		]);

		$province = new Province;
		$province->name = $request->input('name');
		$province->abbreviation = $request->input('abbreviation');
		$province->save();

		flash()->success('Provincia creata con successo.');
		return redirect()->route('admin::province.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required',
			'abbreviation' => 'required',
		]);

		$province = Province::findOrFail($id);
		$province->name = $request->input('name');
		$province->abbreviation = $request->input('abbreviation');
		$province->save();

		flash()->success('Provincia aggiornata con successo.');
		return redirect()->route('admin::province.index');
	}

	public function destroy($id) {
		$province = Province::findOrFail($id);
		$province->delete();

		flash()->success('Provincia rimossa con successo.');
		return redirect()->route('admin::province.index');
	}

	public function statistics() {
		$provinces = Province::orderBy('name')->get();
		$top_provinces = Province::orderBy('views', 'desc')->take(5)->get();

		return view('admin.statistics.statistics_province')
			->with('provinces', $provinces)
			->with('top_provinces', $top_provinces);
	}

	public function statisticsReset($id) {
		$province = Province::findOrFail($id);
		$province->views = 0;
		$province->save();

		flash()->success('Statistiche azzerate per la provincia '.$province->name.'.');
		return redirect()->route('admin::province.statistics');
	}
}