<?php

namespace App\Http\Controllers\Admin;

use App\Models\Realestate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class RealestateController extends Controller {

	public function edit($id) {
		return view('admin.other.realestate')
			->with('realestate', Realestate::findOrFail($id));
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required',
			'address' => 'required',
			'p_iva' => 'required|numeric',
			'telephone' => 'required',
			'email' => 'required|email',
			'agenti_imm' => 'numeric'
		]);

		$realestates = Realestate::findOrFail($id);
		$realestates->name = $request->input('name');
		$realestates->address = $request->input('address');
		$realestates->p_iva = $request->input('p_iva');
		$realestates->telephone = $request->input('telephone');
		$realestates->telephone_2 = $request->input('telephone_2');
		$realestates->cellular = $request->input('cellular');
		$realestates->cellular_2 = $request->input('cellular_2');
		$realestates->fax = $request->input('fax');
		$realestates->fax_2 = $request->input('fax_2');
		$realestates->email = $request->input('email');
		$realestates->agenti_imm = $request->input('agenti_imm');
		$realestates->rea = $request->input('rea');
		$realestates->save();

		flash()->success('Contatti aggiornati con successo.');
		return redirect()->route('admin::realestate.edit', [$id]);
	}
}