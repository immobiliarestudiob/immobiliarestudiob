<?php

namespace App\Http\Controllers\Admin;

use App\Models\Photo;
use App\Models\Property;

use Response;
use Image;
use File;
use Input;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class PhotoController extends Controller {

	public function store(Request $request, $name = null, $folder = null) {
		if ($request->hasFile('photo')) {
			if ($request->file('photo')->isValid()) {
				$this->validate($request, [
					'photo' => 'required|image',
					'property_id' => 'integer',
					'is_planimetry' => 'boolean',
					'is_main' => 'boolean',
					'is_hidden' => 'boolean'
				]);

				if ($folder != null) {
					$destination_path = $folder;
				} else {
					if ($request->input('property_id') > 0) {
						$property = Property::findOrFail($request->input('property_id'));
						$destination_path = 'properties/'.$property->rifimm;
					} else {
						$destination_path = 'other';
					}
				}

				$photo_file = $request->file('photo');
				$img = Image::make($request->file('photo'));

				if (!File::exists( storage_path('app/public/images/'.$destination_path))) {
					File::makeDirectory(storage_path('app/public/images/'.$destination_path));
				}

				$photo_original_name = $photo_file->getClientOriginalName();
				$photo_extension = $photo_file->extension();
				if ($name != null) {
					$photo_name = $name.'.'.$photo_extension;
				} else {
					do {
						$photo_name = $this->randomizeFileName($property->rifimm, $photo_extension);
					} while ( File::exists( storage_path('app/public/images/'.$destination_path.'/'.$photo_name)));
				}

				$img->save(storage_path('app/public/images/'.$destination_path.'/'.$photo_name), 60);

				$photo_db = new Photo;
				if ($request->input('property_id') > 0)
					$photo_db->property_id = $request->input('property_id');
				$photo_db->name = $photo_name;
				$photo_db->original_name = $photo_original_name;
				$photo_db->perc_photo = $destination_path.'/'.$photo_name;
				$photo_db->save();

				flash()->success('Foto caricata con successo.');
				return $photo_db->id;

			} else {
				flash()->error('Problemi di caricamento della foto!');
				return null;
			}
		} else {
			flash()->error('Foto non trovata!');
			return null;
		}
	}

	public function destroy($id) {
		if ($id != null) {
			$photo = Photo::findOrFail($id);
			if(File::exists( storage_path('app/public/images/'.$photo->perc_photo) )) {
				File::delete( storage_path('app/public/images/'.$photo->perc_photo ));
				flash()->success('Foto rimossa con successo.');
			} else {
				flash()->error('Foto non trovata!');
			}
			$photo->delete();
		}
	}

	public function randomizeFileName($name, $estension = 'jpg') {
		return $name.'_'.rand(100, 999).'.'.$estension;
	}
}