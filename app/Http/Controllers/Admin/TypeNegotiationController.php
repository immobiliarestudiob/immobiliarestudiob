<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeNegotiation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeNegotiationController extends Controller {

	public function index() {
		return view('admin.type.type_negotiation')
			->with('type_negotiations', TypeNegotiation::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'is_visible_price' => 'boolean',
		]);

		$type_negotiation = new TypeNegotiation;
		$type_negotiation->name_ita = $request->input('name_ita');
		$type_negotiation->name_fra = $request->input('name_fra');
		$type_negotiation->name_eng = $request->input('name_eng');
		$type_negotiation->is_visible_price = $request->has('is_visible_price');
		$type_negotiation->save();

		flash()->success('Negoziazione creata con successo.');
		return redirect()->route('admin::type_negotiation.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'is_visible_price' => 'boolean',
		]);

		$type_negotiation = TypeNegotiation::findOrFail($id);
		$type_negotiation->name_ita = $request->input('name_ita');
		$type_negotiation->name_fra = $request->input('name_fra');
		$type_negotiation->name_eng = $request->input('name_eng');
		$type_negotiation->is_visible_price = $request->has('is_visible_price');
		$type_negotiation->save();

		flash()->success('Negoziazione aggiornata con successo.');
		return redirect()->route('admin::type_negotiation.index');
	}

	public function destroy($id) {
		$type_negotiation = TypeNegotiation::findOrFail($id);
		$type_negotiation->delete();

		flash()->success('Negoziazione rimossa con successo.');
		return redirect()->route('admin::type_negotiation.index');
	}
}