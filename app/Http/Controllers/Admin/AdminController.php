<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class AdminController extends Controller {

	public function root() {
		if (Auth::guest())
			return redirect()->route('auth::login_get');

		if (! Auth::user()->hasAnyRole(['agent', 'admin']))
			return redirect()->route('web::user.root', [Auth::id()]);

		return redirect()->route('admin::property.index');
	}
}