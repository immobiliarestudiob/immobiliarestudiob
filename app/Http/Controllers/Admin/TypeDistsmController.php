<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeDistsm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeDistsmController extends Controller {

	public function index() {
		return view('admin.type.type_distsm')
			->with('type_distsms', TypeDistsm::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_distsm = new TypeDistsm;
		$type_distsm->name_ita = $request->input('name_ita');
		$type_distsm->name_fra = $request->input('name_fra');
		$type_distsm->name_eng = $request->input('name_eng');
		$type_distsm->save();

		flash()->success('Distanza MareMonti creata con successo.');
		return redirect()->route('admin::type_distsm.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_distsm = TypeDistsm::findOrFail($id);
		$type_distsm->name_ita = $request->input('name_ita');
		$type_distsm->name_fra = $request->input('name_fra');
		$type_distsm->name_eng = $request->input('name_eng');
		$type_distsm->save();

		flash()->success('Distanza MareMonti aggiornata con successo.');
		return redirect()->route('admin::type_distsm.index');
	}

	public function destroy($id) {
		$type_distsm = TypeDistsm::findOrFail($id);
		$type_distsm->delete();

		flash()->success('Distanza MareMonti rimossa con successo.');
		return redirect()->route('admin::type_distsm.index');
	}
}