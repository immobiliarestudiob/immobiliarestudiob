<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeBox;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TypeBoxController extends Controller {

	public function index() {
		return view('admin.type.type_box')
			->with('type_boxes', TypeBox::orderBy('name_ita')->get());
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_box = new TypeBox;
		$type_box->name_ita = $request->input('name_ita');
		$type_box->name_fra = $request->input('name_fra');
		$type_box->name_eng = $request->input('name_eng');
		$type_box->save();

		flash()->success('Box creato con successo.');
		return redirect()->route('admin::type_box.index');
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_box = TypeBox::findOrFail($id);
		$type_box->name_ita = $request->input('name_ita');
		$type_box->name_fra = $request->input('name_fra');
		$type_box->name_eng = $request->input('name_eng');
		$type_box->save();

		flash()->success('Box aggiornato con successo.');
		return redirect()->route('admin::type_box.index');
	}

	public function destroy($id) {
		$type_box = TypeBox::findOrFail($id);
		$type_box->delete();

		flash()->success('Box rimosso con successo.');
		return redirect()->route('admin::type_box.index');
	}
}