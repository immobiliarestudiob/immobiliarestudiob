<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;

class UserController extends Controller
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function index()
    {
        $users = $this->userService->all(['id', 'name', 'surname', 'email']);
        return view('admin.user.index')
            ->with('users', $users);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        /** @var User $user */
        $user = $this->userService->store($request->all());

        if ($user) {
            flash()->success(trans('admin.male.store', ['class' => 'Utente']));
            return redirect()->route('admin::user.edit', [$user->id]);
        } else {
            flash()->error(trans('admin.error.store'));
            return back()->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function show($id)
    {
        /** @var User $user */
        $user = $this->userService->findOrFail($id, ['id']);
        return view('admin.user.show')
            ->with('user', $user);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function edit($id)
    {
        /** @var User $user */
        $user = $this->userService->findOrFail($id,
            ['id', 'name', 'surname', 'email', 'telephone', 'username', 'confirmed']
        );
        return view('admin.user.edit')
            ->with('user', $user);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        if ($this->userService->update($request->all(), $id)) {
            flash()->success(trans('admin.male.update', ['class' => 'Utente']));
            return redirect()->route('admin::user.edit', [$id]);
        } else {
            flash()->error(trans('admin.error.update'));
            return back()->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userService->findOrFail($id);
        if ($user->hasAnyRole(['agent', 'admin'])) {
            flash()->error('Impossibile rimuovere questo utente!');
            return redirect()->route('admin::user.index');
        }

        $this->userService->delete($id);

        flash()->success('Utente rimosso con successo.');
        return redirect()->route('admin::user.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function preferred($id)
    {
        /** @var User $user */
        $user = $this->userService->findOrFail($id);
        $preferredProperties = $user->preferredProperties;

        return view('admin.user.preferred')
            ->with('user', $user)
            ->with('preferredProperties', $preferredProperties);
    }

    public function uploadPhoto(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($user->hasPhoto())
            $this->destroyPhoto($id);

        $tmp_photo_id = app('App\Http\Controllers\Admin\PhotoController')->store($request, 'user_' . $id, 'users');

        $user->photo_id = $tmp_photo_id;
        $user->save();

        flash()->success('Foto profilo caricata con successo.');
        return redirect()->route('admin::user.edit', [$id]);
    }

    public function destroyPhoto($id)
    {
        $user = User::findOrFail($id);
        app('App\Http\Controllers\Admin\PhotoController')->destroy($user->photo_id);
        $user->photo_id = null;
        $user->save();

        flash()->success('Foto profilo rimossa con successo.');
        return redirect()->route('admin::user.edit', [$id]);
    }

    public function notification($id)
    {
        $user = User::findOrFail($id);
        //return $user->notifications;
        return view('admin.user.notification')
            ->with('notifications', $user->notifications);
    }

    public function notificationShow($user_id, $not_id)
    {
        $user = User::findOrFail($user_id);
        $notif = null;
        foreach ($user->notifications as $notification) {
            if ($notification->id == $not_id) {
                $notif = $notification;
                break;
            }
        }
        if ($notif == null)
            abort(404);

        $notif->markAsRead();
        if ($notif->type == 'App\Notifications\NewPreferredProperty')
            return redirect()->route('admin::user.preferred', $notif->data['user_id']);
        if ($notif->type == 'App\Notifications\NewPreferredSearch')
            return redirect()->route('admin::search.index', [$notif->data['user_id']]);

        return back();
    }
}