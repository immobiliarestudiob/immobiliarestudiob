<?php

namespace App\Http\Controllers\Web;

use View;
use Lang;
use Mail;
use Cookie;
use SEOMeta;
use OpenGraph;

use App\Http\Requests;
use App\Models\Search;
use App\Models\Property;
use App\Models\Realestate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebController extends Controller
{

    public function __construct()
    {
        View::share('realestate', Realestate::first());
    }

    public function home()
    {
        $this->setSEO('Home');
        return view('web.home');
    }

    public function about()
    {
        $this->setSEO(trans('web.title_about'));
        return view('web.about');
    }

    public function contact()
    {
        $this->setSEO(trans('web.title_contact'));
        return view('web.contact');
    }

    public function request(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'mail' => 'required|email',
            'message' => 'required'
        ]);

        Mail::to('info@immobiliarestudiob.com')
            ->send(new \App\Mail\RequestFromWeb($request));

        return redirect()
            ->back()
            ->with('success_message', trans('web.request_message_sent'));
    }

    public function search(Request $request)
    {
        $this->setSEO('Search');

        if ($request->has('rifimm'))
            return redirect()->route('web::details', $request->rifimm);

        $view_grid = true;
        if ($request->has('view_grid'))
            $view_grid = $request->view_grid;

        $order = 'plh';
        if ($request->has('order'))
            $order = $request->order;

        $search = new Search;
        $properties = $search->fill($request->all())->getMatchProperties();
        switch ($order) {
            case 'phl':
                $properties = $properties->orderBy('price', 'desc');
                break;
            case 'alh':
                $properties = $properties->orderBy('square_meters', 'asc');
                break;
            case 'ahl':
                $properties = $properties->orderBy('square_meters', 'desc');
                break;
            case 'plh':
            default:
                $properties = $properties->orderBy('price', 'asc');
                break;
        }

        return view('web.search')
            ->with('title_search', $search->getTitle())
            ->with('properties', $properties->paginate(12))
            ->with('properties_map', $properties->visibleOnMap()->get())
            ->with('view_grid', $view_grid)
            ->with('search_parameters', $search->getSearchParameters());
    }

    public function detailsProperty(Request $request, $rifimm)
    {
        $property = Property::visible()->where('rifimm', $rifimm)->firstOrFail();

        $recent_properties = collect();
        if ($request->hasCookie('recent_properties'))
            $recent_properties = collect(Cookie::get('recent_properties'));
        $recent_properties = $recent_properties->prepend($rifimm)->unique();
        Cookie::queue(cookie()->forever('recent_properties', $recent_properties));

        $this->setSEODetail($property);
        return view('web.detail')
            ->with('property', $property);
    }

    public function printProperty(Request $request, $rifimm)
    {
        $property = Property::visible()->where('rifimm', $rifimm)->firstOrFail();

        $this->setSEODetail($property);
        return view('web.print')
            ->with('property', $property);
    }

    private function setSEO($title)
    {
        SEOMeta::setTitle($title);
        SEOMeta::setDescription(trans('web.meta_description'));

        OpenGraph::setTitle($title . ' - Studio B Immobiliare S.R.L.');
        OpenGraph::setDescription(trans('web.meta_description'));

        switch (Lang::getLocale()) {
            case 'it':
                OpenGraph::addProperty('locale', 'it-it');
                OpenGraph::addProperty('locale:alternate', ['en-gb', 'fr-fr']);
                break;
            case 'fr':
                OpenGraph::addProperty('locale', 'fr-fr');
                OpenGraph::addProperty('locale:alternate', ['en-gb', 'it-it']);
                break;
            case 'en':
            default:
                OpenGraph::addProperty('locale', 'en-gb');
                OpenGraph::addProperty('locale:alternate', ['it-it', 'fr-fr']);
                break;
        }
    }

    private function setSEODetail($property)
    {
        SEOMeta::setTitle(trans('web.title_details', ['rifimm' => $property->rifimm]));
        SEOMeta::setDescription($property->getShortDescription(300));

        OpenGraph::setTitle(trans('web.title_details', ['rifimm' => $property->rifimm]));
        OpenGraph::setDescription($property->getShortDescription(300));
        if ($property->hasMainPhoto())
            OpenGraph::addImage($property->mainPhoto()->getLarge());
        foreach ($property->getNormalPhotos($use_main = false) as $photo)
            OpenGraph::addImage($photo->getLarge());
    }
}