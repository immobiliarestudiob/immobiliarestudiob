<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Property;

use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class UserController extends Controller {

	private function checkUser($user_id) {
		if (Auth::user()->hasRole('public'))
			if (Auth::id() != $user_id)
				abort(403);
	}

	public function dashboard($user_id) {
		$this->checkUser($user_id);

		$user = User::findOrFail($user_id);
		return view('web.user.dashboard')
			->with('user', $user)
			->with('recommended', $user->getRecommended()->paginate(12))
			->with('menu_user', 'dashboard');
	}

	public function preferred($user_id) {
		$this->checkUser($user_id);

		$user = User::findOrFail($user_id);
		$preferred_properties = $user->preferredProperties()->visible()->paginate(12);
		return view('web.user.preferred')
			->with('user', $user)
			->with('preferred_properties', $preferred_properties)
			->with('menu_user', 'preferred');
	}

	public function search($user_id) {
		$this->checkUser($user_id);

		$user = User::findOrFail($user_id);
		return view('web.user.search')
			->with('user', $user)
			->with('searchs', $user->search()->paginate(12))
			->with('menu_user', 'search');
	}

	public function edit($user_id) {
		$this->checkUser($user_id);

		return view('web.user.edit')
			->with('user', User::findOrFail($user_id))
			->with('menu_user', 'edit');
	}

	public function update(Request $request, $user_id) {
		$this->checkUser($user_id);

		$user = User::findOrFail($user_id);
		$this->validate($request, [
			'username' => 'required|min:5',
			'password' => 'confirmed',
			'email' => 'required|email|unique:users,email,'.$user->id
		]);

		$user->username = $request->input('username');
		$user->name = $request->input('name');
		$user->surname = $request->input('surname');
		$user->email = $request->input('email');
		$user->telephone = $request->input('telephone');
		if ($request->has('password')) {
			$user->password = bcrypt($request->input('password'));
		}
		$user->save();

		flash()->success(trans('web.user_update'));
		return redirect()->route('web::user.edit', [$user_id]);
	}

	public function addPreferred($user_id, $property_id) {
		$this->checkUser($user_id);

		$property = Property::findOrFail($property_id);
		$user = User::findOrFail($user_id);
		$user->preferredProperties()->attach($property);
		
		User::where('username', 'loredana')->first()->notify(new \App\Notifications\NewPreferredProperty($property->rifimm, $user_id));
		
		flash()->overlay(trans('web.property_added_preferred'), '');
		return redirect()->back();
	}

	public function removePreferred($user_id, $property_id) {
		$this->checkUser($user_id);

		User::findOrFail($user_id)
			->preferredProperties()
			->detach(Property::findOrFail($property_id));
		flash()->overlay(trans('web.property_removed_preferred'), '');
		return redirect()->back();
	}

	public function addSearch(Request $request, $user_id) {
		$this->checkUser($user_id);

		$search = new \App\Models\Search;
		$search->fill($request->all());
		$search->user_id = Auth::id();
		$search->save();

		User::where('username', 'loredana')->first()->notify(new \App\Notifications\NewPreferredSearch($search->id, $user_id));

		flash()->overlay(trans('web.property_search_added'), '');
		return redirect()->back();
	}

	public function removeSearch($user_id, $search_id) {
		$this->checkUser($user_id);

		$search = User::findOrFail($user_id)->search->where('id', $search_id)->first();
		$search->delete();
		flash()->overlay(trans('web.property_search_removed'), '');
		return redirect()->back();
	}
}