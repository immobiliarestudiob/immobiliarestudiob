<?php

namespace App\Http\Controllers\Api\v1;

use App\Area;
use App\Town;
use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreaController extends Controller {

	public function index() {
		$areas = Area::orderBy('town_id')->orderBy('name')->get();
		return response()->success(compact('areas'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'town_id' => 'required|integer',
		]);

		$area = new Area;
		$area->name = $request->input('name');
		$area->town_id = $request->input('town_id');
		$area->save();

		$success = trans('api.section_stored', ['section' => 'Zona']);
		return response()->success(compact('area', 'success'));
	}

	public function show($id) {
		$area = Area::find($id);
		if ($area) {
			return response()->success(compact('area'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Zona']), 404);
		}
	}

	public function update(Request $request, $id) {
		$area = Area::find($id);
		if ($area) {
			$this->validate($request, [
				'name' => 'required',
				'town_id' => 'required|integer',
			]);
			$area->name = $request->input('name');
			$area->town_id = $request->input('town_id');
			$area->save();

			$success = trans('api.section_updated', ['section' => 'Zona']);
			return response()->success(compact('area', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Zona']), 404);
		}
	}

	public function destroy($id) {
		$area = Area::find($id);
		if ($area) {
			app('App\Http\Controllers\Api\v1\PhotoController')->destroy($area->photo_id);
			$area->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Zona']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Zona']), 404);
		}
	}

	public function uploadPhoto(Request $request, $id) {
		$area = Area::find($id);
		if ($area) {
			$tmp_photo_id = app('App\Http\Controllers\Api\v1\PhotoController')->store($request);

			$area->photo_id = $tmp_photo_id;
			$area->save();

			$success = trans('api.photo_section_uploaded', ['section' => 'Zona']);
			return response()->success(compact('area', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Zona']), 404);
		}
	}

	public function destroyPhoto($id) {
		$area = Area::find($id);
		if ($area) {
			app('App\Http\Controllers\Api\v1\PhotoController')->destroy($area->photo_id);

			$area->photo_id = null;
			$area->save();

			$success = trans('api.photo_section_deleted', ['section' => 'Zona']);
			return response()->success(compact('area', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Zona']), 404);
		}
	}
}