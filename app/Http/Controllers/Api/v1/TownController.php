<?php

namespace App\Http\Controllers\Api\v1;

use App\Town;
use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TownController extends Controller {

	public function index() {
		$towns = Town::orderBy('province_id')->orderBy('name')->get();
		return response()->success(compact('towns'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'province_id' => 'integer',
			//'lat_long' => '',
		]);

		$town = new Town;
		$town->name = $request->input('name');
		$town->province_id = $request->input('province_id');
		//$town->lat_long = $request->input('lat_long');
		$town->save();

		$success = trans('api.section_stored', ['section' => 'Comune']);
		return response()->success(compact('town', 'success'));
	}

	public function show($id) {
		$town = Town::find($id);
		if ($town) {
			return response()->success(compact('town'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}

	public function update(Request $request, $id) {
		$town = Town::find($id);
		if ($town) {
			$this->validate($request, [
				'name' => 'required',
				'province_id' => 'integer',
				//'lat_long' => '',
			]);
			$town->name = $request->input('name');
			$town->province_id = $request->input('province_id');
			//$town->lat_long = $request->input('lat_long');
			$town->save();

			$success = trans('api.section_updated', ['section' => 'Comune']);
			return response()->success(compact('town', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}

	public function destroy($id) {
		$town = Town::find($id);
		if ($town) {
			$areas = $town->areas;
			foreach ($areas as $area) {
				app('App\Http\Controllers\Api\v1\AreaController')->destroy($area->id);
			}

			app('App\Http\Controllers\Api\v1\PhotoController')->destroy($town->photo_id);
			$town->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Comune']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}

	public function uploadPhoto(Request $request, $id) {
		$town = Town::find($id);
		if ($town) {
			$tmp_photo_id = app('App\Http\Controllers\Api\v1\PhotoController')->store($request);

			$town->photo_id = $tmp_photo_id;
			$town->save();

			$success = trans('api.photo_section_uploaded', ['section' => 'Comune']);
			return response()->success(compact('town', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}

	public function destroyPhoto($id) {
		$town = Town::find($id);
		if ($town) {
			app('App\Http\Controllers\Api\v1\PhotoController')->destroy($town->photo_id);

			$town->photo_id = null;
			$town->save();

			$success = trans('api.photo_section_deleted', ['section' => 'Comune']);
			return response()->success(compact('town', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}

	public function getAreas($id) {
		$town = Town::find($id);
		if ($town) {
			$areas = $town->areas()->orderBy('name')->get();
			return response()->success(compact('areas'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Comune']), 404);
		}
	}
}