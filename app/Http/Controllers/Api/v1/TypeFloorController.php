<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeFloor;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeFloorController extends Controller {

	public function index() {
		$type_floors = TypeFloor::orderBy('name_ita')->get();
		return response()->success(compact('type_floors'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_floor = new TypeFloor;
		$type_floor->name_ita = $request->input('name_ita');
		$type_floor->name_fra = $request->input('name_fra');
		$type_floor->name_eng = $request->input('name_eng');
		$type_floor->save();

		$success = trans('api.section_stored', ['section' => 'Piano']);
		return response()->success(compact('type_floor', 'success'));
	}

	public function show($id) {
		$type_floor = TypeFloor::find($id);
		if ($type_floor) {
			return response()->success(compact('type_floor'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Piano']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_floor = TypeFloor::find($id);
		if ($type_floor) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_floor->name_ita = $request->input('name_ita');
			$type_floor->name_fra = $request->input('name_fra');
			$type_floor->name_eng = $request->input('name_eng');
			$type_floor->save();

			$success = trans('api.section_updated', ['section' => 'Piano']);
			return response()->success(compact('type_floor', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Piano']), 404);
		}
	}

	public function destroy($id) {
		$type_floor = TypeFloor::find($id);
		if ($type_floor) {
			$type_floor->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Piano']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Piano']), 404);
		}
	}
}