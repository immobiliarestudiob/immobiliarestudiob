<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeGarden;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeGardenController extends Controller {

	public function index() {
		$type_gardens = TypeGarden::orderBy('name_ita')->get();
		return response()->success(compact('type_gardens'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_garden = new TypeGarden;
		$type_garden->name_ita = $request->input('name_ita');
		$type_garden->name_fra = $request->input('name_fra');
		$type_garden->name_eng = $request->input('name_eng');
		$type_garden->save();

		$success = trans('api.section_stored', ['section' => 'Giardino']);
		return response()->success(compact('type_garden', 'success'));
	}

	public function show($id) {
		$type_garden = TypeGarden::find($id);
		if ($type_garden) {
			return response()->success(compact('type_garden'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Giardino']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_garden = TypeGarden::find($id);
		if ($type_garden) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_garden->name_ita = $request->input('name_ita');
			$type_garden->name_fra = $request->input('name_fra');
			$type_garden->name_eng = $request->input('name_eng');
			$type_garden->save();

			$success = trans('api.section_updated', ['section' => 'Giardino']);
			return response()->success(compact('type_garden', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Giardino']), 404);
		}
	}

	public function destroy($id) {
		$type_garden = TypeGarden::find($id);
		if ($type_garden) {
			$type_garden->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Giardino']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Giardino']), 404);
		}
	}
}