<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeOccupation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeOccupationController extends Controller {

	public function index() {
		$type_occupations = TypeOccupation::orderBy('name_ita')->get();
		return response()->success(compact('type_occupations'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_occupation = new TypeOccupation;
		$type_occupation->name_ita = $request->input('name_ita');
		$type_occupation->name_fra = $request->input('name_fra');
		$type_occupation->name_eng = $request->input('name_eng');
		$type_occupation->save();

		$success = trans('api.section_stored', ['section' => 'Occupazione']);
		return response()->success(compact('type_occupation', 'success'));
	}

	public function show($id) {
		$type_occupation = TypeOccupation::find($id);
		if ($type_occupation) {
			return response()->success(compact('type_occupation'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Occupazione']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_occupation = TypeOccupation::find($id);
		if ($type_occupation) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_occupation->name_ita = $request->input('name_ita');
			$type_occupation->name_fra = $request->input('name_fra');
			$type_occupation->name_eng = $request->input('name_eng');
			$type_occupation->save();

			$success = trans('api.section_updated', ['section' => 'Occupazione']);
			return response()->success(compact('type_occupation', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Occupazione']), 404);
		}
	}

	public function destroy($id) {
		$type_occupation = TypeOccupation::find($id);
		if ($type_occupation) {
			$type_occupation->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Occupazione']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Occupazione']), 404);
		}
	}
}