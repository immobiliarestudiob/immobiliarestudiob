<?php

namespace App\Http\Controllers\Api\v1;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller {

	public function index() {
		$categories = Category::where('upcategory_id', null)->get();
		foreach ($categories as $index => $category) {
			$category->subCategories;
		}

		return response()->success(compact('categories'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'upcategory_id' => 'integer',
		]);


		$category = new Category;
		$category->name_ita = $request->input('name_ita');
		$category->name_fra = $request->input('name_fra');
		$category->name_eng = $request->input('name_eng');
		if ($request->has('upcategory_id'))
			$category->upcategory_id = $request->input('upcategory_id');
		$category->save();

		$success = trans('api.section_stored', ['section' => 'Categoria']);
		return response()->success(compact('category', 'success'));
	}

	public function show($id) {
		$category = Category::find($id);
		if ($category) {
			return response()->success(compact('category'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Categoria']), 404);
		}
	}

	public function update(Request $request, $id) {
		$category = Category::find($id);
		if ($category) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
				'upcategory_id' => 'integer|different:'.$id,
			]);
			$category->name_ita = $request->input('name_ita');
			$category->name_fra = $request->input('name_fra');
			$category->name_eng = $request->input('name_eng');
			if ($request->has('upcategory_id'))
				$category->upcategory_id = $request->input('upcategory_id');
			$category->save();

			$success = trans('api.section_updated', ['section' => 'Categoria']);
			return response()->success(compact('category', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Categoria']), 404);
		}
	}

	public function destroy($id) {
		$category = Category::find($id);
		if ($category) {
			$subcategories = $category->subCategories();
			$subcategories->delete();
			$category->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Categoria']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Categoria']), 404);
		}
	}
}