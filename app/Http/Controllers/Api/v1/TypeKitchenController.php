<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeKitchen;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeKitchenController extends Controller {

	public function index() {
		$type_kitchens = TypeKitchen::orderBy('name_ita')->get();
		return response()->success(compact('type_kitchens'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_kitchen = new TypeKitchen;
		$type_kitchen->name_ita = $request->input('name_ita');
		$type_kitchen->name_fra = $request->input('name_fra');
		$type_kitchen->name_eng = $request->input('name_eng');
		$type_kitchen->save();

		$success = trans('api.section_stored', ['section' => 'Cucina']);
		return response()->success(compact('type_kitchen', 'success'));
	}

	public function show($id) {
		$type_kitchen = TypeKitchen::find($id);
		if ($type_kitchen) {
			return response()->success(compact('type_kitchen'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Cucina']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_kitchen = TypeKitchen::find($id);
		if ($type_kitchen) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_kitchen->name_ita = $request->input('name_ita');
			$type_kitchen->name_fra = $request->input('name_fra');
			$type_kitchen->name_eng = $request->input('name_eng');
			$type_kitchen->save();

			$success = trans('api.section_updated', ['section' => 'Cucina']);
			return response()->success(compact('type_kitchen', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Cucina']), 404);
		}
	}

	public function destroy($id) {
		$type_kitchen = TypeKitchen::find($id);
		if ($type_kitchen) {
			$type_kitchen->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Cucina']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Cucina']), 404);
		}
	}
}