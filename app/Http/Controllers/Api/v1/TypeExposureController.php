<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeExposure;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeExposureController extends Controller {

	public function index() {
		$type_exposures = TypeExposure::orderBy('name_ita')->get();
		return response()->success(compact('type_exposures'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_exposure = new TypeExposure;
		$type_exposure->name_ita = $request->input('name_ita');
		$type_exposure->name_fra = $request->input('name_fra');
		$type_exposure->name_eng = $request->input('name_eng');
		$type_exposure->save();

		$success = trans('api.section_stored', ['section' => 'Esposizione']);
		return response()->success(compact('type_exposure', 'success'));
	}

	public function show($id) {
		$type_exposure = TypeExposure::find($id);
		if ($type_exposure) {
			return response()->success(compact('type_exposure'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Esposizione']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_exposure = TypeExposure::find($id);
		if ($type_exposure) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_exposure->name_ita = $request->input('name_ita');
			$type_exposure->name_fra = $request->input('name_fra');
			$type_exposure->name_eng = $request->input('name_eng');
			$type_exposure->save();

			$success = trans('api.section_updated', ['section' => 'Esposizione']);
			return response()->success(compact('type_exposure', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Esposizione']), 404);
		}
	}

	public function destroy($id) {
		$type_exposure = TypeExposure::find($id);
		if ($type_exposure) {
			$type_exposure->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Esposizione']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Esposizione']), 404);
		}
	}
}