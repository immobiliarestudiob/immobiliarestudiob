<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeNegotiation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeNegotiationController extends Controller {

	public function index() {
		$type_negotiations = TypeNegotiation::orderBy('name_ita')->get();
		return response()->success(compact('type_negotiations'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_negotiation = new TypeNegotiation;
		$type_negotiation->name_ita = $request->input('name_ita');
		$type_negotiation->name_fra = $request->input('name_fra');
		$type_negotiation->name_eng = $request->input('name_eng');
		$type_negotiation->save();

		$success = trans('api.section_stored', ['section' => 'Negozizione']);
		return response()->success(compact('type_negotiation', 'success'));
	}

	public function show($id) {
		$type_negotiation = TypeNegotiation::find($id);
		if ($type_negotiation) {
			return response()->success(compact('type_negotiation'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Negozizione']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_negotiation = TypeNegotiation::find($id);
		if ($type_negotiation) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_negotiation->name_ita = $request->input('name_ita');
			$type_negotiation->name_fra = $request->input('name_fra');
			$type_negotiation->name_eng = $request->input('name_eng');
			$type_negotiation->save();

			$success = trans('api.section_updated', ['section' => 'Negozizione']);
			return response()->success(compact('type_negotiation', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Negozizione']), 404);
		}
	}

	public function destroy($id) {
		$type_negotiation = TypeNegotiation::find($id);
		if ($type_negotiation) {
			$type_negotiation->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Negozizione']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Negozizione']), 404);
		}
	}
}