<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller {

	public function index() {
		$users = User::get();
		return response()->success(compact('users'));
	}

	public function show($id) {
		$user = User::find($id);
		if ($user) {
			return response()->success(compact('user'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'User']), 404);
		}
	}

	public function update(Request $request, $id) {
		$user = User::find($id);
		if ($user) {
			$this->validate($request, [
				'username' => 'required|min:5',
				'password' => 'confirmed',

				'name' => 'required',
				'surname' => 'required',
				'email' => 'required|email|unique:users,email,'.$user->id,
				'telephone' => 'required',
			]);
			$user->username = $request->input('username');
			if ($request->has('password')) {
				$user->password = bcrypt($request->input('password'));
			}
			$user->name = $request->input('name');
			$user->surname = $request->input('surname');
			$user->email = $request->input('email');
			$user->telephone = $request->input('telephone');
			$user->save();

			$success = trans('api.section_updated', ['section' => 'User']);
			return response()->success(compact('user', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'User']), 404);
		}
	}

	public function uploadPhoto(Request $request, $id) {
		$user = User::find($id);
		if ($user) {
			$tmp_photo_id = app('App\Http\Controllers\Api\v1\PhotoController')->store($request);

			$user->photo_id = $tmp_photo_id;
			$user->save();

			$success = trans('api.photo_section_uploaded', ['section' => 'User']);
			return response()->success(compact('user', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'User']), 404);
		}
	}

	public function destroyPhoto($id) {
		$user = User::find($id);
		if ($user) {
			app('App\Http\Controllers\Api\v1\PhotoController')->destroy($user->photo_id);

			$user->photo_id = null;
			$user->save();

			$success = trans('api.photo_section_deleted', ['section' => 'User']);
			return response()->success(compact('user', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'User']), 404);
		}
	}
}