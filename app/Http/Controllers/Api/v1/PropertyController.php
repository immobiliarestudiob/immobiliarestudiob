<?php

namespace App\Http\Controllers\Api\v1;

use App\Area;
use App\Category;
use App\Province;
use App\Typology;
use App\Town;
use App\TypeBox;
use App\TypeDistsm;
use App\TypeExposure;
use App\TypeFloor;
use App\TypeGarden;
use App\TypeHeating;
use App\TypeKitchen;
use App\TypeNegotiation;
use App\TypeOccupation;
use App\TypeStatus;

use App\Property;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PropertyController extends Controller {

	public function index() {
		$properties = Property::orderBy('rifimm')->get();
		return response()->success(compact('properties'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'rifimm' => 'required|unique:property,rifimm',
			'agent_id' => 'required|integer|exists:agents,id',
			'category_id' => 'required|integer|exists:categories,id',
			'typology_id' => 'required|integer|exists:typologies,id',
			'province_id' => 'required|integer|exists:provinces,id',
			'town_id' => 'required|integer|exists:towns,id',
			'area_id' => 'integer|exists:areas,id',
			'is_to_sale' => 'required|boolean',
			'price' => 'required|integer',
			'type_negotiation_id' => 'required|integer|exists:type_negotiations,id',
			'type_floor_id' => 'integer|exists:type_floors,id',
			'total_floor' => 'integer',
			'room' => 'integer',
			'bath' => 'integer',
			'locals' => 'integer',
			'square_meters' => 'integer',
			'type_heating_id' => 'integer|exists:type_heating,id',
			'type_kitchen_id' => 'integer|exists:type_kitchens,id',
			'type_occupation_id' => 'integer|exists:type_occupations,id',
			'type_status_id' => 'integer|exists:type_status,id',
			'type_box_id' => 'integer|exists:type_box,id',
			'type_distsm_id' => 'integer|exists:type_distsm,id',
			'type_garden_id' => 'integer|exists:type_gardens,id',
			'garden_square_meters' => 'integer',
			'type_exposure_id' => 'integer|exists:type_exposures,id',
			'condominium_fees' => 'integer',
			'is_elevator' => 'boolean',
			'is_terrace' => 'boolean',
			'is_balcony' => 'boolean',
			'is_act_of_commission' => 'boolean',
			'is_floorplans' => 'boolean',
			'is_furnished' => 'boolean',
			'is_seafront' => 'boolean',
			'is_pool' => 'boolean',
			'view' => 'integer',
			'is_showcase' => 'boolean',
			'is_showcase_tv' => 'boolean',
			'is_special_price' => 'boolean',
			'is_visible' => 'boolean',
			'is_old_town' => 'boolean',
			'energetic_class_value' => 'integer',
		]);


		$property = new Property;
		$property->rifimm = $request->rifimm;
		$property->agent_id = $request->agent_id;
		$property->category_id = $request->category_id;
		$property->typology_id = $request->typology_id;
		$property->province_id = $request->province_id;
		$property->town_id = $request->town_id;
		$property->area_id = $request->area_id;
		$property->address = $request->address;
		$property->is_to_sale = $request->is_to_sale;
		$property->price = $request->price;
		$property->type_negotiation_id = $request->type_negotiation_id;
		$property->type_floor_id = $request->type_floor_id;
		$property->total_floor = $request->total_floor;
		$property->room = $request->room;
		$property->bath = $request->bath;
		$property->locals = $request->locals;
		$property->square_meters = $request->square_meters;
		$property->description_ita = $request->description_ita;
		$property->description_fra = $request->description_fra;
		$property->description_eng = $request->description_eng;
		$property->type_heating_id = $request->type_heating_id;
		$property->type_kitchen_id = $request->type_kitchen_id;
		$property->type_occupation_id = $request->type_occupation_id;
		$property->type_status_id = $request->type_status_id;
		$property->type_box_id = $request->type_box_id;
		$property->type_distsm_id = $request->type_distsm_id;
		$property->type_garden_id = $request->type_garden_id;
		$property->garden_square_meters = $request->garden_square_meters;
		$property->type_exposure_id = $request->type_exposure_id;
		$property->condominium_fees = $request->condominium_fees;
		$property->is_elevator = $request->is_elevator;
		$property->is_terrace = $request->is_terrace;
		$property->is_balcony = $request->is_balcony;
		$property->is_act_of_commission = $request->is_act_of_commission;
		$property->is_floorplans = $request->is_floorplans;
		$property->is_furnished = $request->is_furnished;
		$property->is_seafront = $request->is_seafront;
		$property->is_pool = $request->is_pool;
		$property->view = $request->view;
		$property->is_showcase = $request->is_showcase;
		$property->is_showcase_tv = $request->is_showcase_tv;
		$property->is_special_price = $request->is_special_price;
		$property->is_visible = $request->is_visible;
		$property->is_old_town = $request->is_old_town;
		$property->energetic_class = $request->energetic_class;
		$property->energetic_class_value = $request->energetic_class_value;
		$property->save();

		$success = trans('api.property_stored', ['rifimm' => $property->rifimm]);
		return response()->success(compact('property', 'success'));
	}

	public function show($id) {
		$property = Property::find($id);
		if ($property) {
			return response()->success(compact('property'));
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function update(Request $request, $id) {
		$property = Property::find($id);
		if ($property) {

			$this->validate($request, [
				'rifimm' => 'required|unique:property,rifimm,'.$property->id,
				'agent_id' => 'required|integer|exists:agents,id',
				'category_id' => 'required|integer|exists:categories,id',
				'typology_id' => 'required|integer|exists:typologies,id',
				'province_id' => 'required|integer|exists:provinces,id',
				'town_id' => 'required|integer|exists:towns,id',
				'area_id' => 'integer|exists:areas,id',
				'is_to_sale' => 'required|boolean',
				'price' => 'required|integer',
				'type_negotiation_id' => 'required|integer|exists:type_negotiations,id',
				'type_floor_id' => 'integer|exists:type_floors,id',
				'total_floor' => 'integer',
				'room' => 'integer',
				'bath' => 'integer',
				'locals' => 'integer',
				'square_meters' => 'integer',
				'type_heating_id' => 'integer|exists:type_heating,id',
				'type_kitchen_id' => 'integer|exists:type_kitchens,id',
				'type_occupation_id' => 'integer|exists:type_occupations,id',
				'type_status_id' => 'integer|exists:type_status,id',
				'type_box_id' => 'integer|exists:type_box,id',
				'type_distsm_id' => 'integer|exists:type_distsm,id',
				'type_garden_id' => 'integer|exists:type_gardens,id',
				'garden_square_meters' => 'integer',
				'type_exposure_id' => 'integer|exists:type_exposures,id',
				'condominium_fees' => 'integer',
				'is_elevator' => 'boolean',
				'is_terrace' => 'boolean',
				'is_balcony' => 'boolean',
				'is_act_of_commission' => 'boolean',
				'is_floorplans' => 'boolean',
				'is_furnished' => 'boolean',
				'is_seafront' => 'boolean',
				'is_pool' => 'boolean',
				'view' => 'integer',
				'is_showcase' => 'boolean',
				'is_showcase_tv' => 'boolean',
				'is_special_price' => 'boolean',
				'is_visible' => 'boolean',
				'is_old_town' => 'boolean',
				'energetic_class_value' => 'integer',
			]);

			$property->rifimm = $request->rifimm;
			$property->agent_id = $request->agent_id;
			$property->category_id = $request->category_id;
			$property->typology_id = $request->typology_id;
			$property->province_id = $request->province_id;
			$property->town_id = $request->town_id;
			$property->area_id = $request->area_id;
			$property->address = $request->address;
			$property->is_to_sale = $request->is_to_sale;
			$property->price = $request->price;
			$property->type_negotiation_id = $request->type_negotiation_id;
			$property->type_floor_id = $request->type_floor_id;
			$property->total_floor = $request->total_floor;
			$property->room = $request->room;
			$property->bath = $request->bath;
			$property->locals = $request->locals;
			$property->square_meters = $request->square_meters;
			$property->description_ita = $request->description_ita;
			$property->description_fra = $request->description_fra;
			$property->description_eng = $request->description_eng;
			$property->type_heating_id = $request->type_heating_id;
			$property->type_kitchen_id = $request->type_kitchen_id;
			$property->type_occupation_id = $request->type_occupation_id;
			$property->type_status_id = $request->type_status_id;
			$property->type_box_id = $request->type_box_id;
			$property->type_distsm_id = $request->type_distsm_id;
			$property->type_garden_id = $request->type_garden_id;
			$property->garden_square_meters = $request->garden_square_meters;
			$property->type_exposure_id = $request->type_exposure_id;
			$property->condominium_fees = $request->condominium_fees;
			$property->is_elevator = $request->is_elevator;
			$property->is_terrace = $request->is_terrace;
			$property->is_balcony = $request->is_balcony;
			$property->is_act_of_commission = $request->is_act_of_commission;
			$property->is_floorplans = $request->is_floorplans;
			$property->is_furnished = $request->is_furnished;
			$property->is_seafront = $request->is_seafront;
			$property->is_pool = $request->is_pool;
			$property->view = $request->view;
			$property->is_showcase = $request->is_showcase;
			$property->is_showcase_tv = $request->is_showcase_tv;
			$property->is_special_price = $request->is_special_price;
			$property->is_visible = $request->is_visible;
			$property->is_old_town = $request->is_old_town;
			$property->energetic_class = $request->energetic_class;
			$property->energetic_class_value = $request->energetic_class_value;
			$property->save();

			$success = trans('api.property_updated', ['rifimm' => $property->rifimm]);
			return response()->success(compact('property', 'success'));
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function destroy($id) {
		$property = Property::find($id);
		if ($property) {
			$rifimm = $property->rifimm;
			$property->delete();

			return response()->success( array('success' => trans('api.property_deleted', ['rifimm' => $property->rifimm]) ) );
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function trashed() {
		$properties = Property::onlyTrashed()->orderBy('rifimm')->get();
		return response()->success(compact('properties'));
	}

	public function restore($id) {
		$property = Property::onlyTrashed()->find($id);
		if ($property) {
			$property->restore();

			return response()->success( array('success' => trans('api.property_restored', ['rifimm' => $property->rifimm]) ) );
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function forceDelete($id) {
		$property = Property::onlyTrashed()->find($id);
		if ($property) {
			$photos = $property->photos();
			foreach ($photos as $photo) {
				app('App\Http\Controllers\Api\v1\PhotoController')->destroy($photo->id);
			}
			$property->forceDelete();

			return response()->success( array('success' => trans('api.property_force_deleted', ['rifimm' => $property->rifimm]) ) );
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function changeVisible($id, $is_active) {
		$property = Property::find($id);
		if ($property) {
			$property->is_visible = $is_active;
			$property->save();

			$success;
			if ($is_active) {
				$success = trans('api.property_actived', ['rifimm' => $property->rifimm]);
			} else {
				$success = trans('api.property_suspended', ['rifimm' => $property->rifimm]);
			}
			return response()->success(compact('property', 'success'));
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function changeShocase($id, $is_showcase) {
		$property = Property::find($id);
		if ($property) {
			$property->is_showcase = $is_showcase;
			$property->save();

			$success;
			if ($is_showcase) {
				$success = trans('api.property_showcase_added', ['rifimm' => $property->rifimm]);
			} else {
				$success = trans('api.property_sshowcase_removed', ['rifimm' => $property->rifimm]);
			}
			return response()->success(compact('property', 'success'));
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}

	public function changeShocaseTV($id, $is_showcase_tv) {
		$property = Property::find($id);
		if ($property) {
			$property->is_showcase_tv = $is_showcase_tv;
			$property->save();

			$success;
			if ($is_showcase_tv) {
				$success = trans('api.property_showcase_tv_added', ['rifimm' => $property->rifimm]);
			} else {
				$success = trans('api.property_sshowcase_tv_removed', ['rifimm' => $property->rifimm]);
			}
			return response()->success(compact('property', 'success'));
		} else {
			return response()->error(trans('api.property_not_found'), 404);
		}
	}
}