<?php

namespace App\Http\Controllers\Api\v1;

use App\Realestate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RealestateController extends Controller {

	public function index() {
		$realestates = Realestate::get();
		return response()->success(compact('realestates'));
	}

	public function show($id) {
		$realestate = Realestate::find($id);
		if ($realestate) {
			return response()->success(compact('realestate'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Agenzia']), 404);
		}
	}

	public function update(Request $request, $id) {
		$realestate = Realestate::find($id);
		if ($realestate) {
			$this->validate($request, [
				'name' => 'required',
				'address' => 'required',
				'p_iva' => 'required|numeric',
				'telephone' => 'required',
				'email' => 'required|email',
				'agenti_imm' => 'numeric'
			]);
			$realestate->name = $request->input('name');
			$realestate->address = $request->input('address');
			$realestate->p_iva = $request->input('p_iva');
			$realestate->telephone = $request->input('telephone');
			$realestate->telephone_2 = $request->input('telephone_2');
			$realestate->cellular = $request->input('cellular');
			$realestate->cellular_2 = $request->input('cellular_2');
			$realestate->fax = $request->input('fax');
			$realestate->fax_2 = $request->input('fax_2');
			$realestate->email = $request->input('email');
			$realestate->agenti_imm = $request->input('agenti_imm');
			$realestate->rea = $request->input('rea');
			$realestate->save();

			$success = trans('api.section_updated', ['section' => 'Agenzia']);
			return response()->success(compact('realestate', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Agenzia']), 404);
		}
	}
}