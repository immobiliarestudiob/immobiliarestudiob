<?php

namespace App\Http\Controllers\Api\v1;

use App\Province;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller {

	public function index() {
		$provinces = Province::orderBy('name')->get();
		return response()->success(compact('provinces'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'abbreviation' => 'required',
		]);

		$province = new Province;
		$province->name = $request->input('name');
		$province->abbreviation = $request->input('abbreviation');
		$province->save();

		$success = trans('api.section_stored', ['section' => 'Provincia']);
		return response()->success(compact('province', 'success'));
	}

	public function show($id) {
		$province = Province::find($id);
		if ($province) {
			return response()->success(compact('province'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Provincia']), 404);
		}
	}

	public function update(Request $request, $id) {
		$province = Province::find($id);
		if ($province) {
			$this->validate($request, [
				'name' => 'required',
				'abbreviation' => 'required',
			]);
			$province->name = $request->input('name');
			$province->abbreviation = $request->input('abbreviation');
			$province->save();

			$success = trans('api.section_updated', ['section' => 'Provincia']);
			return response()->success(compact('province', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Provincia']), 404);
		}
	}

	public function destroy($id) {
		$province = Province::find($id);
		if ($province) {
			$towns = $province->towns;
			foreach ($towns as $town) {
				app('App\Http\Controllers\Api\v1\AreaController')->destroy($town->id);
			}

			$province->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Provincia']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Provincia']), 404);
		}
	}

	public function getTowns(Request $request, $id) {
		if ($request->ajax()) {
			$towns = Province::findOrFail($id)->towns()->orderBy('name')->get();
			return response()->json( $towns );
		}
	}
}