<?php

namespace App\Http\Controllers\Api\v1;

use App\Photo;
use App\Property;
use File;
use Storage;
use Session;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PhotoController extends Controller {

	public function store(Request $request) {
		if ($request->hasFile('photo')) {
			if ($request->file('photo')->isValid()) {
				$this->validate($request, [
					'photo' => 'required|image',
					'property_id' => 'integer',
					'is_planimetry' => 'boolean',
					'is_main' => 'boolean',
					'is_hidden' => 'boolean',
				]);

				if ($request->input('property_id') > 0) {
					$property = Property::findOrFail($request->input('property_id'));
					$destination_path = public_path().'/images/property/'.$property->rifimm;

					if(!File::exists($destination_path))
						Storage::makeDirectory('/images/property/'.$property->rifimm);

				} else
				$destination_path = public_path().'/images/other';

				$photo = $request->file('photo');
				$original_name = $photo->getClientOriginalName();
				do {
					$random_name = $this->randomizeFileName($original_name);
				} while (File::exists($destination_path.'/'.$random_name));

				$photo->move($destination_path, $random_name);

				$photo_db = new Photo;
				$photo_db->original_name = $original_name;
				$photo_db->perc_photo = $destination_path.'/'.$random_name;
				$photo_db->save();

				flash()->success(trans('api.photo_uploaded'));
				return $photo_db->id;

			} else {
				flash()->error(trans('api.photo_not_valid'));
				return null;
			}
		} else {
			flash()->error(trans('api.photo_not_found'));
			return null;
		}
	}

	public function show($id) {
		$photo = Photo::findOrFail($id);
		return $photo->getBig();
	}

	public function destroy($id) {
		if ($id != null) {
			$photo = Photo::findOrFail($id);
			if(File::exists($photo->perc_photo)) {
				File::delete($photo->perc_photo);
				flash()->success(trans('api.photo_deleted'));
			} else {
				flash()->success(trans('api.photo_not_found'));
			}
			$photo->delete();
		}
	}

	public function randomizeFileName($real_file_name) {
		$name_parts = @explode( ".", $real_file_name );
		$ext = "";
		if ( count( $name_parts ) > 0 ) {
			$ext = $name_parts[count( $name_parts ) - 1];
		}
		return substr(md5(uniqid(rand(),1)), -16) . "." . $ext;
	}
}