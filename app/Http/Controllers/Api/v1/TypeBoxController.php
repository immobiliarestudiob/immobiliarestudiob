<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeBox;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeBoxController extends Controller {

	public function index() {
		$type_boxs = TypeBox::orderBy('name_ita')->get();
		return response()->success(compact('type_boxs'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_box = new TypeBox;
		$type_box->name_ita = $request->input('name_ita');
		$type_box->name_fra = $request->input('name_fra');
		$type_box->name_eng = $request->input('name_eng');
		$type_box->save();

		$success = trans('api.section_stored', ['section' => 'Box']);
		return response()->success(compact('type_box', 'success'));
	}

	public function show($id) {
		$type_box = TypeBox::find($id);
		if ($type_box) {
			return response()->success(compact('type_box'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Box']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_box = TypeBox::find($id);
		if ($type_box) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_box->name_ita = $request->input('name_ita');
			$type_box->name_fra = $request->input('name_fra');
			$type_box->name_eng = $request->input('name_eng');
			$type_box->save();

			$success = trans('api.section_updated', ['section' => 'Box']);
			return response()->success(compact('type_box', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Box']), 404);
		}
	}

	public function destroy($id) {
		$type_box = TypeBox::find($id);
		if ($type_box) {
			$type_box->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Box']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Box']), 404);
		}
	}
}