<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeDistsm;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeDistsmController extends Controller {

	public function index() {
		$type_distsms = TypeDistsm::orderBy('name_ita')->get();
		return response()->success(compact('type_distsms'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_distsm = new TypeDistsm;
		$type_distsm->name_ita = $request->input('name_ita');
		$type_distsm->name_fra = $request->input('name_fra');
		$type_distsm->name_eng = $request->input('name_eng');
		$type_distsm->save();

		$success = trans('api.section_stored', ['section' => 'Distanza MM']);
		return response()->success(compact('type_distsm', 'success'));
	}

	public function show($id) {
		$type_distsm = TypeDistsm::find($id);
		if ($type_distsm) {
			return response()->success(compact('type_distsm'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Distanza MM']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_distsm = TypeDistsm::find($id);
		if ($type_distsm) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_distsm->name_ita = $request->input('name_ita');
			$type_distsm->name_fra = $request->input('name_fra');
			$type_distsm->name_eng = $request->input('name_eng');
			$type_distsm->save();

			$success = trans('api.section_updated', ['section' => 'Distanza MM']);
			return response()->success(compact('type_distsm', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Distanza MM']), 404);
		}
	}

	public function destroy($id) {
		$type_distsm = TypeDistsm::find($id);
		if ($type_distsm) {
			$type_distsm->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Distanza MM']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Distanza MM']), 404);
		}
	}
}