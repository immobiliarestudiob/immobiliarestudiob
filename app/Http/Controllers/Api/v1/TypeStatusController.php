<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeStatus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeStatusController extends Controller {

	public function index() {
		$type_statuss = TypeStatus::orderBy('name_ita')->get();
		return response()->success(compact('type_statuss'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_status = new TypeStatus;
		$type_status->name_ita = $request->input('name_ita');
		$type_status->name_fra = $request->input('name_fra');
		$type_status->name_eng = $request->input('name_eng');
		$type_status->save();

		$success = trans('api.section_stored', ['section' => 'Stato']);
		return response()->success(compact('type_status', 'success'));
	}

	public function show($id) {
		$type_status = TypeStatus::find($id);
		if ($type_status) {
			return response()->success(compact('type_status'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Stato']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_status = TypeStatus::find($id);
		if ($type_status) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_status->name_ita = $request->input('name_ita');
			$type_status->name_fra = $request->input('name_fra');
			$type_status->name_eng = $request->input('name_eng');
			$type_status->save();

			$success = trans('api.section_updated', ['section' => 'Stato']);
			return response()->success(compact('type_status', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Stato']), 404);
		}
	}

	public function destroy($id) {
		$type_status = TypeStatus::find($id);
		if ($type_status) {
			$type_status->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Stato']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Stato']), 404);
		}
	}
}