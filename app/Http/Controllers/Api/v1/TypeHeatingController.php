<?php

namespace App\Http\Controllers\Api\v1;

use App\TypeHeating;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypeHeatingController extends Controller {

	public function index() {
		$type_heatings = TypeHeating::orderBy('name_ita')->get();
		return response()->success(compact('type_heatings'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
		]);

		$type_heating = new TypeHeating;
		$type_heating->name_ita = $request->input('name_ita');
		$type_heating->name_fra = $request->input('name_fra');
		$type_heating->name_eng = $request->input('name_eng');
		$type_heating->save();

		$success = trans('api.section_stored', ['section' => 'Riscaldamento']);
		return response()->success(compact('type_heating', 'success'));
	}

	public function show($id) {
		$type_heating = TypeHeating::find($id);
		if ($type_heating) {
			return response()->success(compact('type_heating'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Riscaldamento']), 404);
		}
	}

	public function update(Request $request, $id) {
		$type_heating = TypeHeating::find($id);
		if ($type_heating) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
			]);
			$type_heating->name_ita = $request->input('name_ita');
			$type_heating->name_fra = $request->input('name_fra');
			$type_heating->name_eng = $request->input('name_eng');
			$type_heating->save();

			$success = trans('api.section_updated', ['section' => 'Riscaldamento']);
			return response()->success(compact('type_heating', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Riscaldamento']), 404);
		}
	}

	public function destroy($id) {
		$type_heating = TypeHeating::find($id);
		if ($type_heating) {
			$type_heating->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Riscaldamento']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Riscaldamento']), 404);
		}
	}
}