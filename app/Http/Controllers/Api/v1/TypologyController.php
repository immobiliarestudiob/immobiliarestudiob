<?php

namespace App\Http\Controllers\Api\v1;

use App\Typology;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TypologyController extends Controller {

	public function index() {
		$typologies = Typology::where('uptypology_id', null)->get();
		foreach ($typologies as $index => $typology) {
			$typology->subTypologies;
		}

		return response()->success(compact('typologies'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'name_ita' => 'required',
			'name_fra' => 'required',
			'name_eng' => 'required',
			'uptypology_id' => 'integer',
		]);


		$typology = new Typology;
		$typology->name_ita = $request->input('name_ita');
		$typology->name_fra = $request->input('name_fra');
		$typology->name_eng = $request->input('name_eng');
		if ($request->has('uptypology_id'))
			$typology->uptypology_id = $request->input('uptypology_id');
		$typology->save();

		$success = trans('api.section_stored', ['section' => 'Tipologia']);
		return response()->success(compact('typology', 'success'));
	}

	public function show($id) {
		$typology = Typology::find($id);
		if ($typology) {
			return response()->success(compact('typology'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Tipologia']), 404);
		}
	}

	public function update(Request $request, $id) {
		$typology = Typology::find($id);
		if ($typology) {
			$this->validate($request, [
				'name_ita' => 'required',
				'name_fra' => 'required',
				'name_eng' => 'required',
				'uptypology_id' => 'integer|different:'.$id,
			]);
			$typology->name_ita = $request->input('name_ita');
			$typology->name_fra = $request->input('name_fra');
			$typology->name_eng = $request->input('name_eng');
			if ($request->has('uptypology_id'))
				$typology->uptypology_id = $request->input('uptypology_id');
			$typology->save();

			$success = trans('api.section_updated', ['section' => 'Tipologia']);
			return response()->success(compact('typology', 'success'));
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Tipologia']), 404);
		}
	}

	public function destroy($id) {
		$typology = Typology::find($id);
		if ($typology) {
			$subtypologies = $typology->subTypologies;
			$subtypologies->delete();
			$typology->delete();

			return response()->success( array('success' => trans('api.section_deleted', ['section' => 'Tipologia']) ) );
		} else {
			return response()->error(trans('api.section_not_found', ['section' => 'Tipologia']), 404);
		}
	}
}