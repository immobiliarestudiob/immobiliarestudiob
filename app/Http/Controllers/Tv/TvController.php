<?php

namespace App\Http\Controllers\Tv;

use App\Models\Property;

use Cookie;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class TvController extends Controller {

	public function home() {
		$properties = Property::visible()->where('is_showcase_tv', true)->get();
		return view('tv.home', [
			'properties' => $properties,
		]);
	}
}