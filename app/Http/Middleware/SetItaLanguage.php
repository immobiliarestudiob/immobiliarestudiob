<?php

namespace App\Http\Middleware;

use App;
use Closure;

class SetItaLanguage {

    public function handle($request, Closure $next) {
        App::setLocale('it');

        return $next($request);
    }
}