<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware {

	public function handle($request, Closure $next, $role) {
		if (Auth::guest())
			return redirect()->route('auth::login_get');

		$roles = is_array($role) ? $role : explode('|', $role);

		if (! Auth::user()->hasAnyRole($roles))
			abort(403);

		return $next($request);
	}
}