<?php

namespace App;

/**
 * Class Constants
 * @package App
 */
class Constants
{
    const MODELS_DIRECTORY = 'App\Models\\';

    const DEFAULT = 'default';

    // =DATES
    const DATE_DISPLAY_FORMAT = 'd/m/Y';
    const DATE_TIME_DISPLAY_FORMAT = 'd/m/Y, H:i';

    // =LOGGERS
    const LOGGER_CORE_QUEUE = 'CoreQueueLogger';
    const LOGGER_BATCH_EXPORT_QUEUE = 'BatchExportQueueLogger';
    const LOGGER_BATCH_IMPORT_QUEUE = 'BatchImportQueueLogger';

    // =ROLES
    const ROLE_PUBLIC = 'public';
    const ROLE_AGENT = 'agent';
    const ROLE_ADMINISTRATOR = 'admin';

    // =PAGINATE
    const PAGINATE_DEFAULT_PER_PAGE = 12;

    // =QUEUES
    const QUEUE_MAIL = 'mail';
    const QUEUE_ASYNCHRONOUS_DELAY = 10000;

    // =SEARCH_HANDLERS
    const RELATION_NEEDLE = '__';
    const SELECT_FIELDS = [
        'status',
        'batch_enabled'
    ];

    // =DATES
    const BEGINNING_OF_DAY = ' 00:00:00';
    const END_OF_DAY = ' 23:59:59';
    const DATE_DISPLAY_SUFFIX = '_display';
    const DATE_FIELDS = [
        'date_from',
        'date_to',
    ];
    // used to check if the filter must query the "created_at" column
    const CREATED_AT_FIELDS = ['date_from', 'date_to'];
    // used to check if the search filter is a "from" date filter
    const FROM_DATE_FIELDS = ['date_from', 'contract_start_date'];
}
