<?php

use Illuminate\Database\Seeder;

class TownsTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Town::class, 30)->create();
	}
}