<?php

use Illuminate\Database\Seeder;

class TypeHeatingTableSeeder extends Seeder {
	public function run() {
		factory(App\Models\TypeHeating::class, 10)->create();
	}
}