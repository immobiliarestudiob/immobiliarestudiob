<?php

use Illuminate\Database\Seeder;

class TypologiesTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Typology::class, 50)->create();
	}
}