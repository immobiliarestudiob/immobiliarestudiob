<?php

use Illuminate\Database\Seeder;

class TypeGardensTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeGarden::class, 10)->create();
	}
}