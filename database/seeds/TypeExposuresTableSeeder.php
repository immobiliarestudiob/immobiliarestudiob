<?php

use Illuminate\Database\Seeder;

class TypeExposuresTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeExposure::class, 10)->create();
	}
}