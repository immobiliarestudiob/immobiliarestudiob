<?php

use Illuminate\Database\Seeder;

class TypeDistsmTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeDistsm::class, 10)->create();
	}
}