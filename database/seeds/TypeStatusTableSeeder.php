<?php

use Illuminate\Database\Seeder;

class TypeStatusTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeStatus::class, 10)->create();
	}
}