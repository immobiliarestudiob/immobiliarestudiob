<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Province::class, 5)->create();
	}
}