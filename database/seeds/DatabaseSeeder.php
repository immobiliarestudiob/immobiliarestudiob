<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	public function run() {
		/*
		$this->call(RealestatesTableSeeder::class);

		$this->call(UsersTableSeeder::class);

		$this->call(ProvincesTableSeeder::class);
		$this->call(TownsTableSeeder::class);
		$this->call(AreasTableSeeder::class);

		$this->call(CategoriesTableSeeder::class);
		$this->call(TypologiesTableSeeder::class);

		$this->call(TypeBoxTableSeeder::class);
		$this->call(TypeDistsmTableSeeder::class);
		$this->call(TypeExposuresTableSeeder::class);
		$this->call(TypeFloorTableSeeder::class);
		$this->call(TypeGardensTableSeeder::class);
		$this->call(TypeHeatingTableSeeder::class);
		$this->call(TypeKitchensTableSeeder::class);
		$this->call(TypeNegotiationsTableSeeder::class);
		$this->call(TypeOccupationsTableSeeder::class);
		$this->call(TypeStatusTableSeeder::class);

		$this->call(PropertiesTableSeeder::class);
		*/
		$this->call(RoleTableSeeder::class);
	}
}