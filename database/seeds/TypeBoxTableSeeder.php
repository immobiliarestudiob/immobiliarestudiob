<?php

use Illuminate\Database\Seeder;

class TypeBoxTableSeeder extends Seeder {
	public function run() {
		factory(App\Models\TypeBox::class, 10)->create();
	}
}