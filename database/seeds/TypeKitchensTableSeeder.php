<?php

use Illuminate\Database\Seeder;

class TypeKitchensTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeKitchen::class, 10)->create();
	}
}