<?php

use Illuminate\Database\Seeder;

class TypeOccupationsTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeOccupation::class, 10)->create();
	}
}