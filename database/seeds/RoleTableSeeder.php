<?php

use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\User;

class RoleTableSeeder extends Seeder {

	public function run() {
		$role_public = new Role();
		$role_public->name = 'public';
		$role_public->description = 'Public user';
		$role_public->save();

		$role_agent = new Role();
		$role_agent->name = 'agent';
		$role_agent->description = 'Agent';
		$role_agent->save();

		$role_admin = new Role();
		$role_admin->name = 'admin';
		$role_admin->description = 'Administrator';
		$role_admin->save();

		User::where('name', 'loredana')->first()->roles()->attach($role_agent);
		User::where('name', 'elisa')->first()->roles()->attach($role_agent);
	}
}