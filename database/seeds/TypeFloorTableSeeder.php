<?php

use Illuminate\Database\Seeder;

class TypeFloorTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeFloor::class, 10)->create();
	}
}