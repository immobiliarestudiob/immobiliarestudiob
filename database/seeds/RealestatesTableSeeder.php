<?php

use Illuminate\Database\Seeder;

class RealestatesTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Realestate::class, 1)->create();
	}
}