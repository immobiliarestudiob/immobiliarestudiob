<?php

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Area::class, 50)->create();
	}
}