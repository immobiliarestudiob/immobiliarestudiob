<?php

use Illuminate\Database\Seeder;

class TypeNegotiationsTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\TypeNegotiation::class, 10)->create();
	}
}