<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder {

	public function run() {
		factory(App\Models\Property::class, 200)->create();
	}
}