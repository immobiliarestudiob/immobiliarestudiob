<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Property::class, function (Faker $faker) {
	$area = App\Models\Area::all()->random(1)->first();
	$energeticClass = ['A+', 'A', 'B', 'C', 'D', 'E', 'F', 'G'];

	return [
		'rifimm' => $faker->stateAbbr.$faker->numberBetween(101, 999),
		'user_id' => App\Models\User::all()->random(1)->first()->id,
		'category_id' => App\Models\Category::all()->random(1)->first()->id,
		'typology_id' => App\Models\Typology::all()->random(1)->first()->id,
		'province_id' => $area->town->province->id,
		'town_id' => $area->town->id,
		'area_id' => $area->id,
		'address' => $faker->address,
		'address_geocode' => $faker->latitude.'-'.$faker->longitude,
		'is_to_sale' => $faker->boolean(90),
		'price' => $faker->numberBetween(5000, 10000000),
		'type_negotiation_id' => App\Models\TypeNegotiation::all()->random(1)->first()->id,
		'type_floor_id' => App\Models\TypeFloor::all()->random(1)->first()->id,
		'total_floor' => $faker->randomDigitNotNull,
		'room' => $faker->randomDigitNotNull,
		'bath' => $faker->randomDigitNotNull,
		'locals' => $faker->randomDigitNotNull,
		'square_meters' => $faker->numberBetween(10, 1000),
		'description_ita' => $faker->paragraphs(5, true),
		'description_fra' => $faker->paragraphs(5, true),
		'description_eng' => $faker->paragraphs(5, true),
		'type_heating_id' => App\Models\TypeHeating::all()->random(1)->first()->id,
		'type_kitchen_id' => App\Models\TypeKitchen::all()->random(1)->first()->id,
		'type_occupation_id' => App\Models\TypeOccupation::all()->random(1)->first()->id,
		'type_status_id' => App\Models\TypeStatus::all()->random(1)->first()->id,
		'type_box_id' => App\Models\TypeBox::all()->random(1)->first()->id,
		'type_distsm_id' => App\Models\TypeDistsm::all()->random(1)->first()->id,
		'type_garden_id' => App\Models\TypeGarden::all()->random(1)->first()->id,
		'garden_square_meters' => $faker->numberBetween(10, 1000),
		'type_exposure_id' => App\Models\TypeExposure::all()->random(1)->first()->id,
		'is_elevator' => $faker->boolean(),
		'is_terrace' => $faker->boolean(),
		'is_balcony' => $faker->boolean(),
		'is_act_of_commission' => $faker->boolean(),
		'is_floorplans' => $faker->boolean(),
		'is_furnished' => $faker->boolean(),
		'is_seafront' => $faker->boolean(),
		'is_pool' => $faker->boolean(),
		'views' => $faker->numberBetween(0, 10000),
		'is_showcase' => $faker->boolean(20),
		'is_showcase_tv' => $faker->boolean(20),
		'is_special_price' => $faker->boolean(20),
		'is_visible' => $faker->boolean(80),
		'is_old_town' => $faker->boolean(40),
		'energetic_class' => $energeticClass[rand(0, 7)],
	];
});