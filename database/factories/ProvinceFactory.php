<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Province::class, function (Faker $faker) {
	return [
		'name' => $faker->state,
		'abbreviation' => $faker->stateAbbr,
		'views' => $faker->numberBetween(0, 10000),
	];
});