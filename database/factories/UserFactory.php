<?php

use Faker\Generator as Faker;

$factory->define(App\Models\User::class, function (Faker $faker) {
	static $password;

	return [
		'username' => $faker->userName,
		'password' => $password ?: $password = bcrypt('secret'),
		'name' => $faker->name,
		'surname' => $faker->name,
		'email' => $faker->safeEmail,
		'telephone' => $faker->phoneNumber,
		'remember_token' => str_random(10),
		'is_active' => $faker->boolean(75),
	];
});