<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Town::class, function (Faker $faker) {
	return [
		'name' => $faker->city,
		'province_id' => App\Models\Province::all()->random(1)->first()->id,
		'views' => $faker->numberBetween(0, 10000),
	];
});