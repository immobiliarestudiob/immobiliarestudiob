<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Realestate::class, function (Faker $faker) {
	return [
		'name' => $faker->company,
		'address' => $faker->address,
		'p_iva' => $faker->numberBetween(10000000000, 99999999999),
		'telephone' => $faker->tollFreePhoneNumber,
		'cellular' => $faker->tollFreePhoneNumber,
		'email' => $faker->companyEmail,
		'agenti_imm' => 'IM-'.$faker->numberBetween(100000, 999999),
		'rea' => $faker->numberBetween(1000, 9999),
	];
});