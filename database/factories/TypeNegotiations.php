<?php

use Faker\Generator as Faker;

$factory->define(App\Models\TypeNegotiation::class, function (Faker $faker) {
	return [
		'name_ita' => $faker->word,
		'name_fra' => $faker->word,
		'name_eng' => $faker->word,
		'is_visible_price' => $faker->boolean(75),
	];
});
