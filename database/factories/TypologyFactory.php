<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Typology::class, function (Faker $faker) {
	$enumClass = ['apartment', 'house', 'commercial', 'land'];
	return [
		'name_ita' => $faker->word,
		'name_fra' => $faker->word,
		'name_eng' => $faker->word,
		'views' => $faker->numberBetween(0, 10000),
		'class' => $enumClass[rand(0, 3)],
	];
});
