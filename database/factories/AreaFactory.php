<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Area::class, function (Faker $faker) {
	return [
		'name' => $faker->streetName,
		'town_id' => App\Models\Town::all()->random(1)->first()->id,
		'views' => $faker->numberBetween(0, 10000)
	];
});