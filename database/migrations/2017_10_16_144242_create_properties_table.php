    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rifimm')->unique();
            $table->integer('user_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('typology_id')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('town_id')->nullable();
            $table->integer('area_id')->nullable();
            $table->string('address')->nullable();
            $table->string('address_geocode')->nullable();
            $table->boolean('address_visible')->default(true);
            $table->boolean('is_to_sale')->default(true);
            $table->integer('price')->nullable();
            $table->integer('type_negotiation_id')->nullable();
            $table->integer('type_floor_id')->nullable();
            $table->integer('total_floor')->nullable();
            $table->integer('room')->nullable();
            $table->integer('bath')->nullable();
            $table->integer('locals')->nullable();
            $table->integer('square_meters')->nullable();
            $table->text('description_ita')->nullable();
            $table->text('description_fra')->nullable();
            $table->text('description_eng')->nullable();
            $table->integer('type_heating_id')->nullable();
            $table->integer('type_kitchen_id')->nullable();
            $table->integer('type_occupation_id')->nullable();
            $table->integer('type_status_id')->nullable();
            $table->integer('type_box_id')->nullable();
            $table->integer('type_distsm_id')->nullable();
            $table->integer('type_garden_id')->nullable();
            $table->integer('garden_square_meters')->nullable();
            $table->integer('type_exposure_id')->nullable();
            $table->integer('condominium_fees')->nullable();
            $table->boolean('is_elevator')->default(false);
            $table->boolean('is_terrace')->default(false);
            $table->boolean('is_balcony')->default(false);
            $table->boolean('is_act_of_commission')->default(false);
            $table->boolean('is_floorplans')->default(false);
            $table->boolean('is_furnished')->default(false);
            $table->boolean('is_seafront')->default(false);
            $table->boolean('is_pool')->default(false);
            $table->integer('views')->default(0);
            $table->boolean('is_showcase')->default(false);
            $table->boolean('is_showcase_tv')->default(true);
            $table->boolean('is_special_price')->default(false);
            $table->boolean('is_visible')->default(false);
            $table->boolean('is_old_town')->default(false);
            $table->string('energetic_class')->default('G');
            $table->integer('energetic_class_value')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
