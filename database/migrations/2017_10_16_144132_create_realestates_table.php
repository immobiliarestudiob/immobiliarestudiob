<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealestatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realestates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('p_iva');
            $table->string('telephone')->nullable();
            $table->string('telephone_2')->nullable();
            $table->string('cellular')->nullable();
            $table->string('cellular_2')->nullable();
            $table->string('fax')->nullable();
            $table->string('fax_2')->nullable();
            $table->string('email')->nullable();
            $table->string('agenti_imm');
            $table->string('rea');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('realestates');
    }
}
