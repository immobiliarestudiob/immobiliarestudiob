<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypologiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typologies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ita')->nullable();
            $table->string('name_fra')->nullable();
            $table->string('name_eng')->nullable();
            $table->integer('uptypology_id')->nullable();
            $table->integer('views')->default(0);
            $table->enum('class', ['apartment', 'house', 'commercial', 'land']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('typologies');
    }
}