<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeNegotiationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_negotiations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ita')->nullable();
            $table->string('name_fra')->nullable();
            $table->string('name_eng')->nullable();
            $table->boolean('is_visible_price')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('type_negotiations');
    }
}
