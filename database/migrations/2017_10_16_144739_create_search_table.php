<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('typology_id')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('town_id')->nullable();
            $table->integer('area_id')->nullable();
            $table->integer('min_price')->nullable();
            $table->integer('max_price')->nullable();
            $table->integer('min_room')->nullable();
            $table->integer('max_room')->nullable();
            $table->integer('min_bath')->nullable();
            $table->integer('max_bath')->nullable();
            $table->integer('min_locals')->nullable();
            $table->integer('max_locals')->nullable();
            $table->integer('min_square_meters')->nullable();
            $table->integer('max_square_meters')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('search');
    }
}
